#!/bin/bash

npm install
grunt dist
#cd src/doc
#markdoc build
#cd ../..
#cat doc/media/css/style.css src/doc/extra.css > build/tmp.css
#cp build/tmp.css doc/media/css/style.css
cp -r \
    build/SocketPool.swf \
    build/fonts \
    build/css \
    build/images \
    build/viewer.html \
    build/editor.html \
    build/test.html \
    build/api-viewer.html \
    dist
cp -r \
    build/js/jacks.js \
    build/js/loadall.js \
    dist/js
cp -r \
    src/js/editor.js \
    src/js/test.js \
    src/js/canvas/Constraints.js \
    src/js/info/ClusterPick.js \
    src/js/undefine.js \
    dist/js
cp build/js/main.js dist/js/main.js
cp build/js/main.js.map dist/js/main.js.map
cp -r \
    build/lib/require.js \
    build/lib/swfobject.js \
    build/lib/vkbeautify.js \
    build/lib/bootstrap.min.js \
    build/lib/jquery-2.0.3.min.js \
    build/lib/d3.js \
    dist/lib
