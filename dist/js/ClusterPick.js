define(['underscore'],
function (_)
{
  'use strict';

  function ClusterPick(root, monitor, types, amlist, FEDERATEDLIST)
  {
    this.root = root;
    this.monitor = monitor;
    this.types = types;
    this.amList = amlist;
    this.FEDERATEDLIST = FEDERATEDLIST;
    this.CreateClusterStatus = function ()
    {
//      console.log(JSON.stringify(monitor), JSON.stringify(types), JSON.stringify(amlist), JSON.stringify(FEDERATEDLIST));
      return CreateClusterStatusImplementation(this.root, this.monitor, this.types, this.amList, this.FEDERATEDLIST);
    }.bind(this);
  }

  

  function CreateClusterStatusImplementation(root, monitor, types, amlist, FEDERATEDLIST) {
    var isInitializing = true;
/*
    if (monitor == null || $.isEmptyObject(monitor)) {
      return;
    }
*/

    root.each(function() {
      if ($(this).hasClass("pickered")) {
	return;
      }
      $(this).addClass("pickered");
	    
      var resourceType = "PC";
      var label = $(this).find('.control-label').attr('name');
      if (types && label && types[label] && types[label]['emulab-xen']) {
        resourceType = "VM";
      }
      var parent = $(this).parent();

      var html = ClusterStatusHTML(parent.find('.form-control option'),
				   FEDERATEDLIST);
      
      var selected = parent.find('select :selected')
      html.find('.dropdown-toggle .value').attr('data-value', selected.attr('value'));   
      html.find('.dropdown-toggle .value').html(selected.text());

      parent.find('.form-control').after(html);
      parent.find('select.form-control').addClass('hidden');

      html.find('.dropdown-menu a').on('click', function(event){
	if (event.screenX !== 0 && event.screenY !== 0) {
	  StatusClickEvent(parent, this);
	  var value = parent.find('.cluster_picker_status .value').attr('data-value');
	  if (! isInitializing)
	  {
	    parent.find('#profile_where').trigger('change', value);
	  }
	}
      });

      html.find('.dropdown-toggle').dropdown();

//      html.find('.dropdown-toggle').on('click', function () {
//	html.find('.dropdown-toggle').dropdown();
//      });

      _.each(amlist, function(item) {
	var name = item.name;
	var key = item.id;
	var data = monitor[key];
	var target = parent.find('.cluster_picker_status .dropdown-menu .enabled a:contains("'+name+'")');
	if (data && !$.isEmptyObject(data)) {
	  // Calculate testbed rating and set up tooltips.
	  var rating = CalculateRating(data, resourceType);
		    
	  target.parent()
	    .attr('data-health', rating[0])
	    .attr('data-rating', rating[1]);

	  var classes = AssignStatusClass(rating[0], rating[1]);
	  target.addClass(classes[0]).addClass(classes[1]);
	  
	  target.append(StatsLineHTML(classes, rating[2]));
	}
      });


      parent.find('.cluster_picker_status .dropdown-menu .enabled.native')
//	.sort(sort)
	.prependTo(parent.find('.cluster_picker_status .dropdown-menu'));
      parent.find('.cluster_picker_status .dropdown-menu .enabled.federated')
	.sort(sort)
	.insertAfter(parent.find('.cluster_picker_status .dropdown-menu .federatedDivider'));
      
      parent.find('.cluster_picker_status .dropdown-menu .enabled a')[0].click();
    });
    
    root.find('[data-toggle="tooltip"]').tooltip();
    isInitializing = false;
  }

  function sort(a, b)
  {
    var aHealth = Math.ceil((+a.dataset.health)/50);
    var bHealth = Math.ceil((+b.dataset.health)/50);
	
    if (aHealth > bHealth) {
      return -1;
    }
    else if (aHealth < bHealth) {
      return 1;
    }
    return +b.dataset.rating - +a.dataset.rating;
  };

  function ClusterStatusHTML(options, fedlist) {
    var html = $('<div class="cluster_picker_status btn-group">'
		 +'<button type="button" class="form-control btn btn-default dropdown-toggle" data-toggle="dropdown">'
		 +'<span class="value"></span>'
		 +'<span class="caret"></span>'
		 +'</button>'
		 +'<ul class="dropdown-menu" role="menu">'
		 +'<li role="separator" class="divider federatedDivider"><div>Federated Clusters<div></li>'
		 +'<li role="separator" class="divider disabledDivider"></li>'
		 +'</ul>'
		 +'</div>');

    var dropdown = html.find('.dropdown-menu .disabledDivider');
    var federated = html.find('.dropdown-menu .federatedDivider');
    var disabled = 0;
    var fed = 0;
    $(options).each(function() {
      var name = $(this).html();
      var value = $(this).attr('value');
      if ($(this).prop('disabled')) {
	dropdown.after('<li class="disabled"><a data-toggle="tooltip" data-placement="right" data-html="true" title="<div>This testbed is incompatible with the selected profile</div>" href="#" value="'+value+'">'+name+'</a></li>')
	disabled++;
      }
      else {
	if (_.contains(fedlist, value)) {
	  federated.after('<li class="enabled federated"><a href="#" value="'+value+'">'+name+'</a></li>');
	  fed++;
	}
	else {
	  var optvalue = value;
	  // Look for Please Select option
	  if (optvalue == "") {
	    optvalue = $(this).text();
	  }
	  federated.before('<li class="enabled native"><a href="#" value="'+optvalue+'">'+name+'</a></li>');
	}
      }
    });

    if (!disabled) {
      html.find('.disabledDivider').remove();
    }

    if (!fed) {
      html.find('.federatedDivider').remove();
    }

    return html;
  }

  function StatusClickEvent(html, that) {
    html.find('.dropdown-toggle .value').attr('data-value', $(that).attr('value'));   
    html.find('.dropdown-toggle .value').html($(that).text());   
		    
    if ($(that).find('.picker_stats').length) {
      if (!html.find('.dropdown-toggle > .picker_stats').length) {
	html.find('.dropdown-toggle').append('<div class="picker_stats"></div>');
      }
      else {
	html.find('.dropdown-toggle > .picker_stats').html('');
      }

      html.find('.dropdown-toggle > .picker_stats').append($(that).find('.picker_stats').html());
    }

    html.find('.selected').removeClass('selected');
    $(that).parent().addClass('selected');
  }

  function CalculateRating(data, type) {
    var health = 0;
    var rating = 0;
    var tooltip = [];

    
    if (data.status == 'SUCCESS') {
      if (data.health) {
	health = data.health;
	tooltip[0] = '<div>Testbed is '
	if (health > 50) {
	  tooltip[0] += 'healthy';
	}
	else {
	  tooltip[0] += 'unhealthy';
	}
	tooltip[0] += '</div>';
      }
      else {
	health = 100;
	tooltip[0] = '<div>Site is up</div>'
      }
    }
    else {
      tooltip[0] = '<div>Site is down</div>'
      return [health, rating, tooltip];
    }
    
    var available, max, label;
    if (type == 'VM') {
      available = parseInt(data.VMsAvailable);
      max = parseInt(data.VMsTotal);
      label = 'VMs';
    } 
    else {
      available = parseInt(data.rawPCsAvailable);
      max = parseInt(data.rawPCsTotal);
      label = 'PCs';
    }
    
    if (!isNaN(available) && !isNaN(max)) {
      var ratio = available/max;
      rating = available;
      tooltip[1] = '<div>'+available+'/'+max+' ('+Math.round(ratio*100)+'%) '+label+' available</div>';
    }
    
    return [health, rating, tooltip];
  }

  function AssignStatusClass(health, rating) {
    var result = [];
    if (health >= 50) {
      result[0] = 'status_healthy';
    }
    else if (health > 0) {
      result[0] = 'status_unhealthy';
    }
    else {
      result[0] = 'status_down';
    }
    
    if (rating > 20) {
      result[1] = 'resource_healthy';
    }
    else if (rating > 10) {
      result[1] = 'resource_unhealthy';
    }
    else {
      result[1] = 'resource_down';
    }
    
    return result;
  }

  function StatsLineHTML(classes, title) {
    var title1 = '';
    if (title[1]) {
      title1 = ' data-toggle="tooltip" data-placement="right" data-html="true" title="'+title[1]+'"';
    }
    return '<div class="tooltip_div"'+title1+'><div class="picker_stats" data-toggle="tooltip" data-placement="left" data-html="true" title="'+title[0]+'">'
      +'<span class="picker_status '+classes[0]+' '+classes[1]+'"><span class="circle"></span></span>'
      +'</div></div>';
  }
  
  return ClusterPick;
});
