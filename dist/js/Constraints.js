define(['underscore'],
function (_)
{
  'use strict';

  var types = {
    'aggregates': 'aggregates',
    'images': 'images',
    'types': 'types',
    'hardware': 'hardware',
    'linkTypes': 'linkTypes',
    'sharedvlans': 'sharedvlans',
    'linkNodes1': 'types',
    'linkNodes2': 'types'
  };

  var debug = false;

  function Constraints(context)
  {
    this.groups = {};
    this.possible = {};
    this.addPossibles(context.canvasOptions);
    this.allowAllSets(context.constraints);
    this.cache = {};
  }

  Constraints.prototype.addPossibles = function (options)
  {
    _.each(_.keys(types), function (type) {
      var typeOption = types[type];
      var list = this.possible[type] || [];
      var newValues = _.without(_.pluck(options[typeOption], 'id'), undefined);
      list = _.union(list, newValues);
      this.possible[type] = list;
    }.bind(this));
    this.cache = {};
  }

  Constraints.prototype.allowAllSets = function (list)
  {
    var that = this;
    _.each(list, function (set) {
      that.allowSet(set, true);
      if (set['node'] || set['node2'])
      {
	var swapped = _.clone(set);
	swapped['node'] = set['node2'];
	swapped['node2'] = set['node'];
	that.allowSet(swapped, false);
	if (set['node'] && set['link'] && ! set['node2'])
	{
	  swapped = _.clone(set);
	  swapped['node2'] = swapped['node'];
	  that.allowSet(swapped, false);
	}
	else if (set['node2'] && set['link'] && ! set['node'])
	{
	  swapped = _.clone(set);
	  swapped['node'] = swapped['node2'];
	  that.allowSet(swapped, false);
	}
      }
    });
    this.cache = {};
  };

  Constraints.prototype.allowSet = function (set, shouldComplain)
  {
    var that = this;
    var groupKey = makeGroupKey(set);
    that.groups[groupKey] = that.groups[groupKey] ||
      {
	node: { 'hash': {}, 'keys': null },
	'template': set
      };
    var list = makeTailList(set);
    if (checkTail(that.groups[groupKey].node, list))
    {
      if (shouldComplain)
      {
	  console.log('Duplicate constraint', set);
      }
    }
    else
    {
      enableTail(that.groups[groupKey].node, list);
    }
  };

  // All sets in the same group can be thought of as members of an
  // ordered list with each item in the list corresponding to a
  // particular subclause/key
  function makeTailList(set, template, possible)
  {
    if (! template)
    {
      template = set;
    }
    var list = [];
    var run = function (subClause, key) {
      if (template[subClause] && template[subClause][key])
      {
	if (set[subClause] && set[subClause][key] &&
	    (! possible || possible[key].indexOf(set[subClause][key]) !== -1))
	{
	  list.push(set[subClause][key]);
	}
	else
	{
	  list.push(undefined);
	}
      }
    };
    run('node', 'aggregates');
    run('node', 'images');
    run('node', 'types');
    run('node', 'hardware');

    run('node2', 'aggregates');
    run('node2', 'images');
    run('node2', 'types');
    run('node2', 'hardware');

    run('link', 'aggregates');
    run('link', 'linkTypes');
    run('link', 'sharedvlans');
    run('link', 'linkNodes1');
    run('link', 'linkNodes2');
//    var typeList = _.keys(types);
//    _.each(['node', 'node2', 'link'], function (subClause) {
//      _.each(typeList, function (key) {
//      });
//    });
    return list;
  }

  function allPossible(set, possible)
  {
    var result = true;
    _.each(_.keys(set), function (subClause) {
      _.each(_.keys(set[subClause]), function (key) {
	if (set[subClause][key] !== undefined && (! possible || possible[key].indexOf(set[subClause][key]) === -1))
	{
	  result = false;
	}
      });
    });
    return result;
  }

  function enableTail(node, list)
  {
    if (list.length === 1)
    {
      _.each(list[0], function (item) {
	node.hash[item] = true;
      });
    }
    else if (list.length > 1)
    {
      if (list[0] !== undefined)
      {
	var remainder = _.rest(list);
	_.each(list[0], function (item) {
	  node.hash[item] = node.hash[item] || { 'hash': {}, keys: null };
	  enableTail(node.hash[item], remainder);
	});
      }
    }
  }

  function checkTail(node, list)
  {
    var result = false;
    if (node)
    {
      if (list.length === 1)
      {
	if (list[0] === undefined)
	{
	  result = _.contains(_.values(node.hash), true);
	}
	else
	{
	  result = (node.hash[list[0]] === true);
	}
      }
      else if (list.length > 1)
      {
	var checks = [];
	if (list[0] === undefined)
	{
	  if (node.keys === null)
	  {
	    node.keys = _.keys(node.hash);
	  }
	  checks = node.keys;
	}
	else
	{
	  checks = [list[0], '*'];
	}
	var i = 0;
	for (; i < checks.length; i += 1)
	{
	  result = checkTail(node.hash[checks[i]], _.rest(list));
	  if (result)
	  {
	    break;
	  }
	}
      }
    }
    return result;
  }

  // Returns a list of valid ids of unboundType inside of the
  // unboundSubclause which are a valid match when combined with every
  // bound item in the boundList
  Constraints.prototype.getValidList = function (boundList,
						 unboundSubclause,
						 unboundType,
						 rejected,
						 rejectBreakdown)
  {
    if (debug) { console.log('List for: ', unboundSubclause, unboundType); }
    var that = this;
    var result = [];
    _.each(that.possible[unboundType], function (item) {
/*
      var candidateList = [];
      _.each(boundList, function (item) {
	var clone = {};
	_.each(_.keys(item), function (key) {
	  clone[key] = _.clone(item[key]);
	});
	candidateList.push(clone);
      });
*/
      var candidateList = boundList;
      _.each(candidateList, function (candidate) {
	if (debug) { console.log('-', unboundType, item); }
	if (! candidate[unboundSubclause])
	{
	  candidate[unboundSubclause] = {};
	}
	candidate[unboundSubclause][unboundType] = item;
      });
      var allValid = true;
      _.each(candidateList, function (candidate) {
	if (! that.isValid(candidate))
	{
	  allValid = false;
	  if (rejectBreakdown !== undefined)
	  {
	    if (rejectBreakdown[item] === undefined)
	    {
	      rejectBreakdown[item] = [];
	    }
	    rejectBreakdown[item].push(candidate);
	  }
	}
      });
      if (allValid)
      {
	result.push(item);
      }
      else if (rejected)
      {
	rejected.push(item);
      }
    });
    if (debug) { console.log('Overall result is', result); }
    return result;
  };

  // Returns true if all candidates in the candidate list match every
  // group.
  Constraints.prototype.allValid = function (candidateList)
  {
    var that = this;
    var result = true;
    _.each(candidateList, function (candidate) {
      result = result && that.isValid(candidate);
    });
    if (debug) { console.log('-', 'End Candidate List: ', candidateList); }
    return result;
  };

  // Check to see if a single candidate is valid. A candidate is valid
  // if it matches every group. Candidate subkeys contain strings
  // while clause subkeys contain lists of strings.
  Constraints.prototype.isValid = function (candidate)
  {
    var that = this;
    var result = true;
    var name = uniqueName(candidate);
    if (this.cache[name] !== undefined)
    {
      result = this.cache[name];
    }
    else
    {
      if (allPossible(candidate, this.possible))
      {
	console.log('allPossible', candidate, this.possible);
	_.each(_.keys(that.groups), function (groupKey) {
	  result = result && that.isValidForGroup(candidate, that.groups[groupKey]);
	});
      }
      else
      {
	result = true;
      }
      this.cache[name] = result;
    }
    if (debug) { console.log('--', 'Candidate: ', result, candidate); }
    return result;
  };

  // A group is a set of clauses which target identical keys. These
  // are not explicit in the input, but are implicitly grouped by
  // looking at the actual keys. A group is valid if any clause in the
  // group matches.
  Constraints.prototype.isValidForGroup = function (candidate, group)
  {
    var result = false;
    var list = makeTailList(candidate, group.template, this.possible);
    result = checkTail(group.node, list);
/*
    _.each(group, function (clause) {
      result = result || matchClause(candidate, clause, this.possible);
    }.bind(this));
*/
    if (debug) { console.log('---', 'Group: ', result, candidate, group); }
    return result;
  };

  // A clause is the atomic unit of the whitelist. This is one item on
  // the list of constraints. If all the sub-keys match, the whole
  // thing matches.
  function matchClause(candidate, clause, possible)
  {
    var result = true;
    if (candidate['link'] || ! clause['link'])
    {
      _.each(_.keys(clause), function (key) {
	result = result && (! candidate[key] || ! clause[key] ||
			    matchSubkeys(candidate[key], clause[key],
					 possible));
      });
    }
    if (debug) { console.log('----', 'Clause: ', result, candidate, clause); }
    return result;
  };

  // Clauses are logically divided into three subkey components for
  // clarity. These are the 'node', 'link', and 'node2'
  // sub-clauses. All of the sub-clauses have to match in order for
  // the clause as a whole to match.
  function matchSubkeys(candidateSub, clauseSub, possible)
  {
    var result = true;
    _.each(_.keys(clauseSub), function (key) {
      result = result && (! candidateSub[key] || ! clauseSub[key] ||
			  possible[key].indexOf(candidateSub[key]) === -1 ||
			  _.contains(clauseSub[key], '*') ||
			  _.contains(clauseSub[key], candidateSub[key]));
    });
    if (debug) { console.log('-----', 'Subkeys: ', result, candidateSub, clauseSub); }
    return result;
  };

  function makeGroupKey(obj)
  {
    var result = '';
    var keyList = _.keys(obj);
    keyList.sort();
    _.each(keyList, function (outer) {
      if (obj[outer])
      {
	var innerKeys = _.keys(obj[outer]);
	innerKeys.sort();
	result += ':' + outer + ':' + innerKeys.join('~');
      }
    });
    return result;
  }

  function uniqueName(candidate)
  {
    return JSON.stringify(candidate, undefined, 0);
  }

  return Constraints;
});
