//     Underscore.js 1.8.3
//     http://underscorejs.org
//     (c) 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.
(function(){function n(n){function t(t,r,e,u,i,o){for(;i>=0&&o>i;i+=n){var a=u?u[i]:i;e=r(e,t[a],a,t)}return e}return function(r,e,u,i){e=b(e,i,4);var o=!k(r)&&m.keys(r),a=(o||r).length,c=n>0?0:a-1;return arguments.length<3&&(u=r[o?o[c]:c],c+=n),t(r,e,u,o,c,a)}}function t(n){return function(t,r,e){r=x(r,e);for(var u=O(t),i=n>0?0:u-1;i>=0&&u>i;i+=n)if(r(t[i],i,t))return i;return-1}}function r(n,t,r){return function(e,u,i){var o=0,a=O(e);if("number"==typeof i)n>0?o=i>=0?i:Math.max(i+a,o):a=i>=0?Math.min(i+1,a):i+a+1;else if(r&&i&&a)return i=r(e,u),e[i]===u?i:-1;if(u!==u)return i=t(l.call(e,o,a),m.isNaN),i>=0?i+o:-1;for(i=n>0?o:a-1;i>=0&&a>i;i+=n)if(e[i]===u)return i;return-1}}function e(n,t){var r=I.length,e=n.constructor,u=m.isFunction(e)&&e.prototype||a,i="constructor";for(m.has(n,i)&&!m.contains(t,i)&&t.push(i);r--;)i=I[r],i in n&&n[i]!==u[i]&&!m.contains(t,i)&&t.push(i)}var u=this,i=u._,o=Array.prototype,a=Object.prototype,c=Function.prototype,f=o.push,l=o.slice,s=a.toString,p=a.hasOwnProperty,h=Array.isArray,v=Object.keys,g=c.bind,y=Object.create,d=function(){},m=function(n){return n instanceof m?n:this instanceof m?void(this._wrapped=n):new m(n)};"undefined"!=typeof exports?("undefined"!=typeof module&&module.exports&&(exports=module.exports=m),exports._=m):u._=m,m.VERSION="1.8.3";var b=function(n,t,r){if(t===void 0)return n;switch(null==r?3:r){case 1:return function(r){return n.call(t,r)};case 2:return function(r,e){return n.call(t,r,e)};case 3:return function(r,e,u){return n.call(t,r,e,u)};case 4:return function(r,e,u,i){return n.call(t,r,e,u,i)}}return function(){return n.apply(t,arguments)}},x=function(n,t,r){return null==n?m.identity:m.isFunction(n)?b(n,t,r):m.isObject(n)?m.matcher(n):m.property(n)};m.iteratee=function(n,t){return x(n,t,1/0)};var _=function(n,t){return function(r){var e=arguments.length;if(2>e||null==r)return r;for(var u=1;e>u;u++)for(var i=arguments[u],o=n(i),a=o.length,c=0;a>c;c++){var f=o[c];t&&r[f]!==void 0||(r[f]=i[f])}return r}},j=function(n){if(!m.isObject(n))return{};if(y)return y(n);d.prototype=n;var t=new d;return d.prototype=null,t},w=function(n){return function(t){return null==t?void 0:t[n]}},A=Math.pow(2,53)-1,O=w("length"),k=function(n){var t=O(n);return"number"==typeof t&&t>=0&&A>=t};m.each=m.forEach=function(n,t,r){t=b(t,r);var e,u;if(k(n))for(e=0,u=n.length;u>e;e++)t(n[e],e,n);else{var i=m.keys(n);for(e=0,u=i.length;u>e;e++)t(n[i[e]],i[e],n)}return n},m.map=m.collect=function(n,t,r){t=x(t,r);for(var e=!k(n)&&m.keys(n),u=(e||n).length,i=Array(u),o=0;u>o;o++){var a=e?e[o]:o;i[o]=t(n[a],a,n)}return i},m.reduce=m.foldl=m.inject=n(1),m.reduceRight=m.foldr=n(-1),m.find=m.detect=function(n,t,r){var e;return e=k(n)?m.findIndex(n,t,r):m.findKey(n,t,r),e!==void 0&&e!==-1?n[e]:void 0},m.filter=m.select=function(n,t,r){var e=[];return t=x(t,r),m.each(n,function(n,r,u){t(n,r,u)&&e.push(n)}),e},m.reject=function(n,t,r){return m.filter(n,m.negate(x(t)),r)},m.every=m.all=function(n,t,r){t=x(t,r);for(var e=!k(n)&&m.keys(n),u=(e||n).length,i=0;u>i;i++){var o=e?e[i]:i;if(!t(n[o],o,n))return!1}return!0},m.some=m.any=function(n,t,r){t=x(t,r);for(var e=!k(n)&&m.keys(n),u=(e||n).length,i=0;u>i;i++){var o=e?e[i]:i;if(t(n[o],o,n))return!0}return!1},m.contains=m.includes=m.include=function(n,t,r,e){return k(n)||(n=m.values(n)),("number"!=typeof r||e)&&(r=0),m.indexOf(n,t,r)>=0},m.invoke=function(n,t){var r=l.call(arguments,2),e=m.isFunction(t);return m.map(n,function(n){var u=e?t:n[t];return null==u?u:u.apply(n,r)})},m.pluck=function(n,t){return m.map(n,m.property(t))},m.where=function(n,t){return m.filter(n,m.matcher(t))},m.findWhere=function(n,t){return m.find(n,m.matcher(t))},m.max=function(n,t,r){var e,u,i=-1/0,o=-1/0;if(null==t&&null!=n){n=k(n)?n:m.values(n);for(var a=0,c=n.length;c>a;a++)e=n[a],e>i&&(i=e)}else t=x(t,r),m.each(n,function(n,r,e){u=t(n,r,e),(u>o||u===-1/0&&i===-1/0)&&(i=n,o=u)});return i},m.min=function(n,t,r){var e,u,i=1/0,o=1/0;if(null==t&&null!=n){n=k(n)?n:m.values(n);for(var a=0,c=n.length;c>a;a++)e=n[a],i>e&&(i=e)}else t=x(t,r),m.each(n,function(n,r,e){u=t(n,r,e),(o>u||1/0===u&&1/0===i)&&(i=n,o=u)});return i},m.shuffle=function(n){for(var t,r=k(n)?n:m.values(n),e=r.length,u=Array(e),i=0;e>i;i++)t=m.random(0,i),t!==i&&(u[i]=u[t]),u[t]=r[i];return u},m.sample=function(n,t,r){return null==t||r?(k(n)||(n=m.values(n)),n[m.random(n.length-1)]):m.shuffle(n).slice(0,Math.max(0,t))},m.sortBy=function(n,t,r){return t=x(t,r),m.pluck(m.map(n,function(n,r,e){return{value:n,index:r,criteria:t(n,r,e)}}).sort(function(n,t){var r=n.criteria,e=t.criteria;if(r!==e){if(r>e||r===void 0)return 1;if(e>r||e===void 0)return-1}return n.index-t.index}),"value")};var F=function(n){return function(t,r,e){var u={};return r=x(r,e),m.each(t,function(e,i){var o=r(e,i,t);n(u,e,o)}),u}};m.groupBy=F(function(n,t,r){m.has(n,r)?n[r].push(t):n[r]=[t]}),m.indexBy=F(function(n,t,r){n[r]=t}),m.countBy=F(function(n,t,r){m.has(n,r)?n[r]++:n[r]=1}),m.toArray=function(n){return n?m.isArray(n)?l.call(n):k(n)?m.map(n,m.identity):m.values(n):[]},m.size=function(n){return null==n?0:k(n)?n.length:m.keys(n).length},m.partition=function(n,t,r){t=x(t,r);var e=[],u=[];return m.each(n,function(n,r,i){(t(n,r,i)?e:u).push(n)}),[e,u]},m.first=m.head=m.take=function(n,t,r){return null==n?void 0:null==t||r?n[0]:m.initial(n,n.length-t)},m.initial=function(n,t,r){return l.call(n,0,Math.max(0,n.length-(null==t||r?1:t)))},m.last=function(n,t,r){return null==n?void 0:null==t||r?n[n.length-1]:m.rest(n,Math.max(0,n.length-t))},m.rest=m.tail=m.drop=function(n,t,r){return l.call(n,null==t||r?1:t)},m.compact=function(n){return m.filter(n,m.identity)};var S=function(n,t,r,e){for(var u=[],i=0,o=e||0,a=O(n);a>o;o++){var c=n[o];if(k(c)&&(m.isArray(c)||m.isArguments(c))){t||(c=S(c,t,r));var f=0,l=c.length;for(u.length+=l;l>f;)u[i++]=c[f++]}else r||(u[i++]=c)}return u};m.flatten=function(n,t){return S(n,t,!1)},m.without=function(n){return m.difference(n,l.call(arguments,1))},m.uniq=m.unique=function(n,t,r,e){m.isBoolean(t)||(e=r,r=t,t=!1),null!=r&&(r=x(r,e));for(var u=[],i=[],o=0,a=O(n);a>o;o++){var c=n[o],f=r?r(c,o,n):c;t?(o&&i===f||u.push(c),i=f):r?m.contains(i,f)||(i.push(f),u.push(c)):m.contains(u,c)||u.push(c)}return u},m.union=function(){return m.uniq(S(arguments,!0,!0))},m.intersection=function(n){for(var t=[],r=arguments.length,e=0,u=O(n);u>e;e++){var i=n[e];if(!m.contains(t,i)){for(var o=1;r>o&&m.contains(arguments[o],i);o++);o===r&&t.push(i)}}return t},m.difference=function(n){var t=S(arguments,!0,!0,1);return m.filter(n,function(n){return!m.contains(t,n)})},m.zip=function(){return m.unzip(arguments)},m.unzip=function(n){for(var t=n&&m.max(n,O).length||0,r=Array(t),e=0;t>e;e++)r[e]=m.pluck(n,e);return r},m.object=function(n,t){for(var r={},e=0,u=O(n);u>e;e++)t?r[n[e]]=t[e]:r[n[e][0]]=n[e][1];return r},m.findIndex=t(1),m.findLastIndex=t(-1),m.sortedIndex=function(n,t,r,e){r=x(r,e,1);for(var u=r(t),i=0,o=O(n);o>i;){var a=Math.floor((i+o)/2);r(n[a])<u?i=a+1:o=a}return i},m.indexOf=r(1,m.findIndex,m.sortedIndex),m.lastIndexOf=r(-1,m.findLastIndex),m.range=function(n,t,r){null==t&&(t=n||0,n=0),r=r||1;for(var e=Math.max(Math.ceil((t-n)/r),0),u=Array(e),i=0;e>i;i++,n+=r)u[i]=n;return u};var E=function(n,t,r,e,u){if(!(e instanceof t))return n.apply(r,u);var i=j(n.prototype),o=n.apply(i,u);return m.isObject(o)?o:i};m.bind=function(n,t){if(g&&n.bind===g)return g.apply(n,l.call(arguments,1));if(!m.isFunction(n))throw new TypeError("Bind must be called on a function");var r=l.call(arguments,2),e=function(){return E(n,e,t,this,r.concat(l.call(arguments)))};return e},m.partial=function(n){var t=l.call(arguments,1),r=function(){for(var e=0,u=t.length,i=Array(u),o=0;u>o;o++)i[o]=t[o]===m?arguments[e++]:t[o];for(;e<arguments.length;)i.push(arguments[e++]);return E(n,r,this,this,i)};return r},m.bindAll=function(n){var t,r,e=arguments.length;if(1>=e)throw new Error("bindAll must be passed function names");for(t=1;e>t;t++)r=arguments[t],n[r]=m.bind(n[r],n);return n},m.memoize=function(n,t){var r=function(e){var u=r.cache,i=""+(t?t.apply(this,arguments):e);return m.has(u,i)||(u[i]=n.apply(this,arguments)),u[i]};return r.cache={},r},m.delay=function(n,t){var r=l.call(arguments,2);return setTimeout(function(){return n.apply(null,r)},t)},m.defer=m.partial(m.delay,m,1),m.throttle=function(n,t,r){var e,u,i,o=null,a=0;r||(r={});var c=function(){a=r.leading===!1?0:m.now(),o=null,i=n.apply(e,u),o||(e=u=null)};return function(){var f=m.now();a||r.leading!==!1||(a=f);var l=t-(f-a);return e=this,u=arguments,0>=l||l>t?(o&&(clearTimeout(o),o=null),a=f,i=n.apply(e,u),o||(e=u=null)):o||r.trailing===!1||(o=setTimeout(c,l)),i}},m.debounce=function(n,t,r){var e,u,i,o,a,c=function(){var f=m.now()-o;t>f&&f>=0?e=setTimeout(c,t-f):(e=null,r||(a=n.apply(i,u),e||(i=u=null)))};return function(){i=this,u=arguments,o=m.now();var f=r&&!e;return e||(e=setTimeout(c,t)),f&&(a=n.apply(i,u),i=u=null),a}},m.wrap=function(n,t){return m.partial(t,n)},m.negate=function(n){return function(){return!n.apply(this,arguments)}},m.compose=function(){var n=arguments,t=n.length-1;return function(){for(var r=t,e=n[t].apply(this,arguments);r--;)e=n[r].call(this,e);return e}},m.after=function(n,t){return function(){return--n<1?t.apply(this,arguments):void 0}},m.before=function(n,t){var r;return function(){return--n>0&&(r=t.apply(this,arguments)),1>=n&&(t=null),r}},m.once=m.partial(m.before,2);var M=!{toString:null}.propertyIsEnumerable("toString"),I=["valueOf","isPrototypeOf","toString","propertyIsEnumerable","hasOwnProperty","toLocaleString"];m.keys=function(n){if(!m.isObject(n))return[];if(v)return v(n);var t=[];for(var r in n)m.has(n,r)&&t.push(r);return M&&e(n,t),t},m.allKeys=function(n){if(!m.isObject(n))return[];var t=[];for(var r in n)t.push(r);return M&&e(n,t),t},m.values=function(n){for(var t=m.keys(n),r=t.length,e=Array(r),u=0;r>u;u++)e[u]=n[t[u]];return e},m.mapObject=function(n,t,r){t=x(t,r);for(var e,u=m.keys(n),i=u.length,o={},a=0;i>a;a++)e=u[a],o[e]=t(n[e],e,n);return o},m.pairs=function(n){for(var t=m.keys(n),r=t.length,e=Array(r),u=0;r>u;u++)e[u]=[t[u],n[t[u]]];return e},m.invert=function(n){for(var t={},r=m.keys(n),e=0,u=r.length;u>e;e++)t[n[r[e]]]=r[e];return t},m.functions=m.methods=function(n){var t=[];for(var r in n)m.isFunction(n[r])&&t.push(r);return t.sort()},m.extend=_(m.allKeys),m.extendOwn=m.assign=_(m.keys),m.findKey=function(n,t,r){t=x(t,r);for(var e,u=m.keys(n),i=0,o=u.length;o>i;i++)if(e=u[i],t(n[e],e,n))return e},m.pick=function(n,t,r){var e,u,i={},o=n;if(null==o)return i;m.isFunction(t)?(u=m.allKeys(o),e=b(t,r)):(u=S(arguments,!1,!1,1),e=function(n,t,r){return t in r},o=Object(o));for(var a=0,c=u.length;c>a;a++){var f=u[a],l=o[f];e(l,f,o)&&(i[f]=l)}return i},m.omit=function(n,t,r){if(m.isFunction(t))t=m.negate(t);else{var e=m.map(S(arguments,!1,!1,1),String);t=function(n,t){return!m.contains(e,t)}}return m.pick(n,t,r)},m.defaults=_(m.allKeys,!0),m.create=function(n,t){var r=j(n);return t&&m.extendOwn(r,t),r},m.clone=function(n){return m.isObject(n)?m.isArray(n)?n.slice():m.extend({},n):n},m.tap=function(n,t){return t(n),n},m.isMatch=function(n,t){var r=m.keys(t),e=r.length;if(null==n)return!e;for(var u=Object(n),i=0;e>i;i++){var o=r[i];if(t[o]!==u[o]||!(o in u))return!1}return!0};var N=function(n,t,r,e){if(n===t)return 0!==n||1/n===1/t;if(null==n||null==t)return n===t;n instanceof m&&(n=n._wrapped),t instanceof m&&(t=t._wrapped);var u=s.call(n);if(u!==s.call(t))return!1;switch(u){case"[object RegExp]":case"[object String]":return""+n==""+t;case"[object Number]":return+n!==+n?+t!==+t:0===+n?1/+n===1/t:+n===+t;case"[object Date]":case"[object Boolean]":return+n===+t}var i="[object Array]"===u;if(!i){if("object"!=typeof n||"object"!=typeof t)return!1;var o=n.constructor,a=t.constructor;if(o!==a&&!(m.isFunction(o)&&o instanceof o&&m.isFunction(a)&&a instanceof a)&&"constructor"in n&&"constructor"in t)return!1}r=r||[],e=e||[];for(var c=r.length;c--;)if(r[c]===n)return e[c]===t;if(r.push(n),e.push(t),i){if(c=n.length,c!==t.length)return!1;for(;c--;)if(!N(n[c],t[c],r,e))return!1}else{var f,l=m.keys(n);if(c=l.length,m.keys(t).length!==c)return!1;for(;c--;)if(f=l[c],!m.has(t,f)||!N(n[f],t[f],r,e))return!1}return r.pop(),e.pop(),!0};m.isEqual=function(n,t){return N(n,t)},m.isEmpty=function(n){return null==n?!0:k(n)&&(m.isArray(n)||m.isString(n)||m.isArguments(n))?0===n.length:0===m.keys(n).length},m.isElement=function(n){return!(!n||1!==n.nodeType)},m.isArray=h||function(n){return"[object Array]"===s.call(n)},m.isObject=function(n){var t=typeof n;return"function"===t||"object"===t&&!!n},m.each(["Arguments","Function","String","Number","Date","RegExp","Error"],function(n){m["is"+n]=function(t){return s.call(t)==="[object "+n+"]"}}),m.isArguments(arguments)||(m.isArguments=function(n){return m.has(n,"callee")}),"function"!=typeof/./&&"object"!=typeof Int8Array&&(m.isFunction=function(n){return"function"==typeof n||!1}),m.isFinite=function(n){return isFinite(n)&&!isNaN(parseFloat(n))},m.isNaN=function(n){return m.isNumber(n)&&n!==+n},m.isBoolean=function(n){return n===!0||n===!1||"[object Boolean]"===s.call(n)},m.isNull=function(n){return null===n},m.isUndefined=function(n){return n===void 0},m.has=function(n,t){return null!=n&&p.call(n,t)},m.noConflict=function(){return u._=i,this},m.identity=function(n){return n},m.constant=function(n){return function(){return n}},m.noop=function(){},m.property=w,m.propertyOf=function(n){return null==n?function(){}:function(t){return n[t]}},m.matcher=m.matches=function(n){return n=m.extendOwn({},n),function(t){return m.isMatch(t,n)}},m.times=function(n,t,r){var e=Array(Math.max(0,n));t=b(t,r,1);for(var u=0;n>u;u++)e[u]=t(u);return e},m.random=function(n,t){return null==t&&(t=n,n=0),n+Math.floor(Math.random()*(t-n+1))},m.now=Date.now||function(){return(new Date).getTime()};var B={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;"},T=m.invert(B),R=function(n){var t=function(t){return n[t]},r="(?:"+m.keys(n).join("|")+")",e=RegExp(r),u=RegExp(r,"g");return function(n){return n=null==n?"":""+n,e.test(n)?n.replace(u,t):n}};m.escape=R(B),m.unescape=R(T),m.result=function(n,t,r){var e=null==n?void 0:n[t];return e===void 0&&(e=r),m.isFunction(e)?e.call(n):e};var q=0;m.uniqueId=function(n){var t=++q+"";return n?n+t:t},m.templateSettings={evaluate:/<%([\s\S]+?)%>/g,interpolate:/<%=([\s\S]+?)%>/g,escape:/<%-([\s\S]+?)%>/g};var K=/(.)^/,z={"'":"'","\\":"\\","\r":"r","\n":"n","\u2028":"u2028","\u2029":"u2029"},D=/\\|'|\r|\n|\u2028|\u2029/g,L=function(n){return"\\"+z[n]};m.template=function(n,t,r){!t&&r&&(t=r),t=m.defaults({},t,m.templateSettings);var e=RegExp([(t.escape||K).source,(t.interpolate||K).source,(t.evaluate||K).source].join("|")+"|$","g"),u=0,i="__p+='";n.replace(e,function(t,r,e,o,a){return i+=n.slice(u,a).replace(D,L),u=a+t.length,r?i+="'+\n((__t=("+r+"))==null?'':_.escape(__t))+\n'":e?i+="'+\n((__t=("+e+"))==null?'':__t)+\n'":o&&(i+="';\n"+o+"\n__p+='"),t}),i+="';\n",t.variable||(i="with(obj||{}){\n"+i+"}\n"),i="var __t,__p='',__j=Array.prototype.join,"+"print=function(){__p+=__j.call(arguments,'');};\n"+i+"return __p;\n";try{var o=new Function(t.variable||"obj","_",i)}catch(a){throw a.source=i,a}var c=function(n){return o.call(this,n,m)},f=t.variable||"obj";return c.source="function("+f+"){\n"+i+"}",c},m.chain=function(n){var t=m(n);return t._chain=!0,t};var P=function(n,t){return n._chain?m(t).chain():t};m.mixin=function(n){m.each(m.functions(n),function(t){var r=m[t]=n[t];m.prototype[t]=function(){var n=[this._wrapped];return f.apply(n,arguments),P(this,r.apply(m,n))}})},m.mixin(m),m.each(["pop","push","reverse","shift","sort","splice","unshift"],function(n){var t=o[n];m.prototype[n]=function(){var r=this._wrapped;return t.apply(r,arguments),"shift"!==n&&"splice"!==n||0!==r.length||delete r[0],P(this,r)}}),m.each(["concat","join","slice"],function(n){var t=o[n];m.prototype[n]=function(){return P(this,t.apply(this._wrapped,arguments))}}),m.prototype.value=function(){return this._wrapped},m.prototype.valueOf=m.prototype.toJSON=m.prototype.value,m.prototype.toString=function(){return""+this._wrapped},"function"==typeof define&&define.amd&&define("underscore",[],function(){return m})}).call(this);
//# sourceMappingURL=underscore-min.map;
(function(){var t=this;var e=t.Backbone;var i=[];var r=i.push;var s=i.slice;var n=i.splice;var a;if(typeof exports!=="undefined"){a=exports}else{a=t.Backbone={}}a.VERSION="1.0.0";var h=t._;if(!h&&typeof require!=="undefined")h=require("underscore");a.$=t.jQuery||t.Zepto||t.ender||t.$;a.noConflict=function(){t.Backbone=e;return this};a.emulateHTTP=false;a.emulateJSON=false;var o=a.Events={on:function(t,e,i){if(!l(this,"on",t,[e,i])||!e)return this;this._events||(this._events={});var r=this._events[t]||(this._events[t]=[]);r.push({callback:e,context:i,ctx:i||this});return this},once:function(t,e,i){if(!l(this,"once",t,[e,i])||!e)return this;var r=this;var s=h.once(function(){r.off(t,s);e.apply(this,arguments)});s._callback=e;return this.on(t,s,i)},off:function(t,e,i){var r,s,n,a,o,u,c,f;if(!this._events||!l(this,"off",t,[e,i]))return this;if(!t&&!e&&!i){this._events={};return this}a=t?[t]:h.keys(this._events);for(o=0,u=a.length;o<u;o++){t=a[o];if(n=this._events[t]){this._events[t]=r=[];if(e||i){for(c=0,f=n.length;c<f;c++){s=n[c];if(e&&e!==s.callback&&e!==s.callback._callback||i&&i!==s.context){r.push(s)}}}if(!r.length)delete this._events[t]}}return this},trigger:function(t){if(!this._events)return this;var e=s.call(arguments,1);if(!l(this,"trigger",t,e))return this;var i=this._events[t];var r=this._events.all;if(i)c(i,e);if(r)c(r,arguments);return this},stopListening:function(t,e,i){var r=this._listeners;if(!r)return this;var s=!e&&!i;if(typeof e==="object")i=this;if(t)(r={})[t._listenerId]=t;for(var n in r){r[n].off(e,i,this);if(s)delete this._listeners[n]}return this}};var u=/\s+/;var l=function(t,e,i,r){if(!i)return true;if(typeof i==="object"){for(var s in i){t[e].apply(t,[s,i[s]].concat(r))}return false}if(u.test(i)){var n=i.split(u);for(var a=0,h=n.length;a<h;a++){t[e].apply(t,[n[a]].concat(r))}return false}return true};var c=function(t,e){var i,r=-1,s=t.length,n=e[0],a=e[1],h=e[2];switch(e.length){case 0:while(++r<s)(i=t[r]).callback.call(i.ctx);return;case 1:while(++r<s)(i=t[r]).callback.call(i.ctx,n);return;case 2:while(++r<s)(i=t[r]).callback.call(i.ctx,n,a);return;case 3:while(++r<s)(i=t[r]).callback.call(i.ctx,n,a,h);return;default:while(++r<s)(i=t[r]).callback.apply(i.ctx,e)}};var f={listenTo:"on",listenToOnce:"once"};h.each(f,function(t,e){o[e]=function(e,i,r){var s=this._listeners||(this._listeners={});var n=e._listenerId||(e._listenerId=h.uniqueId("l"));s[n]=e;if(typeof i==="object")r=this;e[t](i,r,this);return this}});o.bind=o.on;o.unbind=o.off;h.extend(a,o);var d=a.Model=function(t,e){var i;var r=t||{};e||(e={});this.cid=h.uniqueId("c");this.attributes={};h.extend(this,h.pick(e,p));if(e.parse)r=this.parse(r,e)||{};if(i=h.result(this,"defaults")){r=h.defaults({},r,i)}this.set(r,e);this.changed={};this.initialize.apply(this,arguments)};var p=["url","urlRoot","collection"];h.extend(d.prototype,o,{changed:null,validationError:null,idAttribute:"id",initialize:function(){},toJSON:function(t){return h.clone(this.attributes)},sync:function(){return a.sync.apply(this,arguments)},get:function(t){return this.attributes[t]},escape:function(t){return h.escape(this.get(t))},has:function(t){return this.get(t)!=null},set:function(t,e,i){var r,s,n,a,o,u,l,c;if(t==null)return this;if(typeof t==="object"){s=t;i=e}else{(s={})[t]=e}i||(i={});if(!this._validate(s,i))return false;n=i.unset;o=i.silent;a=[];u=this._changing;this._changing=true;if(!u){this._previousAttributes=h.clone(this.attributes);this.changed={}}c=this.attributes,l=this._previousAttributes;if(this.idAttribute in s)this.id=s[this.idAttribute];for(r in s){e=s[r];if(!h.isEqual(c[r],e))a.push(r);if(!h.isEqual(l[r],e)){this.changed[r]=e}else{delete this.changed[r]}n?delete c[r]:c[r]=e}if(!o){if(a.length)this._pending=true;for(var f=0,d=a.length;f<d;f++){this.trigger("change:"+a[f],this,c[a[f]],i)}}if(u)return this;if(!o){while(this._pending){this._pending=false;this.trigger("change",this,i)}}this._pending=false;this._changing=false;return this},unset:function(t,e){return this.set(t,void 0,h.extend({},e,{unset:true}))},clear:function(t){var e={};for(var i in this.attributes)e[i]=void 0;return this.set(e,h.extend({},t,{unset:true}))},hasChanged:function(t){if(t==null)return!h.isEmpty(this.changed);return h.has(this.changed,t)},changedAttributes:function(t){if(!t)return this.hasChanged()?h.clone(this.changed):false;var e,i=false;var r=this._changing?this._previousAttributes:this.attributes;for(var s in t){if(h.isEqual(r[s],e=t[s]))continue;(i||(i={}))[s]=e}return i},previous:function(t){if(t==null||!this._previousAttributes)return null;return this._previousAttributes[t]},previousAttributes:function(){return h.clone(this._previousAttributes)},fetch:function(t){t=t?h.clone(t):{};if(t.parse===void 0)t.parse=true;var e=this;var i=t.success;t.success=function(r){if(!e.set(e.parse(r,t),t))return false;if(i)i(e,r,t);e.trigger("sync",e,r,t)};R(this,t);return this.sync("read",this,t)},save:function(t,e,i){var r,s,n,a=this.attributes;if(t==null||typeof t==="object"){r=t;i=e}else{(r={})[t]=e}if(r&&(!i||!i.wait)&&!this.set(r,i))return false;i=h.extend({validate:true},i);if(!this._validate(r,i))return false;if(r&&i.wait){this.attributes=h.extend({},a,r)}if(i.parse===void 0)i.parse=true;var o=this;var u=i.success;i.success=function(t){o.attributes=a;var e=o.parse(t,i);if(i.wait)e=h.extend(r||{},e);if(h.isObject(e)&&!o.set(e,i)){return false}if(u)u(o,t,i);o.trigger("sync",o,t,i)};R(this,i);s=this.isNew()?"create":i.patch?"patch":"update";if(s==="patch")i.attrs=r;n=this.sync(s,this,i);if(r&&i.wait)this.attributes=a;return n},destroy:function(t){t=t?h.clone(t):{};var e=this;var i=t.success;var r=function(){e.trigger("destroy",e,e.collection,t)};t.success=function(s){if(t.wait||e.isNew())r();if(i)i(e,s,t);if(!e.isNew())e.trigger("sync",e,s,t)};if(this.isNew()){t.success();return false}R(this,t);var s=this.sync("delete",this,t);if(!t.wait)r();return s},url:function(){var t=h.result(this,"urlRoot")||h.result(this.collection,"url")||U();if(this.isNew())return t;return t+(t.charAt(t.length-1)==="/"?"":"/")+encodeURIComponent(this.id)},parse:function(t,e){return t},clone:function(){return new this.constructor(this.attributes)},isNew:function(){return this.id==null},isValid:function(t){return this._validate({},h.extend(t||{},{validate:true}))},_validate:function(t,e){if(!e.validate||!this.validate)return true;t=h.extend({},this.attributes,t);var i=this.validationError=this.validate(t,e)||null;if(!i)return true;this.trigger("invalid",this,i,h.extend(e||{},{validationError:i}));return false}});var v=["keys","values","pairs","invert","pick","omit"];h.each(v,function(t){d.prototype[t]=function(){var e=s.call(arguments);e.unshift(this.attributes);return h[t].apply(h,e)}});var g=a.Collection=function(t,e){e||(e={});if(e.url)this.url=e.url;if(e.model)this.model=e.model;if(e.comparator!==void 0)this.comparator=e.comparator;this._reset();this.initialize.apply(this,arguments);if(t)this.reset(t,h.extend({silent:true},e))};var m={add:true,remove:true,merge:true};var y={add:true,merge:false,remove:false};h.extend(g.prototype,o,{model:d,initialize:function(){},toJSON:function(t){return this.map(function(e){return e.toJSON(t)})},sync:function(){return a.sync.apply(this,arguments)},add:function(t,e){return this.set(t,h.defaults(e||{},y))},remove:function(t,e){t=h.isArray(t)?t.slice():[t];e||(e={});var i,r,s,n;for(i=0,r=t.length;i<r;i++){n=this.get(t[i]);if(!n)continue;delete this._byId[n.id];delete this._byId[n.cid];s=this.indexOf(n);this.models.splice(s,1);this.length--;if(!e.silent){e.index=s;n.trigger("remove",n,this,e)}this._removeReference(n)}return this},set:function(t,e){e=h.defaults(e||{},m);if(e.parse)t=this.parse(t,e);if(!h.isArray(t))t=t?[t]:[];var i,s,a,o,u,l;var c=e.at;var f=this.comparator&&c==null&&e.sort!==false;var d=h.isString(this.comparator)?this.comparator:null;var p=[],v=[],g={};for(i=0,s=t.length;i<s;i++){if(!(a=this._prepareModel(t[i],e)))continue;if(u=this.get(a)){if(e.remove)g[u.cid]=true;if(e.merge){u.set(a.attributes,e);if(f&&!l&&u.hasChanged(d))l=true}}else if(e.add){p.push(a);a.on("all",this._onModelEvent,this);this._byId[a.cid]=a;if(a.id!=null)this._byId[a.id]=a}}if(e.remove){for(i=0,s=this.length;i<s;++i){if(!g[(a=this.models[i]).cid])v.push(a)}if(v.length)this.remove(v,e)}if(p.length){if(f)l=true;this.length+=p.length;if(c!=null){n.apply(this.models,[c,0].concat(p))}else{r.apply(this.models,p)}}if(l)this.sort({silent:true});if(e.silent)return this;for(i=0,s=p.length;i<s;i++){(a=p[i]).trigger("add",a,this,e)}if(l)this.trigger("sort",this,e);return this},reset:function(t,e){e||(e={});for(var i=0,r=this.models.length;i<r;i++){this._removeReference(this.models[i])}e.previousModels=this.models;this._reset();this.add(t,h.extend({silent:true},e));if(!e.silent)this.trigger("reset",this,e);return this},push:function(t,e){t=this._prepareModel(t,e);this.add(t,h.extend({at:this.length},e));return t},pop:function(t){var e=this.at(this.length-1);this.remove(e,t);return e},unshift:function(t,e){t=this._prepareModel(t,e);this.add(t,h.extend({at:0},e));return t},shift:function(t){var e=this.at(0);this.remove(e,t);return e},slice:function(t,e){return this.models.slice(t,e)},get:function(t){if(t==null)return void 0;return this._byId[t.id!=null?t.id:t.cid||t]},at:function(t){return this.models[t]},where:function(t,e){if(h.isEmpty(t))return e?void 0:[];return this[e?"find":"filter"](function(e){for(var i in t){if(t[i]!==e.get(i))return false}return true})},findWhere:function(t){return this.where(t,true)},sort:function(t){if(!this.comparator)throw new Error("Cannot sort a set without a comparator");t||(t={});if(h.isString(this.comparator)||this.comparator.length===1){this.models=this.sortBy(this.comparator,this)}else{this.models.sort(h.bind(this.comparator,this))}if(!t.silent)this.trigger("sort",this,t);return this},sortedIndex:function(t,e,i){e||(e=this.comparator);var r=h.isFunction(e)?e:function(t){return t.get(e)};return h.sortedIndex(this.models,t,r,i)},pluck:function(t){return h.invoke(this.models,"get",t)},fetch:function(t){t=t?h.clone(t):{};if(t.parse===void 0)t.parse=true;var e=t.success;var i=this;t.success=function(r){var s=t.reset?"reset":"set";i[s](r,t);if(e)e(i,r,t);i.trigger("sync",i,r,t)};R(this,t);return this.sync("read",this,t)},create:function(t,e){e=e?h.clone(e):{};if(!(t=this._prepareModel(t,e)))return false;if(!e.wait)this.add(t,e);var i=this;var r=e.success;e.success=function(s){if(e.wait)i.add(t,e);if(r)r(t,s,e)};t.save(null,e);return t},parse:function(t,e){return t},clone:function(){return new this.constructor(this.models)},_reset:function(){this.length=0;this.models=[];this._byId={}},_prepareModel:function(t,e){if(t instanceof d){if(!t.collection)t.collection=this;return t}e||(e={});e.collection=this;var i=new this.model(t,e);if(!i._validate(t,e)){this.trigger("invalid",this,t,e);return false}return i},_removeReference:function(t){if(this===t.collection)delete t.collection;t.off("all",this._onModelEvent,this)},_onModelEvent:function(t,e,i,r){if((t==="add"||t==="remove")&&i!==this)return;if(t==="destroy")this.remove(e,r);if(e&&t==="change:"+e.idAttribute){delete this._byId[e.previous(e.idAttribute)];if(e.id!=null)this._byId[e.id]=e}this.trigger.apply(this,arguments)}});var _=["forEach","each","map","collect","reduce","foldl","inject","reduceRight","foldr","find","detect","filter","select","reject","every","all","some","any","include","contains","invoke","max","min","toArray","size","first","head","take","initial","rest","tail","drop","last","without","indexOf","shuffle","lastIndexOf","isEmpty","chain"];h.each(_,function(t){g.prototype[t]=function(){var e=s.call(arguments);e.unshift(this.models);return h[t].apply(h,e)}});var w=["groupBy","countBy","sortBy"];h.each(w,function(t){g.prototype[t]=function(e,i){var r=h.isFunction(e)?e:function(t){return t.get(e)};return h[t](this.models,r,i)}});var b=a.View=function(t){this.cid=h.uniqueId("view");this._configure(t||{});this._ensureElement();this.initialize.apply(this,arguments);this.delegateEvents()};var x=/^(\S+)\s*(.*)$/;var E=["model","collection","el","id","attributes","className","tagName","events"];h.extend(b.prototype,o,{tagName:"div",$:function(t){return this.$el.find(t)},initialize:function(){},render:function(){return this},remove:function(){this.$el.remove();this.stopListening();return this},setElement:function(t,e){if(this.$el)this.undelegateEvents();this.$el=t instanceof a.$?t:a.$(t);this.el=this.$el[0];if(e!==false)this.delegateEvents();return this},delegateEvents:function(t){if(!(t||(t=h.result(this,"events"))))return this;this.undelegateEvents();for(var e in t){var i=t[e];if(!h.isFunction(i))i=this[t[e]];if(!i)continue;var r=e.match(x);var s=r[1],n=r[2];i=h.bind(i,this);s+=".delegateEvents"+this.cid;if(n===""){this.$el.on(s,i)}else{this.$el.on(s,n,i)}}return this},undelegateEvents:function(){this.$el.off(".delegateEvents"+this.cid);return this},_configure:function(t){if(this.options)t=h.extend({},h.result(this,"options"),t);h.extend(this,h.pick(t,E));this.options=t},_ensureElement:function(){if(!this.el){var t=h.extend({},h.result(this,"attributes"));if(this.id)t.id=h.result(this,"id");if(this.className)t["class"]=h.result(this,"className");var e=a.$("<"+h.result(this,"tagName")+">").attr(t);this.setElement(e,false)}else{this.setElement(h.result(this,"el"),false)}}});a.sync=function(t,e,i){var r=k[t];h.defaults(i||(i={}),{emulateHTTP:a.emulateHTTP,emulateJSON:a.emulateJSON});var s={type:r,dataType:"json"};if(!i.url){s.url=h.result(e,"url")||U()}if(i.data==null&&e&&(t==="create"||t==="update"||t==="patch")){s.contentType="application/json";s.data=JSON.stringify(i.attrs||e.toJSON(i))}if(i.emulateJSON){s.contentType="application/x-www-form-urlencoded";s.data=s.data?{model:s.data}:{}}if(i.emulateHTTP&&(r==="PUT"||r==="DELETE"||r==="PATCH")){s.type="POST";if(i.emulateJSON)s.data._method=r;var n=i.beforeSend;i.beforeSend=function(t){t.setRequestHeader("X-HTTP-Method-Override",r);if(n)return n.apply(this,arguments)}}if(s.type!=="GET"&&!i.emulateJSON){s.processData=false}if(s.type==="PATCH"&&window.ActiveXObject&&!(window.external&&window.external.msActiveXFilteringEnabled)){s.xhr=function(){return new ActiveXObject("Microsoft.XMLHTTP")}}var o=i.xhr=a.ajax(h.extend(s,i));e.trigger("request",e,o,i);return o};var k={create:"POST",update:"PUT",patch:"PATCH","delete":"DELETE",read:"GET"};a.ajax=function(){return a.$.ajax.apply(a.$,arguments)};var S=a.Router=function(t){t||(t={});if(t.routes)this.routes=t.routes;this._bindRoutes();this.initialize.apply(this,arguments)};var $=/\((.*?)\)/g;var T=/(\(\?)?:\w+/g;var H=/\*\w+/g;var A=/[\-{}\[\]+?.,\\\^$|#\s]/g;h.extend(S.prototype,o,{initialize:function(){},route:function(t,e,i){if(!h.isRegExp(t))t=this._routeToRegExp(t);if(h.isFunction(e)){i=e;e=""}if(!i)i=this[e];var r=this;a.history.route(t,function(s){var n=r._extractParameters(t,s);i&&i.apply(r,n);r.trigger.apply(r,["route:"+e].concat(n));r.trigger("route",e,n);a.history.trigger("route",r,e,n)});return this},navigate:function(t,e){a.history.navigate(t,e);return this},_bindRoutes:function(){if(!this.routes)return;this.routes=h.result(this,"routes");var t,e=h.keys(this.routes);while((t=e.pop())!=null){this.route(t,this.routes[t])}},_routeToRegExp:function(t){t=t.replace(A,"\\$&").replace($,"(?:$1)?").replace(T,function(t,e){return e?t:"([^/]+)"}).replace(H,"(.*?)");return new RegExp("^"+t+"$")},_extractParameters:function(t,e){var i=t.exec(e).slice(1);return h.map(i,function(t){return t?decodeURIComponent(t):null})}});var I=a.History=function(){this.handlers=[];h.bindAll(this,"checkUrl");if(typeof window!=="undefined"){this.location=window.location;this.history=window.history}};var N=/^[#\/]|\s+$/g;var P=/^\/+|\/+$/g;var O=/msie [\w.]+/;var C=/\/$/;I.started=false;h.extend(I.prototype,o,{interval:50,getHash:function(t){var e=(t||this).location.href.match(/#(.*)$/);return e?e[1]:""},getFragment:function(t,e){if(t==null){if(this._hasPushState||!this._wantsHashChange||e){t=this.location.pathname;var i=this.root.replace(C,"");if(!t.indexOf(i))t=t.substr(i.length)}else{t=this.getHash()}}return t.replace(N,"")},start:function(t){if(I.started)throw new Error("Backbone.history has already been started");I.started=true;this.options=h.extend({},{root:"/"},this.options,t);this.root=this.options.root;this._wantsHashChange=this.options.hashChange!==false;this._wantsPushState=!!this.options.pushState;this._hasPushState=!!(this.options.pushState&&this.history&&this.history.pushState);var e=this.getFragment();var i=document.documentMode;var r=O.exec(navigator.userAgent.toLowerCase())&&(!i||i<=7);this.root=("/"+this.root+"/").replace(P,"/");if(r&&this._wantsHashChange){this.iframe=a.$('<iframe src="javascript:0" tabindex="-1" />').hide().appendTo("body")[0].contentWindow;this.navigate(e)}if(this._hasPushState){a.$(window).on("popstate",this.checkUrl)}else if(this._wantsHashChange&&"onhashchange"in window&&!r){a.$(window).on("hashchange",this.checkUrl)}else if(this._wantsHashChange){this._checkUrlInterval=setInterval(this.checkUrl,this.interval)}this.fragment=e;var s=this.location;var n=s.pathname.replace(/[^\/]$/,"$&/")===this.root;if(this._wantsHashChange&&this._wantsPushState&&!this._hasPushState&&!n){this.fragment=this.getFragment(null,true);this.location.replace(this.root+this.location.search+"#"+this.fragment);return true}else if(this._wantsPushState&&this._hasPushState&&n&&s.hash){this.fragment=this.getHash().replace(N,"");this.history.replaceState({},document.title,this.root+this.fragment+s.search)}if(!this.options.silent)return this.loadUrl()},stop:function(){a.$(window).off("popstate",this.checkUrl).off("hashchange",this.checkUrl);clearInterval(this._checkUrlInterval);I.started=false},route:function(t,e){this.handlers.unshift({route:t,callback:e})},checkUrl:function(t){var e=this.getFragment();if(e===this.fragment&&this.iframe){e=this.getFragment(this.getHash(this.iframe))}if(e===this.fragment)return false;if(this.iframe)this.navigate(e);this.loadUrl()||this.loadUrl(this.getHash())},loadUrl:function(t){var e=this.fragment=this.getFragment(t);var i=h.any(this.handlers,function(t){if(t.route.test(e)){t.callback(e);return true}});return i},navigate:function(t,e){if(!I.started)return false;if(!e||e===true)e={trigger:e};t=this.getFragment(t||"");if(this.fragment===t)return;this.fragment=t;var i=this.root+t;if(this._hasPushState){this.history[e.replace?"replaceState":"pushState"]({},document.title,i)}else if(this._wantsHashChange){this._updateHash(this.location,t,e.replace);if(this.iframe&&t!==this.getFragment(this.getHash(this.iframe))){if(!e.replace)this.iframe.document.open().close();this._updateHash(this.iframe.location,t,e.replace)}}else{return this.location.assign(i)}if(e.trigger)this.loadUrl(t)},_updateHash:function(t,e,i){if(i){var r=t.href.replace(/(javascript:|#).*$/,"");t.replace(r+"#"+e)}else{t.hash="#"+e}}});a.history=new I;var j=function(t,e){var i=this;var r;if(t&&h.has(t,"constructor")){r=t.constructor}else{r=function(){return i.apply(this,arguments)}}h.extend(r,i,e);var s=function(){this.constructor=r};s.prototype=i.prototype;r.prototype=new s;if(t)h.extend(r.prototype,t);r.__super__=i.prototype;return r};d.extend=g.extend=S.extend=b.extend=I.extend=j;var U=function(){throw new Error('A "url" property or function must be specified')};var R=function(t,e){var i=e.error;e.error=function(r){if(i)i(t,r,e);t.trigger("error",t,r,e)}}}).call(this);
/*
//@ sourceMappingURL=backbone-min.map
*/;
define("backbone", ["underscore"], (function (global) {
    return function () {
        var ret, fn;
        return ret || global.Backbone;
    };
}(this)));

define('js/Transition',['underscore', 'backbone'],
function (_, Backbone)
{
  

  function Transition(rootDom)
  {
    this.rootDom = rootDom;
    this.lastWaitText = null;
    this.pageHasFailed = false;
  }

  Transition.prototype.page = function (goFrom, goTo)
  {
    this.rootDom.find('button.logout').show();
/*
    if (!(goFrom.hasClass('hides')))
    {
      var item = goFrom;
      item.addClass('hides');
      item.one('transitionend', function() {
	item.addClass('afterHide');

	goTo.removeClass('afterHide');
	setTimeout(function() {
	  goTo.removeClass('hides');
	}, 20);
      });
    }
*/
    goFrom.hide();
    goTo.show();
    goTo.removeClass('hides');
    goTo.removeClass('afterHide');
  };

  Transition.prototype.startWaiting = function (oldNode, text)
  {
//    var shouldAppend = (this.lastWaitText === null);
    var shouldAppend = true;
    if (oldNode)
    {
      this.page(oldNode, this.rootDom.find('#waitContainer'));
    }
//    this.hideWait(this.lastWaitText);
    if (text)
    {
      this.lastWaitText = $('<h1>' + text + '</h1>');
      if (shouldAppend)
      {
	this.rootDom.find('#waitText').append(this.lastWaitText);
      }
    }
    else
    {
      this.rootDom.find('#waitText').html('');
    }
  };

  Transition.prototype.stopWaiting = function (newNode)
  {
    this.page(this.rootDom.find('#waitContainer'), newNode);
    this.rootDom.find('#waitText').html('');
//    this.hideWait(this.lastWaitText);
//    this.lastWaitText = null;
  };

  Transition.prototype.hideWait = function (node)
  {
    var that = this;
    if (node)
    {
      node.addClass('hides')
	.one('transitionend', function (event) {
	  node.remove();
	  if (that.lastWaitText)
	  {
	    that.rootDom.find('#waitText').append(that.lastWaitText);
	  }
	});
    }
  }


  Transition.prototype.startFail = function (node, message, error)
  {
    if (! this.pageHasFailed)
    {
      this.page(node, this.rootDom.find('#failContainer'));
      $('#failTitle').html(_.escape(message));
      if (error)
      {
	this.rootDom.find('#failMessage').html(_.escape(vkbeautify.json(JSON.stringify(error))));
      }
      this.pageHasFailed = true;
    }
  };

  Transition.prototype.switchNav = function (target)
  {
    this.rootDom.find('.navbar').each(function() {
      $(this).addClass('hidden');
    });
    target.removeClass('hidden');
  };

  return Transition;
});

define('js/cache',['underscore'],
function (_)
{
  

  var cache = {};

  var localStorageParams = {};
  var sessionStorageParams = {};

  var initialized = false;

  var sessionStorageItems = [
    "passphrase"
  ];

  var localStorageItems = [
    "userName",
    "userUrn",
    "userCert",
    "userKey",
    "userCredential",
    "provider",
    "cabundle",
    "salist",
    "amlist"
  ];

  function initializeCache()
  {
    initialized = true;
    try {
      sessionStorageParams = JSON.parse(window.sessionStorage.params);
      localStorageParams = JSON.parse(window.localStorage.params);
    } catch (e) { }
  }

  cache.get = function(cachedItem)
  {
    var result = null;
    if (!initialized)
    {
      initializeCache();
    }
    if (_.contains(sessionStorageItems, cachedItem))
    {
      try {
	result = sessionStorageParams[cachedItem];
      } catch (e) { }
    }
    else if (_.contains(localStorageItems, cachedItem))
    {
      try {
	result = localStorageParams[cachedItem];
      } catch (e) { }
    }
    return result;
  };

  cache.set = function(cacheName, cacheValue)
  {
    if (!initialized)
    {
      initializeCache();
    }
    if (_.contains(sessionStorageItems, cacheName))
    {
      try {
	sessionStorageParams[cacheName] = cacheValue;
	sessionStorage.params = JSON.stringify(sessionStorageParams);
      } catch (e) { }
    }
    else if (_.contains(localStorageItems, cacheName))
    {
      try {
	localStorageParams[cacheName] = cacheValue;
	localStorage.params = JSON.stringify(localStorageParams);
      } catch (e) { }
    }
  };

  cache.remove = function(cacheName)
  {
    if (!initialized)
    {
      initializeCache();
    }
    if (_.contains(sessionStorageItems, cacheName))
    {
      try {
	delete sessionStorageParams[cacheName];
	sessionStorage.params = JSON.stringify(sessionStorageParams);
      } catch (e) { }
    }
    if (_.contains(localStorageItems, cacheName))
    {
      try {
	delete localStorageParams[cacheName];
	localStorage.params = JSON.stringify(sessionStorageParams);
      } catch (e) { }
    }
  };

  return cache;
});

define('lib/PrettyXML',[],
function ()
{
  

  return {

    format: function (xml) {
      var formatted = '';
      var reg = /(>)\s*(<)(\/*)/g;
      xml = xml.toString().replace(reg, '$1\r\n$2$3');
      var pad = 0;
      var nodes = xml.split('\r\n');
      for(var n in nodes) {
	var node = nodes[n];
	var indent = 0;
	if (node.match(/.+<\/\w[^>]*>$/)) {
	  indent = 0;
	} else if (node.match(/^<\/\w/)) {
	  if (pad !== 0) {
            pad -= 1;
	  }
	} else if (node.match(/^<\w[^>]*[^\/]>.*$/)) {
	  indent = 1;
	} else {
	  indent = 0;
	}
	
	var padding = '';
	for (var i = 0; i < pad; i++) {
	  padding += '  ';
	}
  
	formatted += padding + node + '\r\n';
	pad += indent;
      }
      return formatted;
    }

  };
});

define('js/canvas/Constraints',['underscore'],
function (_)
{
  

  var types = {
    'aggregates': 'aggregates',
    'images': 'images',
    'types': 'types',
    'hardware': 'hardware',
    'linkTypes': 'linkTypes',
    'sharedvlans': 'sharedvlans',
    'linkNodes1': 'types',
    'linkNodes2': 'types'
  };

  var debug = false;

  function Constraints(context)
  {
    this.groups = {};
    this.possible = {};
    this.addPossibles(context.canvasOptions);
    this.allowAllSets(context.constraints);
    this.cache = {};
  }

  Constraints.prototype.addPossibles = function (options)
  {
    _.each(_.keys(types), function (type) {
      var typeOption = types[type];
      var list = this.possible[type] || [];
      var newValues = _.without(_.pluck(options[typeOption], 'id'), undefined);
      list = _.union(list, newValues);
      this.possible[type] = list;
    }.bind(this));
    this.cache = {};
  }

  Constraints.prototype.allowAllSets = function (list)
  {
    var that = this;
    _.each(list, function (set) {
      that.allowSet(set, true);
      if (set['node'] || set['node2'])
      {
	var swapped = _.clone(set);
	swapped['node'] = set['node2'];
	swapped['node2'] = set['node'];
	that.allowSet(swapped, false);
	if (set['node'] && set['link'] && ! set['node2'])
	{
	  swapped = _.clone(set);
	  swapped['node2'] = swapped['node'];
	  that.allowSet(swapped, false);
	}
	else if (set['node2'] && set['link'] && ! set['node'])
	{
	  swapped = _.clone(set);
	  swapped['node'] = swapped['node2'];
	  that.allowSet(swapped, false);
	}
      }
    });
    this.cache = {};
  };

  Constraints.prototype.allowSet = function (set, shouldComplain)
  {
    var that = this;
    var groupKey = makeGroupKey(set);
    that.groups[groupKey] = that.groups[groupKey] ||
      {
	node: { 'hash': {}, 'keys': null },
	'template': set
      };
    var list = makeTailList(set);
    if (checkTail(that.groups[groupKey].node, list))
    {
      if (shouldComplain)
      {
	  console.log('Duplicate constraint', set);
      }
    }
    else
    {
      enableTail(that.groups[groupKey].node, list);
    }
  };

  // All sets in the same group can be thought of as members of an
  // ordered list with each item in the list corresponding to a
  // particular subclause/key
  function makeTailList(set, template, possible)
  {
    if (! template)
    {
      template = set;
    }
    var list = [];
    var run = function (subClause, key) {
      if (template[subClause] && template[subClause][key])
      {
	if (set[subClause] && set[subClause][key] &&
	    (! possible || possible[key].indexOf(set[subClause][key]) !== -1))
	{
	  list.push(set[subClause][key]);
	}
	else
	{
	  list.push(undefined);
	}
      }
    };
    run('node', 'aggregates');
    run('node', 'images');
    run('node', 'types');
    run('node', 'hardware');

    run('node2', 'aggregates');
    run('node2', 'images');
    run('node2', 'types');
    run('node2', 'hardware');

    run('link', 'aggregates');
    run('link', 'linkTypes');
    run('link', 'sharedvlans');
    run('link', 'linkNodes1');
    run('link', 'linkNodes2');
//    var typeList = _.keys(types);
//    _.each(['node', 'node2', 'link'], function (subClause) {
//      _.each(typeList, function (key) {
//      });
//    });
    return list;
  }

  function allPossible(set, possible)
  {
    var result = true;
    _.each(_.keys(set), function (subClause) {
      _.each(_.keys(set[subClause]), function (key) {
	if (set[subClause][key] !== undefined && (! possible || possible[key].indexOf(set[subClause][key]) === -1))
	{
	  result = false;
	}
      });
    });
    return result;
  }

  function enableTail(node, list)
  {
    if (list.length === 1)
    {
      _.each(list[0], function (item) {
	node.hash[item] = true;
      });
    }
    else if (list.length > 1)
    {
      if (list[0] !== undefined)
      {
	var remainder = _.rest(list);
	_.each(list[0], function (item) {
	  node.hash[item] = node.hash[item] || { 'hash': {}, keys: null };
	  enableTail(node.hash[item], remainder);
	});
      }
    }
  }

  function checkTail(node, list)
  {
    var result = false;
    if (node)
    {
      if (list.length === 1)
      {
	if (list[0] === undefined)
	{
	  result = _.contains(_.values(node.hash), true);
	}
	else
	{
	  result = (node.hash[list[0]] === true);
	}
      }
      else if (list.length > 1)
      {
	var checks = [];
	if (list[0] === undefined)
	{
	  if (node.keys === null)
	  {
	    node.keys = _.keys(node.hash);
	  }
	  checks = node.keys;
	}
	else
	{
	  checks = [list[0], '*'];
	}
	var i = 0;
	for (; i < checks.length; i += 1)
	{
	  result = checkTail(node.hash[checks[i]], _.rest(list));
	  if (result)
	  {
	    break;
	  }
	}
      }
    }
    return result;
  }

  // Returns a list of valid ids of unboundType inside of the
  // unboundSubclause which are a valid match when combined with every
  // bound item in the boundList
  Constraints.prototype.getValidList = function (boundList,
						 unboundSubclause,
						 unboundType,
						 rejected,
						 rejectBreakdown)
  {
    if (debug) { console.log('List for: ', unboundSubclause, unboundType); }
    var that = this;
    var result = [];
    _.each(that.possible[unboundType], function (item) {
/*
      var candidateList = [];
      _.each(boundList, function (item) {
	var clone = {};
	_.each(_.keys(item), function (key) {
	  clone[key] = _.clone(item[key]);
	});
	candidateList.push(clone);
      });
*/
      var candidateList = boundList;
      _.each(candidateList, function (candidate) {
	if (debug) { console.log('-', unboundType, item); }
	if (! candidate[unboundSubclause])
	{
	  candidate[unboundSubclause] = {};
	}
	candidate[unboundSubclause][unboundType] = item;
      });
      var allValid = true;
      _.each(candidateList, function (candidate) {
	if (! that.isValid(candidate))
	{
	  allValid = false;
	  if (rejectBreakdown !== undefined)
	  {
	    if (rejectBreakdown[item] === undefined)
	    {
	      rejectBreakdown[item] = [];
	    }
	    rejectBreakdown[item].push(candidate);
	  }
	}
      });
      if (allValid)
      {
	result.push(item);
      }
      else if (rejected)
      {
	rejected.push(item);
      }
    });
    if (debug) { console.log('Overall result is', result); }
    return result;
  };

  // Returns true if all candidates in the candidate list match every
  // group.
  Constraints.prototype.allValid = function (candidateList)
  {
    var that = this;
    var result = true;
    _.each(candidateList, function (candidate) {
      result = result && that.isValid(candidate);
    });
    if (debug) { console.log('-', 'End Candidate List: ', candidateList); }
    return result;
  };

  // Check to see if a single candidate is valid. A candidate is valid
  // if it matches every group. Candidate subkeys contain strings
  // while clause subkeys contain lists of strings.
  Constraints.prototype.isValid = function (candidate)
  {
    var that = this;
    var result = true;
    var name = uniqueName(candidate);
    if (this.cache[name] !== undefined)
    {
      result = this.cache[name];
    }
    else
    {
      if (allPossible(candidate, this.possible))
      {
	console.log('allPossible', candidate, this.possible);
	_.each(_.keys(that.groups), function (groupKey) {
	  result = result && that.isValidForGroup(candidate, that.groups[groupKey]);
	});
      }
      else
      {
	result = true;
      }
      this.cache[name] = result;
    }
    if (debug) { console.log('--', 'Candidate: ', result, candidate); }
    return result;
  };

  // A group is a set of clauses which target identical keys. These
  // are not explicit in the input, but are implicitly grouped by
  // looking at the actual keys. A group is valid if any clause in the
  // group matches.
  Constraints.prototype.isValidForGroup = function (candidate, group)
  {
    var result = false;
    var list = makeTailList(candidate, group.template, this.possible);
    result = checkTail(group.node, list);
/*
    _.each(group, function (clause) {
      result = result || matchClause(candidate, clause, this.possible);
    }.bind(this));
*/
    if (debug) { console.log('---', 'Group: ', result, candidate, group); }
    return result;
  };

  // A clause is the atomic unit of the whitelist. This is one item on
  // the list of constraints. If all the sub-keys match, the whole
  // thing matches.
  function matchClause(candidate, clause, possible)
  {
    var result = true;
    if (candidate['link'] || ! clause['link'])
    {
      _.each(_.keys(clause), function (key) {
	result = result && (! candidate[key] || ! clause[key] ||
			    matchSubkeys(candidate[key], clause[key],
					 possible));
      });
    }
    if (debug) { console.log('----', 'Clause: ', result, candidate, clause); }
    return result;
  };

  // Clauses are logically divided into three subkey components for
  // clarity. These are the 'node', 'link', and 'node2'
  // sub-clauses. All of the sub-clauses have to match in order for
  // the clause as a whole to match.
  function matchSubkeys(candidateSub, clauseSub, possible)
  {
    var result = true;
    _.each(_.keys(clauseSub), function (key) {
      result = result && (! candidateSub[key] || ! clauseSub[key] ||
			  possible[key].indexOf(candidateSub[key]) === -1 ||
			  _.contains(clauseSub[key], '*') ||
			  _.contains(clauseSub[key], candidateSub[key]));
    });
    if (debug) { console.log('-----', 'Subkeys: ', result, candidateSub, clauseSub); }
    return result;
  };

  function makeGroupKey(obj)
  {
    var result = '';
    var keyList = _.keys(obj);
    keyList.sort();
    _.each(keyList, function (outer) {
      if (obj[outer])
      {
	var innerKeys = _.keys(obj[outer]);
	innerKeys.sort();
	result += ':' + outer + ':' + innerKeys.join('~');
      }
    });
    return result;
  }

  function uniqueName(candidate)
  {
    return JSON.stringify(candidate, undefined, 0);
  }

  return Constraints;
});

/*
 * rspecCommon.js
 *
 * Common constants and utility functions for dealing with rspecs
 *
 */

define('js/canvas/rspecCommon',['underscore', 'backbone'],
function (_, Backbone)
{
  

  var rspecCommon = {};

  rspecCommon.rspecNamespace = 'http://www.geni.net/resources/rspec/3';
  rspecCommon.emulabNamespace = 'http://www.protogeni.net/resources/rspec/ext/emulab/1';
  rspecCommon.jacksNamespace = 'http://www.protogeni.net/resources/rspec/ext/jacks/1';
  rspecCommon.openflow3Namespace = 'http://www.geni.net/resources/rspec/ext/openflow/3';
  rspecCommon.openflow4Namespace = 'http://www.geni.net/resources/rspec/ext/openflow/4';
  rspecCommon.vlanNamespace ='http://www.geni.net/resources/rspec/ext/shared-vlan/1';

  // Search for all elements called 'selector' in the namespace of the
  // dom object passed.
  rspecCommon.findSameNS = function (dom, selector)
  {
    var result = $();
    for (var i = 0; i < dom.length; i += 1)
    {
      var namespace = dom[i].namespaceURI;
      var list = dom[i].getElementsByTagNameNS(namespace, selector);
      result = result.add($(list));
    }
    return result;
  };

  // Search in the same namespace for a child matching selector and
  // return attribute inside that child if it exists
  rspecCommon.findChildAttribute = function (xml, selector, attribute)
  {
    var result = undefined;
    var child = rspecCommon.findSameNS(xml, selector);
    if (child)
    {
      result = child.attr(attribute);
    }
    return result;
  };

  rspecCommon.findOrMake = function (root, tagString)
  {
    var result = root.find(tagString);
    if (result.length === 0)
    {
      var namespace = root[0].namespaceURI;
      result = $($.parseXML('<' + tagString + ' xmlns="' + namespace + '"/>')).find(tagString);
      root.append(result);
    }
    return result;
  };

  rspecCommon.findOrMakeNS = function (root, tagString, namespace)
  {
    var result;
    var target = root[0].getElementsByTagNameNS(namespace, tagString);
    if (target.length === 0)
    {
      result = rspecCommon.makeNode(root, tagString, namespace);
    }
    else
    {
      result = $(target[0]);
    }
    return result;
  };

  rspecCommon.makeNode = function (root, tagString, namespace)
  {
    var result = $($.parseXML('<' + tagString + ' xmlns="' + namespace + '"></' + tagString + '>')).find(tagString);
    root.append(result);
    return result;
  };

  rspecCommon.removeList = function (root, tag, namespace)
  {
    var oldList = root[0].getElementsByTagNameNS(namespace, tag);
    _.each(oldList, function (item) {
      $(item).remove();
    });
  };

  rspecCommon.replaceList = function (root, list, tag, keys, namespace)
  {
    _.each(list, function (item) {
      var newXml = item.rspec;
      if (! newXml)
      {
	newXml = $($.parseXML('<' + tag + ' xmlns="' + namespace + '"></' + tag + '>')).find(tag);
      }
      _.each(keys, function (key) {
	if (item[key])
	{
	  newXml.attr(key, item[key]);
	}
	else
	{
	  newXml.removeAttr(key)
	}
      });
      root.append(newXml);
    });
  };

  rspecCommon.removeNS = function (root, tagString, namespace)
  {
    var target = root[0].getElementsByTagNameNS(namespace, tagString);
    if (target.length > 0)
    {
      $(target[0]).remove();
    }
  };

  return rspecCommon;
});

/*
 * rspecParser.js
 *
 * Parse Requests, Manifests, and Advertisements
 *
 */

define('js/canvas/rspecParser',['underscore', 'backbone', 'js/canvas/rspecCommon'],
function (_, Backbone, rs)
{
  

  var testGroup = 0;
  var rspecParser = {};

  // arg {
  //  rspec: String to parse
  //  context: Overall Jacks context
  //  errorModal: Handle to error modal for error display
  // }
  rspecParser.parse = function (arg)
  {
    var xmlString = arg.rspec;
    var context = arg.context;
    var errorModal = arg.errorModal;
    var result = null;

    if (! result)
    {
      result = {
	namespace: null,
	remainder: [],
	nodes: [],
	links: [],
	interfaces: {},
	interfaceList: [],
	sites: {},
	hosts: {}
      };
    }
    var nodeCount = 0;
    var defaultSite = {
      urn: undefined,
      name: undefined,
      custom: {},
      id: _.uniqueId()
    };

    try
    {
      var findEncoding = RegExp('^\\s*<\\?[^?]*\\?>');
      var match = findEncoding.exec(xmlString);
      if (match)
      {
	xmlString = xmlString.slice(match[0].length);
      }
      var xml = $.parseXML(xmlString);
      var xmlRoot = $(xml.documentElement);
      result.namespace = xmlRoot.namespaceURI;
      rs.findSameNS(xmlRoot, 'node').each(function(){
	  parseNode($(this), result, nodeCount, defaultSite, context);
	  $(this).remove();
	  nodeCount += 1;
	});

      var linkCount = 0;
      var used_iface = {};
      rs.findSameNS(xmlRoot, 'link').each(function() {
	  var repeatIface =
	    parseLink($(this), result, linkCount, context, used_iface);
	if (repeatIface)
	{
	  throw "An interface was used in more than one link";
	}
	  $(this).remove();
	  linkCount += 1;
	});

      result.remainder = xmlRoot.children();

      _.each(result.interfaceList, function (iface) {
	  if (! iface.linkId) {
	    console.log('Failed to find link for interface:', iface);
	  }
	});

      _.each(result.links, function (link) {
	_.each(link.nodeIndices, function (nodeIndex) {
	  link.endpoints.push(result.nodes[nodeIndex]);
	});
      });
      $(xmlRoot[0].getElementsByTagNameNS(rs.emulabNamespace, 'vhost')).each(function() {
	parseHost($(this), result);
      });
    }
    catch (e)
    {
      errorModal.update({
	verbatim: true,
	title: 'Cannot Parse Rspec',
	contents: 'Jacks cannot parse your rspec. Please send the rspec you tried to load and the following error message to jacks-support@protogeni.net <h1>Error:</h1><textarea class="form-control" rows="10">' + _.escape(e.toString()) + '</textarea><h1>Rspec:</h1><textarea class="form-control" rows="10">' + _.escape(xmlString) + '</textarea>'});
      console.log(e);
      result = {
	namespace: null,
	remainder: [],
	nodes: [],
	links: [],
	interfaces: {},
	interfaceList: [],
	sites: {}
      };
      nodeCount = 0;
    }

    if (nodeCount === 0)
    {
      result.sites[defaultSite.id] = defaultSite;
    }

    return result;
  };

  function parseNode(xml, result, count, defaultSite, context)
  {
    var node = {
      custom: {},
      warnings: {},
      execute: [],
      group: defaultSite.id,
      hardware: rs.findChildAttribute(xml, 'hardware_type', 'name'),
      hostport: undefined,
      icon: undefined,
      id: _.uniqueId(),
      image: undefined,
      imageVersion: rs.findChildAttribute(xml, 'disk_image', 'version'),
      index: count,
      install: [],
      interfaces: [],
      logins: undefined,
      name: xml.attr('client_id'),
      routable: false,
      routable_ip: undefined,
      sliverId: xml.attr('sliver_id'),
      sshurl: undefined,
      type: rs.findChildAttribute(xml, 'sliver_type', 'name'),
      xml: xml,
      dataset_id: undefined,
      dataset_mount: undefined,
      dataset_mount_option: 'remote'
    };

    node.image = rs.findChildAttribute(xml, 'disk_image', 'name');
    if (! node.image)
    {
      node.image = rs.findChildAttribute(xml, 'disk_image', 'url');
    }

    var iconParent = $(xml[0].getElementsByTagNameNS(rs.jacksNamespace, 'icon'));
    if (iconParent.length > 0)
    {
      node.icon = iconParent.attr('url');
    }

    var nomacParent = $(xml[0].getElementsByTagNameNS(rs.jacksNamespace, 'nomac'));
    if (nomacParent.length > 0)
    {
      node.nomac = true;
    }

    var taggingParent = $(xml[0].getElementsByTagNameNS(rs.emulabNamespace, 'vlan_tagging'));
    if (taggingParent.length > 0)
    {
      node.nontrivial = (taggingParent.attr('enabled') === 'true');
    }

    var foundIcon = _.findWhere(context.canvasOptions.icons,
				{ id: node.icon });
    if (node.icon && ! foundIcon)
    {
      node.custom.icon = true;
    }

    var foundImage = _.findWhere(context.canvasOptions.images,
				 { id: node.image });
    if (node.image && ! foundImage)
    {
      node.custom.image = true;
    }

    var foundHardware = _.findWhere(context.canvasOptions.hardware,
				    { id: node.hardware });
    if (node.hardware && ! foundHardware)
    {
      node.custom.hardware = true;
    }

    var foundType = _.findWhere(context.canvasOptions.types,
				{ id: node.type });
    if (node.type && ! foundType)
    {
      node.custom.type = true;
    }

    if (node.type && node.type === 'emulab-blockstore')
    {
      var blockstore = $(xml[0].getElementsByTagNameNS(rs.emulabNamespace, 'blockstore'));
      if (blockstore.length > 0)
      {
	this.dataset_mount = blockstore.attr('mountpoint');
	this.dataset_option = 'remote';
	if (blockstore.attr('rwclone') === 'true')
	{
	  this.dataset_option = 'rwclone';
	}
	else if (blockstore.attr('readonly') === 'true')
	{
	  this.dataset_option = 'readonly';
	}
	this.dataset_id = blockstore.attr('dataset');
      }
    }
    
    var services = rs.findSameNS(xml, 'services');
    services.each(function () {
      var executeItems = rs.findSameNS($(this), 'execute');
      executeItems.each(function () {
	node.execute.push({
	  'rspec': $(this),
	  'command': $(this).attr('command'),
	  'shell': $(this).attr('shell')
	});
      });
      var installItems = rs.findSameNS($(this), 'install');
      installItems.each(function () {
	node.install.push({
	  'rspec': $(this),
	  'url': $(this).attr('url'),
	  'install_path': $(this).attr('install_path')
	});
      });
    });

    var routableItems = xml[0].getElementsByTagNameNS(rs.emulabNamespace, 'routable_control_ip');
    if (routableItems.length > 0)
    {
      node.routable = true;
      var hostItems = rs.findSameNS(xml, 'host');
      if (hostItems.length > 0)
      {
	node.routable_ip = $(hostItems[0]).attr('ipv4');
      }
    }
    var cmUrn = xml.attr('component_manager_id');
    var wirelessElement = $(xml[0].getElementsByTagNameNS(rs.emulabNamespace, 'wireless-site'));
    var wirelessName = null;
    var wirelessType = null;
      if (wirelessElement.length > 0)
      {
	  wirelessName = wirelessElement.attr('id');
	  wirelessType = wirelessElement.attr('type');
	  wirelessUrn = wirelessElement.attr('urn');
	  if (wirelessName)
	  {
	      node.group = findOrMakeSite(wirelessUrn, wirelessName, result.sites, context, wirelessType).id;
	  }
      }
    var siteElement = $(xml[0].getElementsByTagNameNS(rs.jacksNamespace, 'site'));
    var siteName = null;
    if (siteElement.length > 0)
    {
      siteName = siteElement.attr('id');
    }
    if (cmUrn || siteName)
    {
      node.group = findOrMakeSite(cmUrn, siteName, result.sites, context, 'site').id;
    }
    else if (! result.sites[defaultSite.id])
    {
      // This is already part of the default site. Make sure the
      // default site is in the sites list.
      result.sites[defaultSite.id] = defaultSite;
    }

    rs.findSameNS(xml, 'interface').each(function() {
      var iface = {
	name: $(this).attr('client_id'),
	nodeName: node.name,
	node_index: count,
	id: _.uniqueId(),
	node: node,
	nodeId: node.id,
	mac: $(this).attr('mac_address'),
	bandwidth: undefined,
      };
      var ip = $(this.getElementsByTagNameNS(rs.rspecNamespace, 'ip'));
      if (ip.length > 0)
      {
	iface.ip = ip.attr('address');
	iface.ipType = ip.attr('type');
	iface.netmask = ip.attr('netmask');
      }
      node.interfaces.push(iface.id);
      result.interfaces[iface.id] = iface;
      result.interfaceList.push(iface);
    });

    if (services.length > 0)
    {
      var login  = rs.findSameNS(services, 'login');
      login.each(function () {
	var user   = $(this).attr('username');
	var host   = $(this).attr('hostname');
	var port   = $(this).attr('port');

	if (host !== undefined && port !== undefined)
	{
	  node.hostport  = host + ':' + port;
	  if (user !== undefined)
	  {
	    node.sshurl = 'ssh://' + user + '@' + host + ':' + port + '/';
	  }
	}
	if (user !== undefined &&
	    host !== undefined &&
	    port !== undefined)
	{
	  var newLogin = user + '@' + host + ':' + port;
	  if (node.logins)
	  {
	    node.logins += ', ' + newLogin;
	  }
	  else
	  {
	    node.logins = newLogin;
	  }
	}
      });
    }

    nodeHost(xml, result, node);

    //mergeNode(node, result.nodes);
    result.nodes.push(node);
    return node;
  }
/*
  function mergeNode(node, nodeList)
  {
    var found = null;
    var clashes = _.where(nodeList, { name: node.name });
    var i = 0;
    var index = 0;
    for (var i = 0; i < clashes.length; i += 1)
    {
      if (clashes[i].group === node.group)
      {
	found = clashes[i];
	index = i;
	break;
      }
    }
  }
*/
  function parseLink(xml, result, count, context, used_iface)
  {
    var link = {
      custom: {},
      warnings: {},
      index: count,
      interfaces: [],
      linkType: rs.findChildAttribute(xml, 'link_type', 'name'),
      name: xml.attr('client_id'),
      openflow: undefined,
      nodeIndices: [],
      endpoints: [],
      endpointNames: [],
      id: _.uniqueId(),
      sliverId: xml.attr('sliver_id'),
      sharedvlan: undefined,
      transitSites: {},
      xml: xml,
      interswitch: true
    };
    var foundType = _.findWhere(context.canvasOptions.linkTypes,
				{ id: link.linkType });
    if (link.linkType && ! foundType)
    {
      link.custom.linkType = true;
    }

    var shared = $(xml[0].getElementsByTagNameNS(rs.vlanNamespace,
						 'link_shared_vlan'));
    if (shared.length > 0)
    {
      link.sharedvlan = shared.attr('name');
    }

    var interswitch = $(xml[0].getElementsByTagNameNS(rs.emulabNamespace,
						      'interswitch'));
    if (interswitch.length > 0)
    {
      link.interswitch = (interswitch.attr('allow') === 'yes');
    }

    var foundShared = _.findWhere(context.canvasOptions.sharedvlans,
				  { id: link.sharedvlan });
    if (link.sharedvlan && ! foundShared)
    {
      link.custom.sharedvlan = true;
    }

    var openflow = parseOpenflow(xml);
    if (openflow)
    {
      link.openflow = openflow;
    }

    var repeatIface = false;
    var ifacerefs = rs.findSameNS(xml, 'interface_ref');
    _.each(ifacerefs, function (ref) {
      var targetName = $(ref).attr('client_id');
      /*
       * First we have map the client_ids to the node by
       * searching all of the interfaces we put into the
       * list above.
       *
       */
      _.each(result.interfaceList, function (iface) {
      	if (iface.name == targetName)
	{
	  if (used_iface[iface.name])
	  {
	    repeatIface = true;
	  }
	  else
	  {
	    used_iface[iface.name] = 1;
	  }
	  link.nodeIndices.push(iface.node_index);
	  link.endpointNames.push(iface.nodeName);
	  link.interfaces.push(iface.id);
	  iface.linkId = link.id;
	}
      });
    });

    var sites = $(xml[0].getElementsByTagNameNS(rs.rspecNamespace,
						'component_manager'));
    sites.each(function () {
      var name = $(this).attr('name');
      var found = false;
      _.each(link.nodeIndices, function (nodeId) {
	var node = result.nodes[nodeId];
	var currentSite = result.sites[node.group];
	if (currentSite.urn && currentSite.urn === name)
	{
	  found = true;
	}
      });
      if (! found)
      {
	link.transitSites[name] = 1;
      }
    });

    var properties = $(xml[0].getElementsByTagNameNS(rs.rspecNamespace,
						     'property'));
    properties.each(function () {
      var sourceName = $(this).attr('source_id');
      var bw = $(this).attr('capacity');
      if (bw !== null && bw !== undefined && bw !== '')
      {
	_.each(result.interfaceList, function (iface) {
      	  if (iface.name == sourceName)
	  {
	    iface.bandwidth = bw;
	  }
	});
      }
    });

    if (! repeatIface)
    {
      result.links.push(link);
    }
    return repeatIface;
  }

  function findOrMakeSite(cmUrn, name, sites, context, type)
  {
    var result;
    var siteList;
    // First try to match by Component Manager URN
    if (cmUrn)
    {
      siteList = _.where(_.values(sites), { urn: cmUrn });
      if (siteList.length > 0)
      {
	result = siteList[0];
	if (! result.name)
	{
	  result.name = name;
	}
      }
    }
    // If that doesn't work, try to match by Jacks site ID
    if (! result && name)
    {
      siteList = _.where(_.values(sites), { name: name });
      if (siteList.length > 0)
      {
	result = siteList[0];
	if (! result.urn)
	{
	  result.urn = cmUrn;
	}
      }
    }
    // If all else fails, just create a new one
    if (! result)
    {
      result = {
	custom: {},
	urn: cmUrn,
        id: _.uniqueId(),
        type: type
      };
      if (name)
      {
	result.name = name;
      }

      var foundUrn = _.findWhere(context.canvasOptions.aggregates,
				 { id: result.urn });
      if (result.urn && ! foundUrn)
      {
	result.custom.urn = true;
      }

      sites[result.id] = result;
    }
    return result;
  }

  function parseOpenflow(xml)
  {
    var result;
    var openflowList =
      $(xml[0].getElementsByTagNameNS(rs.emulabNamespace, 'openflow_controller'));
    if (openflowList.length > 0)
    {
      result = openflowList.attr('url');
    }

    if (! result)
    {
      openflowList = $(xml[0].getElementsByTagNameNS(rs.openflow3Namespace, 'controller'));
      if (openflowList.length > 0)
      {
	result = openflowList.attr('url');
      }
    }

    if (! result)
    {
      openflowList = $(xml[0].getElementsByTagNameNS(rs.openflow4Namespace, 'controller'));
      if (openflowList.length > 0)
      {
	result = openflowList.attr('url');
      }
    }

    return result;
  }

  function nodeHost(xml, result, node)
  {
    var vmlist = $(xml[0].getElementsByTagNameNS(rs.emulabNamespace, 'vmlist'));
    if (vmlist.length > 0)
    {
      vmlist = rs.findSameNS(vmlist, 'vm');
      var newHost = {
	id: _.uniqueId(),
	name: xml.attr('client_id'),
	attachedNode: xml.attr('client_id'),
	nodes: []
      };
      node.isHost = xml.attr('client_id');
      vmlist.each(function () {
	newHost.nodes.push($(this).attr('client_id'));
      });
      result.hosts[newHost.id] = newHost;
    }
  }

  function parseHost(xml, result)
  {
    var newHost = {
      id: _.uniqueId(),
      name: xml.attr('client_id'),
      attachedNode: undefined,
      nodes: []
    };
    var vmlist = rs.findSameNS(xml, 'vm');
    vmlist.each(function () {
      newHost.nodes.push($(this).attr('client_id'));
    });
    result.hosts[newHost.id] = newHost;
  }

  return rspecParser;
});

/**
 * @license RequireJS text 2.0.3 Copyright (c) 2010-2012, The Dojo Foundation All Rights Reserved.
 * Available via the MIT or new BSD license.
 * see: http://github.com/requirejs/text for details
 */
/*jslint regexp: true */
/*global require: false, XMLHttpRequest: false, ActiveXObject: false,
  define: false, window: false, process: false, Packages: false,
  java: false, location: false */

define('text',['module'], function (module) {
    

    var text, fs,
        progIds = ['Msxml2.XMLHTTP', 'Microsoft.XMLHTTP', 'Msxml2.XMLHTTP.4.0'],
        xmlRegExp = /^\s*<\?xml(\s)+version=[\'\"](\d)*.(\d)*[\'\"](\s)*\?>/im,
        bodyRegExp = /<body[^>]*>\s*([\s\S]+)\s*<\/body>/im,
        hasLocation = typeof location !== 'undefined' && location.href,
        defaultProtocol = hasLocation && location.protocol && location.protocol.replace(/\:/, ''),
        defaultHostName = hasLocation && location.hostname,
        defaultPort = hasLocation && (location.port || undefined),
        buildMap = [],
        masterConfig = (module.config && module.config()) || {};

    text = {
        version: '2.0.3',

        strip: function (content) {
            //Strips <?xml ...?> declarations so that external SVG and XML
            //documents can be added to a document without worry. Also, if the string
            //is an HTML document, only the part inside the body tag is returned.
            if (content) {
                content = content.replace(xmlRegExp, "");
                var matches = content.match(bodyRegExp);
                if (matches) {
                    content = matches[1];
                }
            } else {
                content = "";
            }
            return content;
        },

        jsEscape: function (content) {
            return content.replace(/(['\\])/g, '\\$1')
                .replace(/[\f]/g, "\\f")
                .replace(/[\b]/g, "\\b")
                .replace(/[\n]/g, "\\n")
                .replace(/[\t]/g, "\\t")
                .replace(/[\r]/g, "\\r")
                .replace(/[\u2028]/g, "\\u2028")
                .replace(/[\u2029]/g, "\\u2029");
        },

        createXhr: masterConfig.createXhr || function () {
            //Would love to dump the ActiveX crap in here. Need IE 6 to die first.
            var xhr, i, progId;
            if (typeof XMLHttpRequest !== "undefined") {
                return new XMLHttpRequest();
            } else if (typeof ActiveXObject !== "undefined") {
                for (i = 0; i < 3; i += 1) {
                    progId = progIds[i];
                    try {
                        xhr = new ActiveXObject(progId);
                    } catch (e) {}

                    if (xhr) {
                        progIds = [progId];  // so faster next time
                        break;
                    }
                }
            }

            return xhr;
        },

        /**
         * Parses a resource name into its component parts. Resource names
         * look like: module/name.ext!strip, where the !strip part is
         * optional.
         * @param {String} name the resource name
         * @returns {Object} with properties "moduleName", "ext" and "strip"
         * where strip is a boolean.
         */
        parseName: function (name) {
            var strip = false, index = name.indexOf("."),
                modName = name.substring(0, index),
                ext = name.substring(index + 1, name.length);

            index = ext.indexOf("!");
            if (index !== -1) {
                //Pull off the strip arg.
                strip = ext.substring(index + 1, ext.length);
                strip = strip === "strip";
                ext = ext.substring(0, index);
            }

            return {
                moduleName: modName,
                ext: ext,
                strip: strip
            };
        },

        xdRegExp: /^((\w+)\:)?\/\/([^\/\\]+)/,

        /**
         * Is an URL on another domain. Only works for browser use, returns
         * false in non-browser environments. Only used to know if an
         * optimized .js version of a text resource should be loaded
         * instead.
         * @param {String} url
         * @returns Boolean
         */
        useXhr: function (url, protocol, hostname, port) {
            var uProtocol, uHostName, uPort,
                match = text.xdRegExp.exec(url);
            if (!match) {
                return true;
            }
            uProtocol = match[2];
            uHostName = match[3];

            uHostName = uHostName.split(':');
            uPort = uHostName[1];
            uHostName = uHostName[0];

            return (!uProtocol || uProtocol === protocol) &&
                   (!uHostName || uHostName.toLowerCase() === hostname.toLowerCase()) &&
                   ((!uPort && !uHostName) || uPort === port);
        },

        finishLoad: function (name, strip, content, onLoad) {
            content = strip ? text.strip(content) : content;
            if (masterConfig.isBuild) {
                buildMap[name] = content;
            }
            onLoad(content);
        },

        load: function (name, req, onLoad, config) {
            //Name has format: some.module.filext!strip
            //The strip part is optional.
            //if strip is present, then that means only get the string contents
            //inside a body tag in an HTML string. For XML/SVG content it means
            //removing the <?xml ...?> declarations so the content can be inserted
            //into the current doc without problems.

            // Do not bother with the work if a build and text will
            // not be inlined.
            if (config.isBuild && !config.inlineText) {
                onLoad();
                return;
            }

            masterConfig.isBuild = config.isBuild;

            var parsed = text.parseName(name),
                nonStripName = parsed.moduleName + '.' + parsed.ext,
                url = req.toUrl(nonStripName),
                useXhr = (masterConfig.useXhr) ||
                         text.useXhr;

            //Load the text. Use XHR if possible and in a browser.
            if (!hasLocation || useXhr(url, defaultProtocol, defaultHostName, defaultPort)) {
                text.get(url, function (content) {
                    text.finishLoad(name, parsed.strip, content, onLoad);
                }, function (err) {
                    if (onLoad.error) {
                        onLoad.error(err);
                    }
                });
            } else {
                //Need to fetch the resource across domains. Assume
                //the resource has been optimized into a JS module. Fetch
                //by the module name + extension, but do not include the
                //!strip part to avoid file system issues.
                req([nonStripName], function (content) {
                    text.finishLoad(parsed.moduleName + '.' + parsed.ext,
                                    parsed.strip, content, onLoad);
                });
            }
        },

        write: function (pluginName, moduleName, write, config) {
            if (buildMap.hasOwnProperty(moduleName)) {
                var content = text.jsEscape(buildMap[moduleName]);
                write.asModule(pluginName + "!" + moduleName,
                               "define(function () { return '" +
                                   content +
                               "';});\n");
            }
        },

        writeFile: function (pluginName, moduleName, req, write, config) {
            var parsed = text.parseName(moduleName),
                nonStripName = parsed.moduleName + '.' + parsed.ext,
                //Use a '.js' file name so that it indicates it is a
                //script that can be loaded across domains.
                fileName = req.toUrl(parsed.moduleName + '.' +
                                     parsed.ext) + '.js';

            //Leverage own load() method to load plugin value, but only
            //write out values that do not have the strip argument,
            //to avoid any potential issues with ! in file names.
            text.load(nonStripName, req, function (value) {
                //Use own write() method to construct full module value.
                //But need to create shell that translates writeFile's
                //write() to the right interface.
                var textWrite = function (contents) {
                    return write(fileName, contents);
                };
                textWrite.asModule = function (moduleName, contents) {
                    return write.asModule(moduleName, fileName, contents);
                };

                text.write(pluginName, nonStripName, textWrite, config);
            }, config);
        }
    };

    if (masterConfig.env === 'node' || (!masterConfig.env &&
            typeof process !== "undefined" &&
            process.versions &&
            !!process.versions.node)) {
        //Using special require.nodeRequire, something added by r.js.
        fs = require.nodeRequire('fs');

        text.get = function (url, callback) {
            var file = fs.readFileSync(url, 'utf8');
            //Remove BOM (Byte Mark Order) from utf8 files if it is there.
            if (file.indexOf('\uFEFF') === 0) {
                file = file.substring(1);
            }
            callback(file);
        };
    } else if (masterConfig.env === 'xhr' || (!masterConfig.env &&
            text.createXhr())) {
        text.get = function (url, callback, errback) {
            var xhr = text.createXhr();
            xhr.open('GET', url, true);

            //Allow overrides specified in config
            if (masterConfig.onXhr) {
                masterConfig.onXhr(xhr, url);
            }

            xhr.onreadystatechange = function (evt) {
                var status, err;
                //Do not explicitly handle errors, those should be
                //visible via console output in the browser.
                if (xhr.readyState === 4) {
                    status = xhr.status;
                    if (status > 399 && status < 600) {
                        //An http 4xx or 5xx error. Signal an error.
                        err = new Error(url + ' HTTP status: ' + status);
                        err.xhr = xhr;
                        errback(err);
                    } else {
                        callback(xhr.responseText);
                    }
                }
            };
            xhr.send(null);
        };
    } else if (masterConfig.env === 'rhino' || (!masterConfig.env &&
            typeof Packages !== 'undefined' && typeof java !== 'undefined')) {
        //Why Java, why is this so awkward?
        text.get = function (url, callback) {
            var stringBuffer, line,
                encoding = "utf-8",
                file = new java.io.File(url),
                lineSeparator = java.lang.System.getProperty("line.separator"),
                input = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(file), encoding)),
                content = '';
            try {
                stringBuffer = new java.lang.StringBuffer();
                line = input.readLine();

                // Byte Order Mark (BOM) - The Unicode Standard, version 3.0, page 324
                // http://www.unicode.org/faq/utf_bom.html

                // Note that when we use utf-8, the BOM should appear as "EF BB BF", but it doesn't due to this bug in the JDK:
                // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4508058
                if (line && line.length() && line.charAt(0) === 0xfeff) {
                    // Eat the BOM, since we've already found the encoding on this file,
                    // and we plan to concatenating this buffer with others; the BOM should
                    // only appear at the top of a file.
                    line = line.substring(1);
                }

                stringBuffer.append(line);

                while ((line = input.readLine()) !== null) {
                    stringBuffer.append(lineSeparator);
                    stringBuffer.append(line);
                }
                //Make sure we return a JavaScript string and not a Java string.
                content = String(stringBuffer.toString()); //String
            } finally {
                input.close();
            }
            callback(content);
        };
    }

    return text;
});


define('text!html/rspec.xml',[],function () { return '<rspec\r\n    xmlns="http://www.geni.net/resources/rspec/3"\r\n    xmlns:emulab="http://www.protogeni.net/resources/rspec/ext/emulab/1"\r\n    xmlns:tour="http://www.protogeni.net/resources/rspec/ext/apt-tour/1"\r\n    xmlns:jacks="http://www.protogeni.net/resources/rspec/ext/jacks/1"\r\n    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\r\n    xsi:schemaLocation="http://www.geni.net/resources/rspec/3\r\n\t\t\thttp://www.geni.net/resources/rspec/3/request.xsd">\r\n</rspec>\r\n';});

/*
 * rspecGenerator.js
 *
 * Generate an request rspec, maintaining original annotations
 *
 */

define ('js/canvas/rspecGenerator',['underscore', 'backbone', 'js/canvas/rspecCommon',
	'text!html/rspec.xml'],
function (_, Backbone, rs, requestString)
{
  

  var rspecGenerator = {};

  rspecGenerator.request = function(data, context)
  {
    var root = $($.parseXML(requestString)).find('rspec');
    root.attr('type', 'request');
    _.each(data.nodes, function (item) {
      var nodeXml;
      if (item.xml)
      {
	nodeXml = item.xml;
      }
      else
      {
	nodeXml = $($.parseXML('<node xmlns="http://www.geni.net/resources/rspec/3"/>')).find('node');
      }
      updateNode(item, data.interfaces, data.sites, data.lans, nodeXml,
		context);
      root.append(nodeXml);
    });
    _.each(data.lans, function (item) {
      var linkXml;
      if (item.xml)
      {
	linkXml = item.xml;
      }
      else
      {
	linkXml = $($.parseXML('<link xmlns="http://www.geni.net/resources/rspec/3"/>')).find('link');
      }
      updateLink(item, data.interfaces, data.sites, data.nodes, linkXml,
		 context);
      root.append(linkXml);
    });
    _.each(data.extraChildren, function (item) {
      root.append(item);
    });
    var s = new XMLSerializer();
    return s.serializeToString(root[0]);
  }

  function updateNode(node, interfaces, sites, links, xml, context)
  {
    xml.attr('client_id', node.name);
    if (node.sliverId)
    {
      xml.attr('sliver_id', node.sliverId);
    }
    else
    {
      xml.removeAttr('sliver_id');
    }

    if (node.icon)
    {
      var icon = rs.findOrMakeNS(xml, 'icon', rs.jacksNamespace);
      $(icon).attr('url', node.icon);
    }
    else
    {
      rs.removeNS(xml, 'icon', rs.jacksNamespace);
    }

    if (node.nomac)
    {
      rs.findOrMakeNS(xml, 'nomac', rs.jacksNamespace);
    }
    else
    {
      rs.removeNS(xml, 'nomac', rs.jacksNamespace);
    }

    updateNodeSite(node, sites, xml);

    var routable = rs.findOrMakeNS(xml, 'routable_control_ip',
				   rs.emulabNamespace);
    if (! node.routable)
    {
      routable.remove();
    }

    if (node.type || node.image)
    {
      var sliver = rs.findOrMakeNS(xml, 'sliver_type', rs.rspecNamespace);
      if (node.type)
      {
	sliver.attr('name', node.type);
      }
      else
      {
	sliver.removeAttr('name');
      }
      if (node.image)
      {
	var image = rs.findOrMakeNS(sliver, 'disk_image', rs.rspecNamespace);
	if (node.image.substring(0,3) === 'urn')
	{
	  image.attr('name', node.image);
	  image.removeAttr('url');
	}
	else
	{
	  image.attr('url', node.image);
	  image.removeAttr('name');
	}
	if (node.imageVersion)
	{
	  image.attr('version', node.imageVersion);
	}
	else
	{
	  image.removeAttr('version', node.imageVersion);
	}
      }
      else
      {
	rs.removeNS(sliver, 'disk_image', rs.rspecNamespace);
      }
    }
    if (node.hardware)
    {
      var hardware = rs.findOrMakeNS(xml, 'hardware_type', rs.rspecNamespace);
      hardware.attr('name', node.hardware);
    }
    else
    {
      rs.removeNS(xml, 'hardware_type', rs.rspecNamespace);
    }
    var services = rs.findOrMakeNS(xml, 'services', rs.rspecNamespace);
    rs.removeList(services, 'execute', rs.rspecNamespace);
    rs.removeList(services, 'install', rs.rspecNamespace);
    if (node.execute.length > 0 || node.install.length > 0)
    {
      rs.replaceList(services, node.execute, 'execute',
		     ['command', 'shell'], rs.rspecNamespace);
      rs.replaceList(services, node.install, 'install',
		     ['url', 'install_path'], rs.rspecNamespace);
    }

    if (node.type === 'emulab-blockstore')
    {
      var blockstore = rs.findOrMakeNS(xml, 'blockstore', rs.emulabNamespace);
      blockstore.attr('name', node.name + '-bs');
      if (node.dataset_mount)
      {
	blockstore.attr('mountpoint', node.dataset_mount)
      }
      else
      {
	blockstore.removeAttr('mountpoint');
      }
      blockstore.attr('class', 'remote');
      blockstore.attr('placement', 'any');
      if (node.dataset_option === 'remote')
      {
	blockstore.attr('readonly', 'false');
	blockstore.removeAttr('rwclone');
	blockstore.removeAttr('roclone');
      }
      else if (node.dataset_option === 'rwclone')
      {
	blockstore.attr('readonly', 'false');
	blockstore.attr('rwclone', 'true');
	blockstore.removeAttr('roclone');
      }
      else // node.dataset_option === 'readonly'
      {
	blockstore.attr('readonly', 'true');
	blockstore.removeAttr('rwclone');
	blockstore.removeAttr('roclone');
      }
      if (node.dataset_id)
      {
	blockstore.attr('dataset', node.dataset_id);
      }
      else
      {
	blockstore.removeAttr('dataset');
      }
    }
    //else
    //{
    //  rs.removeNS(xml, 'blockstore', rs.emulabNamespace);
    //}

    var xmlIface = {};
    xml.find('interface').each(function () {
      var clientId = $(this).attr('client_id');
      xmlIface[clientId] = $(this);
    });
    var internalIface = {};
    _.each(node.interfaces, function (ifaceId) {
      var iface = interfaces[ifaceId];
      internalIface[iface.name] = iface;
      var ifaceDom = xmlIface[iface.name];
      if (! ifaceDom)
      {
	ifaceDom = $($.parseXML('<interface xmlns="http://www.geni.net/resources/rspec/3"/>')).find('interface');
	xml.append(ifaceDom);
	ifaceDom.attr('client_id', iface.name);
      }
      var ip = rs.findOrMakeNS(ifaceDom, 'ip', rs.rspecNamespace);
      var chosenIp = iface.ip;
      var chosenNetmask = iface.netmask;
      var ifaceLink = links[iface.linkId];
      var linkContext = _.findWhere(context.canvasOptions.linkTypes,
				    { id: ifaceLink.linkType });
      if (! chosenIp && linkContext && linkContext.ip === 'auto')
      {
	chosenIp = '10.';
	chosenIp += Math.floor(iface.linkId / 256) + '.';
	chosenIp += (iface.linkId % 256) + '.';
	chosenIp += (ifaceLink.interfaces.indexOf(iface.id) + 1);
	chosenNetmask = '255.255.255.0';
      }
      if (chosenIp)
      {
	ip.attr('address', chosenIp);
	if (iface.ipType)
	{
	  ip.attr('type', iface.ipType);
	}
	else
	{
	  ip.attr('type', 'ipv4');
	}
	if (chosenNetmask)
	{
	  ip.attr('netmask', chosenNetmask);
	}
	else
	{
	  ip.removeAttr('netmask');
	}
      }
      else
      {
	ip.remove();
      }
    });
    _.each(_.keys(xmlIface), function (iface) {
      var ifaceDom = xmlIface[iface];
      if (! internalIface[iface])
      {
	ifaceDom.remove();
      }
    });
  }

  function updateNodeSite(node, sites, xml)
  {
      if (node.group)
      {
	  var nodeSite = sites[node.group];
	  if (nodeSite.type === 'site')
	  {
	      if (nodeSite.urn)
	      {
		  if (xml.attr('component_manager_id') !== nodeSite.urn)
		  {
		      xml.attr('component_manager_id', nodeSite.urn);
		  }
		  rs.removeNS(xml, 'site', rs.jacksNamespace);
	      }
	      else
	      {
		  xml.removeAttr('component_manager_id');
		  var group = rs.findOrMakeNS(xml, 'site', rs.jacksNamespace);
		  $(group).attr('id', sites[node.group].name);
	      }
	  }
      }
      else
      {
	  xml.removeAttr('component_manager_id');
	  rs.removeNS(xml, 'site', rs.jacksNamespace);
      }
  }

  //----------------------------------------------------------------------

  function updateLink(link, interfaces, sites, nodes, xml, context)
  {
    xml.attr('client_id', link.name);
    var type = link.linkType;
    if (type === 'vlan' || type === 'lan')
    {
      if (linkStitched(link))
      {
	type = 'vlan';
      }
      else
      {
	type = 'lan';
      }
    }
    var linkType = rs.findOrMakeNS(xml, 'link_type', rs.rspecNamespace);
    if (type)
    {
      linkType.attr('name', type);
    }
    else
    {
      linkType.remove();
    }

    var tagged = rs.findOrMakeNS(xml, 'vlan_tagging',
				 rs.emulabNamespace);
    if (link.nontrivial || containsBlockstore(nodes))
    {
      tagged.attr('enabled', 'true');
    }
    else
    {
      tagged.remove();
    }

    var interswitch = rs.findOrMakeNS(xml, 'interswitch',
				      rs.emulabNamespace);
    if (link.interswitch)
    {
      interswitch.remove();
    }
    else
    {
      interswitch.attr('allow', 'no');
    }

    updateOpenflow(xml, link.openflow);

    var sharedvlan = rs.findOrMakeNS(xml, 'link_shared_vlan',
				     rs.vlanNamespace);
    if (link.sharedvlan)
    {
      sharedvlan.attr('name', link.sharedvlan);
    }
    else
    {
      sharedvlan.remove();
    }

    var xmlIface = {};
    xml.find('interface_ref').each(function () {
      var clientId = $(this).attr('client_id');
      xmlIface[clientId] = $(this);
    });
    var internalIface = {};
    _.each(link.interfaces, function (ifaceId) {
      var iface = interfaces[ifaceId];
      internalIface[iface.name] = iface;
      if (! xmlIface[iface.name])
      {
	var ifaceDom = $($.parseXML('<interface_ref xmlns="http://www.geni.net/resources/rspec/3"/>')).find('interface_ref');
	xml.append(ifaceDom);
	ifaceDom.attr('client_id', iface.name);
      }
    });
    _.each(_.keys(xmlIface), function (iface) {
      var ifaceDom = xmlIface[iface];
      if (! internalIface[iface])
      {
	ifaceDom.remove();
      }
    });

    var properties = $(xml[0].getElementsByTagNameNS(rs.rspecNamespace,
						     'property'));
    //if (properties.length == 0)
    //{
      properties.remove();
      _.each(link.interfaces, function (ifaceId) {
	var iface = interfaces[ifaceId];
	var bw = iface.bandwidth;
	var name = iface.name;
	if (bw !== undefined && bw !== null && bw !== '')
	{
	  _.each(link.interfaces, function (destId) {
	    if (destId !== ifaceId)
	    {
	      var destName = interfaces[destId].name;
	      var dom = $($.parseXML('<property xmlns="http://www.geni.net/resources/rspec/3" source_id="' + _.escape(name) + '" dest_id="' + _.escape(destName) + '" capacity="' + _.escape(bw) + '"/>')).find('property');
	      xml.append(dom);
	    }
	  });
	}
      });
    //}
    //else
    //{
    //}

    var linkSites = {};
    _.each(link.endpoints, function (endpoint) {
      if (endpoint.group !== undefined)
      {
	var name = sites[endpoint.group].urn;
	var siteId = sites[endpoint.group].id;
	if (name)
	{
	  linkSites[name] = 'bound';
	}
	else
	{
	  linkSites[name] = 'unbound';
	}
      }
    });
    var cmList = $(xml[0].getElementsByTagNameNS(rs.rspecNamespace,
						 'component_manager'));
    cmList.each(function () {
      var name = $(this).attr('name');
      if (linkSites[name] === 'bound')
      {
	delete linkSites[name];
      }
      else if (! link.transitSites[name])
      {
	$(this).remove();
      }
    });
    var cmList = $(xml[0].getElementsByTagNameNS(rs.jacksNamespace,
						 'site'));
    cmList.each(function () {
      var name = $(this).attr('id');
      if (linkSites[name] === 'unbound')
      {
	delete linkSites[name];
      }
      else if (! link.transitSites[name])
      {
	$(this).remove();
      }
    });
    _.each(_.keys(linkSites), function (newSite) {
      var value = linkSites[newSite];
      var siteXml;
      if (value === 'bound')
      {
	siteXml = rs.makeNode(xml, 'component_manager',
			      rs.rspecNamespace);
	siteXml.attr('name', newSite);
      }
      else
      {
	siteXml = rs.makeNode(xml, 'site',
			      rs.jacksNamespace);
	siteXml.attr('id', newSite);
      }
    });
    var isNomac = findNomac(link, interfaces, nodes, context);
    var nomac = rs.findOrMakeNS(xml, 'link_attribute',
				rs.emulabNamespace);
    if (isNomac)
    {
      nomac.attr('key', 'nomac_learning');
      nomac.attr('value', 'yep');
    }
    else
    {
      nomac.remove();
    }
  }

  function containsBlockstore(nodes)
  {
    var result = false;
    _.each(nodes, function (node) {
      if (node.type === 'emulab-blockstore')
      {
	result = true;
      }
    });
    return result;
  }

  function linkStitched(link)
  {
    var result = false;
    var found;
    _.each(link.endpoints, function (endpoint) {
      if (found === undefined)
      {
	found = endpoint.group;
      }
      else if (found !== endpoint.group)
      {
	result = true;
      }
    });
    return result;
  }

  function updateOpenflow(xml, openflow)
  {
    var openflowList = rs.findOrMakeNS(xml, 'openflow_controller', rs.emulabNamespace);
    if (openflow)
    {
      openflowList.attr('url', openflow);
      openflowList.attr('type', 'primary');
    }
    else
    {
      openflowList.remove();
    }

    openflowList = rs.findOrMakeNS(xml, 'controller', rs.openflow3Namespace);
    if (openflow)
    {
      openflowList.attr('url', openflow);
      openflowList.attr('type', 'primary');
    }
    else
    {
      openflowList.remove();
    }

    openflowList = rs.findOrMakeNS(xml, 'controller', rs.openflow4Namespace);
    if (openflow)
    {
      openflowList.attr('url', openflow);
      openflowList.attr('role', 'primary');
    }
    else
    {
      openflowList.remove();
    }
  }

  function findNomac(link, interfaces, nodes, context)
  {
    var result = false;
    _.each(link.interfaces, function (ifaceId) {
      var iface = interfaces[ifaceId];
      var node = nodes[iface.nodeId];
      var image = node.image;
      var imageContext = _.findWhere(context.canvasOptions.images,
				     { id: image });
      if (node.nomac || (imageContext && imageContext.nomac))
      {
	result = true;
      }
    });
    return result ||
      (link.openflow !== null && link.openflow !== undefined);
  }

  return rspecGenerator;
});

(function() {
  var _ref, _ref1, _ref2, _ref3,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  window.Tourist = window.Tourist || {};

  /*
  A model for the Tour. We'll only use the 'current_step' property.
  */


  Tourist.Model = (function(_super) {
    __extends(Model, _super);

    function Model() {
      _ref = Model.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    Model.prototype._module = 'Tourist';

    return Model;

  })(Backbone.Model);

  window.Tourist.Tip = window.Tourist.Tip || {};

  /*
  The flyout showing the content of each step.
  
  This is the base class containing most of the logic. Can extend for different
  tooltip implementations.
  */


  Tourist.Tip.Base = (function() {
    Base.prototype._module = 'Tourist';

    _.extend(Base.prototype, Backbone.Events);

    Base.prototype.skipButtonTemplate = '<button class="btn btn-default btn-sm pull-right tour-next">Skip this step →</button>';

    Base.prototype.nextButtonTemplate = '<button class="btn btn-primary btn-sm pull-right tour-next">Next step →</button>';

    Base.prototype.finalButtonTemplate = '<button class="btn btn-primary btn-sm pull-right tour-next">Finish up</button>';

    Base.prototype.closeButtonTemplate = '<a class="btn btn-close tour-close" href="#"><i class="glyphicon glyphicon-remove"></i></a>';

    Base.prototype.okButtonTemplate = '<button class="btn btn-sm tour-close btn-primary">Okay</button>';

    Base.prototype.actionLabelTemplate = _.template('<h4 class="action-label"><%= label %></h4>');

    Base.prototype.actionLabels = ['Do this:', 'Then this:', 'Next this:'];

    Base.prototype.highlightClass = 'tour-highlight';

    Base.prototype.template = _.template('<div>\n  <div class="tour-container">\n    <%= close_button %>\n    <%= content %>\n    <p class="tour-counter <%= counter_class %>"><%= counter%></p>\n  </div>\n  <div class="tour-buttons">\n    <%= buttons %>\n  </div>\n</div>');

    function Base(options) {
      this.options = options != null ? options : {};
      this.onClickNext = __bind(this.onClickNext, this);
      this.onClickClose = __bind(this.onClickClose, this);
      this.el = $('<div/>');
      this.initialize(options);
      this._bindClickEvents();
      Tourist.Tip.Base._cacheTip(this);
    }

    Base.prototype.destroy = function() {
      return this.el.remove();
    };

    Base.prototype.render = function(step) {
      this.hide();
      if (step) {
        this._setTarget(step.target || false, step);
        this._setZIndex('');
        this._renderContent(step, this._buildContentElement(step));
        if (step.target) {
          this.show();
        }
        if (step.zIndex) {
          this._setZIndex(step.zIndex, step);
        }
      }
      return this;
    };

    Base.prototype.show = function() {};

    Base.prototype.hide = function() {};

    Base.prototype.setTarget = function(targetElement, step) {
      return this._setTarget(targetElement, step);
    };

    Base.prototype.cleanupCurrentTarget = function() {
      if (this.target && this.target.removeClass) {
        this.target.removeClass(this.highlightClass);
      }
      return this.target = null;
    };

    /*
    Event Handlers
    */


    Base.prototype.onClickClose = function(event) {
      this.trigger('click:close', this, event);
      return false;
    };

    Base.prototype.onClickNext = function(event) {
      this.trigger('click:next', this, event);
      return false;
    };

    /*
    Private
    */


    Base.prototype._getTipElement = function() {};

    Base.prototype._renderContent = function(step, contentElement) {};

    Base.prototype._bindClickEvents = function() {
      var el;
      el = this._getTipElement();
      el.delegate('.tour-close', 'click', this.onClickClose);
      return el.delegate('.tour-next', 'click', this.onClickNext);
    };

    Base.prototype._setTarget = function(target, step) {
      this.cleanupCurrentTarget();
      if (target && step && step.highlightTarget) {
        target.addClass(this.highlightClass);
      }
      return this.target = target;
    };

    Base.prototype._setZIndex = function(zIndex) {
      var el;
      el = this._getTipElement();
      return el.css('z-index', zIndex || '');
    };

    Base.prototype._buildContentElement = function(step) {
      var buttons, content;
      buttons = this._buildButtons(step);
      content = $($.parseHTML(this.template({
        content: step.content,
        buttons: buttons,
        close_button: this._buildCloseButton(step),
        counter: step.final ? '' : "step " + (step.index + 1) + " of " + step.total,
        counter_class: step.final ? 'final' : ''
      })));
      if (!buttons) {
        content.find('.tour-buttons').addClass('no-buttons');
      }
      this._renderActionLabels(content);
      return content;
    };

    Base.prototype._buildButtons = function(step) {
      var buttons;
      buttons = '';
      if (step.okButton) {
        buttons += this.okButtonTemplate;
      }
      if (step.skipButton) {
        buttons += this.skipButtonTemplate;
      }
      if (step.nextButton) {
        buttons += step.final ? this.finalButtonTemplate : this.nextButtonTemplate;
      }
      return buttons;
    };

    Base.prototype._buildCloseButton = function(step) {
      if (step.closeButton) {
        return this.closeButtonTemplate;
      } else {
        return '';
      }
    };

    Base.prototype._renderActionLabels = function(el) {
      var action, actionIndex, actions, label, _i, _len, _results;
      actions = el.find('.action');
      actionIndex = 0;
      _results = [];
      for (_i = 0, _len = actions.length; _i < _len; _i++) {
        action = actions[_i];
        label = $($.parseHTML(this.actionLabelTemplate({
          label: this.actionLabels[actionIndex]
        })));
        label.insertBefore(action);
        _results.push(actionIndex++);
      }
      return _results;
    };

    Base._cacheTip = function(tip) {
      if (!Tourist.Tip.Base._cachedTips) {
        Tourist.Tip.Base._cachedTips = [];
      }
      return Tourist.Tip.Base._cachedTips.push(tip);
    };

    Base.destroy = function() {
      var tip, _i, _len, _ref1;
      if (!Tourist.Tip.Base._cachedTips) {
        return;
      }
      _ref1 = Tourist.Tip.Base._cachedTips;
      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
        tip = _ref1[_i];
        tip.destroy();
      }
      return Tourist.Tip.Base._cachedTips = null;
    };

    return Base;

  })();

  /*
  Bootstrap based tip implementation
  */


  Tourist.Tip.Bootstrap = (function(_super) {
    __extends(Bootstrap, _super);

    function Bootstrap() {
      _ref1 = Bootstrap.__super__.constructor.apply(this, arguments);
      return _ref1;
    }

    Bootstrap.prototype.initialize = function(options) {
      var defs;
      defs = {
        showEffect: null,
        hideEffect: null
      };
      this.options = _.extend(defs, options);
      return this.tip = new Tourist.Tip.BootstrapTip();
    };

    Bootstrap.prototype.destroy = function() {
      this.tip.destroy();
      return Bootstrap.__super__.destroy.call(this);
    };

    Bootstrap.prototype.show = function() {
      var fn;
      if (this.options.showEffect) {
        fn = Tourist.Tip.Bootstrap.effects[this.options.showEffect];
        return fn.call(this, this.tip, this.tip.el);
      } else {
        return this.tip.show();
      }
    };

    Bootstrap.prototype.hide = function() {
      var fn;
      if (this.options.hideEffect) {
        fn = Tourist.Tip.Bootstrap.effects[this.options.hideEffect];
        return fn.call(this, this.tip, this.tip.el);
      } else {
        return this.tip.hide();
      }
    };

    /*
    Private
    */


    Bootstrap.prototype._getTipElement = function() {
      return this.tip.el;
    };

    Bootstrap.prototype._setTarget = function(target, step) {
      Bootstrap.__super__._setTarget.call(this, target, step);
      return this.tip.setTarget(target);
    };

    Bootstrap.prototype._renderContent = function(step, contentElement) {
      var at, my;
      my = step.my || 'left center';
      at = step.at || 'right center';
      this.tip.setContainer(step.container || $('body'));
      this.tip.setContent(contentElement);
      return this.tip.setPosition(step.target || false, my, at);
    };

    return Bootstrap;

  })(Tourist.Tip.Base);

  Tourist.Tip.Bootstrap.effects = {
    slidein: function(tip, element) {
      var OFFSETS, css, easing, easings, offset, side, value, _i, _len;
      OFFSETS = {
        top: 80,
        left: 80,
        right: -80,
        bottom: -80
      };
      side = tip.my.split(' ')[0];
      side = side || 'top';
      offset = OFFSETS[side];
      if (side === 'bottom') {
        side = 'top';
      }
      if (side === 'right') {
        side = 'left';
      }
      value = parseInt(element.css(side));
      element.stop();
      css = {};
      css[side] = value + offset;
      element.css(css);
      element.show();
      css[side] = value;
      easings = ['easeOutCubic', 'swing', 'linear'];
      for (_i = 0, _len = easings.length; _i < _len; _i++) {
        easing = easings[_i];
        if ($.easing[easing]) {
          break;
        }
      }
      element.animate(css, 300, easing);
      return null;
    }
  };

  /*
  Simple implementation of tooltip with bootstrap markup.
  
  Almost entirely deals with positioning. Uses the similar method for
  positioning as qtip2:
  
    my: 'top center'
    at: 'bottom center'
  */


  Tourist.Tip.BootstrapTip = (function() {
    BootstrapTip.prototype.template = '<div class="popover">\n  <div class="arrow"></div>\n  <div class="popover-content"></div>\n</div>';

    BootstrapTip.prototype.FLIP_POSITION = {
      bottom: 'top',
      top: 'bottom',
      left: 'right',
      right: 'left'
    };

    function BootstrapTip(options) {
      var defs;
      defs = {
        offset: 10,
        tipOffset: 10
      };
      this.options = _.extend(defs, options);
      this.el = $($.parseHTML(this.template));
      this.hide();
    }

    BootstrapTip.prototype.destroy = function() {
      return this.el.remove();
    };

    BootstrapTip.prototype.show = function() {
      return this.el.show().addClass('visible');
    };

    BootstrapTip.prototype.hide = function() {
      return this.el.hide().removeClass('visible');
    };

    BootstrapTip.prototype.setTarget = function(target) {
      this.target = target;
      return this._setPosition(this.target, this.my, this.at);
    };

    BootstrapTip.prototype.setPosition = function(target, my, at) {
      this.target = target;
      this.my = my;
      this.at = at;
      return this._setPosition(this.target, this.my, this.at);
    };

    BootstrapTip.prototype.setContainer = function(container) {
      return container.append(this.el);
    };

    BootstrapTip.prototype.setContent = function(content) {
      return this._getContentElement().html(content);
    };

    /*
    Private
    */


    BootstrapTip.prototype._getContentElement = function() {
      return this.el.find('.popover-content');
    };

    BootstrapTip.prototype._getTipElement = function() {
      return this.el.find('.arrow');
    };

    BootstrapTip.prototype._setPosition = function(target, my, at) {
      var clas, css, originalDisplay, position, shift, targetPosition, tip, tipOffset, tipPosition, _ref2;
      if (my == null) {
        my = 'left center';
      }
      if (at == null) {
        at = 'right center';
      }
      if (!target) {
        return;
      }
      _ref2 = my.split(' '), clas = _ref2[0], shift = _ref2[1];
      originalDisplay = this.el.css('display');
      this.el.css({
        top: 0,
        left: 0,
        margin: 0,
        display: 'block'
      }).removeClass('top').removeClass('bottom').removeClass('left').removeClass('right').addClass(this.FLIP_POSITION[clas]);
      if (!target) {
        return;
      }
      tip = this._getTipElement().css({
        left: '',
        right: '',
        top: '',
        bottom: ''
      });
      if (shift !== 'center') {
        tipOffset = {
          left: tip[0].offsetWidth / 2,
          right: 0,
          top: tip[0].offsetHeight / 2,
          bottom: 0
        };
        css = {};
        css[shift] = tipOffset[shift] + this.options.tipOffset;
        css[this.FLIP_POSITION[shift]] = 'auto';
        tip.css(css);
      }
      targetPosition = this._caculateTargetPosition(at, target);
      tipPosition = this._caculateTipPosition(my, targetPosition);
      position = this._adjustForArrow(my, tipPosition);
      this.el.css(position);
      return this.el.css({
        display: originalDisplay
      });
    };

    BootstrapTip.prototype._caculateTargetPosition = function(atPosition, target) {
      var bounds, pos;
      if (Object.prototype.toString.call(target) === '[object Array]') {
        return {
          left: target[0],
          top: target[1]
        };
      }
      bounds = this._getTargetBounds(target);
      pos = this._lookupPosition(atPosition, bounds.width, bounds.height);
      return {
        left: bounds.left + pos[0],
        top: bounds.top + pos[1]
      };
    };

    BootstrapTip.prototype._caculateTipPosition = function(myPosition, targetPosition) {
      var height, pos, width;
      width = this.el[0].offsetWidth;
      height = this.el[0].offsetHeight;
      pos = this._lookupPosition(myPosition, width, height);
      return {
        left: targetPosition.left - pos[0],
        top: targetPosition.top - pos[1]
      };
    };

    BootstrapTip.prototype._adjustForArrow = function(myPosition, tipPosition) {
      var clas, height, position, shift, tip, width, _ref2;
      _ref2 = myPosition.split(' '), clas = _ref2[0], shift = _ref2[1];
      tip = this._getTipElement();
      width = tip[0].offsetWidth;
      height = tip[0].offsetHeight;
      position = {
        top: tipPosition.top,
        left: tipPosition.left
      };
      switch (clas) {
        case 'top':
          position.top += height + this.options.offset;
          break;
        case 'bottom':
          position.top -= height + this.options.offset;
          break;
        case 'left':
          position.left += width + this.options.offset;
          break;
        case 'right':
          position.left -= width + this.options.offset;
      }
      switch (shift) {
        case 'left':
          position.left -= width / 2 + this.options.tipOffset;
          break;
        case 'right':
          position.left += width / 2 + this.options.tipOffset;
          break;
        case 'top':
          position.top -= height / 2 + this.options.tipOffset;
          break;
        case 'bottom':
          position.top += height / 2 + this.options.tipOffset;
      }
      return position;
    };

    BootstrapTip.prototype._lookupPosition = function(position, width, height) {
      var height2, posLookup, width2;
      width2 = width / 2;
      height2 = height / 2;
      posLookup = {
        'top left': [0, 0],
        'left top': [0, 0],
        'top right': [width, 0],
        'right top': [width, 0],
        'bottom left': [0, height],
        'left bottom': [0, height],
        'bottom right': [width, height],
        'right bottom': [width, height],
        'top center': [width2, 0],
        'left center': [0, height2],
        'right center': [width, height2],
        'bottom center': [width2, height]
      };
      return posLookup[position];
    };

    BootstrapTip.prototype._getTargetBounds = function(target) {
      var el, size;
      el = target[0];
      if (typeof el.getBoundingClientRect === 'function') {
        size = el.getBoundingClientRect();
      } else {
        size = {
          width: el.offsetWidth,
          height: el.offsetHeight
        };
      }
      return $.extend({}, size, target.offset());
    };

    return BootstrapTip;

  })();

  /*
  Qtip based tip implementation
  */


  Tourist.Tip.QTip = (function(_super) {
    var ADJUST, OFFSETS, TIP_HEIGHT, TIP_WIDTH;

    __extends(QTip, _super);

    function QTip() {
      this._renderTipBackground = __bind(this._renderTipBackground, this);
      _ref2 = QTip.__super__.constructor.apply(this, arguments);
      return _ref2;
    }

    TIP_WIDTH = 6;

    TIP_HEIGHT = 14;

    ADJUST = 10;

    OFFSETS = {
      top: 80,
      left: 80,
      right: -80,
      bottom: -80
    };

    QTip.prototype.QTIP_DEFAULTS = {
      content: {
        text: ' '
      },
      show: {
        ready: false,
        delay: 0,
        effect: function(qtip) {
          var css, el, offset, side, value;
          el = $(this);
          side = qtip.options.position.my;
          if (side) {
            side = side[side.precedance];
          }
          side = side || 'top';
          offset = OFFSETS[side];
          if (side === 'bottom') {
            side = 'top';
          }
          if (side === 'right') {
            side = 'left';
          }
          value = parseInt(el.css(side));
          css = {};
          css[side] = value + offset;
          el.css(css);
          el.show();
          css[side] = value;
          el.animate(css, 300, 'easeOutCubic');
          return null;
        },
        autofocus: false
      },
      hide: {
        event: null,
        delay: 0,
        effect: false
      },
      position: {
        adjust: {
          method: 'shift shift',
          scroll: false
        }
      },
      style: {
        classes: 'ui-tour-tip',
        tip: {
          height: TIP_WIDTH,
          width: TIP_HEIGHT
        }
      },
      events: {},
      zindex: 2000
    };

    QTip.prototype.initialize = function(options) {
      options = $.extend(true, {}, this.QTIP_DEFAULTS, options);
      this.el.qtip(options);
      this.qtip = this.el.qtip('api');
      return this.qtip.render();
    };

    QTip.prototype.destroy = function() {
      if (this.qtip) {
        this.qtip.destroy();
      }
      return QTip.__super__.destroy.call(this);
    };

    QTip.prototype.show = function() {
      return this.qtip.show();
    };

    QTip.prototype.hide = function() {
      return this.qtip.hide();
    };

    /*
    Private
    */


    QTip.prototype._getTipElement = function() {
      return $('#qtip-' + this.qtip.id);
    };

    QTip.prototype._setTarget = function(targetElement, step) {
      QTip.__super__._setTarget.call(this, targetElement, step);
      return this.qtip.set('position.target', targetElement || false);
    };

    QTip.prototype._renderContent = function(step, contentElement) {
      var at, my,
        _this = this;
      my = step.my || 'left center';
      at = step.at || 'right center';
      this._adjustPlacement(my, at);
      this.qtip.set('content.text', contentElement);
      this.qtip.set('position.container', step.container || $('body'));
      this.qtip.set('position.my', my);
      this.qtip.set('position.at', at);
      this.qtip.set('position.viewport', step.viewport || false);
      this.qtip.set('position.target', step.target || false);
      return setTimeout(function() {
        return _this._renderTipBackground(my.split(' ')[0]);
      }, 10);
    };

    QTip.prototype._adjustPlacement = function(my, at) {
      if (my.indexOf('top') === 0) {
        return this._adjust(0, ADJUST);
      } else if (my.indexOf('bottom') === 0) {
        return this._adjust(0, -ADJUST);
      } else if (my.indexOf('right') === 0) {
        return this._adjust(-ADJUST, 0);
      } else {
        return this._adjust(ADJUST, 0);
      }
    };

    QTip.prototype._adjust = function(adjustX, adjusty) {
      this.qtip.set('position.adjust.x', adjustX);
      return this.qtip.set('position.adjust.y', adjusty);
    };

    QTip.prototype._renderTipBackground = function(direction) {
      var bg, el;
      el = $('#qtip-' + this.qtip.id + ' .qtip-tip');
      bg = el.find('.qtip-tip-bg');
      if (!bg.length) {
        bg = $('<div/>', {
          'class': 'icon icon-tip qtip-tip-bg'
        });
        el.append(bg);
      }
      bg.removeClass('top left right bottom');
      return bg.addClass(direction);
    };

    return QTip;

  })(Tourist.Tip.Base);

  /*
  Simplest implementation of a tooltip. Used in the tests. Useful as an example
  as well.
  */


  Tourist.Tip.Simple = (function(_super) {
    __extends(Simple, _super);

    function Simple() {
      _ref3 = Simple.__super__.constructor.apply(this, arguments);
      return _ref3;
    }

    Simple.prototype.initialize = function(options) {
      return $('body').append(this.el);
    };

    Simple.prototype.show = function() {
      return this.el.show();
    };

    Simple.prototype.hide = function() {
      return this.el.hide();
    };

    Simple.prototype._getTipElement = function() {
      return this.el;
    };

    Simple.prototype._renderContent = function(step, contentElement) {
      return this.el.html(contentElement);
    };

    return Simple;

  })(Tourist.Tip.Base);

  /*
  
  A way to make a tour. Basically, you specify a series of steps which explain
  elements to point at and what to say. This class manages moving between those
  steps.
  
  The 'step object' is a simple js obj that specifies how the step will behave.
  
  A simple Example of a step object:
    {
      content: '<p>Welcome to my step</p>'
      target: $('#something-to-point-at')
      closeButton: true
      highlightTarget: true
      setup: (tour, options) ->
        # do stuff in the interface/bind
      teardown: (tour, options) ->
        # remove stuff/unbind
    }
  
  Basic Step object options:
  
    content - a string of html to put into the step.
    target - jquery object or absolute point: [10, 30]
    highlightTarget - optional bool, true will outline the target with a bright color.
    container - optional jquery element that should contain the step flyout.
                default: $('body')
    viewport - optional jquery element that the step flyout should stay within.
               $(window) is commonly used. default: false
  
    my - string position of the pointer on the tip. default: 'left center'
    at - string position on the element the tip points to. default: 'right center'
    see http://craigsworks.com/projects/qtip2/docs/position/#basics
  
  Step object button options:
  
    okButton - optional bool, true will show a red ok button
    closeButton - optional bool, true will show a grey close button
    skipButton - optional bool, true will show a grey skip button
    nextButton - optional bool, true will show a red next button
  
  Step object function options:
  
    All functions on the step will have the signature '(tour, options) ->'
  
      tour - the Draw.Tour object. Handy to call tour.next()
      options - the step options. An object passed into the tour when created.
                It has the environment that the fns can use to manipulate the
                interface, bind to events, etc. The same object is passed to all
                of a step object's functions, so it is handy for passing data
                between steps.
  
    setup - called before step is shown. Use to scroll to your target, hide/show things, ...
  
      'this' is the step object itself.
  
      MUST return an object. Properties in the returned object will override
      properties in the step object.
  
      i.e. the target might be dynamic so you would specify:
  
      setup: (tour, options) ->
        return { target: $('#point-to-me') }
  
    teardown - function called right before hiding the step. Use to unbind from
      things you bound to in setup().
  
      'this' is the step object itself.
  
      Return nothing.
  
    bind - an array of function names to bind. Use this for event handlers you use in setup().
  
      Will bind functions to the step object as this, and the first 2 args as tour and options.
  
      i.e.
  
      bind: ['onChangeSomething']
      setup: (tour, options) ->
        options.document.bind('change:something', @onChangeSomething)
      onChangeSomething: (tour, options, model, value) ->
        tour.next()
      teardown: (tour, options) ->
        options.document.unbind('change:something', @onChangeSomething)
  */


  Tourist.Tour = (function() {
    _.extend(Tour.prototype, Backbone.Events);

    function Tour(options) {
      var defs, tipOptions;
      this.options = options != null ? options : {};
      this.onChangeCurrentStep = __bind(this.onChangeCurrentStep, this);
      this.next = __bind(this.next, this);
      defs = {
        tipClass: 'Bootstrap'
      };
      this.options = _.extend(defs, this.options);
      this.model = new Tourist.Model({
        current_step: null
      });
      tipOptions = _.extend({
        model: this.model
      }, this.options.tipOptions);
      this.view = new Tourist.Tip[this.options.tipClass](tipOptions);
      this.view.bind('click:close', _.bind(this.stop, this, true));
      this.view.bind('click:next', this.next);
      this.model.bind('change:current_step', this.onChangeCurrentStep);
    }

    /*
    Public
    */


    Tour.prototype.start = function() {
      this.trigger('start', this);
      return this.next();
    };

    Tour.prototype.stop = function(doFinalStep) {
      if (doFinalStep) {
        return this._showCancelFinalStep();
      } else {
        return this._stop();
      }
    };

    Tour.prototype.next = function() {
      var currentStep, index;
      currentStep = this._teardownCurrentStep();
      index = 0;
      if (currentStep) {
        index = currentStep.index + 1;
      }
      if (index < this.options.steps.length) {
        return this._showStep(this.options.steps[index], index);
      } else if (index === this.options.steps.length) {
        return this._showSuccessFinalStep();
      } else {
        return this._stop();
      }
    };

    Tour.prototype.setStepOptions = function(stepOptions) {
      return this.options.stepOptions = stepOptions;
    };

    /*
    Handlers
    */


    Tour.prototype.onChangeCurrentStep = function(model, step) {
      return this.view.render(step);
    };

    /*
    Private
    */


    Tour.prototype._showCancelFinalStep = function() {
      return this._showFinalStep(false);
    };

    Tour.prototype._showSuccessFinalStep = function() {
      return this._showFinalStep(true);
    };

    Tour.prototype._teardownCurrentStep = function() {
      var currentStep;
      currentStep = this.model.get('current_step');
      this._teardownStep(currentStep);
      return currentStep;
    };

    Tour.prototype._stop = function() {
      this._teardownCurrentStep();
      this.model.set({
        current_step: null
      });
      return this.trigger('stop', this);
    };

    Tour.prototype._showFinalStep = function(success) {
      var currentStep, finalStep;
      currentStep = this._teardownCurrentStep();
      finalStep = success ? this.options.successStep : this.options.cancelStep;
      if (_.isFunction(finalStep)) {
        finalStep.call(this, this, this.options.stepOptions);
        finalStep = null;
      }
      if (!finalStep) {
        return this._stop();
      }
      if (currentStep && currentStep.final) {
        return this._stop();
      }
      finalStep.final = true;
      return this._showStep(finalStep, this.options.steps.length);
    };

    Tour.prototype._showStep = function(step, index) {
      if (!step) {
        return;
      }
      step = _.clone(step);
      step.index = index;
      step.total = this.options.steps.length;
      if (!step.final) {
        step.final = this.options.steps.length === index + 1 && !this.options.successStep;
      }
      step = _.extend(step, this._setupStep(step));
      return this.model.set({
        current_step: step
      });
    };

    Tour.prototype._setupStep = function(step) {
      var fn, _i, _len, _ref4;
      if (!(step && step.setup)) {
        return {};
      }
      if (step.bind) {
        _ref4 = step.bind;
        for (_i = 0, _len = _ref4.length; _i < _len; _i++) {
          fn = _ref4[_i];
          step[fn] = _.bind(step[fn], step, this, this.options.stepOptions);
        }
      }
      return step.setup.call(step, this, this.options.stepOptions) || {};
    };

    Tour.prototype._teardownStep = function(step) {
      if (step && step.teardown) {
        step.teardown.call(step, this, this.options.stepOptions);
      }
      return this.view.cleanupCurrentTarget();
    };

    return Tour;

  })();

}).call(this);

define("tourist", ["backbone"], (function (global) {
    return function () {
        var ret, fn;
        return ret || global.Tourist;
    };
}(this)));

/*
 * TourView.js
 *
 * Manager for rspec Tours
 *
 */

define('js/canvas/TourView',['underscore', 'backbone', 'tourist'],
function (_, Backbone, Tourist)
{
  

  function TourView (rootDom, tourContainer, rspec)
  {
    this.rootDom = rootDom;
    this.tourContainer = tourContainer;
    this.rspec = rspec;
    this.tour = null;
    this.trivial = true;
    this.startTour();
  }

  TourView.prototype.startTour = function ()
  {
    var that = this;
    var root = $($.parseXML(this.rspec));
    var stepsNode = root.find('step');
    var steps = [];
    _.each(stepsNode, function (item) {
      var type = $(item).attr('point_type');
      var id = $(item).attr('point_id');
      var target = '#' + type + '-' + id;
      steps.push({
	content: '<p>' + _.escape($(item).find('description').text()) + '</p>',
	highlightTarget: true,
	nextButton: true,
	closeButton: true,
	container: that.tourContainer,
	setup: function(tourArg, options) {
	  return { target: that.rootDom.find(target) };
	},
	my: 'top center',
	at: 'bottom center'
      });
    });

    this.tour = new Tourist.Tour({
      steps: steps,
      tipClass: 'Bootstrap',
      tipOptions:{ showEffect: 'slidein' }
    });

    if (steps.length > 0)
    {
      this.trivial = false;
    }
  };

  TourView.prototype.nonTrivial = function ()
  {
    return ! this.trivial;
  };

  TourView.prototype.update = function ()
  {
    this.tour.view.render(this.tour.model.get('current_step'));
  };

  return TourView;
});

define('js/canvas/NodeOption',['underscore', 'backbone'],
function (_, Backbone)
{
  

  // arguments
  // {
  //   root: jQuery node
  //   options: defaults item
  //   pos: { x: int, y: int } to place it on paletter
  //   toInternalPosition: function to get internal topology coordinates
  function NodeOption(args)
  {
    this.root = args.root;
    this.options = args.options;
    this.pos = args.pos;
    this.toInternalPosition = args.toInternalPosition;
    this.force = args.force;
    this.validList = args.validList;
    this.lazy = null;
    this.marker = null;
    this.lastPos = null;
    _.extend(this, Backbone.Events);
  }

  NodeOption.prototype = {

    render: function ()
    {
      this.lazy = this.makeNode(this.root);
      this.lazy.style('visibility', 'hidden');
      this.marker = this.makeNode(this.root);
      var drag = d3v4.drag()
	.on('start', _.bind(this.dragStart, this))
	.on('drag', _.bind(this.dragMove, this))
	.on('end', _.bind(this.dragEnd, this));
      this.marker.call(drag);
    },

    makeNode: function (root)
    {
      var result = root.append('svg:g')
	.attr('transform', 'translate(' + this.pos.x + ', ' +
	      this.pos.y + ')')
        .attr("class", "node");

      result.append('svg:rect')
	.attr('class', 'nodebox')
	.attr('x', '-30px')
	.attr('y', '-25px')
	.attr('width', '60px')
	.attr('height', '65px');

      var iconUrl = window.JACKS_LOADER.basePath + 'images/default.svg';
      if (this.options.icon)
      {
	iconUrl = this.options.icon;
      }
      result.append('svg:image')
	.attr("class", "nodebox")
	.attr("x", "-20px")
	.attr("y", "-20px")
	.attr("width", "40px")
	.attr("height", "40px")
	.attr("xlink:href", iconUrl);

      var name = 'New Node';
      if (this.options.name)
      {
	name = this.options.name;
      }
      result.append('svg:text')
	.attr("class", "nodetextpalette")
	.attr("x", "0px")
	.attr("y", "35px")
	.text(name);
      return result;
    },

    dragStart: function ()
    {
      this.lazy.style('visibility', 'visible');
      this.lastPos = null;
      this.force.stop();
    },

    dragMove: function ()
    {
      this.marker.attr('transform', 'translate(' + (d3v4.event.x - 20) +
		       ',' + (d3v4.event.y - 10) + ')');
      var oldClosest = null;
      if (this.lastPos)
      {
	oldClosest = this.lastPos.closestSite;
      }
      this.lastPos = this.toInternalPosition({ x: d3v4.event.x,
					       y: d3v4.event.y });
      if (this.lastPos.closestSite !== null)
      {
	this.lastPos.closestSite.update(true, undefined,
					[this.lastPos.topX,
					 this.lastPos.topY]);
      }
      if (this.lastPos.closestSite !== oldClosest &&
	  oldClosest !== null)
      {
	oldClosest.update(true);
      }
      if (this.lastPos.closestSite !== oldClosest &&
	  this.lastPos.closestSite !== null)
      {
	var node = _.clone(this.options);
	node.custom = {};
	if (this.validList.isValidNode(node,
				       this.lastPos.closestSite.model))
	{
	  this.marker.classed('invalid', false);
	}
	else
	{
	  this.marker.classed('invalid', true);
	}
      }
    },

    dragEnd: function ()
    {
      this.lazy.style('visibility', 'hidden');
      if (this.lastPos && this.lastPos.closestSite !== null)
      {
	this.trigger('create-node', this.lastPos.topX, this.lastPos.topY,
		     this.lastPos.closestSite.model.id);
      }
      this.marker.attr('transform', 'translate(' + this.pos.x + ', ' +
		       this.pos.y + ')')
      this.marker.classed('invalid', false);
    },

  };

  return NodeOption;
});

define('js/canvas/TopologyBackground',['underscore', 'backbone', 'js/canvas/NodeOption'],
function (_, Backbone, NodeOption)
{
  

  function TopologyBackground(domRoot, forceNodes, sites, defaults,
			      clickUpdate, context, force, validList)
  {
    this.root = d3v4.select(domRoot[0]);
    this.forceNodes = forceNodes;
    this.sites = sites;
    this.defaults = defaults;
    this.clickUpdate = clickUpdate;
    this.context = context;
    this.force = force;
    this.validList = validList;
    this.zoomScale = 1.0;
    this.zoomX = 0;
    this.zoomY = 0;
    this.width = 1000;
    this.height = 1000;
    this.lastCenter = null;
    this.justClickedOutside = false;
    this.paletteOffset = 300;
    if (context.mode !== 'editor' && ! context.show.selectInfo)
    {
      this.paletteOffset = 0;
    }

    _.extend(this, Backbone.Events);
  }

  TopologyBackground.prototype = {

    render: function ()
    {
      this.outer = this.root.append("svg:svg")
	.attr("class", "topomap")
//	.style("visibility", "hidden")
	.attr("width", this.width)
	.attr("height", this.height)
	.attr("pointer-events", "all");

      this.viewport = this.outer.append('svg:g')
	.attr('width', this.width - this.paletteOffset)
	.attr('height', this.height)
	.attr('transform', 'translate(' + this.paletteOffset + ', 0)');

      this.rect = this.viewport.append("svg:rect")
	.attr("class", "rect")
	.attr("width", this.width - this.paletteOffset)
	.attr("height", this.height)
	.style("fill", "#fff")
	.on("click", _.bind(this.handleClickOutside, this))
	.on("mousedown", _.bind(this.handleMouseDown, this))
	.on("mouseup", _.bind(this.handleMouseUp, this));

      this.inner = this.viewport
	.append('svg:g')
	.attr("class", "vis");

      this.hullBase = this.inner.append('svg:g');
      this.hostBase = this.inner.append('svg:g');
      this.linkBase = this.inner.append('svg:g');

      this.dragLine = this.inner.append('svg:line')
	.attr('class', 'linkline dragline hidden')
	.attr('x1', '0')
	.attr('y1', '0')
	.attr('x2', '0')
	.attr('y2', '0');

      this.nodeBase = this.inner.append('svg:g');
      this.hullLabelBase = this.inner.append('svg:g');
      this.hostLabelBase = this.inner.append('svg:g');

      this.palette = this.outer.append('svg:g')
	.attr('width', this.paletteOffset)
	.attr('height', this.height);
      this.paletteBackground = this.palette.append('svg:rect')
	.attr('fill', '#ddd')
	.attr('width', this.paletteOffset)
	.attr('height', this.height);

      if (this.context.mode === 'editor')
      {
	this.palette.append('svg:text')
          .attr('x', '150px')
	  .attr('y', '30px')
          .attr('class', 'dragtext')
	  .text('Drag to Add');

	this.nodeMarkers = [];
	var i = 0;
	_.each(this.defaults, function (options) {
	  var marker = new NodeOption({
	    root: this.outer,
	    options: options,
	    pos: {
	      x: 75 + (i%3) * 75,
	      y: 70 + 75 * Math.floor(i/3)
	    },
	    toInternalPosition: _.bind(this.toInternalPosition, this),
	    force: this.force,
	    validList: this.validList
	  });
	  marker.on('create-node', function (x, y, site) {
	    this.trigger('create-node', x, y, site, options);
	  }.bind(this));
	  marker.render();
	  this.nodeMarkers.push(marker);
	  i += 1;
	}.bind(this));

	if (this.context.multiSite)
	{
	  this.siteOrigin = { x: 125,
			      y: 100 + 75 * Math.ceil(i/3) };

	  this.lazySite = this.makeSite(this.outer);
	  this.lazySite.style('visibility', 'hidden');
	  this.siteMarker = this.makeSite(this.outer);
	  var siteDrag = d3v4.drag()
	    .on('start', _.bind(this.siteDragStart, this))
	    .on('drag', _.bind(this.siteDragMove, this))
	    .on('end', _.bind(this.siteDragEnd, this));
	  this.siteMarker.call(siteDrag);
	}
      }

      //this.rescaleToZoom();
      this.centerScale();
    },

    makeSite: function (root)
    {
      var result = root.append('svg:g')
	.attr('transform', 'translate(' + this.siteOrigin.x + ', ' +
	      this.siteOrigin.y + ')')
	.attr('class', 'site-marker')
	.attr('style', 'cursor: pointer');

      result.append('svg:path')
        .style("fill", 'rgb(255, 127, 14)')
        .style("stroke", 'rgb(255, 127, 14)')
        .style("stroke-width", 80)
        .style("stroke-linejoin", "round")
        .style("opacity", .6)
	.attr('d', 'M-20,0L70,0Z');
      var label = result.append('svg:g')
	.attr('class', 'sitelabelgroup')
	.attr('transform', 'translate(25, 5)');

      label.append('svg:rect')
        .attr('class', 'labelbox')
        .attr('x', '-40px')
        .attr('y', '-14px')
        .attr('width', '80px')
        .attr('height', '20px');

      label.append('svg:text')
        .attr('x', '0px')
        .attr("class", "sitetext")
	.text('New Site');

      return result;
    },

    resize: function (width, height)
    {
      if (width > 0 && height > 0)
      {
	this.width = width - this.paletteOffset;
	this.height = height;
	this.outer.attr('width', width).attr('height', height);
	this.palette.attr('height', height);
	this.paletteBackground.attr('height', height);
	this.viewport.attr('width', width - this.paletteOffset).attr('height', height);
	this.rect.attr('width', width - this.paletteOffset).attr('height', height);
	//this.rescaleToZoom();
	this.centerScale(true);
      }
    },

    centerScale: function (alwaysRecenter)
    {
      var nodes = _.clone(this.forceNodes);
      _.each(this.sites, function (site) {
	nodes.push(site.label);
      });
      var minX = getExtreme(Math.min, nodes, 'x');
      var maxX = getExtreme(Math.max, nodes, 'x');
      var minY = getExtreme(Math.min, nodes, 'y');
      var maxY = getExtreme(Math.max, nodes, 'y');
      var center = {
	tx: 0,
	ty: 0,
	scale: 1
      };
      if (minX !== null && minY !== null && maxX !== null && maxY !== null)
      {
	var width = maxX - minX + 200;
	var height = maxY - minY + 200;
	center.scale = Math.min(this.width / width,
				this.height / height);
	center.scale = Math.min(center.scale, 1);
//      center.scale *= 1.2; 
	var marginX = (this.width/center.scale - (maxX - minX))/2;
	var marginY = (this.height/center.scale - (maxY - minY))/2;
	center.tx = -(minX*center.scale) + marginX*center.scale;
	center.ty = -(minY*center.scale) + marginY*center.scale;
      }
      if (alwaysRecenter ||
	  ! this.lastCenter ||
	  Math.abs(this.lastCenter.tx - center.tx) > 100 ||
	  Math.abs(this.lastCenter.ty - center.ty) > 100 ||
	  center.scale / this.lastCenter.scale > 1.2 ||
	  center.scale / this.lastCenter.scale < 1.0)
      {
	this.lastCenter = center;
	this.inner.attr('transform',
			'translate(' + center.tx + ',' + center.ty + ')' +
			'scale(' + center.scale + ')');
      }
      this.lastCenter.scale = center.scale;
      this.zoomX = center.tx;
      this.zoomY = center.ty;
      this.zoomScale = center.scale;
    },

    rescaleToZoom: function ()
    {
      this.rescaleTo(this.zoomX, this.zoomY, this.zoomScale);
    },

    rescaleTo: function (x, y, scale)
    {
      var baseWidth = 1000;
      var baseHeight = 1000;
      var minScale = Math.max(this.width / baseWidth,
			      this.height / baseHeight);
      var xScale = scale;
      var yScale = scale;
      if (baseWidth * scale <= this.width)
      {
	xScale = this.width / baseWidth;
      }
      if (baseHeight * scale <= this.height)
      {
	yScale = this.height / baseHeight;
      }
      var finalScale = Math.max(xScale, yScale);

      var tx = Math.min(0, Math.max(this.width - baseWidth * finalScale, x));
      var ty = Math.min(0, Math.max(this.height - baseHeight * finalScale, y));
      this.zoomX = tx;
      this.zoomY = ty;
      this.zoomScale = finalScale;
      this.inner.attr('transform',
		      'translate(' + tx + ',' + ty + ')' +
		      'scale(' + finalScale + ')');
//      this.tour.update();
    },

    showDragLine: function (start, end)
    {
      this.dragLine.classed('hidden', false)
	.attr('x1', start.x)
	.attr('y1', start.y)
	.attr('x2', end.x)
	.attr('y2', end.y);
    },

    hideDragLine: function ()
    {
      this.dragLine.classed('hidden', true);
    },

    handleRescale: function ()
    {
//    //if (! this.isMouseDown)
//    {
//      this.rescaleTo(d3v4.event.translate[0], d3v4.event.translate[1],
//		     d3v4.event.scale);
//    }
    },

    handleMouseDown: function (event)
    {
      var that = this;
      this.justClickedOutside = true;
      _.delay(function () { that.justClickedOutside = false; }, 200);
//      this.domRoot.addClass("unselectable");
    },

    handleMouseUp: function (event)
    {
//      this.domRoot.removeClass("unselectable");
    },

    handleClickOutside: function (event)
    {
      if (this.justClickedOutside)
      {
	this.clickUpdate.trigger('click-outside',
				 { event: _.clone(d3v4.event) });
      }
    },

    siteDragStart: function ()
    {
      this.lazySite.style('visibility', 'visible');
      this.lastSite = null;
      this.force.stop();
    },

    siteDragMove: function ()
    {
      this.siteMarker.attr('transform', 'translate(' + (d3v4.event.x - 20) + ',' + (d3v4.event.y - 10) + ')');
//      this.lastSiteMouse = d3v4.mouse(this.inner);
      var scale = this.zoomScale;
      var tx = this.zoomX;
      var ty = this.zoomY;
      this.lastSite = {
	x: d3v4.event.x,
	y: d3v4.event.y,
	topX: (d3v4.event.x - tx - this.paletteOffset)/scale,
	topY: (d3v4.event.y - ty)/scale
      };
    },

    siteDragEnd: function ()
    {
      this.lazySite.style('visibility', 'hidden');
      if (this.lastSite &&
	  this.lastSite.x > this.paletteOffset &&
	  this.lastSite.y > 0 &&
	  this.lastSite.x < this.width + this.paletteOffset &&
	  this.lastSite.y < this.height)
      {
	this.trigger('create-site', this.lastSite.topX, this.lastSite.topY);
      }
      this.siteMarker.attr('transform', 'translate(' + this.siteOrigin.x +
			   ', ' + this.siteOrigin.y + ')');
    },

    closestSite: function (x, y)
    {
      var closest = null;
      var closestDistance = null;
      _.each(this.sites, function (site) {
	var distance = site.distance(x, y);
	if (closest === null || distance < closestDistance)
	{
	  closest = site;
	  closestDistance = distance;
	}
      });
      return closest;
    },

    toInternalPosition: function (pos)
    {
      var scale = this.zoomScale;
      var tx = this.zoomX;
      var ty = this.zoomY;
      var result = {
	x: pos.x,
	y: pos.y,
	topX: (pos.x - tx - this.paletteOffset)/scale,
	topY: (pos.y - ty)/scale
      };
      if (result.x > this.paletteOffset &&
	  result.y > 0 &&
	  result.x < this.width + this.paletteOffset &&
	  result.y < this.height)
      {
	result.closestSite = this.closestSite(result.topX, result.topY);
      }
      else
      {
	result.closestSite = null;
      }
      return result;
    }

  };

  function getExtreme(f, list, key)
  {
    var result = null;
    _.each(list, function (item) {
      if (item[key] !== null && item[key] !== undefined)
      {
	if (result === null)
	{
	  result = item[key];
	}
	else
	{
	  result = f(result, item[key]);
	}
      }
    });
    return result;
  }

  return TopologyBackground;
});

define('js/canvas/ForceGraph',['underscore', 'backbone'],
function (_, Backbone)
{
  

  function ForceGraph()
  {
    this.nodes = [];
    this.links = [];
    this.force = d3v4.forceSimulation(this.nodes)
      .force('charge', d3v4.forceManyBody())
      .force('link', d3v4.forceLink(this.links))
      .force('center', d3v4.forceCenter())
      .force('x', d3v4.forceX())
      .force('y', d3v4.forceY())
      .alphaMin(0.1);

    
    this.force.force('link')
      .distance(function(d) {
	return d.distance;
      })
      .strength(function(d) {
	return d.strength;
      });

    this.force.force('charge').strength(function (d) {
      return -500;
    });

/*
    this.force.gravity(0.05)
      .charge(-500)
      .size([1000, 1000])
*/
    this.force.on('tick', _.bind(this.tick, this));
    this.shouldDisplay = true;

    _.extend(this, Backbone.Events);
  }

  ForceGraph.prototype = {

    add: function (newNodes, newLinks) {
      var that = this;
      _.each(newNodes, function (node) {
	that.nodes.push(node);
	that.force.nodes(that.nodes);
      });
      _.each(newLinks, function (link) {
	that.links.push(link);
	that.force.force('link').links(that.links);
      });
    },

    remove: function (oldNodes, oldLinks) {
      for (var i = this.nodes.length - 1; i >= 0; i--)
      {
	if (_.contains(oldNodes, this.nodes[i]))
	{
	  this.nodes.splice(i, 1);
	}
      }
      for (var i = this.links - 1; i >= 0; i--)
      {
	if (_.contains(oldLinks, this.links[i]) ||
	    _.contains(oldNodes, this.links[i].source) ||
	    _.contains(oldNodes, this.links[i].target))
	{
	  this.links.splice(i, 1);
	}
      }
      this.tick();
    },

    start: function () {
      this.force.alpha(1);
      this.force.restart();
    },

    stop: function () {
      this.force.stop();
    },

    skipAhead: function () {
      this.shouldDisplay = false;
      this.force.alpha(1);
      this.force.restart();
      for (var i = 0; i < 40; i += 1)
      {
	this.force.tick();
      }
      this.force.stop();
      this.shouldDisplay = true;
      this.force.restart();
    },

    tick: function (event) {
      var forceStopped = false;
/*
      console.log(this.force.alpha());
      if (event && this.force.alpha() < 0.5)//event.alpha < 0.03)
      {
	_.defer(function () {
	  this.force.alpha(0);
	  this.force.stop();
	  console.log('stopping');
	}.bind(this));
	forceStopped = true;
      }
*/
      this.trigger('tick', forceStopped, this.shouldDisplay);
    },

    findEndpoint: function (nodeModel) {
      var result = null;
      _.each(this.nodes, function (node) {
	if (node.model && node.model === nodeModel)
	{
	  result = node;
	}
      });
      return result;
    }

  }

  return ForceGraph;
});

define('js/canvas/NodeView',['underscore', 'backbone'],
function (_, Backbone)
{
  

  var NodeView = Backbone.View.extend({

    initialize: function () {
      this.node = {
	model: this.model,
	view: this
      };
      if (this.model.x !== undefined &&
	  this.model.y !== undefined)
      {
	this.node.x = this.model.x;
	this.node.y = this.model.y;
      }
      this.old = {};
      this.oldPos = { x: this.node.x, y: this.node.y };
      this.options.force.add([this.node], []);
      this.render();
      this.groupChanged = null;
      this.linkDragging = false;
    },

    cleanup: function () {
      this.options.force.remove([this.node], []);
      this.container.remove();
    },

    render: function () {
      var dragnode = d3v4.drag()
	.on('start', _.bind(this.handleDragStart, this))
	.on('drag', _.bind(this.handleDragMove, this))
	.on('end', _.bind(this.handleDragEnd, this));

      var draglink = d3v4.drag()
	.on('start', _.bind(this.linkDragStart, this))
	.on('drag', _.bind(this.linkDragMove, this))
	.on('end', _.bind(this.linkDragEnd, this));

      this.container = this.options.background.nodeBase.append('svg:g')
	.attr("id", this.model.id);

      if (this.options.context.mode === 'editor')
      {
	this.background = this.container.append('svg:g');
	this.backRect = this.background.append('svg:rect')
	  .attr('class', 'nodebackground')
	  .attr('x', '-70px')
	  .attr('y', '-65px')
	  .attr('width', '140px')
	  .attr('height', '145px')
	  .call(draglink)
	  .on('click', _.bind(this.handleBackgroundClick, this))
	  .on('mouseover', _.bind(this.linkHoverStart, this))
	  .on('mousemove', _.bind(this.linkHoverMove, this))
	  .on('mouseout', _.bind(this.linkHoverEnd, this));
      }

      this.foreground = this.container.append('svg:g')
        .attr("class", "node")
	.call(dragnode)
	//.on('click', _.bind(this.handleClick, this))
	.on('mouseover', _.bind(this.foregroundHover, this))
	.on('mouseout', _.bind(this.foregroundHoverEnd, this));

      var nodebox = 'nodebox';
      if (this.model.isHost !== undefined)
      {
	nodebox = 'nodehostbox';
      }

      this.rect = this.foreground.append('svg:rect')
	.attr('class', nodebox)
	.attr('x', '-30px')
	.attr('y', '-25px')
	.attr('width', '60px')
	.attr('height', '65px');

      this.highlight = this.foreground.append('svg:rect')
	.attr("class", "checkbox")
	.attr('x', '-30px')
	.attr('y', '-25px')
	.attr('width', '60px')
	.attr('height', '65px')
	.attr("style", "visibility:hidden;");

      this.status = this.foreground.append('svg:g')
        .attr('class', 'node-status')
        .attr('style', 'visibility: hidden');

      this.status.append('svg:rect')
	.attr('x', '10px')
	.attr('y', '-25px')
	.attr('class', 'node-status-box')
	.attr('width', '20px')
	.attr('height', '20px');

      this.status.append('svg:text')
        .attr('class', 'node-status-text')
	.attr('x', '15px')
	.attr('y', '-10px')
	.text('');

      this.warning = this.foreground.append('svg:g')
	.attr('style', 'visibility:hidden');

      this.warning.append('svg:rect')
        .attr('x', '-40px')
	.attr('y', '-35px')
        .attr('class', 'warningbox')
	.attr('width', '20px')
	.attr('height', '20px');

      this.warning.append('svg:text')
        .attr('class', 'warningboxtext')
        .attr('x', '-32px')
	.attr('y', '-20px')
	.text('!');

      this.icon = this.foreground.append('svg:image')
	.attr("class", "nodebox")
	.attr("x", "-20px")
	.attr("y", "-20px")
	.attr("width", "40px")
	.attr("height", "40px");

      this.label = this.foreground.append('svg:text')
	.attr("class", "nodetext")
	.attr("x", "0px")
	.attr("y", "35px");
    },

    update: function (shouldDisplay, highlightList) {
      if (shouldDisplay)
      {
	if (this.node.x && this.node.y &&
	    (this.node.x !== this.old.x ||
	     this.node.y !== this.old.y))
	{
	  this.container.attr('transform', 'translate(' +
			      this.node.x + ',' +
			      this.node.y + ')');
	  this.model.x = this.node.x;
	  this.model.y = this.node.y;
	}
	if (this.oldPos.x !== this.node.x ||
	    this.oldPos.y !== this.node.y)
	{
	  this.options.background.hideDragLine();
	  this.oldPos = { x: this.node.x, y: this.node.y };
	}
	if (this.model.name !== this.old.name)
	{
	  this.label.text(this.model.name);
	  var textWidth = 50;
	  try
	  {
	    this.label.each(function () {
	      textWidth = this.getBBox().width + 10;
	    });
	  } catch (e) {}
	  textWidth = Math.max(textWidth, 60)

	  this.rect.attr('width', textWidth + 'px');
	  this.rect.attr('x', (-textWidth/2) + 'px');
	  this.highlight.attr('width', textWidth + 'px');
	  this.highlight.attr('x', (-textWidth/2) + 'px');
	}
	var iconUrl = window.JACKS_LOADER.basePath + 'images/default.svg';
	if (this.model.icon)
	{
	  iconUrl = this.model.icon;
	}
	if (iconUrl !== this.old.icon)
	{
	  this.icon.attr("xlink:href", iconUrl);
	}

	this.old = {
	  x: this.node.x,
	  y: this.node.y,
	  name: this.model.name,
	  icon: iconUrl
	};
	if (highlightList)
	{
	  if (_.contains(highlightList, this.model.id))
	  {
	    this.highlight.style('visibility', 'visible')
	      .classed('selected', true);
	  }
	  else
	  {
	    this.highlight.style('visibility', 'hidden')
	      .classed('selected', false);
	  }
	}

	var shouldWarn = false;
	_.each(_.keys(this.model.warnings), function(key) {
	  if (this.model.warnings[key])
	  {
	    shouldWarn = true;
	  }
	}.bind(this));
	if (this.options.context.mode === 'viewer')
	{
	  shouldWarn = false;
	}
	if (shouldWarn !== this.shouldWarn)
	{
	  this.shouldWarn = shouldWarn;
	  if (shouldWarn)
	  {
	    this.warning.style('visibility', 'visible');
	  }
	  else
	  {
	    this.warning.style('visibility', 'hidden');
	  }
	}

      }
    },

    handleClick: function () {
      if (! d3v4.event.defaultPrevented)
      {
	//d3v4.event.preventDefault();
	var data = {
	  type: 'node',
	  item: this.model,
	  modkey: (d3v4.event.ctrlKey || d3v4.event.shiftKey),
	  event: null
	};
	if (d3v4.event.sourceEvent)
	{
	  data.event = _.clone(d3v4.event.sourceEvent);
	}
	else
	{
	  data.event = _.clone(d3v4.event);
	}
	this.options.clickUpdate.trigger('click-event', data);
      }
    },

    handleBackgroundClick: function () {
      if (! d3v4.event.defaultPrevented)
      {
	this.options.clickUpdate.trigger('click-outside');
      }
    },

    foregroundHover: function () {
      this.options.setHoverId(this.model.id);
    },

    foregroundHoverEnd: function () {
      this.options.setHoverId(null);
    },

    handleDragStart: function () {
      this.dragMoveCount = 0;
      this.groupChanged = null;
      this.options.force.stop();
      $('body > .popover').hide();
    },

    handleDragMove: function () {
      this.dragMoveCount += 1;
      var pos = d3v4.mouse(this.options.background.inner.node());
      this.node.vx = 0;
      this.node.vy = 0;
      this.node.x  = pos[0];
      this.node.y  = pos[1];
      var newGroup = this.options.checkGroup(this.node.x, this.node.y,
					     this.model.group);
      if (this.options.context.mode === 'editor' &&
	  newGroup !== this.model.group &&
	  (! this.groupChanged ||
	   Math.abs(this.node.x - this.groupChanged.x) > 60 ||
	   Math.abs(this.node.y - this.groupChanged.y) > 60))
      {
	this.groupChanged = { x: this.node.x, y: this.node.y };
	this.model.group = newGroup;
      }
      this.trigger('update');
    },

    handleDragEnd: function () {
      if (this.dragMoveCount && this.dragMoveCount > 1)
      {
	this.trigger('update-move');
	_.defer(function () {
	    $('html').css('-webkit-user-select', 'initial');
	    $('html').css('-moz-user-select', 'initial');
	  });
      }
      else
      {
	this.handleClick();
      }
    },

    linkHoverStart: function () {
      var pos = d3v4.mouse(this.options.background.inner.node());
      if (! this.linkDragging)
      {
	this.options.background.showDragLine(this.node,
					     { x: pos[0], y: pos[1] });
      }
    },

    linkHoverMove: function () {
      var pos = d3v4.mouse(this.options.background.inner.node());
      if (! this.linkDragging)
      {
	this.options.background.showDragLine(this.node,
					     { x: pos[0], y: pos[1] });
      }
    },

    linkHoverEnd: function () {
      if (! this.linkDragging)
      {
	this.options.background.hideDragLine();
      }
    },

    linkDragStart: function () {
      var pos = d3v4.mouse(this.options.background.inner.node());
      this.linkDragging = true;
      this.options.background.showDragLine(this.node,
					   { x: pos[0], y: pos[1] });
    },

    linkDragMove: function () {
      var pos = d3v4.mouse(this.options.background.inner.node());
      this.options.background.showDragLine(this.node,
					   { x: pos[0], y: pos[1] });
    },

    linkDragEnd: function () {
      this.linkDragging = false;
      this.options.background.hideDragLine();
      this.trigger('create-link', this.model.id);
      _.defer(function () {
	$('html').css('-webkit-user-select', 'initial')
	$('html').css('-moz-user-select', 'initial')
      });
    }

  });

  return NodeView;
});

define('js/canvas/LanView',['underscore', 'backbone'],
function (_, Backbone)
{
  

  var LanView = Backbone.View.extend({

    initialize: function () {
      var that = this;
      this.node = {
	model: this.model,
	view: this,
	fixed: true,
	x: Math.random() * 500,
	y: Math.random() * 500
      };
      this.randOffset = {
	x: Math.floor(Math.random() * 100 - 50),
	y: Math.floor(Math.random() * 100 - 50)
      };
      this.render();

      this.links = [];
      _.each(this.model.endpoints, function (endpoint) {
	that.addEndpoint(endpoint);
      });
      this.options.force.add([this.node], this.links);
      this.listenTo(this.model, 'addEndpoint', this.addEndpoint);
      this.listenTo(this.model, 'removeEndpoint', this.removeEndpoint);
    },

    cleanup: function () {
      this.options.force.remove([this.node], this.links);
      this.lineContainer.remove();
      this.center.remove();
    },

    render: function () {
      var that = this;
      this.lineContainer = this.options.background.linkBase.append('svg:g')
        .attr("class", "link");
      this.lines = [];

      this.center = this.options.background.nodeBase.append('svg:g')
	.attr("class", "boxgroup")
	.on('click', _.bind(this.handleClick, this))
	.on('mouseover', _.bind(this.handleHover, this))
	.on('mouseout', _.bind(this.handleHoverEnd, this))
	.attr("id", this.model.id);

      this.centerRect = this.center.append('svg:rect')
	.attr("class", "linkbox")
	.attr('x', '-10px')
	.attr('y', '-10px')
	.attr('width', '20px')
	.attr('height', '20px');

      this.centerHighlight = this.center.append('svg:rect')
	.attr("class", "checkbox")
	.attr('x', '-10px')
	.attr('y', '-10px')
	.attr('width', '20px')
	.attr('height', '20px')
	.attr("style", "visibility:hidden;");

      this.warning = this.center.append('svg:g')
	.attr('style', 'visibility:hidden');

      this.warning.append('svg:rect')
        .attr('x', '-17px')
	.attr('y', '-17px')
        .attr('class', 'warningbox')
	.attr('width', '14px')
	.attr('height', '14px');

      this.warning.append('svg:text')
        .attr('class', 'warningboxtext')
        .attr('x', '-12px')
	.attr('y', '-5px')
	.text('!');
    },

    update: function (shouldDisplay, highlightList) {
      if (this.links.length > 1)
      {
	var sum = { x: 0, y: 0 };
	_.each(this.links, function (link) {
	  if (link.target.x && link.target.y)
	  {
	    sum.x += link.target.x;
	    sum.y += link.target.y;
	  }
	});
	this.node.fixed = true;
	this.node.fx = sum.x / this.links.length + this.randOffset.x;
	this.node.fy = sum.y / this.links.length + this.randOffset.y;
	this.node.x = this.node.fx;
	this.node.y = this.node.fy;
      }
      else if (this.links.length == 1)
      {
	if (! this.node.x || ! this.node.y)
	{
	  this.node.x = this.links[0].target.x + 50;
	  this.node.y = this.links[0].target.y - 50;
	  delete this.node.fx;
	  delete this.node.fy;
	}
	this.node.fixed = false;
      }
      this.updateForceLinks();

      if (shouldDisplay)
      {
	this.center.attr('transform',
			 'translate(' + this.node.x +
			 ',' + this.node.y + ')');
	for (var i = 0; i < this.links.length; i += 1)
	{
	  if (this.links[i].source.x && this.links[i].source.y &&
	      this.links[i].target.x && this.links[i].target.y)
	  {
	    this.lines[i].attr('x1', this.links[i].source.x)
	      .attr('y1', this.links[i].source.y)
	      .attr('x2', this.links[i].target.x)
	      .attr('y2', this.links[i].target.y);
	  }
	}
	if (highlightList)
	{
	  if (_.contains(highlightList, this.model.id))
	  {
	    this.centerHighlight.style('visibility', 'visible')
	      .classed('selected', true);
	  }
	  else
	  {
	    this.centerHighlight.style('visibility', 'hidden')
	      .classed('selected', false);
	  }
	}

	var shouldWarn = false;
	_.each(_.keys(this.model.warnings), function(key) {
	  if (this.model.warnings[key])
	  {
	    shouldWarn = true;
	  }
	}.bind(this));
	if (this.options.context.mode === 'viewer')
	{
	  shouldWarn = false;
	}
	if (shouldWarn !== this.shouldWarn)
	{
	  this.shouldWarn = shouldWarn;
	  if (shouldWarn)
	  {
	    this.warning.style('visibility', 'visible');
	  }
	  else
	  {
	    this.warning.style('visibility', 'hidden');
	  }
	}

      }
    },

    updateForceLinks: function () {
      var distance = 300;
      var strength = 0.5;
      if (this.withinSite())
      {
	distance = 100;
	strength = 1.5;
      }
      _.each(this.links, function (link) {
	link.distance = distance;
	link.strength = strength;
      });
    },

    withinSite: function ()
    {
      var result = true;
      if (this.model.endpoints.length > 0)
      {
	var first = this.model.endpoints[0].group;
	_.each(this.model.endpoints, function (endpoint) {
	  if (first !== endpoint.group)
	  {
	    result = false;
	  }
	});
      }
      return result;
    },

    addEndpoint: function (endpoint) {
      var link = {};
      link.source = this.node;
      link.target = this.options.force.findEndpoint(endpoint);
      this.links.push(link);
      var line = this.lineContainer.append('svg:line')
	.attr('class', 'linkline')
	.attr("id", 'link-' + this.model.id + '-node-' + endpoint.id);
      this.lines.push(line);
      this.updateForceLinks();
      this.options.force.add([], [link]);
    },

    removeEndpoint: function (endpoint) {
      var link;
      var linkIndex;
      var target = this.options.force.findEndpoint(endpoint);
      _.each(this.links, function (candidate, index) {
	if (candidate.source === this.node && candidate.target === target)
	{
	  link = candidate;
	  linkIndex = index;
	}
      }.bind(this));
      if (link)
      {
	this.lines[linkIndex].remove();
	this.links.splice(linkIndex, 1);
	this.lines.splice(linkIndex, 1);
	this.options.force.remove([], [link]);
      }
    },

    handleClick: function () {
      if (! d3v4.event.defaultPrevented)
      {
	var data = {
	  type: 'link',
	  item: this.model,
	  modkey: (d3v4.event.ctrlKey || d3v4.event.shiftKey),
	  event: _.clone(d3v4.event)
	};
	this.options.clickUpdate.trigger('click-event', data);
      }
    },

    handleHover: function () {
      this.options.setHoverId(this.model.id);
    },

    handleHoverEnd: function () {
      this.options.setHoverId(null);
    }

  });

  return LanView;
});

define('js/canvas/SiteView',['underscore', 'backbone'],
function (_, Backbone)
{
  

  var SiteView = Backbone.View.extend({

    initialize: function () {
      this.justClickedOutside = false;
      this.vertices = [];
      this.links = [];
      this.label = {
	model: this.model,
	view: this
      };
      if (this.model.x !== undefined &&
	  this.model.y !== undefined)
      {
	this.label.x = this.model.x;
	this.label.y = this.model.y;
      }
      this.options.force.add([this.label], []);
      this.render();
    },

    cleanup: function () {
      this.options.force.remove([this.label], this.links);
      if (this.hull)
      {
	this.hull.remove();
      }
      if (this.hullLabel)
      {
	this.hullLabel.remove();
      }
    },

    render: function () {
      var drag = d3v4.drag()
	.on('start', _.bind(this.handleDragStart, this))
	.on('drag', _.bind(this.handleDragMove, this))
	.on('end', _.bind(this.handleDragEnd, this));

      var background = this.options.background;
      this.hull = background.hullBase.append('svg:path')
        .style("fill", this.options.fill)
        .style("stroke", this.options.fill)
        .style("stroke-width", 150)
        .style("stroke-linejoin", "round")
        .style("opacity", .6)
	.on("click", _.bind(this.handleClickOutside, this))
	.on("mousedown", _.bind(this.handleMouseDown, this));

      if (this.options.context.multiSite)
      {
	this.hullLabel = background.hullLabelBase.append('svg:g')
	  .attr('class', 'sitelabelgroup')
          .attr('style', 'cursor:pointer')
	  .call(drag)
	  .on('click', _.bind(this.handleClick, this))
	  .attr("id", this.model.id);

	this.labelRect = this.hullLabel.append('svg:rect')
          .attr('class', 'labelbox')
          .attr('x', '-40px')
          .attr('y', '-14px')
          .attr('width', '80px')
          .attr('height', '20px');

	this.labelSelect = this.hullLabel.append('svg:rect')
          .attr('class', 'checkbox')
          .attr('x', '-40px')
          .attr('y', '-14px')
          .attr('width', '80px')
          .attr('height', '20px')
          .style('visibility', 'hidden');

	this.hullText = this.hullLabel.append('svg:text')
          .attr('x', '0px')
          .attr("class", "sitetext");
      }
    },

    update: function (shouldDisplay, highlightList, dragPoint) {
      this.findVertices();
      this.connectVertices();
      if (shouldDisplay)
      {
	if (this.options.context.multiSite && this.options.siteCount() > 1)
	{
	  this.hull.style("fill", this.options.fill)
            .style("stroke", this.options.fill);
//	  this.hullLabel.attr('class', 'sitelabelgroup');
	}
	else
	{
	  this.hull.style("fill", "#fff")
            .style("stroke", "#fff");
//	  if (this.hullLabel)
//	  {
//	    this.hullLabel.attr('class', 'hidden sitelabelgroup');
//	  }
	}
	this.hull.attr("d", this.groupPath(dragPoint));
	if (this.options.context.multiSite &&
	    (this.options.siteCount() > 1 ||
	     this.options.context.mode === 'editor'))
	{
	    if (this.links.length > 1)
	    {
		this.hullLabel.attr('transform',
				    'translate(' + this.label.x + ',' +
				    this.label.y + ')');
	    }
	    else
	    {
		var that = this;
		var x = this.label.x;
		var y = this.label.y;
		_.each(this.options.force.nodes, function (node) {
		    if (node.model && node.model.group === that.model.id &&
			node.x && node.y)
		    {
			x = node.x;
			y = node.y - 50;
		    }
		});
		this.hullLabel.attr('transform',
				    'translate(' + x + ',' +
				    y + ')');
	    }
	  var name = this.model.name;
	  var re = new RegExp('^[0-9]+$');
	  if (re.test(name))
	  {
	    name = 'Site ' + name;
	  }
	  this.hullText.text(name);
	  var textWidth = 50;
	  try
	  {
	    this.hullText.each(function (d, i) {
	      textWidth = this.getBBox().width;
	    });
	  }
	  catch (e) {}
	  var textHeight = 20;
	  if (this.options.siteCount() === 1)
	  {
	    textWidth += 20;
	    textHeight += 20;
	  }
	  this.labelRect.attr('width', (textWidth + 10) + 'px')
	    .attr('height', textHeight + 'px');
	  this.labelRect.attr('x', (-textWidth/2 - 5) + 'px')
	    .attr('y', (-textHeight/2 - 5) + 'px');
	  this.labelSelect.attr('width', (textWidth + 10) + 'px')
	    .attr('height', textHeight + 'px');
	  this.labelSelect.attr('x', (-textWidth/2 - 5) + 'px')
	    .attr('y', (-textHeight/2 - 5) + 'px');
	  if (highlightList)
	  {
	    if (_.contains(highlightList, this.model.id))
	    {
	      this.hullLabel.selectAll('.checkbox')
		.style('visibility', 'visible')
		.classed('selected', true);
	    }
	    else
	    {
	      this.hullLabel.selectAll('.checkbox')
		.style('visibility', 'hidden')
		.classed('selected', false);
	    }
	  }
	  this.hullLabel.style('visibility', 'visible');
	}
	else
	{
	  if (this.hullLabel)
	  {
	    this.hullLabel.style('visibility', 'hidden');
	  }
	}
      }
    },

    contains: function (x, y, oldGroup) {
      return (this.model.id !== oldGroup &&
	      intersects([x, y, 150], this.vertices));
    },

    distance: function (x, y) {
      return minDistance([x, y], this.vertices);
    },

    connectVertices: function () {
      var freshLinks = []; // Links that need to be added
      var staleLinks = []; // Links that need to be removed
      _.each(this.options.force.nodes, function (node) {
	var link = _.findWhere(this.links, { target: node });
	if (node.model && node.model.group === this.model.id)
	{
	  // Ensure that it is connected to the label node
	  if (! link)
	  {
	    freshLinks.push({
	      source: this.label,
	      target: node,
	      distance: 100,
	      strength: 0.05
	    });
	  }
	}
	else
	{
	  // Ensure that it is not connected to the label node
	  if (link)
	  {
	    staleLinks.push(link);
	  }
	}
      }.bind(this));
      if (freshLinks.length > 0)
      {
	this.links = this.links.concat(freshLinks);
	this.options.force.add([], freshLinks);
      }
      if (staleLinks.length > 0)
      {
	this.links = _.difference(this.links, staleLinks);
	this.options.force.remove([], staleLinks);
      }
    },

    findVertices: function () {
      var that = this;
      this.vertices = [];

      _.each(this.options.force.nodes, function (node) {
	if (node.model && node.model.group === that.model.id &&
	    node.x && node.y)
	{
	  that.vertices.push([node.x, node.y]);
	}
      });

      if (this.label.x === undefined && this.label.y === undefined)
      {
	if (this.vertices.length > 0)
	{
	  this.label.x = (_.max(this.vertices, function (a) { return a[0]; })[0] + _.min(this.vertices, function (a) { return a[0]; })[0])/2;
	  this.label.y = (_.max(this.vertices, function (a) { return a[1]; })[1] + _.min(this.vertices, function (a) { return a[1]; })[1])/2;
	  this.label.vx = 0;//this.label.x;
	  this.label.vy = 0;//this.label.y;
	}
	else
	{
	  this.label.x = 0;
	  this.label.y = 0;
	  this.label.vx = 0;
	  this.label.vy = 0;
	}
      }
/*
      if (this.vertices.length > 1)
      {
	this.label.x = (_.max(this.vertices, function (a) { return a[0]; })[0] + _.min(this.vertices, function (a) { return a[0]; })[0])/2;
	this.label.y = _.min(this.vertices, function (a) { return a[1]; })[1] - 40;
      }
      else if (this.vertices.length === 1)
      {
	var distance = Math.sqrt(Math.pow(this.vertices[0][0] - this.label.x, 2) +
				 Math.pow(this.vertices[0][1] - this.label.y, 2));
	if (distance > 200)
	{
	  var angle = Math.atan2(this.vertices[0][1] - this.label.y,
				 this.vertices[0][0] - this.label.x);
	  this.label.x = this.vertices[0][0] - Math.cos(angle)*200;
	  this.label.y = this.vertices[0][1] - Math.sin(angle)*200;
	}
      }
*/

      if (this.vertices.length !== 1)
      {
        this.vertices.push([this.label.x, this.label.y]);
      }
      if (this.vertices.length === 1)
      {
        this.vertices.push([this.vertices[0][0] - 25, this.vertices[0][1] - 25]);
        this.vertices.push([this.vertices[0][0] + 25, this.vertices[0][1] - 25]);
        this.vertices.push([this.vertices[0][0] + 25, this.vertices[0][1] + 25]);
        this.vertices.push([this.vertices[0][0] - 25, this.vertices[0][1] + 25]);
      }
    },

    groupPath: function(dragPoint) {
      var vertices = this.vertices;
      if (dragPoint)
      {
	vertices = _.clone(vertices);
	vertices.push(dragPoint);
      }
      return 'M' + d3v4.polygonHull(vertices).join('L') + 'Z';
    },

    handleClick: function () {
      if (! d3v4.event.defaultPrevented)
      {
	var data = {
	  type: 'site',
	  item: this.model,
	  modkey: (d3v4.event.ctrlKey || d3v4.event.shiftKey),
	  event: _.clone(d3v4.event)
	};
	this.options.clickUpdate.trigger('click-event', data);
      }
    },

    handleDragStart: function () {
      this.dragMoveCount = 0;
      this.options.force.stop();
    },

    handleDragMove: function () {
      this.dragMoveCount += 1;
      var that = this;
      var found = 0;
      _.each(this.options.force.nodes, function (node) {
	if (node.model && node.model.group === that.model.id)
	{
	  node.vx += 0;
	  node.vy += 0;
	  node.x += d3v4.event.dx;
	  node.y += d3v4.event.dy;
	  found += 1;
	}
      });
      this.label.x += d3v4.event.dx;
      this.label.y += d3v4.event.dy;
      this.label.vx = 0;//this.label.x;
      this.label.vy = 0;//this.label.y;
      this.trigger('update');
    },

    handleDragEnd: function () {
      if (this.dragMoveCount && this.dragMoveCount > 1)
      {
        this.trigger('update-move');
        _.defer(function () {
	  $('html').css('-webkit-user-select', 'initial')
	  $('html').css('-moz-user-select', 'initial')
        });
      }
      else
      {
	this.handleClick();
      }
    },

    handleMouseDown: function ()
    {
      var that = this;
      this.justClickedOutside = true;
      _.delay(function () { that.justClickedOutside = false; }, 200);
    },

    handleClickOutside: function () {
      if (this.justClickedOutside)
      {
	this.options.clickUpdate.trigger('click-outside');
      }
    },

  });

  function minDistance(point, polygon)
  {
    var result = null;
    _.each(polygonEdges(polygon), function (edge) {
      var distance = pointLineSegmentDistance(point, edge);
      if (result === null ||
	  distance < result)
      {
	result = distance;
      }
    });
    return result;
  }

  function intersects(circle, polygon)
  {
    return pointInPolygon(circle, polygon)
        || polygonEdges(polygon).some(function(line) { return pointLineSegmentDistance(circle, line) < circle[2]; });
  }

  function polygonEdges(polygon)
  {
    return polygon.map(function(p, i) {
      return i ? [polygon[i - 1], p] : [polygon[polygon.length - 1], p];
    });
  }

  function pointInPolygon(point, polygon)
  {
    for (var n = polygon.length, i = 0, j = n - 1, x = point[0], y = point[1], inside = false; i < n; j = i++)
    {
      var xi = polygon[i][0], yi = polygon[i][1],
          xj = polygon[j][0], yj = polygon[j][1];
      if ((yi > y ^ yj > y) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi)) inside = !inside;
    }
    return inside;
  }

  function pointLineSegmentDistance(point, line)
  {
    var v = line[0], w = line[1], d, t;
    return Math.sqrt(pointPointSquaredDistance(point, (d = pointPointSquaredDistance(v, w))
        ? ((t = ((point[0] - v[0]) * (w[0] - v[0]) + (point[1] - v[1]) * (w[1] - v[1])) / d) < 0 ? v
        : t > 1 ? w
        : [v[0] + t * (w[0] - v[0]), v[1] + t * (w[1] - v[1])])
        : v));
  }

  function pointPointSquaredDistance(v, w)
  {
    var dx = v[0] - w[0], dy = v[1] - w[1];
    return dx * dx + dy * dy;
  }

  return SiteView;
});

define('js/canvas/HostView',['underscore', 'backbone'],
function (_, Backbone)
{
  

  var HostView = Backbone.View.extend({

    initialize: function () {
      this.justClickedOutside = false;
      this.links = [];
      this.label = {
	model: this.model,
	view: this
      };
      this.center = {
	model: this.model,
	view: this
      };
      if (this.model.x !== undefined &&
	  this.model.y !== undefined)
      {
	this.label.x = this.model.x;
	this.label.y = this.model.y;
	this.center.x = this.model.x;
	this.center.y = this.model.y;
      }
      this.options.force.add([this.center], []);
      this.render();
    },

    cleanup: function () {
      this.options.force.remove([this.label], this.links);
      if (this.hull)
      {
	this.hull.remove();
      }
      if (this.hullLabel)
      {
	this.hullLabel.remove();
      }
    },

    render: function () {
/*
      var drag = d3v4.drag()
	.on('start', _.bind(this.handleDragStart, this))
	.on('drag', _.bind(this.handleDragMove, this))
	.on('end', _.bind(this.handleDragEnd, this));
*/
      var background = this.options.background;
      this.hull = background.hostBase.append('svg:path')
//        .style("fill", this.options.fill)
        .style("fill-opacity", 0)
        .style("stroke", this.options.fill)
        .style("stroke-width", 10)
        .style("stroke-linejoin", "round")
        .style("opacity", 1)
	.on("click", _.bind(this.handleClickOutside, this))
	.on("mousedown", _.bind(this.handleMouseDown, this));

      this.hullLabel = background.hostLabelBase.append('svg:g')
	.attr('class', 'hostlabelgroup')
        .attr('style', 'cursor:pointer')
//	  .call(drag)
	.on('click', _.bind(this.handleClick, this))
	.attr("id", this.model.id)
	.on('mouseover', _.bind(this.hoverStart, this))
	.on('mouseout', _.bind(this.hoverEnd, this));

      this.labelRect = this.hullLabel.append('svg:rect')
//          .attr('class', 'labelbox')
	.style('fill', "rgb(255,255,255)")
	.style('fill-opacity', 1)
        .attr('x', '0px')
        .attr('y', '-14px')
        .attr('width', '80px')
        .attr('height', '20px');

      this.labelSelect = this.hullLabel.append('svg:rect')
        .attr('class', 'checkbox')
        .attr('x', '-40px')
        .attr('y', '-14px')
        .attr('width', '80px')
        .attr('height', '20px')
        .style('visibility', 'hidden');

      this.hullText = this.hullLabel.append('svg:text')
        .attr('x', '0px')
	//.style('stroke', this.options.fill)
	//.style('fill', this.options.fill)
	.style('text-anchor', 'left')
        .attr("class", "hosttext");
    },

    update: function (shouldDisplay, highlightList, dragPoint) {
      if (shouldDisplay)
      {
	var groupPath = this.findVertices();
	this.connectVertices();
/*
	if (this.options.context.multiSite && this.options.siteCount() > 1)
	{
	  this.hull.style("fill", this.options.fill)
            .style("stroke", this.options.fill);
//	  this.hullLabel.attr('class', 'sitelabelgroup');
	}
	else
	{
	  this.hull.style("fill", "#fff")
            .style("stroke", "#fff");
//	  if (this.hullLabel)
//	  {
//	    this.hullLabel.attr('class', 'hidden sitelabelgroup');
//	  }
	}
*/
	if (groupPath !== "")
	{
	  this.hull.attr("d", groupPath);
	  this.hullLabel.attr('transform',
			      'translate(' + this.label.x + ',' +
			      this.label.y + ')');
	}
	var name = this.model.name;
	var re = new RegExp('^[0-9]+$');
	if (re.test(name))
	{
	  name = 'Host ' + name;
	}
	this.hullText.text(name);
	var textWidth = 50;
	try
	{
	  this.hullText.each(function (d, i) {
	    textWidth = this.getBBox().width;
	  });
	}
	catch (e) {}
	if (this.model.attachedNode !== undefined)
	{
	  _.each(this.options.force.nodes, function (node) {
	    if (node.model && node.model.name === this.model.attachedNode)
	    {
	      node.fx = this.label.x + textWidth/2;
	      node.fy = this.label.y + 30;
	      node.x = node.fx;
	      node.y = node.fy;
	    }
	  }.bind(this));
	}
	var textHeight = 20;
	this.labelRect.attr('width', (textWidth + 20) + 'px')
	  .attr('height', textHeight + 'px');

	this.labelRect.attr('x', '-10px')
	  .attr('y', (-textHeight/2 - 5) + 'px');
	this.labelSelect.attr('width', (textWidth + 10) + 'px')
	  .attr('height', textHeight + 'px');
	this.labelSelect.attr('x', (-textWidth/2 - 5) + 'px')
	  .attr('y', (-textHeight/2 - 5) + 'px');
	if (highlightList)
	{
	  if (_.contains(highlightList, this.model.id))
	  {
	    this.hullLabel.selectAll('.checkbox')
	      .style('visibility', 'visible')
	      .classed('selected', true);
	  }
	  else
	  {
	    this.hullLabel.selectAll('.checkbox')
	      .style('visibility', 'hidden')
	      .classed('selected', false);
	  }
	}
	this.hullLabel.style('visibility', 'visible');
      }
/*
      else
      {
	if (this.hullLabel)
	{
	  this.hullLabel.style('visibility', 'hidden');
	}
      }
*/
    },
/*
    contains: function (x, y, oldGroup) {
      return (this.model.id !== oldGroup &&
	      intersects([x, y, 150], this.vertices));
    },

    distance: function (x, y) {
      return minDistance([x, y], this.vertices);
    },
*/
    connectVertices: function () {
      var freshLinks = []; // Links that need to be added
      var staleLinks = []; // Links that need to be removed
      var nodeCount = 0;
      _.each(this.options.force.nodes, function (node) {
	if (node.model && node.model.host === this.model.id)
	{
	  nodeCount += 1;
	}
      }.bind(this));
      _.each(this.options.force.nodes, function (node) {
	var link = _.findWhere(this.links, { target: node });
	if (node.model && node.model.host === this.model.id &&
	    nodeCount > 1)
	{
	  // Ensure that it is connected to the center node
	  if (! link)
	  {
	    freshLinks.push({
	      source: this.center,
	      target: node,
	      distance: 50,
	      strength: 0.1
	    });
	  }
	}
	else
	{
	  // Ensure that it is not connected to the center node
	  if (link)
	  {
	    staleLinks.push(link);
	  }
	}
      }.bind(this));
      if (freshLinks.length > 0)
      {
	this.links = this.links.concat(freshLinks);
	this.options.force.add([], freshLinks);
      }
      if (staleLinks.length > 0)
      {
	this.links = _.difference(this.links, staleLinks);
	this.options.force.remove([], staleLinks);
      }
    },

    findVertices: function () {
      var xList = [];
      var yList = [];

      _.each(this.options.force.nodes, function (node) {
	if (node.model && node.model.host === this.model.id &&
	    node.x && node.y)
	{
	  xList.push(node.x);
	  yList.push(node.y);
	}
      }.bind(this));

      var result = "";
      if (xList.length > 0)
      {
	var minX = _.min(xList);
	var maxX = _.max(xList);
	var minY = _.min(yList);
	var maxY = _.max(yList);
	if (xList.length > 1 && yList.length > 1)
	{
//	  this.center.fx = (minX + maxX)/2;
//	  this.center.fy = (minY + maxY)/2;
//	  this.center.x = (minX + maxX)/2;
//	  this.center.y = (minY + maxY)/2;
	}

	if (this.lastMinX && Math.abs(minX - this.lastMinX) < 30)
	{
	  minX = this.lastMinX;
	}
	if (this.lastMaxX && Math.abs(maxX - this.lastMaxX) < 30)
	{
	  maxX = this.lastMaxX;
	}
	if (this.lastMinY && Math.abs(minY - this.lastMinY) < 30)
	{
	  minY = this.lastMinY;
	}
	if (this.lastMaxY && Math.abs(maxY - this.lastMaxY) < 30)
	{
	  maxY = this.lastMaxY;
	}
	this.lastMinX = minX;
	this.lastMaxX = maxX;
	this.lastMinY = minY;
	this.lastMaxY = maxY;
	
	this.label.x = minX - 40;
	this.label.y = minY - 55;
	var offset = 60;
	result = "M" + (minX-offset) + "," + (minY-offset) + "L" +
	  (minX-offset) + "," + (maxY+offset) + "L" +
	  (maxX+offset) + "," + (maxY+offset) + "L" +
	  (maxX+offset) + "," + (minY-offset) + "L" +
	  (minX-offset) + "," + (minY-offset) + "Z";
      }
      return result;
    },
/*
    groupPath: function(dragPoint) {
      var vertices = this.vertices;
      if (dragPoint)
      {
	vertices = _.clone(vertices);
	vertices.push(dragPoint);
      }
      return 'M' + d3v4.polygonHull(vertices).join('L') + 'Z';
    },
*/
    handleClick: function () {
      if (! d3v4.event.defaultPrevented)
      {
	var data = {
	  type: 'host',
	  item: this.model,
	  modkey: (d3v4.event.ctrlKey || d3v4.event.shiftKey),
	  event: _.clone(d3v4.event)
	};
	this.options.clickUpdate.trigger('click-event', data);
      }
    },

    hoverStart: function () {
      this.options.setHoverId(this.model.id);
    },

    hoverEnd: function () {
      this.options.setHoverId(null);
    },

    handleMouseDown: function ()
    {
      var that = this;
      this.justClickedOutside = true;
      _.delay(function () { that.justClickedOutside = false; }, 200);
    },

    handleClickOutside: function () {
      if (this.justClickedOutside)
      {
	this.options.clickUpdate.trigger('click-outside');
      }
    },

  });

  return HostView;
});

define('js/canvas/ValidList',['underscore'],
function (_)
{
  

  function ValidList(model, constraints)
  {
    this.model = model;
    this.constraints = constraints;
  }

  ValidList.prototype = {

    findNodes: function (node, key)
    {
      var result = {
	allowed: [],
	rejected: []
      };
      var bound = makeAllNodeCandidates(node, this.model);
      result.allowed = this.constraints.getValidList(bound, 'node', key,
						     result.rejected);
      return result;
    },

    findLinks: function (link, key)
    {
      var result = {
	allowed: [],
	rejected: []
      };
      var bound = makeAllLinkCandidates(link, this.model.sites);
      result.allowed = this.constraints.getValidList(bound, 'link', key,
						     result.rejected);
      return result;
    },

    findSites: function (site, key)
    {
      var result = {
	allowed: [],
	rejected: []
      };
      var bound = makeAllSiteCandidates(site, this.model.nodes, this.model.interfaces, this.model.lans, this.model.sites, false);
      result.allowed = this.constraints.getValidList(bound, 'node', key,
						     result.rejected);
      return result;
    },

    isValidNode: function (node, site)
    {
      var candidate = { node: makeSimpleNode(node, site) };
      return this.constraints.isValid(candidate);
    },

    isValidNodeNeighbor: function (node)
    {
      var candidates = makeNodeNeighborCandidates(node, this.model);
      return this.constraints.allValid(candidates);
    },

    isValidLan: function (lan)
    {
      var candidates = makeAllLinkCandidates(lan, this.model.sites);
      return this.constraints.allValid(candidates);
    },

    isValidLink: function (node1, site1, node2, site2)
    {
      var candidate = {
	node: makeSimpleNode(node1, site1),
	link: {},
	node2: makeSimpleNode(node2, site2)
      };
      return this.constraints.isValid(candidate);
    },

    getNodeCandidates: function (skipLinks)
    {
      var result = [];
      _.each(_.values(this.model.sites), function (site) {
	result = result.concat(makeAllSiteCandidates(site, this.model.nodes, this.model.interfaces, this.model.lans, this.model.sites, skipLinks));
      }.bind(this));
      return result;
    },

    getNodeCandidatesBySite: function (skipLinks)
    {
      var result = {};
      _.each(_.values(this.model.sites), function (site) {
	result[site.name] = makeAllSiteCandidates(site, this.model.nodes, this.model.interfaces, this.model.lans, this.model.sites, skipLinks);
      }.bind(this));
      return result;
    },
  };

  function makeNodeNeighborCandidates(node, topo)
  {
    var result = [];
    var nodeSite = topo.sites[node.group];
    _.each(node.interfaces, function (ifaceUnique) {
      var iface = topo.interfaces[ifaceUnique];
      var link = _.where(topo.lans,
			 { id: iface.linkId })[0]; 
      _.each(link.endpoints, function (endpoint) {
	if (endpoint.id !== node.id)
	{
	  var endpointSite = topo.sites[endpoint.group];
	  result.push({ node: makeSimpleNode(node, nodeSite),
			link: makeSimpleLink(link, topo.sites),
			node2: makeSimpleNode(endpoint, endpointSite) });
	}
      });
    });
    return result;
  }

  function makeAllNodeCandidates(node, topo)
  {
    var result = makeNodeNeighborCandidates(node, topo);
    var nodeSite = topo.sites[node.group];
    result.push({ node: makeSimpleNode(node, nodeSite) });
    return result;
  }

  function makeAllLinkCandidates(link, sites, defaultSite)
  {
    var result = [];
    for (var i = 0; i < link.endpoints.length; i += 1)
    {
      var site1 = sites[link.endpoints[i].group];
      for (var j = i + 1; j < link.endpoints.length; j += 1)
      {
	var site2 = sites[link.endpoints[j].group];
	if (defaultSite === site2)
	{
	  var temp = site1;
	  site1 = site2;
	  site2 = temp;
	}
	result.push({ node: makeSimpleNode(link.endpoints[i], site1),
		      link: makeSimpleLink(link),
		      node2: makeSimpleNode(link.endpoints[j], site2) });
      }
    }
    return result;
  }

  function makeAllSiteCandidates(site, nodes, ifaces, links, sites, skipLinks)
  {
    var used = {};
    var result = [];
    var linkMap = {};
    _.each(nodes, function (node) {
      if (node.group === site.id)
      {
	var newNode = { node: makeSimpleNode(node, site) };
	var newName = JSON.stringify(newNode, undefined, 0);
	if (used[newName] === undefined)
	{
	  used[newName] = 1;
	  result.push(newNode);
	  _.each(node.interfaces, function (ifaceId) {
	    var linkId = ifaces[ifaceId].linkId;
	    linkMap[linkId] = links[linkId];
	  });
	}
      }
    });
    if (! skipLinks)
    {
      _.each(_.values(linkMap), function(link) {
	var links = makeAllLinkCandidates(link, sites, site);
	result = result.concat(links);
      });
    }
    return result;
  }

  function makeSimpleNode(node, site)
  {
    var result =  {
      'images': node.image,
      'types': node.type,
      'hardware': node.hardware
    };
    if (node.custom.image)
    {
      result.images = undefined;
    }
    if (node.custom.type)
    {
      result.types = undefined;
    }
    if (node.custom.hardware)
    {
      result.hardware = undefined;
    }
    if (site.urn && ! site.custom.urn)
    {
      result.aggregates = site.urn;
    }
    return result;
  }

  function makeSimpleLink(link)
  {
    var result = {
      'linkTypes': link.linkType,
      'sharedvlan': link.sharedvlan
    };
    if (link.custom.linkType)
    {
      result.linkTypes = undefined;
    }
    if (link.custom.shardvlan)
    {
      result.sharedvlan = undefined;
    }
    return result;
  }

  function getIncidentLinkTypes(node, topoData)
  {
    var types = {};
    _.each(node.interfaces, function (ifaceUnique) {
      var iface = topoData.interfaces[ifaceUnique];
      var link = _.where(topoData.links, { id: iface.linkId })[0];
      if (link.linkType)
      {
	types[link.linkType] = 1;
      }
    });
    return _.keys(types);
  }

  return ValidList;
});


define('js/canvas/TopologyView',['underscore', 'backbone',
	'js/canvas/TopologyBackground', 'js/canvas/ForceGraph',
	'js/canvas/NodeView', 'js/canvas/LanView', 'js/canvas/SiteView',
	'js/canvas/HostView', 'js/canvas/ValidList'],
function (_, Backbone,
	 TopologyBackground, ForceGraph, NodeView, LanView, SiteView,
	 HostView, ValidList)
{
  

  var TopologyView = Backbone.View.extend({

    events: {
    },

    initialize: function () {
      this.force = new ForceGraph();
      this.nodes = [];
      this.lans = [];
      this.hosts = [];
      this.sites = [];
      this.siteFill = d3v4.schemeCategory10;
      this.validList = new ValidList(this.model, this.options.constraints);
      this.background = new TopologyBackground(this.$el,
					       this.force.nodes,
					       this.sites,
					       this.options.defaults,
					       this.options.clickUpdate,
					       this.options.context,
					       this.force,
					       this.validList);
      this.hoverId = null;

      this.listenTo(this.model, 'addNodes', this.addNodes);
      this.listenTo(this.model, 'addLans', this.addLans);
      this.listenTo(this.model, 'addSites', this.addSites);
      this.listenTo(this.model, 'addHosts', this.addHosts);
      this.listenTo(this.model, 'removeNodes', this.removeNodes);
      this.listenTo(this.model, 'removeLans', this.removeLans);
      this.listenTo(this.model, 'removeSites', this.removeSites);
      this.listenTo(this.model, 'firstGraph', this.firstGraph);
      this.listenTo(this.model, 'change', this.update);
      this.listenTo(this.force, 'tick', this.updateGraph);
      this.listenTo(this.background, 'create-site', this.createSite);
      this.listenTo(this.background, 'create-node', this.createNode);

      this.render();
    },

    render: function () {
      var that = this;
      this.background.render();
/*
      _.each(this.nodes, function (node) {
	node.render();
      });
      _.each(this.lans, function (lan) {
	lan.render();
      });
      _.each(this.sites, function (site) {
	site.render();
      });
*/
      this.updateGraph();
    },

    show: function () {
      this.$el.show();
    },

    hide: function () {
      this.$el.hide();
    },

    resize: function (width, height) {
      this.background.resize(width, height);
    },

    startForce: function () {
      this.force.start();
    },

    update: function () {
      this.updateNoScale(false, true);
    },

    updateNoScale: function (stopped, shouldDisplay, highlightList) {
      _.each(this.nodes, function (node) {
	node.update(shouldDisplay, highlightList);
      });
      _.each(this.lans, function (lan) {
	lan.update(shouldDisplay, highlightList);
      });
      _.each(this.sites, function (site) {
	site.update(shouldDisplay, highlightList);
      });
      _.each(this.hosts, function (host) {
	host.update(shouldDisplay, highlightList);
      });
    },

    updateGraph: function (stopped, shouldDisplay, highlightList) {
      this.updateNoScale(stopped, shouldDisplay, highlightList);
      this.background.centerScale(true);
    },

    firstGraph: function () {
      this.force.skipAhead();
      this.updateGraph(true, true);
    },

    addNodes: function (newNodes) {
      this.add(newNodes, NodeView, this.nodes);
      this.updateGraph(false, true);
    },

    addLans: function (newLans) {
      this.add(newLans, LanView, this.lans);
      this.updateGraph(false, true);
    },

    addSites: function (newSites) {
      this.add(newSites, SiteView, this.sites);
      this.updateGraph(false, true);
    },

    addHosts: function (newHosts) {
      this.add(newHosts, HostView, this.hosts);
      this.updateGraph(false, true);
    },

    removeNodes: function (oldIds) {
      this.nodes = this.remove(oldIds, this.nodes);
      this.updateGraph(false, true);
    },

    removeLans: function (oldIds) {
      this.lans = this.remove(oldIds, this.lans);
      this.updateGraph(false, true);
    },

    removeSites: function (oldIds) {
      this.sites = this.remove(oldIds, this.sites);
      this.updateGraph(false, true);
    },

    add: function (items, ItemType, list)
    {
      var that = this;
      _.each(items, function (item) {
	var fillColor = list.length % that.siteFill.length;
	var newView = new ItemType({
	  model: item,
	  force: that.force,
	  background: that.background,
	  fill: that.siteFill[fillColor],
	  clickUpdate: that.options.clickUpdate,
	  checkGroup: _.bind(that.checkGroup, that),
	  setHoverId: _.bind(that.setHoverId, that),
	  siteCount: _.bind(that.siteCount, that),
	  context: that.options.context
	});
	that.listenTo(newView, 'update', that.update);
	that.listenTo(newView, 'update-move', that.updateGraph);
	that.listenTo(newView, 'create-link', that.createLink);
	list.push(newView);
      });
    },

    remove: function (ids, list)
    {
      var result = list;
      _.each(ids, function (id) {
	var found;
	var foundIndex;
	_.each (list, function(candidate, index) {
	  if (id === candidate.options.model.id)
	  {
	    found = candidate;
	    foundIndex = index;
	  }
	}.bind(this));
	if (found)
	{
	  found.cleanup();
	  list.splice(foundIndex, 1);
	}
      });
      return result;
    },

    checkGroup: function (x, y, oldGroup)
    {
      var result = oldGroup;
      _.each(this.sites, function (site) {
	if (site.contains(x, y, oldGroup))
	{
	  result = site.model.id;
	}
      });
      return result;
    },

    setHoverId: function (newId)
    {
      this.hoverId = newId;
    },

    siteCount: function ()
    {
      return this.sites.length;
    },

    setHighlights: function (list, type)
    {
      this.updateGraph(true, true, list);
    },

    createNode: function (x, y, siteId, options)
    {
      this.model.addEmptyNode(x, y, siteId, options);
      this.force.start();
      this.force.stop();
    },

    createSite: function (x, y)
    {
      this.model.addEmptySite(x, y);
      this.force.start();
      this.force.stop();
    },

    createLink: function (sourceId)
    {
      this.model.attemptConnection(sourceId, this.hoverId);
      this.force.start();
      this.force.stop();
    }

  });


  return TopologyView;
});

define('js/canvas/TopologyModel',['underscore', 'backbone', 'js/canvas/ValidList'],
function (_, Backbone, ValidList)
{
  

  function TopologyModel(context, constraints, errorModal)
  {
    this.context = context;
    this.validList = new ValidList(this, constraints);
    this.errorModal = errorModal;
    this.nodes = {};
    this.lans = {};
    this.interfaces = {};
    this.sites = {};
    this.hosts = {};
    this.idList = {};
    this.extraChildren = [];
    // A hash of arrays. Key is IP address. Array is a list of interface IDs. There should only ever be 1 interface ID at most for each key. Otherwise the user has assigned a duplicate address.
    this.ipUsage = {};
    _.extend(this, Backbone.Events);
  }

  TopologyModel.prototype = {

    addGraph: function (newGraph, sourceUrn) {
      var firstGraph = (_.keys(this.nodes).length === 0);

      var bornSites = [];
      _.each(newGraph.sites, function (site) {
	this.insertSite(site, newGraph, bornSites);
      }.bind(this));

      var bornNodes = [];
      // Reaper nodes are old nodes which are not kept
      var reaperNodes = [];
      // Ghosts are new nodes which are not kept
      var ghostToOld = {};
      var bornToOld = {};
      _.each(newGraph.nodes, function (node) {
	this.insertNode(node, newGraph, reaperNodes, bornNodes,
			bornToOld, ghostToOld, sourceUrn);
      }.bind(this));

      var reaperLans = [];
      _.each(newGraph.links, function (lan) {
	this.insertLan(lan, newGraph, bornToOld, ghostToOld,
		       newGraph.interfaces, this.interfaces, reaperLans);
      }.bind(this));

      _.each(newGraph.interfaces, function (iface) {
	this.interfaces[iface.id] = iface;
      }.bind(this));

      var bornHosts = [];
      _.each(newGraph.hosts, function (host) {
	this.insertHost(host, newGraph, bornHosts);
      }.bind(this));

      this.extraChildren = newGraph.remainder;
      this.removeNodes(reaperNodes);
      this.removeLinks(reaperLans);
      _.each(_.keys(this.interfaces), function (ifaceId) {
	var iface = this.interfaces[ifaceId];
	var node;
	if (! iface.linkId) {
	  node = this.nodes[iface.nodeId];
	  node.interfaces = _.without(node.interfaces, ifaceId);
	  delete this.interfaces[ifaceId];
	} else if (! this.nodes[iface.nodeId]) {
	  if (iface.ip)
	  {
	    this.switchIp(ifaceId, null, iface.ip);
	  }
	  var link = this.lans[iface.linkId];
	  node = _.findWhere(link.endpoints, { id: iface.nodeId });
	  link.interfaces = _.without(link.interfaces, ifaceId);
	  link.endpoints = _.without(link.endpoints, node);
	  delete this.interfaces[ifaceId];
	}
      }.bind(this));
      if (this.context.mode !== 'viewer')
      {
	_.each(bornNodes, function (node) {
	  this.checkNode(node);
	}.bind(this));
	_.each(newGraph.links, function (link) {
	  this.checkLan(link);
	}.bind(this));
      }
      
      _.each(newGraph.sites, function (site) {
	if (this.sites[site.id])// && ! this.sites[site.id].name)
	{
	  this.sites[site.id].name = this.generateSiteName(this.sites[site.id]);
	}
      }.bind(this));
      var reaperSites = [];
      _.each(bornSites, function (site) {
	if (this.countNodes(site.id, bornNodes) === 0 &&
	    (_.keys(this.nodes).length > 0 || bornNodes.length > 0))
	{
	  reaperSites.push(site.id);
	}
      }.bind(this));
      _.each(reaperSites, function (siteId) {
	bornSites = _.without(bornSites, _.findWhere(bornSites, {id: siteId}));
      }.bind(this));
      this.removeSites(reaperSites);

      this.trigger('addSites', bornSites);
      this.trigger('addHosts', bornHosts);
      this.trigger('addNodes', bornNodes);
      this.trigger('addLans', newGraph.links);
      this.trigger('change');
      if (firstGraph)
      {
	this.trigger('firstGraph');
      }
    },

    nodesMatch: function (oldNode, newNode) {
      var oldSite = this.sites[oldNode.group];
      var newSite = this.sites[newNode.group];
      var siteMatch = (oldSite.urn && newSite.urn &&
		       oldSite.urn === newSite.urn &&
		       oldNode.name === newNode.name);
      var sliverMatch = (oldNode.sliverId && newNode.sliverId &&
			 oldNode.sliverId === newNode.sliverId);
      return siteMatch || sliverMatch;
    },

    insertNode: function (newNode, newGraph,
			  reaperList, bornList, bornToOld, ghostToOld, sourceUrn) {
      newNode.sourceUrn = sourceUrn;
      // New nodes replace old nodes
      var newSite = this.sites[newNode.group];
      var oldSite, oldNode;
      _.each(_.values(this.nodes), function (candidate) {
	var site = this.sites[candidate.group];
	if (this.nodesMatch(candidate, newNode))
	{
	  oldNode = candidate;
	  oldSite = site;
	}
      }.bind(this));

      // Destroy old node if there is an old node and if either the
      // new node has a sliver id or the old node lacks a sliver id
//      var destroyOld = (oldNode &&
//			(! newNode.sliverId ||
//			 oldNode.sliverId));
//      var destroyOld = true;
//      if (destroyOld && oldNode)
      var hasReplaced = false;
      if (oldNode)
      {
	newNode.x = oldNode.x;
	newNode.y = oldNode.y;
	reaperList.push(oldNode.id);
	if (oldNode.sliverId && ! newNode.sliverId)
	{
	  _.each(_.keys(oldNode), function (key) {
	    if (key !== 'id' && key !== 'interfaces')
	    {
	      newNode[key] = oldNode[key];
	    }
	  }.bind(this));
	}
      }
      var isManifest = (this.context.isManifest === true);
      var hasSliver = (newNode.sliverId !== null && newNode.sliverId !== undefined)
      // Add the new node if there is no old node, or if we destroyed it
//      if (destroyOld || ! oldNode)
//      {
      if (! isManifest || hasSliver)
      {
	this.nodes[newNode.id] = newNode;
	bornList.push(newNode);
	bornToOld[newNode.id] = oldNode;
      }
//      }
//      else
//      {
//	ghostToOld[newNode.id] = oldNode;
//      }
    },

    insertLan: function (newLan, newGraph, bornToOld, ghostToOld, newInterfaces, oldInterfaces, reaperLans) {
/*
      _.each(_.values(this.lans), function (candidate) {
	if (candidate.name === newLan.name)
	{
	  var allMatch = true;
	  _.each(newLan.interfaces, function (ifaceId) {
	    var newId = newInterfaces[ifaceId].nodeId;
	    var mapped = ghostToOld[newId];
	    if (mapped === undefined || mapped === null)
	    {
	      mapped = bornToOld[newId];
	    }
	    var found = false;
	    if (mapped !== undefined && mapped !== null)
	    {
	      _.each(candidate.interfaces, function (oldIfaceId) {
		var oldId = this.interfaces[oldIfaceId].nodeId;
		if (oldId === mapped.id)
		{
		  found = true;
		}
	      });
	    }
	    if (! found)
	    {
	      allMatch = false;
	    }
	  });
	  if (allMatch)
	  {
	    reaperLans.push(candidate.id);
	  }
	}
      });
*/
      _.extend(newLan, Backbone.Events);
      this.lans[newLan.id] = newLan;
    },

    insertSite: function (newSite, newGraph, bornList) {
      // New sites are ignored and not added
      var foundId = undefined;
      _.each(_.values(this.sites), function (candidate) {
	if (candidate.urn && newSite.urn &&
	    candidate.urn === newSite.urn)
	{
	  foundId = candidate.id;
	}
      }.bind(this));
      if (foundId === undefined)
      {
	bornList.push(newSite);
	this.sites[newSite.id] = newSite;
      }
      else
      {
	_.each(_.values(newGraph.nodes), function (node) {
	  if (node.group === newSite.id)
	  {
	    node.group = foundId;
	  }
	}.bind(this));
      }
    },

    insertHost: function (newHost, newGraph, bornList) {
      bornList.push(newHost);
      this.hosts[newHost.id] = newHost;
      _.each(newHost.nodes, function (client_id) {
	var id = null;
	_.each(_.values(this.nodes), function (candidate) {
	  if (candidate.name === client_id)
	  {
	    id = candidate.id;
	  }
	});
	if (id && this.nodes[id])
	{
	  this.nodes[id].host = newHost.id;
	}
      }.bind(this));
    },

    countNodes: function (siteId, nodeList) {
      var result = 0;
      _.each(nodeList, function (node) {
	if (node.group === siteId)
	{
	  result += 1;
	}
      }.bind(this));
      return result;
    },

    addEmptyNode: function (x, y, siteId, option) {
      var name = this.generateNodeName();
      var newNode = {
	custom: {},
	warnings: {},
	id: _.uniqueId(),
	interfaces: [],
	name: name,
	x: x,
	y: y,
	group: siteId,
	type: option.type,
	image: option.image,
	imageVersion: undefined,
	hardware: option.hardware,
	icon: option.icon,
	execute: [],
	install: [],
	routable: false,
	dataset_option: 'remote'
      };
      if (option.execute)
      {
	newNode.execute = option.execute;
      }
      if (option.install)
      {
	newNode.install = option.install;
      }
      if (option.routable)
      {
	newNode.routable = option.routable;
      }
      var image = _.findWhere(this.context.canvasOptions.images,
			      { id: option.image });
      if (image)
      {
	newNode.imageVersion = image.version;
      }
      if (! this.validList.isValidNode(newNode, this.sites[siteId]))
      {
	this.errorModal.update({ title: 'Incompatible Node Warning',
				 contents: 'This node is not listed as compatible with the site you have placed it in. Your topology may be impossible to allocate.' });
      }
      this.nodes[newNode.id] = newNode;
      this.checkNode(newNode);
      this.trigger('addNodes', [newNode]);
    },

    addEmptySite: function (x, y) {
      var newSite = {
	custom: {},
	id: _.uniqueId(),
	x: x,
	y: y,
	type: 'site'
      };
      newSite.name = this.generateSiteName(newSite);
      this.sites[newSite.id] = newSite;
      this.trigger('addSites', [newSite]);
    },

    attemptConnection: function (sourceId, targetId) {
      if (sourceId !== null && targetId !== null && targetId !== sourceId)
      {
	if (this.nodes[sourceId] &&
	    this.nodes[targetId])
	{
	  this.addEmptyLink(sourceId, targetId);
	}
	else if (this.nodes[sourceId] && this.lans[targetId])
	{
	  this.addNodeToLan(sourceId, targetId);
	}
	else if (this.nodes[targetId] && this.lans[sourceId])
	{
	  this.addNodeToLan(targetId, sourceId);
	}
      }
    },

    addEmptyLink: function (sourceId, targetId) {
      var newLan = {
	custom: {},
	warnings: {},
	id: _.uniqueId(),
	name: this.generateLinkName(),
	interfaces: [],
	nodeIndices: [],
	endpoints: [],
	endpointNames: [],
	transitSites: {},
	interswitch: true
      };
      _.extend(newLan, Backbone.Events);
      this.lans[newLan.id] = newLan;
      this.addNodeToLan(sourceId, newLan.id);
      this.addNodeToLan(targetId, newLan.id);
      var source = this.nodes[sourceId];
      var target = this.nodes[targetId];
      if (! this.validList.isValidLink(source, this.sites[source.group],
				       target, this.sites[target.group]))
      {
	this.errorModal.update({ title: 'Incompatible Link Warning',
				 contents: 'This link is not listed as compatible with the nodes you have connected. Your topology may be impossible to allocate.' });
      }
      this.checkLan(newLan);
      this.trigger('addLans', [newLan]);
    },

    addNodeToLan: function (nodeId, lanId) {
      var node = this.nodes[nodeId];
      var lan = this.lans[lanId];
      if (! _.contains(lan.endpoints, node) &&
	  ! this.wouldThreeSite(node, lan))
      {
	lan.nodeIndices.push(nodeId);
	lan.endpoints.push(node);
	lan.endpointNames.push(node.name);
	var iface = {
	  id: _.uniqueId(),
	  name: this.generateInterfaceName(),
	  nodeName: node.name,
	  node: node,
	  linkId: lanId,
	  nodeId: nodeId,
	  mac: undefined,
	  bandwidth: undefined,
	};
	this.interfaces[iface.id] = iface;
	node.interfaces.push(iface.id);
	lan.interfaces.push(iface.id);
	this.checkLan(lan);
	this.checkNode(node);
	lan.trigger('addEndpoint', node);
	this.trigger('change');
      }
    },

    wouldThreeSite: function (node, lan) {
      var sites = {};
      sites[node.group] = true;
      _.each(lan.endpoints, function (endpoint) {
	sites[endpoint.group] = true;
      });
      var result = false;
      if (_.keys(sites).length > 2)
      {
	result = true;
      }
      return result;
    },

    getItems: function (type) {
      return _.values(this.getType(type));
    },

    getType: function (type) {
      if (type === 'node')
      {
	return this.nodes;
      }
      else if (type === 'link')
      {
	return this.lans;
      }
      else if (type === 'site')
      {
	return this.sites;
      }
      else
      {
	return {};
      }
    },

    hasId: function (id) {
      return (this.nodes[id] || this.lans[id] || this.sites[id]);
    },

    findMixed: function (list) {
      var result = [];
      _.each(list, function (id) {
	if (this.nodes[id])
	{
	  result.push(this.nodes[id]);
	}
	else if (this.lans[id])
	{
	  result.push(this.lans[id]);
	}
	else if (this.sites[id])
	{
	  result.push(this.sites[id]);
	}
      }.bind(this));
      return result;
    },

    uniqueName: function(inPrefix)
    {
      var prefix = inPrefix;
      if (! prefix)
      {
	prefix = '';
      }
      if (this.idList[prefix] === undefined)
      {
	this.idList[prefix] = 0;
      }
      var id = prefix + this.idList[prefix];
      this.idList[prefix] += 1;
      return id;
    },

    generateNodeName: function ()
    {
      var id = this.uniqueName('node-');
      while (_.where(this.nodes, { name: id }).length > 0)
      {
	id = this.uniqueName('node-');
      }
      return id;
    },

    generateLinkName: function ()
    {
      var id = this.uniqueName('link-');
      while (_.where(this.lans, { name: id }).length > 0)
      {
	id = this.uniqueName('link-');
      }
      return id;
    },

    generateInterfaceName: function ()
    {
      var id = this.uniqueName('interface-');
      while (_.where(this.interfaces, { name: id }).length > 0)
      {
	id = this.uniqueName('interface-');
      }
      return id;
    },

    generateSiteName: function (site)
    {
      var result;
      if (site.urn && site.type === 'site')
      {
	if (this.context.canvasOptions.aggregates)
	{
	  var found = _.findWhere(this.context.canvasOptions.aggregates,
				  { id: site.urn });
	  if (found)
	  {
	    result = found.name;
	  }
	}
	if (result === undefined)
	{
	  var split = site.urn.split('+');
	  if (split.length === 4)
	  {
	    result = site.urn.split('+')[1];
	  }
	  else
	  {
	    result = site.urn;
	  }
	}
      }
      if (result === undefined && site.name)
      {
	result = site.name;
      }
      if (result === undefined)
      {
	//result = this.uniqueName('Site '); // Rob likes small consecutive integers
	var attempt = 1; // Start at 1 and not zero. *sob*
	result = 'Site ' + attempt;
	while (_.where(this.sites, { name: result }).length > 0)
	{
	  ++attempt;
	  //result = this.uniqueName('Site ');
	  result = 'Site ' + attempt;
	}
      }
      return result;
    },

    changeAttribute: function (itemsToChange, key, value, type)
    {
      var that = this;
      var items;
      if (type === 'node')
      {
	items = this.nodes;
      }
      else if (type === 'link')
      {
	items = this.lans;
      }
      else
      {
	items = this.sites;
      }
      _.each(itemsToChange, _.bind(function(d) {
	var item = _.where(items, {id: d})[0];
	item[key] = value;
	if (key === 'name' && type === 'node')
	{
	  _.each(item.interfaces, function (iface) {
	    iface.nodeName = item.name;
	  });
	}
      }, this));
      if (key === 'urn' && type === 'site')
      {
	_.each(itemsToChange, _.bind(function(d) {
	  var item = _.where(items, {id: d})[0];
	  item.name = undefined;
	  item.name = this.generateSiteName(item);
	}, this));
	this.trigger('changeAggregate');
      }
      _.each(itemsToChange, _.bind(function(d) {
	var item = _.where(items, {id: d})[0];
	if (type === 'node')
	{
	  this.checkNode(item);
	  _.each(item.interfaces, function (ifaceId) {
	    var iface = this.interfaces[ifaceId];
	    var linkId = iface.linkId;
	    var link = this.lans[linkId];
	    this.checkLan(link);
	  }.bind(this));
	}
	else if (type === 'link')
	{
	  this.checkLan(item);
	  _.each(item.endpoints, function (endpoint) {
	    this.checkNode(endpoint);
	  }.bind(this));
	}
      }, this));
      this.trigger('change');
    },

    removeItems: function (idList, type)
    {
      if (type === 'node')
      {
	this.removeNodes(idList);
      }
      else if (type === 'link')
      {
	this.removeLinks(idList);
      }
      else if (type === 'site')
      {
	this.removeSites(idList);
      }
    },

    removeNodes: function (idList)
    {
      var linkReaper = [];
      _.each(idList, function (id) {
	var node = this.nodes[id];
	while (node.interfaces.length > 0)
	{
	  var iface = this.interfaces[node.interfaces[0]];
	  var link = this.lans[iface.linkId];
	  this.removeNodeFromLan(node, link);
	  if (link.interfaces.length < 2)
	  {
	    linkReaper.push(link.id);
	  }
	}
	delete this.nodes[id];
      }.bind(this));
      this.trigger('removeNodes', idList);
      this.trigger('change');
      this.removeLinks(_.unique(linkReaper));
    },

    removeLinks: function (idList)
    {
      _.each(idList, function (id) {
	var link = this.lans[id];
	while (link.interfaces.length > 0)
	{
	  var iface = this.interfaces[link.interfaces[0]];
	  var node = this.nodes[iface.nodeId];
	  this.removeNodeFromLan(node, link);
	}
	delete this.lans[id];
      }.bind(this));
      this.trigger('removeLans', idList);
      this.trigger('change');
    },

    removeNodeFromLan: function (node, link)
    {
      var iface;
      _.each(node.interfaces, function (id) {
	var candidate = this.interfaces[id];
	if (candidate.linkId === link.id)
	{
	  iface = candidate;
	}
      }.bind(this));
      if (iface)
      {
	this.switchIp(iface.id, iface.ip, null);
	delete this.interfaces[iface.id];
	node.interfaces = _.without(node.interfaces, iface.id);
	link.interfaces = _.without(link.interfaces, iface.id);
	link.endpoints = _.without(link.endpoints, node);
	link.trigger('removeEndpoint', node);
	this.trigger('change');
      }
    },

    removeSites: function (idList)
    {
      var reaperList = [];
      _.each(idList, function (id) {
	var membership = _.where(this.nodes, { group: id});
	if (membership.length === 0)
	{
	  reaperList.push(id);
	  delete this.sites[id];
	}
      }.bind(this));
      if (reaperList.length > 0)
      {
	this.trigger('removeSites', reaperList);
	this.trigger('change');
      }
    },

    // Do bookkeeping when an IP address changes.
    switchIp: function (ifaceId, oldValue, newValue)
    {
      if (oldValue)
      {
	this.ipUsage[oldValue] = _.without(this.ipUsage[oldValue], ifaceId);
	if (this.ipUsage[oldValue].length === 0)
	{
	  delete this.ipUsage[oldValue];
	}
      }
      if (newValue)
      {
	if (! this.ipUsage[newValue])
	{
	  this.ipUsage[newValue] = [];
	}
	this.ipUsage[newValue] = _.union(this.ipUsage[newValue], [ifaceId]);
      }
      var iface = this.interfaces[ifaceId];
      this.checkLan(this.lans[iface.linkId]);
      this.trigger('change');
    },

    checkNode: function (node)
    {
      node.warnings.constraintNode = (! this.validList.isValidNode(node, this.sites[node.group]));
      node.warnings.adjacentLink = (! this.validList.isValidNodeNeighbor(node));
    },

    checkLan: function (lan)
    {
      lan.warnings.openflow = (lan.openflow === '');
      lan.warnings.adjacentNode = (! this.validList.isValidLan(lan));

      var linkTypes = this.validList.findLinks(lan, 'linkTypes');
      var allowed = _.without(linkTypes.allowed, 'gre-tunnel', 'egre-tunnel');
      lan.warnings.defaultLinkType = (allowed.length === 0);

      var foundDuplicate = false;
      _.each(lan.interfaces, function (ifaceId) {
	var iface = this.interfaces[ifaceId];
	if (iface.ip && this.ipUsage[iface.ip] &&
	    this.ipUsage[iface.ip].length > 1)
	{
	  foundDuplicate = true;
	}
      }.bind(this));
      lan.warnings.duplicateAddress = foundDuplicate;
    },

  };

  return TopologyModel;
});

define('lib/View',['underscore', 'backbone'],
function (_, Backbone)
{
  

  var View = function(options) {
    options = options || {};
    bindUnderscore(this);
    this.options = options;
    this.cid = _.uniqueId('view');
    this.$el = $([]);
    this.children = [];
    this.rendered = false;
    this.shown = true;
    this.initialize.apply(this, arguments);
  };

  _.extend(View.prototype, Backbone.Events, {

    $: function(selector)
    {
      return this.$el.find(selector);
    },

    // $on(eventName, method)
    // $on(eventName, selector, method)
    // $on(eventName, jQuery, method)
    $on: function(eventName, selector, method)
    {
      var base = this.$el;
      if (! method)
      {
	method = selector;
	selector = undefined;
      }
      if (selector !== undefined && ! _.isString(selector))
      {
	base = selector;
	selector = undefined;
      }

      var boundMethod = _.bind(method, this);
      var fullName = '';
      var names = eventName.split(' ');
      this._each(names, function (name) {
	if (name !== '')
	{
	  if (fullName !== '')
	  {
	    fullName += ' ';
	  }
	  fullName += name + '.selectBind' + this.cid;
	}
      });
      if (selector === undefined)
      {
	base.on(fullName, boundMethod);
      }
      else
      {
	base.on(fullName, selector, boundMethod);
      }
    },

    $off: function()
    {
      this.$el.off('.selectBind' + this.cid);
    },

    superCleanup: function ()
    {
      this.stopListening();
      this.$off();
      this._each(this.children, function (child) {
	child.$el.remove();
	child.cleanup();
      });
    },

    superRender: function (el)
    {
      this.$el = el || this.$el;
      this.rendered = true;
      this.update(this.options.state);
      return this.$el;
    },

    superShow: function ()
    {
      if (! this.shown)
      {
	this.$el.show();
	this.shown = true;
      }
    },

    superHide: function ()
    {
      if (this.shown)
      {
	this.$el.hide();
	this.shown = false;
      }
    },

    initialize: function (options) {},

    cleanup: function () { this.superCleanup() },

    render: function (el) { this.superRender(el) },

    update: function (state) {},

    show: function () { this.superShow() },

    hide: function () { this.superHide() }

  });

  View.extend = function(protoProps, staticProps)
  {
    var parent = this;
    var child;

    if (protoProps && _.has(protoProps, 'constructor'))
    {
      child = protoProps.constructor;
    }
    else
    {
      child = function(){ return parent.apply(this, arguments); };
    }

   _.extend(child, parent, staticProps);

    var Surrogate = function(){ this.constructor = child; };
    Surrogate.prototype = parent.prototype;
    child.prototype = new Surrogate;

    if (protoProps) _.extend(child.prototype, protoProps);

    child.__super__ = parent.prototype;

    return child;
  };

  var underscoreMethods = ['each', 'forEach',
			   'map', 'collect',
			   'reduce', 'inject', 'foldl',
			   'reduceRight', 'foldr',
			   'find', 'detect',
			   'filter', 'select',
			   'reject', 'every', 'all',
			   'some', 'any',
			   'max', 'min',
			   'sortBy', 'groupBy', 'indexBy', 'countBy'];

  function bindUnderscore(that)
  {
    that._ = {};
    _.each(underscoreMethods, function (method) {
      that['_' + method] = function()
      {
	var args = _.toArray(arguments);
	if (args.length >= 2 && _.isFunction(args[1]) &&
	    args[1] !== undefined && args[1] !== null)
	{
	  args[1] = _.bind(args[1], that);
	}
	return _[method].apply(_, args);
      };
    });
  }

  return View;
});

define('js/info/TextInputView',['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  

  var TextInputView = View.extend({

    // options {
    //  data: opaque value returned in change event
    //  state: Initial state
    // }
    initialize: function (options)
    {
      this.value = null;
      this.disabled = null;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<input type="text">');
	this.$on('change keyup paste', this.$el, this.change);
      }
      return this.superRender();
    },

    // state {
    //  value: New value
    //  disabled: True if this input should be disabled
    //  shown: True if this input should be displayed
    // }
    update: function (state)
    {
      if (state)
      {
	this.setValue(state.value);
	if (state.disabled === true)
	{
	  this.disable();
	}
	else if (state.disabled === false)
	{
	  this.enable();
	}
	if (state.shown === true)
	{
	  this.show();
	}
	else if (state.shown === false)
	{
	  this.hide();
	}
      }
    },

    disable: function ()
    {
      if (this.disabled !== true)
      {
	this.$el.attr('readonly', 'readonly');
      }
      this.disabled = true;
    },

    enable: function ()
    {
      if (this.disabled !== false)
      {
	this.$el.removeAttr('readonly');
      }
      this.disabled = false;
    },

    setValue: function (inText)
    {
      var text = inText || '';
      if (text !== this.value)
      {
	this.$el.val(text);
      }
      this.value = text;
    },

    change: function (event)
    {
      this.value = this.$el.val();
      this.trigger('change', { value: this.value,
			       data: this.options.data });
    },

  });

  return TextInputView;
});

define('js/info/TextField',['underscore', 'backbone', 'lib/View', 'js/info/TextInputView'],
function (_, Backbone, View, TextInputView)
{
  

  var TextField = View.extend({

    // options {
    //   title: Title of this field
    //   state: Initial state
    //   data: Opaque data object passed back on change events
    // }
    initialize: function (options)
    {
      this.input = new TextInputView();
      this.children.push(this.input);
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	var title = $('<h4>')
	  .html(_.escape(this.options.title))
	  .appendTo(this.$el);
	this.input.render()
	  .addClass('form-control')
	  .appendTo(this.$el);
	this.listenTo(this.input, 'change', this.change);
	this.update(this.options.state);
      }
      return this.superRender();
    },

    // state {
    //   values: List of values to place in this box
    //   isViewer: True if this is read-only viewer mode
    //   type: 'node' or 'site' or 'link' or 'none'
    //   disabled: True if this box should be disabled
    // }
    update: function (state)
    {
      if (state)
      {
	var unique = _.unique(state.values);
	var values = unique.join(', ');
	var disabled = (state.isViewer || unique.length > 1 ||
			state.disabled);
	this.input.update({
	  value: values,
	  disabled: disabled
	});
      }
    },

    change: function (state)
    {
      this.trigger('change', { value: state.value,
			       data: this.options.data });
    }

  });

  return TextField;
});

define('js/info/ReadOnlyField',['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  

  var ReadOnlyField = View.extend({

    initialize: function (options)
    {
      this.value = null;
      this.title = options.title;
      this.help = options.help;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	if (this.help)
	{
	  var help = $('<span><strong>?</strong></span>')
	    .addClass('badge info-label pull-right')
	    .data('toggle', 'tooltip')
	    .attr('title', this.help)
	    .data('placement', 'bottom');
	  help.appendTo(this.$el);
	  help.tooltip();
	}
	var title = $('<strong/>')
	  .html(_.escape(this.title))
	  .appendTo(this.$el);
	this.$el.append(' <span/>');
      }
      return this.superRender();
    },

    // state {
    //   values: List of values to place in this box
    //   isViewer: True if this is read-only viewer mode
    //   type: 'node' or 'site' or 'link' or 'none'
    //   disabled: True if this box should be disabled
    // }
    update: function (state)
    {
      if (state)
      {
	var valueList = [];
	this._each(state.values, function (value) {
	  if (value !== null && value !== undefined && value !== '')
	  {
	    valueList.push(_.escape(value));
	  }
	});
	if (valueList.length > 0)
	{
	  this.$('strong').show();
	  this.$('span').html(valueList.join('<br>')).show();
	}
	else
	{
	  this.$('strong').hide();
	  this.$('span').hide();
	}
      }
    }

  });

  return ReadOnlyField;
});

define('js/info/ConstrainedField',['underscore', 'backbone', 'lib/View', 'js/canvas/ValidList'],
function (_, Backbone, View, ValidList)
{
  

  var ConstrainedField = View.extend({

    // options {
    //   title: Title of this field
    //   choices: List of possible options
    //   contraints: Constraint system on choices
    //   data: Opaque data object passed back on change events
    // }
    initialize: function (options)
    {
      this.title = options.title;
      this.choices = options.choices;
      this.constraints = options.constraints;
      this.optionKey = options.optionKey;
      this.data = options.data;
      this.help = options.help;
      this.placeholder = options.placeholder || '';
      this.dropdownField = null;
      this.freeformField = null;
      this.versionField = null;

      this.constrainedChoices = null;
      this.dropdownValue = null;
      this.value = null;
      this.isVersion = null;
      this.version = null;
      this.isFreeform = null;
      this.disabled = null;
      this.freeformDisabled = null;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	if (this.help)
	{
	  var help = $('<span><strong>?</strong></span>')
	    .addClass('badge info-label pull-right')
	    .data('toggle', 'tooltip')
	    .attr('title', this.help)
	    .data('placement', 'bottom');
	  help.appendTo(this.$el);
	  help.tooltip();
	}
	if (this.title)
	{
	  $('<h4>')
	    .html(_.escape(this.title))
	    .appendTo(this.$el);
	}
	this.dropdownField = $('<input type="hidden">')
	  .addClass('form-control dropdown')
	  .appendTo(this.$el);
	this.freeformField = $('<textarea placeholder="' + this.placeholder + '" spellcheck="false"></textarea>')
	  .addClass('form-control custom')
	  .hide()
	  .appendTo(this.$el);
	this.versionField = $('<input type="text" placeholder="version">')
	  .addClass('form-control custom')
	  .hide()
	  .appendTo(this.$el);
	this.$on('change', this.dropdownField, this.change);
	this.$on('change keyup paste', this.freeformField,
		 this.changeFreeform);
	this.$on('change keyup paste', this.versionField,
		 this.changeVersion);
      }
      return this.superRender();
    },

    // state {
    //   values: List of values to place in this box
    //   isViewer: True if this is read-only viewer mode
    //   type: 'node' or 'site' or 'link' or 'none'
    //   disabled: True if this box should be disabled
    //   freeform: True if we should be using the freeform box
    //   model: Global view of the topology
    //   selection: Current items selected (node, site, or link)
    // }
    update: function (state)
    {
      if (state)
      {
	var unique = _.unique(state.values);
	var values = unique.join(', ');

	var freeform = (state.freeform ||
			(values !== '' &&
			 (unique.length > 1 ||
			  ! this.hasChoice(unique))));
	var newChoices = this.calculateChoicesIfNew(values, freeform, state, false);
	if (newChoices !== null)
	{
	  this.updateChoices(newChoices.options);
	  this.dropdownField.select2('val', newChoices.choice);
	}

	if (values !== this.value)
	{
	  this.freeformField.val(values);
	}

	if (freeform !== this.isFreeform)
	{
	  if (freeform)
	  {
	    this.freeformField.show();
	  }
	  else
	  {
	    this.freeformField.hide();
	  }
	}

	var version = '';
	if (state.selection.length === 1 && state.selection[0].imageVersion)
	{
	  version = state.selection[0].imageVersion;
	}
	if (version !== this.version)
	{
	  this.versionField.val(version);
	}

	var isVersion = (freeform && this.optionKey === 'images' &&
			 state.selection.length === 1);
	if (isVersion !== this.isVersion)
	{
	  if (isVersion)
	  {
	    this.versionField.show();
	  }
	  else
	  {
	    this.versionField.hide();
	  }
	}

	var disabled = (state.isViewer || state.disabled || unique.length > 1)
	if (disabled !== this.disabled)
	{
	  if (disabled)
	  {
	    this.dropdownField.select2('enable', false);
	  }
	  else
	  {
	    this.dropdownField.select2('enable', true);
	  }
	}

	var freeformDisabled = freeform && (disabled || unique.length > 1);
	if (freeformDisabled !== this.freeformDisabled)
	{
	  if (freeformDisabled)
	  {
	    this.freeformField.attr('readonly', 'readonly');
	    this.versionField.attr('readonly', 'readonly');
	  }
	  else
	  {
	    this.freeformField.removeAttr('readonly');
	    this.versionField.removeAttr('readonly');
	  }
	}
	this.value = values; 
	this.isFreeform = freeform;
	this.version = version;
	this.isVersion = isVersion;
	this.disabled = disabled;
	this.freeformDisabled = freeformDisabled;
      }
    },

    calculateChoicesIfNew: function (values, freeform, state, alwaysNew)
    {
      var result = null;
      var dropdownValue = values;
      if (dropdownValue === '')
      {
	dropdownValue = '(any)';
      }
      if (freeform)
      {
	dropdownValue = 'Other...';
      }

      var validList = new ValidList(state.model, this.constraints);
      var constrainedChoices = this.runConstraints(state.selection,
						   state.type,
						   validList);
      if (alwaysNew || dropdownValue !== this.dropdownValue ||
	  this.isDifferentChoices(constrainedChoices))
      {
	result = {
	  choice: dropdownValue,
	  options: constrainedChoices
	};
      }
      this.constrainedChoices = constrainedChoices;
      this.dropdownValue = dropdownValue;
      return result;
    },

    updateChoices: function (constrainedChoices)
    {
      var allowed = constrainedChoices.allowed;
      var data = { results: [] };
      data.results.push({ id: '(any)', text: '(any)' });
      this._each(allowed, function(item) {
	data.results.push({
	  id: item,
	  text: _.findWhere(this.choices, { id: item }).name
	});
      });
      data.results.sort(compareText);
      data.results.push({ id: 'Other...', text: 'Other...' });
      if (constrainedChoices.rejected.length > 0)
      {
	var rejected = [];
	this._each(constrainedChoices.rejected, function (item) {
	  rejected.push({
	    id: item, 
	    text: _.findWhere(this.choices, { id: item }).name
	  });
	});
	rejected.sort(compareText);
	data.results.push({
	  text: 'Unavailable',
	  children: rejected,
	  disabled: true
	});
      }

      function format(item) {
	if (item.url)
	{
	  return '<img class="dropdown-icon" width="20" height="20" src="' +
	    item.url + '"> ' + _.escape(item.text);
	}
	else
	{
	  return _.escape(item.text);
	}
      }

      var that = this;
      this.dropdownField.select2('destroy');
      this.dropdownField.select2({
	initSelection: function (element, callback) {
	  var id = element.val();
	  var name = id;
	  var choice = _.findWhere(that.choices, { id: id });
	  if (choice)
	  {
	    name = choice.name;
	  }
	  var newItem = { id: id, text: name };
	  if (that.optionKey === 'icons' && id !== '(any)' && id !== 'Other...')
	  {
	    newItem.url = id;
	  }
	  callback(newItem);
	},
//	sortResults: function(results, container, query) {
//	  //if (query.term) {
//	    return results.sort(compareText);
//	  //}
//	},
	query: function (query) {
	  query.callback(data);
	},
	formatResult: format,
	formatSelection: format,
	escapeMarkup: function (m) { return m; }
      });
    },

    change: function (event)
    {
      var value;
      var isFreeform = false;
      if (event.val === '(any)')
      {
	value = undefined;
	isFreeform = false;
      }
      else if (event.val === 'Other...')
      {
	value = this.value;
	isFreeform = true;
      }
      else
      {
	value = event.val;
	isFreeform = false;
      }
      var result = {
	value: value,
	freeform: isFreeform,
	data: this.data
      };
      if (this.optionKey === 'images' && event.val !== 'Other...')
      {
	result.hasVersion = true;
	result.version = undefined;
	var choice = _.findWhere(this.choices, { id: value });
	if (choice)
	{
	  result.version = choice.version;
	}
      }
      this.trigger('change', result);
    },

    changeFreeform: function (event)
    {
      this.value = this.freeformField.val();
      var result = {
	value: this.value,
	freeform: this.isFreeform,
	data: this.data
      };
      if (this.optionKey === 'images')
      {
	this.version = this.versionField.val();
	result.hasVersion = true;
	result.version = this.version;
      }
      this.trigger('change', result);
    },

    changeVersion: function (event)
    {
      this.version = this.versionField.val();
      var result = {
	value: this.value,
	freeform: this.isFreeform,
	data: this.data
      };
      if (this.optionKey === 'images')
      {
	this.version = this.versionField.val();
	result.hasVersion = true;
	result.version = this.version;
      }
      this.trigger('change', result);
    },

    hasChoice: function (list)
    {
      var result = false;
      this._each(list, function(item) {
	result = result ||
	  _.findWhere(this.choices, { id: item }) !== undefined;
      });
      return result;
    },

    runConstraints: function (selection, selectionType, validList)
    {
      var result = {
	allowed: [],
	rejected: []
      };
      if (selection.length > 0)
      {
	if (this.optionKey === 'icons')
	{
	  result = {
	    allowed: _.pluck(this.choices, 'id'),
	    rejected: []
	  };
	}
	else
	{
	  if (selectionType === 'node')
	  {
	    result = validList.findNodes(selection[0],
					 this.optionKey);
	  }
	  else if (selectionType === 'link')
	  {
	    result = validList.findLinks(selection[0],
					 this.optionKey);
	  }
	  else if (selectionType === 'site')
	  {
	    result = validList.findSites(selection[0],
					 this.optionKey);
	  }
	}
      }
      return result;
    },

    isDifferentChoices: function (newChoices)
    {
      var isDifferent = false;
      var old = this.constrainedChoices;
      if (old === null)
      {
	isDifferent = true;
      }
      else
      {
	this._each(['allowed', 'rejected'], function (status) {
	  if (newChoices[status].length !== old[status].length)
	  {
	    isDifferent = true;
	  }
	  else
	  {
	    for (var i = 0; i < newChoices[status].length; i += 1)
	    {
	      if (newChoices[status][i] !== old[status][i])
	      {
		isDifferent = true;
		break;
	      }
	    }
	  }
	});
      }
      return isDifferent;
    }

  });
  
  function compareText(a, b)
  {
    if (a.text < b.text)
    {
      return -1;
    }
    else if (a.text == b.text)
    {
      return 0;
    }
    else
    {
      return 1;
    }
  }


  return ConstrainedField;
});

define('js/info/CheckboxView',['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  

  var CheckboxView = View.extend({

    // options {
    //  state: Initial state
    //  data: opaque value returned in change event
    // }
    initialize: function (options)
    {
      this.checked = null;
      this.label = null;
      this.disabled = null;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div class="checkbox-info" style="margin-right: 20px"/>');
	this.labelEl = $('<label/>').appendTo(this.$el);
	this.checkbox = $('<input type="checkbox">')
	  .appendTo(this.labelEl);
	this.$on('change', this.checkbox, this.change);
      }
      return this.superRender();
    },

    // state {
    //  value: True if box is checked
    //  label: New label
    //  disabled: True if box is disabled
    //  shown: True if this component should be shown
    // }
    update: function (state)
    {
      if (state)
      {
	if (state.value === true)
	{
	  this.check();
	}
	else if (state.value === false)
	{
	  this.clear();
	}

	if (state.label !== undefined && state.label !== null)
	{
	  this.setLabel(state.label);
	}

	if (state.disabled === true)
	{
	  this.disable();
	}
	else if (state.disabled === false)
	{
	  this.enable();
	}

	if (state.shown === true)
	{
	  this.show();
	}
	else if (state.shown === false)
	{
	  this.hide();
	}
      }
    },

    check: function ()
    {
      if (this.checked !== true)
      {
	this.checkbox.prop('checked', true);
      }
      this.checked = true;
    },

    clear: function ()
    {
      if (this.checked !== false)
      {
	this.checkbox.prop('checked', false);
      }
      this.checked = false;
    },

    disable: function ()
    {
      if (this.disabled !== true)
      {
	this.checkbox.attr('disabled', true);
      }
      this.disabled = true;
    },

    enable: function ()
    {
      if (this.disabled !== false)
      {
	this.checkbox.attr('disabled', false);
      }
      this.disabled = false;
    },

    setLabel: function (label)
    {
      var text = label || '';
      if (text !== this.label)
      {
	var current = this.labelEl.contents().last();
	if (! current.is('input:checkbox'))
	{
	  current.remove();
	}
	this.labelEl.append(_.escape(text));
      }
      this.label = text;
    },

    change: function (event)
    {
      this.trigger('change', {
	value: this.$('input').prop('checked'),
	data: this.options.data
      });
    }

  });

  return CheckboxView;
});


define('js/info/ToggleField',['underscore', 'backbone', 'lib/View', 'js/info/CheckboxView'],
function (_, Backbone, View, CheckboxView)
{
  

  var ToggleField = View.extend({

    // options {
    //   title: Title of this field
    //   state: Initial state
    //   data: Opaque data object passed back on change
    // }
    initialize: function (options)
    {
      this.checkbox = new CheckboxView();
      this.children.push(this.checkbox);
      this.help = options.help;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	if (this.help)
	{
	  var help = $('<span><strong>?</strong></span>')
	    .addClass('badge info-label pull-right')
	    .data('toggle', 'tooltip')
	    .attr('title', this.help)
	    .data('placement', 'bottom');
	  help.appendTo(this.$el);
	  help.tooltip();
	}
	this.checkbox.render()
	  .appendTo(this.$el);
	this.listenTo(this.checkbox, 'change', this.change);
      }
      return this.superRender();
    },

    // state {
    //   values: List of values to place here
    //   isViewer: True if this is read-only viewer mode
    //   disabled: True if box should be disabled
    // }
    update: function (state)
    {
      if (state)
      {
	var unique = _.unique(state.values);
	var shown = (unique.length === 1);
	var checked = false;
	if (shown && unique[0])
	{
	  checked = true;
	}
	this.checkbox.update({
	  value: checked,
	  label: this.options.title,
	  disabled: (state.isViewer || state.disabled),
	  shown: shown,
	});
      }
    },

    change: function (state)
    {
      this.trigger('change', {
	value: state.value,
	data: this.options.data
      });
    }

  });

  return ToggleField;
});

define('js/canvas/InstallDisplay',['underscore', 'backbone'],
function (_, Backbone)
{
  

  var template = '<div class="panel-body"><label>URL:</label><input type="text" class="form-control" id="url" value="" placeholder="ex: http://example.com/mystuff.tar.gz"><label>Install Path:</label><input type="text" class="form-control" id="path" value="" placeholder="ex: /local"><button id="remove" class="btn btn-danger">Remove</button></div>';

  var InstallDisplay = Backbone.View.extend({

    events: {
      'click #remove': 'removeItem'
    },

    initialize: function () {
      this.render();
      this.$el.on('change keyup paste', 'input',
		  _.bind(this.changeItem, this));
    },

    cleanup: function () {
      this.$el.off('change keyup paste', 'input');
      this.$el.remove();
    },

    render: function () {
      this.$el.html(template);
    },

    update: function (install) {
      if (this.$('#url').val() !== install.url)
      {
	this.$('#url').val(install.url);
      }
      if (this.$('#path').val() !== install.install_path)
      {
	this.$('#path').val(install.install_path);
      }
    },

    removeItem: function () {
      this.trigger('remove', { item: this });
    },

    changeItem: function () {
      _.defer(function () {
	this.trigger('change', { item: this,
				 url: this.$('#url').val(),
				 install_path: this.$('#path').val() });
      }.bind(this));
    }

  });

  return InstallDisplay;
});

define('js/canvas/ExecuteDisplay',['underscore', 'backbone'],
function (_, Backbone)
{
  

  var template = '<div class="panel-body"><label>Command:</label><input type="text" class="form-control" id="command" value="" placeholder="ex: sh /local/myscript.sh"><input type="text" class="form-control" id="shell" disabled="true" value="/bin/sh"><button id="remove" class="btn btn-danger">Remove</button></div>';

  var ExecuteDisplay = Backbone.View.extend({

    events: {
      'click #remove': 'removeItem'
    },

    initialize: function () {
      this.render();
      this.$el.on('change keyup paste', 'input',
		  _.bind(this.changeItem, this));
    },

    cleanup: function () {
      this.$el.off('change keyup paste', 'input');
      this.$el.remove();
    },

    render: function () {
      this.$el.html(template);
    },

    update: function (execute) {
      if (this.$('#command').val() !== execute.command)
      {
	this.$('#command').val(execute.command);
      }
      if (this.$('#shell').val() !== execute.shell)
      {
	this.$('#shell').val(execute.shell);
      }
    },

    removeItem: function () {
      this.trigger('remove', { item: this });
    },

    changeItem: function () {
      _.defer(function () {
	this.trigger('change', { item: this,
				 command: this.$('#command').val(),
				 shell: this.$('#shell').val() });
      }.bind(this));
    }

  });

  return ExecuteDisplay;
});

define('js/canvas/InterfaceDisplay',['underscore', 'backbone'],
function (_, Backbone)
{
  

  var template = '<div class="panel-body"><h5>Interface to <strong id="nodeName"></strong></h5><label>Name:</label><input type="text" class="form-control" id="name" value="" placeholder="ex: interface-3"><label>Bandwidth (in kbps):</label><input type="text" class="form-control" id="bandwidth" value="" placeholder="ex: 100000"><label>IP:</label><input type="text" class="form-control" id="ip" value="" placeholder="ex: 192.168.6.7"><label>Netmask:</label><input type="text" class="form-control" id="netmask" value="" placeholder="ex: 255.255.255.0"><div id="mac-container"><strong>MAC:</strong> <span id="mac"></span></div><button id="remove" class="btn btn-danger">Remove</button></div>';

  var InterfaceDisplay = Backbone.View.extend({

    events: {
      'click #remove': 'removeItem'
    },

    initialize: function () {
      this.render();
      this.$el.on('change keyup paste', 'input',
		  _.bind(this.changeItem, this));
    },

    cleanup: function () {
      this.$el.off('change keyup paste', 'input');
      this.$el.remove();
    },

    render: function () {
      this.$el.html(template);
    },

    update: function (iface) {
      this.$('#nodeName').html(_.escape(iface.node.name));
      if (this.$('#name').val() !== iface.name)
      {
	this.$('#name').val(iface.name);
      }
      if (this.$('#bandwidth').val() !== iface.bandwidth)
      {
	this.$('#bandwidth').val(iface.bandwidth);
      }
      if (this.$('#ip').val() !== iface.ip)
      {
	this.$('#ip').val(iface.ip);
      }
      if (this.$('#netmask').val() !== iface.netmask)
      {
	this.$('#netmask').val(iface.netmask);
      }
      if (iface.mac)
      {
	this.$('#mac-container').show();
	this.$('#mac').html(_.escape(iface.mac));
      }
      else
      {
	this.$('#mac-container').hide();
      }
    },

    removeItem: function () {
      this.trigger('remove', { item: this });
    },

    changeItem: function () {
      _.defer(function () {
	this.trigger('change', { item: this,
				 name: this.$('#name').val(),
				 bandwidth: this.$('#bandwidth').val(),
				 ip: this.$('#ip').val(),
				 netmask: this.$('#netmask').val() });
      }.bind(this));
    }

  });

  return InterfaceDisplay;
});

define('js/canvas/ListDisplay',['underscore', 'backbone'],
function (_, Backbone)
{
  

  var ListDisplay = Backbone.View.extend({

    events: {
      'click #add': 'addItem'
    },

    initialize: function () {
      this.domItems = [];
    },

    cleanup: function () {
      this.stopListening();
      _.each(this.domItems, function (item) {
	item.cleanup();
      });
    },

    render: function () {
    },

    update: function (list) {
      for (var i = 0; i < this.domItems.length; i += 1)
      {
	if (i < list.length)
	{
	  this.domItems[i].update(list[i]);
	}
	else
	{
	  this.stopListening(this.domItems[i]);
	  this.domItems[i].cleanup();
	}
      }
      for (var i = this.domItems.length; i < list.length; i += 1)
      {
	var newElement = $('<div class="info-list-item panel panel-default"/>');
	var newItem = new this.options.ItemDisplay({ el: newElement });
	this.listenTo(newItem, 'change', this.changeItem);
	this.listenTo(newItem, 'remove', this.removeItem);
	newItem.update(list[i]);
	this.domItems.push(newItem);
	this.$('.list').append(newElement);
      }
      this.domItems.splice(list.length, this.domItems.length);
    },

    addItem: function () {
      this.trigger('add');
    },

    removeItem: function (changed) {
      var index = this.domItems.indexOf(changed.item)
      this.trigger('remove', { index: index });
    },

    changeItem: function (changed) {
      var index = this.domItems.indexOf(changed.item);
      this.trigger('change', { index: index,
			       changed: changed });
    }

  });

  return ListDisplay;
});

// This object manages all of the listdisplays which actually render
// individual lists

define('js/info/ListManager',['underscore', 'backbone', 'lib/View',
	'js/canvas/InstallDisplay', 'js/canvas/ExecuteDisplay',
	'js/canvas/InterfaceDisplay', 'js/canvas/ListDisplay'],
function (_, Backbone, View,
	  InstallDisplay, ExecuteDisplay, InterfaceDisplay, ListDisplay)
{
  

  var ListManager = View.extend({

    initialize: function (options)
    {
      this.title = options.title;
      this.displayType = options.display;
      this.data = options.data;
      this.display = null;
      this.container = null;
      this.disabled = null;
      this.outside = null;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = (el || $('<div/>'));
	this.outside = $('<div/>')
	  .append('<hr>')
	  .appendTo(this.$el);
	var title = $('<h4>')
	  .html(_.escape(this.title))
	  .appendTo(this.outside);
	this.container = $('<div/>')
	  .appendTo(this.outside);
	var list = $('<div>')
	  .addClass('list')
	  .appendTo(this.container);
	if (this.displayType === ExecuteDisplay ||
	    this.displayType === InstallDisplay)
	{
	  var add = $('<button>Add</button>')
	    .addClass('btn btn-success')
	    .attr('id', 'add')
	    .appendTo(this.container);
	}
	this.display = new ListDisplay({
	  el: this.container,
	  ItemDisplay: this.displayType
	});
	this.listenTo(this.display, 'add', this.add);
	this.listenTo(this.display, 'remove', this.remove);
	this.listenTo(this.display, 'change', this.change);
      }
      return this.superRender();
    },

    // state {
    //   values: List of values to place in this box
    //   isViewer: True if this is read-only viewer mode
    //   type: 'node' or 'site' or 'link' or 'none'
    //   disabled: True if this box should be disabled
    // }
    update: function (state)
    {
      if (state)
      {
	var disabled = state.disabled || state.isViewer;
	if (disabled !== this.disabled)
	{
	  if (disabled)
	  {
	    this.$('button').attr('disabled', 'true');
	    this.$('input').attr('readonly', 'readonly');
	  }
	  else
	  {
	    this.$('button').removeAttr('disabled');
	    this.$('input').removeAttr('readonly');
	  }
	  if (state.isViewer)
	  {
	    this.$('button').hide();
	  }
	}
	if (state.isViewer &&
	    (! state.values || state.values.length !== 1 ||
	     (state.values.length === 1 && state.values[0].length === 0)))
	{
	  this.outside.hide();
	}
	else
	{
	  this.outside.show();
	}
	if (state.values && state.values.length === 1)
	{
	  this.display.update(state.values[0]);
	}
	this.disabled = disabled;
      }
    },

    add: function (state)
    {
      if (this.displayType === ExecuteDisplay)
      {
	this.trigger('addToList', {
	  data: this.data,
	  item: {
	    command: '',
	    shell: '/bin/sh'
	  }
	});
      }
      else if (this.displayType === InstallDisplay)
      {
	this.trigger('addToList', {
	  data: this.data,
	  item: {
	    url: '',
	    install_path: ''
	  }
	});
      }
    },

    remove: function (state)
    {
      this.trigger('removeFromList', {
	index: state.index,
	data: this.data
      });
    },

    change: function (state)
    {
      this.trigger('changeList', {
	index: state.index,
	changed: state.changed,
	data: this.data
      });
    }

  });

/*
    addExecute: function ()
    {
      if (this.highlights.length === 1)
      {
	var current = this.topoData.nodes[this.highlights[0]];
	current.execute.push({ command: '', shell: '/bin/sh' });
	this.showAttributes();
      }
    },

    addInstall: function ()
    {
      if (this.highlights.length === 1)
      {
	var current = this.topoData.nodes[this.highlights[0]];
	current.install.push({ url: '', install_path: '' });
	this.showAttributes();
      }
    },

    removeExecute: function (removed)
    {
      if (this.highlights.length === 1)
      {
	var current = this.topoData.nodes[this.highlights[0]];
	current.execute.splice(removed.index, 1);
	this.showAttributes();
      }
    },

    removeInstall: function (removed)
    {
      if (this.highlights.length === 1)
      {
	var current = this.topoData.nodes[this.highlights[0]];
	current.install.splice(removed.index, 1);
	this.showAttributes();
      }
    },

    changeExecute: function (changed)
    {
      if (this.highlights.length === 1)
      {
	var current = this.topoData.nodes[this.highlights[0]];
	_.each(changed.changed, function (value, key) {
	  current.execute[changed.index][key] = value;
	});
	this.showAttributes();
      }
    },


    changeInstall: function (changed)
    {
      if (this.highlights.length === 1)
      {
	var current = this.topoData.nodes[this.highlights[0]];
	_.each(changed.changed, function (value, key) {
	  current.install[changed.index][key] = value;
	});
	this.showAttributes();
      }
    },

    changeInterface: function (changed)
    {
      if (this.highlights.length === 1)
      {
	var link = this.topoData.lans[this.highlights[0]];
	var id = link.interfaces[changed.index];
	var current = this.topoData.interfaces[id];
	console.log(current, changed);
	_.each(changed.changed, function (value, key) {
	  current[key] = value;
	});
      }
    },
*/

  return ListManager;
});

define('js/info/OpenflowField',['underscore', 'backbone', 'lib/View',
	'js/info/CheckboxView', 'js/info/TextInputView'],
function (_, Backbone, View, CheckboxView, TextInputView)
{
  

  var OpenflowDisplay = View.extend({

    // options {
    //   state: Initial state
    //   data: Opaque data object passed back on change events
    // }
    initialize: function (options)
    {
      this.checkbox = new CheckboxView();
      this.children.push(this.checkbox);
      this.input = new TextInputView();
      this.children.push(this.input);
      this.help = options.help;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	if (this.help)
	{
	  var help = $('<span><strong>?</strong></span>')
	    .addClass('badge info-label pull-right')
	    .data('toggle', 'tooltip')
	    .attr('title', this.help)
	    .data('placement', 'bottom');
	  help.appendTo(this.$el);
	  help.tooltip();
	}
	this.checkbox.render()
	  .appendTo(this.$el);
	this.input.render()
	  .attr('placeholder', 'ex: tcp:controller_ip:6633')
	  .appendTo(this.$el);
	this.listenTo(this.checkbox, 'change', this.changeCheckbox);
	this.listenTo(this.input, 'change', this.changeInput);
      }
      return this.superRender();
    },

    // state {
    //   values: List of values to place in this box
    //   isViewer: True if this is read-only viewer mode
    //   type: 'node' or 'site' or 'link' or 'none'
    //   disabled: True if this box should be disabled
    // }
    update: function (state)
    {
      if (state)
      {
	var unique = _.unique(state.values);
	if (unique.length === 1)
	{
	  var value = unique[0];
	  var checked = (value !== undefined);
	  if (! checked)
	  {
	    value = '';
	  }
	  var disabled = (state.disabled || state.isViewer)
	  this.checkbox.update({
	    value: checked,
	    disabled: disabled,
	    shown: true,
	    label: this.options.title
	  });
	  this.input.update({
	    value: value,
	    disabled: disabled,
	    shown: checked
	  });
	}
	else
	{
	  this.checkbox.hide();
	  this.input.hide();
	}
      }
    },

    changeCheckbox: function (state)
    {
      if (state.value)
      {
	this.trigger('change', { value: '', data: this.options.data });
      }
      else
      {
	this.trigger('change', { value: undefined, data: this.options.data });
      }
    },

    changeInput: function (state)
    {
      this.trigger('change', { value: state.value, data: this.options.data });
    },

  });

  return OpenflowDisplay;
});

define('js/info/ButtonView',['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  

  var ButtonView = View.extend({

    // options {
    //  state: Initial state
    //  data: opque value returned in change event
    // }
    initialize: function (options)
    {
      this.label = null;
      this.disabled = null;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<button type="button">');
	this.$el.addClass('btn');
	this.$on('click', this.$el, this.change);
      }
      return this.superRender();
    },

    // state {
    //  label: Button label
    //  disabled: True if button is disabled
    //  shown: True if button is shown
    // }
    update: function (state)
    {
      if (state)
      {
	if (state.label !== undefined && state.label !== null)
	{
	  this.setLabel(state.label);
	}

	if (state.disabled === true)
	{
	  this.disable();
	}
	else if (state.disabled === false)
	{
	  this.enable();
	}

	if (state.shown === true)
	{
	  this.show();
	}
	else if (state.shown === false)
	{
	  this.hide();
	}
      }
    },

    disable: function ()
    {
      if (this.disabled !== true)
      {
	this.$el.attr('disabled', true);
      }
      this.disabled = true;
    },

    enable: function ()
    {
      if (this.disabled !== false)
      {
	this.$el.attr('disabled', false);
      }
      this.disabled = false;
    },

    setLabel: function (label)
    {
      var text = label || '';
      if (text !== this.label)
      {
	this.$el.html(_.escape(text));
      }
      this.label = text;
    },

    change: function (event)
    {
      this.trigger('change', { data: this.options.data });
    }

  });

  return ButtonView;
});

define('js/info/WarningField',['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  

  var WarningField = View.extend({

    initialize: function (options)
    {
      this.oldContents = null;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('#warning-field-container');
	this.$el.html('');
	this.warnEl = $('<div id="warning-field" class="alert alert-danger" style="display: none; margin-top: 10px" role="alert"></div>')
	  .appendTo(this.$el);
      }
      this.rendered = true;
      this.update(this.options.state);
      return $('<div>');
    },

    // state {
    //   values: List of values to place in this box
    //   isViewer: True if this is read-only viewer mode
    //   type: 'node' or 'site' or 'link' or 'none'
    // }
    update: function (state)
    {
      if (state)
      {
	var contents = this.processValues(state.values, state.isViewer);
	if (contents !== this.oldContents)
	{
	  this.oldContents = contents;
	  if (contents)
	  {
	    this.$('.alert.alert-danger').show().html(contents);
	  }
	  else
	  {
	    this.$('.alert.alert-danger').hide();
	  }
	}
      }
    },

    processValues: function (values, isViewer)
    {
      var result = null;
      var warnings = {};
      _.each(values, function (value) {
	if (value)
	{
	  _.each(_.keys(value), function (key) {
	    if (value[key])
	    {
	      warnings[key] = true;
	    }
	  });
	}
      });
      var keys = _.keys(warnings);
      var hasWarnings = keys.length > 0;
      if (hasWarnings && ! isViewer)
      {
	result = '<p><strong>Warning:</strong></p>'
	_.each(keys, function (key) {
	  if (warnings[key])
	  {
	    if (messages[key])
	    {
	      result += '<p>' + messages[key] + '</p>';
	    }
	    else
	    {
	      console.log('Unknown Warning: ' + key);
	    }
	  }
	});
      }
      return result;
    },

  });

  var messages = {
    openflow: 'This LAN has enabled openflow, but has no controller specified.',
    adjacentNode: 'This LAN has settings that may be incompatible with at least one adjacent node.',
    defaultLinkType: 'This LAN has a default link type that may fail. Try setting the link type.',
    duplicateAddress: 'This LAN has an IP address which is duplicated elsewhere.',
    adjacentLink: 'This node has settings that may be incompatible with at least one adjacent LAN.',
    constraintNode: 'This node has settings that may be incompatible with each other or its aggregate.'
  };

  return WarningField;
});

define('js/info/ClusterPick',['underscore'],
function (_)
{
  

  function ClusterPick(root, monitor, types, amlist, FEDERATEDLIST)
  {
    this.root = root;
    this.monitor = monitor;
    this.types = types;
    this.amList = amlist;
    this.FEDERATEDLIST = FEDERATEDLIST;
    this.CreateClusterStatus = function ()
    {
//      console.log(JSON.stringify(monitor), JSON.stringify(types), JSON.stringify(amlist), JSON.stringify(FEDERATEDLIST));
      return CreateClusterStatusImplementation(this.root, this.monitor, this.types, this.amList, this.FEDERATEDLIST);
    }.bind(this);
  }

  

  function CreateClusterStatusImplementation(root, monitor, types, amlist, FEDERATEDLIST) {
    var isInitializing = true;
/*
    if (monitor == null || $.isEmptyObject(monitor)) {
      return;
    }
*/

    root.each(function() {
      if ($(this).hasClass("pickered")) {
	return;
      }
      $(this).addClass("pickered");
	    
      var resourceType = "PC";
      var label = $(this).find('.control-label').attr('name');
      if (types && label && types[label] && types[label]['emulab-xen']) {
        resourceType = "VM";
      }
      var parent = $(this).parent();

      var html = ClusterStatusHTML(parent.find('.form-control option'),
				   FEDERATEDLIST);
      
      var selected = parent.find('select :selected')
      html.find('.dropdown-toggle .value').attr('data-value', selected.attr('value'));   
      html.find('.dropdown-toggle .value').html(selected.text());

      parent.find('.form-control').after(html);
      parent.find('select.form-control').addClass('hidden');

      html.find('.dropdown-menu a').on('click', function(event){
	if (event.screenX !== 0 && event.screenY !== 0) {
	  StatusClickEvent(parent, this);
	  var value = parent.find('.cluster_picker_status .value').attr('data-value');
	  if (! isInitializing)
	  {
	    parent.find('#profile_where').trigger('change', value);
	  }
	}
      });

      html.find('.dropdown-toggle').dropdown();

//      html.find('.dropdown-toggle').on('click', function () {
//	html.find('.dropdown-toggle').dropdown();
//      });

      _.each(amlist, function(item) {
	var name = item.name;
	var key = item.id;
	var data = monitor[key];
	var target = parent.find('.cluster_picker_status .dropdown-menu .enabled a:contains("'+name+'")');
	if (data && !$.isEmptyObject(data)) {
	  // Calculate testbed rating and set up tooltips.
	  var rating = CalculateRating(data, resourceType);
		    
	  target.parent()
	    .attr('data-health', rating[0])
	    .attr('data-rating', rating[1]);

	  var classes = AssignStatusClass(rating[0], rating[1]);
	  target.addClass(classes[0]).addClass(classes[1]);
	  
	  target.append(StatsLineHTML(classes, rating[2]));
	}
      });


      parent.find('.cluster_picker_status .dropdown-menu .enabled.native')
//	.sort(sort)
	.prependTo(parent.find('.cluster_picker_status .dropdown-menu'));
      parent.find('.cluster_picker_status .dropdown-menu .enabled.federated')
	.sort(sort)
	.insertAfter(parent.find('.cluster_picker_status .dropdown-menu .federatedDivider'));
      
      parent.find('.cluster_picker_status .dropdown-menu .enabled a')[0].click();
    });
    
    root.find('[data-toggle="tooltip"]').tooltip();
    isInitializing = false;
  }

  function sort(a, b)
  {
    var aHealth = Math.ceil((+a.dataset.health)/50);
    var bHealth = Math.ceil((+b.dataset.health)/50);
	
    if (aHealth > bHealth) {
      return -1;
    }
    else if (aHealth < bHealth) {
      return 1;
    }
    return +b.dataset.rating - +a.dataset.rating;
  };

  function ClusterStatusHTML(options, fedlist) {
    var html = $('<div class="cluster_picker_status btn-group">'
		 +'<button type="button" class="form-control btn btn-default dropdown-toggle" data-toggle="dropdown">'
		 +'<span class="value"></span>'
		 +'<span class="caret"></span>'
		 +'</button>'
		 +'<ul class="dropdown-menu" role="menu">'
		 +'<li role="separator" class="divider federatedDivider"><div>Federated Clusters<div></li>'
		 +'<li role="separator" class="divider disabledDivider"></li>'
		 +'</ul>'
		 +'</div>');

    var dropdown = html.find('.dropdown-menu .disabledDivider');
    var federated = html.find('.dropdown-menu .federatedDivider');
    var disabled = 0;
    var fed = 0;
    $(options).each(function() {
      var name = $(this).html();
      var value = $(this).attr('value');
      if ($(this).prop('disabled')) {
	dropdown.after('<li class="disabled"><a data-toggle="tooltip" data-placement="right" data-html="true" title="<div>This testbed is incompatible with the selected profile</div>" href="#" value="'+value+'">'+name+'</a></li>')
	disabled++;
      }
      else {
	if (_.contains(fedlist, value)) {
	  federated.after('<li class="enabled federated"><a href="#" value="'+value+'">'+name+'</a></li>');
	  fed++;
	}
	else {
	  var optvalue = value;
	  // Look for Please Select option
	  if (optvalue == "") {
	    optvalue = $(this).text();
	  }
	  federated.before('<li class="enabled native"><a href="#" value="'+optvalue+'">'+name+'</a></li>');
	}
      }
    });

    if (!disabled) {
      html.find('.disabledDivider').remove();
    }

    if (!fed) {
      html.find('.federatedDivider').remove();
    }

    return html;
  }

  function StatusClickEvent(html, that) {
    html.find('.dropdown-toggle .value').attr('data-value', $(that).attr('value'));   
    html.find('.dropdown-toggle .value').html($(that).text());   
		    
    if ($(that).find('.picker_stats').length) {
      if (!html.find('.dropdown-toggle > .picker_stats').length) {
	html.find('.dropdown-toggle').append('<div class="picker_stats"></div>');
      }
      else {
	html.find('.dropdown-toggle > .picker_stats').html('');
      }

      html.find('.dropdown-toggle > .picker_stats').append($(that).find('.picker_stats').html());
    }

    html.find('.selected').removeClass('selected');
    $(that).parent().addClass('selected');
  }

  function CalculateRating(data, type) {
    var health = 0;
    var rating = 0;
    var tooltip = [];

    
    if (data.status == 'SUCCESS') {
      if (data.health) {
	health = data.health;
	tooltip[0] = '<div>Testbed is '
	if (health > 50) {
	  tooltip[0] += 'healthy';
	}
	else {
	  tooltip[0] += 'unhealthy';
	}
	tooltip[0] += '</div>';
      }
      else {
	health = 100;
	tooltip[0] = '<div>Site is up</div>'
      }
    }
    else {
      tooltip[0] = '<div>Site is down</div>'
      return [health, rating, tooltip];
    }
    
    var available, max, label;
    if (type == 'VM') {
      available = parseInt(data.VMsAvailable);
      max = parseInt(data.VMsTotal);
      label = 'VMs';
    } 
    else {
      available = parseInt(data.rawPCsAvailable);
      max = parseInt(data.rawPCsTotal);
      label = 'PCs';
    }
    
    if (!isNaN(available) && !isNaN(max)) {
      var ratio = available/max;
      rating = available;
      tooltip[1] = '<div>'+available+'/'+max+' ('+Math.round(ratio*100)+'%) '+label+' available</div>';
    }
    
    return [health, rating, tooltip];
  }

  function AssignStatusClass(health, rating) {
    var result = [];
    if (health >= 50) {
      result[0] = 'status_healthy';
    }
    else if (health > 0) {
      result[0] = 'status_unhealthy';
    }
    else {
      result[0] = 'status_down';
    }
    
    if (rating > 20) {
      result[1] = 'resource_healthy';
    }
    else if (rating > 10) {
      result[1] = 'resource_unhealthy';
    }
    else {
      result[1] = 'resource_down';
    }
    
    return result;
  }

  function StatsLineHTML(classes, title) {
    var title1 = '';
    if (title[1]) {
      title1 = ' data-toggle="tooltip" data-placement="right" data-html="true" title="'+title[1]+'"';
    }
    return '<div class="tooltip_div"'+title1+'><div class="picker_stats" data-toggle="tooltip" data-placement="left" data-html="true" title="'+title[0]+'">'
      +'<span class="picker_status '+classes[0]+' '+classes[1]+'"><span class="circle"></span></span>'
      +'</div></div>';
  }
  
  return ClusterPick;
});

define('js/info/SiteField',['underscore', 'backbone', 'lib/View', 'js/canvas/ValidList', 'js/info/ClusterPick', 'js/info/ConstrainedField'],
function (_, Backbone, View, ValidList, ClusterPick, ConstrainedField)
{
  

  var SiteField = View.extend({

    initialize: function (options)
    {
      this.skipUpdate = false;
      this.picker = null;
      this.amList = this.makeAmList(options.choices);
      this.constrained = new ConstrainedField(options);
      //this.constrained.initialize(options);
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	var siteOptions = this.getOptions(_.pluck(this.options.choices, 'id'), false, [])
	this.$el = el || $(
	"<div><div class='form-group cluster-group'>" +
	  "    <h4>Site</h4>" +
	  "    <div class=''>" +
	  "      <select name='where' id='profile_where' " +
	  "              class='form-control'>" +
	  "        <option value='(any)'>(any)</option>" +
	  siteOptions +
	  "      </select>" +
	  "    </div></div>");
	this.$on('change', this.$('select'), this.change);
	this.picker = new ClusterPick(this.$('.cluster-group'), null, null, null, null);
	this.constrained.render();
      }
      return this.superRender();
    },

    update: function (state)
    {
      if (state)
      {
	this.picker.monitor = this.makeMonitor(state);
	this.picker.types = this.makeTypes(state);
	this.picker.amList = this.amList;
	this.picker.FEDERATEDLIST = this.makeFederatedList(state);
	var unique = _.unique(state.values);
	var dropdownValue = unique.join(', ');
	var newChoices = this.constrained.calculateChoicesIfNew(dropdownValue, false, state, false);
	if (this.skipUpdate)
	{
	  this.skipUpdate = false;
	}
	else
	{
	  if (newChoices)
	  {
	    this.updateChoices(newChoices.options, unique);
	    this.$('select').val(newChoices.choice);
	  }
	}
      }
    },

    updateChoices: function (constrainedChoices, values)
    {
      var allowed = constrainedChoices.allowed;
      var html = '<option value="(any)">(any)</option>';
      html += this.getOptions(constrainedChoices.allowed,
			      false, values);
      html += this.getOptions(constrainedChoices.rejected,
			      true, values);
      this.$off();
      this.$el.html(
	"<div class='form-group cluster-group'>" +
	  "    <h4>Site</h4>" +
	  "    <div class=''>" +
	  "      <select name='where' id='profile_where' " +
	  "              class='form-control'>" +
	  "      </select>" +
	  "    </div>" +
	  "</div>"
);
      this.$('#profile_where').html(html);
      this.$on('change', this.$('select'), this.change);
      this.picker.root = $('.cluster-group');
      this.picker.CreateClusterStatus();
    },

    getOptions: function (list, isDisabled, selected)
    {
      var result = '';
      var disabledString = '';
      if (isDisabled)
      {
	disabledString = ' disabled="true"';
      }
      var idToName = {};
      var idToHidden = {};
      _.each(this.options.choices, function (item) {
	idToName[item.id] = item.name;
	idToHidden[item.id] = item.hidden;
      });
      /*
      var sortFunction = function(a, b) {
	return idToName[a] < idToName[b];
      };
      var sorted = _.sortBy(list, sortFunction);
      console.log('sorted', sorted);
      console.log('list', list);
      */
      this._each(list, function(value) {
	if (idToHidden[value] === undefined ||
	    idToHidden[value] === false)
	{
	  var selectedString = '';
	  if (_.contains(selected, value)) {
	    selectedString = ' selected';
	  }
	  var text = idToName[value];
	  result += '\n<option value="' + value + '"' + disabledString +
	    selectedString + '>' + text + '</option>';
	}
      });
      return result;
    },

    makeMonitor: function (state)
    {
      var result = {};
      _.each(state.model.context.canvasOptions.aggregates, function (site) {
	var status = {
	  rawPCsAvailable: NaN,
	  rawPCsTotal: NaN,
	  VMsAvailable: NaN,
	  VMsTotal: NaN,
	  health: null
	};
	if (site.status === 'up')
	{
	  status.status = 'SUCCESS';
	  result[site.id] = status;
	}
	else if (site.status === 'down')
	{
	  status.status = 'FAILURE';
	  result[site.id] = status;
	}
      }.bind(this));
      return result;
    },

    makeTypes: function (state)
    {
      return {};
    },

    makeAmList: function (choices)
    {
      var amList = [];
      _.each(choices, function (choice) {
	if (choice.hidden === undefined || choice.hidden === false)
	{
	  amList.push(choice);
	}
      }.bind(this));
      return amList;
    },

    makeFederatedList: function (state)
    {
      return [];
    },

    change: function (event, newValue)
    {
      this.skipUpdate = true;
      var value = event
      if (newValue !== undefined)
      {
	value = newValue
      }
      if (value === '(any)' || value === '')
      {
	value = undefined;
      }
      var result = {
	value: value,
	data: this.options.data
      };
      this.trigger('change', result);
    }

  });

  return SiteField;
});

define('js/info/RadioView',['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  

  var RadioView = View.extend({

    // options {
    //   state: Initial state
    //   data: opaque value returned in change event
    //   choices: List of choices (id strings)
    //   labels: List of labels to attach to value
    // }
    initialize: function (options)
    {
      console.log(options);
      this.value = undefined;
      this.choices = options.choices;
      this.labels = options.labels;
      this.disabledOptions = null;
      this.disabled = null;
      this.id = _.uniqueId('jacksradio_');
      this.inputElements = [];
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div style="margin-right: 20px"/>');
	var i = 0;
	for (; i < this.choices.length; i += 1)
	{
	  var inputEl = $('<input type="radio" name="' + this.id + '" value="' + this.choices[i] + '">');
	  var labelEl = $('<label/>')
	      .append(inputEl)
	      .append(this.labels[i]);
	  var divEl = $('<div class="checkbox">')
	      .append(labelEl);
	  this.$el.append(divEl);
	  this.inputElements.push(inputEl);
	  this.$on('change', inputEl, this.change);
	};
      }
      return this.superRender();
    },

    // state {
    //  value: Choice id of box to be checked
    //  disabled: True if radio is disabled
    //  shown: True if this component should be shown
    // }
    update: function (state)
    {
      if (state)
      {
	var valueIndex = _.indexOf(this.choices, state.value);
	_.each(this.inputElements, function (inputEl, index) {
	  if (this.value !== valueIndex)
	  {
	    if (index === valueIndex) {
	      inputEl.prop('checked', true);
	    } else {
	      inputEl.prop('checked', false);
	    }
	  }
	  if (this.disabled !== state.disabled)
	  {
	    if (state.disabled)
	    {
	      inputEl.attr('disabled', true);
	    }
	    else
	    {
	      inputEl.attr('disabled', false);
	    }
	  }
	}.bind(this));

	if (state.shown === true)
	{
	  this.show();
	}
	else if (state.shown === false)
	{
	  this.hide();
	}
	this.value = valueIndex;
	this.disabled = state.disabled;
      }
    },

    change: function (event)
    {
      var value = -1;
      _.each(this.inputElements, function (inputEl, index) {
	if (inputEl.prop('checked')) {
	  value = index;
	}
      });
      this.trigger('change', {
	value: value,
	data: this.options.data
      });
      console.log('change RadioView');
    }

  });

  return RadioView;
});

define('js/info/SelectField',['underscore', 'backbone', 'lib/View', 'js/info/RadioView'],
function (_, Backbone, View, RadioView)
{
  

  var SelectField = View.extend({

    // options {
    //   state: Initial sate
    //   data: Opaque data object passed back on change
    //   choices: id's to use for various choices
    //   labels: List of labels to attach to values
    // }
    initialize: function (options)
    {
      this.radio = new RadioView({ choices: options.choices,
				   labels: options.labels});
      this.children.push(this.radio);
      this.help = options.help;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	if (this.help)
	{
	  var help = $('<span><strong>?</strong></span>')
	    .addClass('badge info-label pull-right')
	    .data('toggle', 'tooltip')
	    .attr('title', this.help)
	    .data('placement', 'bottom');
	  help.appendTo(this.$el);
	  help.tooltip();
	}
	this.radio.render()
	  .appendTo(this.$el);
	this.listenTo(this.radio, 'change', this.change);
      }
      return this.superRender();
    },

    // state {
    //  value: List of values to place here
    //  isViewer: True if this is read-only view mode
    //  disabled: True if selector should be disabled
    // }
    update: function (state)
    {
      if (state)
      {
	var unique = _.unique(state.values);
	var shown = (unique.length === 1);
	var selected = undefined;
	if (shown)
	{
	  selected = unique[0];
	}
	this.radio.update({
	  value: selected,
	  disabled: state.disabled,
	  shown: shown
	});
      }
    },

    change: function (state)
    {
      this.trigger('change', {
	value: state.value,
	data: this.options.data
      });
    }
    
  });

  return SelectField;
});

define('js/info/ImageField',['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  

  var ImageField = View.extend({
    // options {
    //  dynamicImages: Object containing all dynamically fetched images
    //  staticImages: Images based on the context
    // }
    initialize: function (options)
    {
      this.value = undefined;
      this.picker = undefined;
      var system = [];
      _.each(options.staticImages, function (image) {
	var result = {};
	result.urn = image.id;
	result.description = image.name;
	system.push(result);
      });
      this.staticImages = {'my-images': [],
			   'project': [],
			   'system': system,
			   'public': []};
      if (options.dynamicImages)
      {
	this.dynamicImages = {};
	_.each(options.dynamicImages, function (category, key) {
	  var result = [];
	  _.each(category, function (image) {
	    var newImage = {};
	    newImage.urn = image.urn;
	    newImage.version = image.version;
	    newImage.description = image.description;
	    newImage.deprecated = image.deprecated;
	    newImage.deprecated_message = image.deprecated_message;
	    if (image.types)
	    {
	      newImage.types = {};
	      _.each(image.types.split(','), function (type) {
		newImage.types[type] = true;
	      }.bind(this));
	    }
	    result.push(newImage);
	  }.bind(this));
	  this.dynamicImages[key] = result;
	}.bind(this));
      }
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	var title = $('<h4>Disk Image</h4>')
	    .appendTo(this.$el);
	this.openContainer = $('<span class="input-group"/>')
	  .appendTo(this.$el);
	this.current = $('<input type="text" readonly '+
			 'class="form-control" '+
			 'value="">')
	  .appendTo(this.openContainer);
	this.buttonEl = $('<span class="input-group-btn"><button class="btn btn-success"'+
			  'style="height: 34px; margin: 0;" '+
			  'type="button">'+
			  '<span class="glyphicon glyphicon-pencil">'+
			  '</span></button></span>')
	  .appendTo(this.openContainer);
	this.pickerContainer =
	  $('<div class="image-picker-background"><div class="image-picker-container"/></div>')
	  .appendTo($('body'))
	  .hide();
	this.$on('click', this.openContainer.find('button'), _.bind(this.open, this));
      }
      return this.superRender();
    },

    update: function (state)
    {
      if (state)
      {
	if (this.picker === undefined)
	{
	  this.picker = new jacksmod.ImagePicker();
	  this.picker.el.appendTo(this.pickerContainer.find('.image-picker-container'));
	  this.picker.on('selected', this.confirm.bind(this));
	  this.picker.on('closed', this.cancel.bind(this));
	}
	this.hardware = _.unique(_.pluck(state.selection, 'hardware'));
	var unique = _.unique(state.values);
	if (unique.length != 1)
	{
	  this.current.val('Multiple Options');
	  this.value = undefined;
	}
	else
	{
	  if (unique[0])
	  {
	    this.current.val(ImageDisplay(unique[0]));
	  }
	  else
	  {
	    this.current.val('Default Image');
	  }
	  this.value = unique[0];
	}
	if (state.isViewer)
	{
	  this.openContainer.hide();
	}
      }
    },

    open: function (state)
    {
      this.pickerContainer.show();
      if (this.dynamicImages)
      {
	this.picker.pick(this.value, this.dynamicImages, this.hardware);
      }
      else
      {
	this.picker.pick(this.value, this.staticImages);
      }
    },

    cancel: function (state)
    {
      this.pickerContainer.hide();
    },

    confirm: function (state)
    {
      this.pickerContainer.hide();
      this.trigger('change', { value: state,
			       data: this.options.data });
    }
  });
      var globalImages = [
	{
	  urn: 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU16-64-STD',
	  version: '',
	  description: 'Ubuntu 16.04 standard image'
	},
	{
	  urn: 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU14-64-STD',
	  version: '',
	  description: 'Ubuntu 14.04 standard image'
	},
	{
	  urn: 'urn:publicid:IDN+emulab.net+image+emulab-ops//CENTOS66-64-STD',
	  version: '',
	  description: 'CentOS 6.6 standard image'
	},
	{
	  urn: 'urn:publicid:IDN+emulab.net+image+emulab-ops//CENTOS71-64-STD',
	  version: '',
	  description: 'CentOS 7.1 standard image'
	},
      	{
	  urn: 'urn:publicid:IDN+emulab.net+image+emulab-ops//FBSD103-64-STD',
	  version: '',
	  description: 'FreeBSD 10.3 standard image'
	},
      ];
      var userImages = [
	{
	  urn: 'urn:publicid:IDN+emulab.net+image+testbed//JONS_COOL_IMAGE',
	  version: '45ac6de',
	  description: 'This image is super cool because it was created in an awesome fashion. You should totally pick this image, dood.'
	},
	{
	  urn: 'urn:publicid:IDN+emulab.net+image+testbed//JONS_BAD_HAIR_DAY_IMAGE',
	  version: 'deadbe4f',
	  description: 'You don\'t want this image, man. It was created under a bad moon in the middle of a total solar eclipse and is cursed for all time.'
	},
      ];
      
  function ImageDisplay(v)
  {
    var sp = v.split('+');
    var display;
    if (sp.length >= 4)
    {
      if (sp[3].substr(0, 12) == 'emulab-ops//')
      {
	display = sp[3].substr(12);
      }
      else if (sp[3].substr(0, 11) == 'emulab-ops:')
      {
	display = sp[3].substr(11);
      }
      else
      {
	display = sp[3];
      }
    }
    else
    {
      display = v;
    }
    return display;
  }

  return ImageField;
});

/*
 * Info.js
 *
 * Module for viewing and editing the side info box
 *
 */

define('js/info/Info',['underscore', 'backbone', 'lib/View',
	'js/info/TextField', 'js/info/ReadOnlyField',
	'js/info/ConstrainedField', 'js/info/ToggleField',
	'js/canvas/Constraints', 'js/info/ListManager',
	'js/canvas/InstallDisplay', 'js/canvas/ExecuteDisplay',
	'js/canvas/InterfaceDisplay', 'js/info/OpenflowField',
	'js/info/ButtonView', 'js/info/WarningField', 
	'js/canvas/ListDisplay', 'js/info/SiteField',
	'js/info/SelectField', 'js/info/ImageField'],
function (_, Backbone, View,
	  TextField, ReadOnlyField,
	  ConstrainedField, ToggleField,
	  Constraints, ListManager,
	  InstallDisplay, ExecuteDisplay,
	  InterfaceDisplay, OpenflowField,
	  ButtonView, WarningField,
	  ListDisplay, SiteField,
	  SelectField, ImageField)
{
  


/*
  fieldOptions = [
    {
      id: 'logins',
      on: {
	mode: 'node',
	key: 'type',
	value: [],
      },
      type: {
	id: 'span',
	title: 'SSH to',
      },
    },
    {
      id: 'type',
      on: {
	mode: 'node',
      },
      type: {
	type: 'dropdown',
	title: 'Node Type',
	options: [
	]
      }
    },
  ];
*/

  var fieldOptions = [
    {
      key: 'name',
      on: 'any',
      type: TextField,
      title: 'Name'
    },
    {
      key: 'logins',
      on: 'node',
      type: ReadOnlyField,
      title: 'SSH to'
    },
    {
      key: 'type',
      on: 'node',
      type: ConstrainedField,
      optionKey: 'types',
      title: 'Node Type',
      help: 'The container type of this node. Either a virtual container or raw-pc for a bare metal machine.'
    },
    {
      key: 'hardware',
      on: 'node',
      type: ConstrainedField,
      hideOnDataset: true,
      optionKey: 'hardware',
      title: 'Hardware Type',
      help: 'The physical hardware that this node runs on.'
    },
    {
      key: 'image',
      on: 'node',
      hideOnDataset: true,
      type: ImageField,
      optionKey: 'images',
      title: 'Disk Image',
      placeholder: 'image URN or URL',
      help: 'The initial disk state and operating system of this node.'
    },
    {
      key: 'dataset_id',
      on: 'node',
      datasetOnly: true,
      type: TextField,
      title: 'Dataset URN',
      help: 'URN for this dataset. Listed on the portal page of every dataset.'
    },
    {
      key: 'dataset_mount',
      on: 'node',
      datasetOnly: true,
      type: TextField,
      title: 'Mount Point',
      help: 'The path that this dataset will be mounted on any linked node'
    },
    {
      key: 'dataset_option',
      on: 'node',
      datasetOnly: true,
      type: SelectField,
      title: 'Dataset Access',
      choices: ['remote', 'rwclone', 'readonly'],
      labels: ['Remote Access', 'Clone (locally editable, changes are ephemeral)', 'Read Only'],
      help: 'Choose whether to access the dataset remotely, create a read/write clone (no changes propagate back to master copy), or access it in read-only mode'
    },
    {
      key: 'nomac',
      on: 'node',
      hideOnDataset: true,
      type: ToggleField,
      title: 'Disable MAC Learning (For OVS Images Only)',
      help: 'MAC Learning is an OS feature which interferes with OVS. Leave it enabled unless you plan on running OVS on this node.'
    },
    {
      key: 'routable',
      on: 'node',
      hideOnDataset: true,
      type: ToggleField,
      title: 'Require Routable IP',
      help: 'Check this if you need a public IP address on this node that is reachable from the commodity Internet. If this box is unchecked, you may get a private IP.'
    },
    {
      key: 'routable_ip',
      on: 'node',
      type: ReadOnlyField,
      title: 'Routable IP',
      help: 'This public IP address can be used to contact the node.'
    },
    {
      key: 'icon',
      on: 'node',
      type: ConstrainedField,
      optionKey: 'icons',
      title: 'Icon',
      help: 'The icon is only used for displaying the topology. Use it as a visual shorthand.'
    },
    {
      key: 'install',
      on: 'node',
      type: ListManager,
      display: InstallDisplay,
      title: 'Install Tarball',
    },
    {
      key: 'execute',
      on: 'node',
      type: ListManager,
      display: ExecuteDisplay,
      title: 'Execute Command'
    },
    {
      key: 'linkType',
      on: 'link',
      type: ConstrainedField,
      optionKey: 'linkTypes',
      title: 'Link Type',
      help: 'The type of connection. Tunnels are virtual links that run over the control network. LANs and VLANs are provisioned on experimental interfaces inside of the switch fabric.'
    },
    {
      key: 'nontrivial',
      on: 'link',
      type: ToggleField,
      title: 'Force non-trivial',
      help: 'A trivial link is one that goes over the loopback interface. This option prevents links from using the loopback interface even when two virtual nodes are on the same physical node.'
    },
    {
      key: 'interswitch',
      on: 'link',
      type: ToggleField,
      title: 'Allow interswitch mapping',
      help: 'This option allows the link to span multiple switches on the experimental switch fabric.'
    },
    {
      key: 'openflow',
      on: 'link',
      type: OpenflowField,
      title: 'Enable Openflow',
      help: 'If the switch supports Openflow, this option will enable it on the ports of the link and use the specified controller.'
    },
    {
      key: 'sharedvlan',
      on: 'link',
      type: ConstrainedField,
      optionKey: 'sharedvlans',
      title: 'Shared VLan',
      help: 'Shared VLans are a way for multiple experiments to interact over the experimental switch fabric. If you pick a Shared VLan then all other links from any experiment that pick the same Shared VLan will see your traffic and vice versa.'
    },
    {
      key: 'warnings',
      on: 'any',
      type: WarningField,
      title: 'Warning'
    },
    {
      key: 'urn',
      on: 'site',
      type: SiteField,
      optionKey: 'aggregates',
      title: 'Aggregate'
    },
    {
      key: 'interfaces',
      on: 'link',
      type: ListManager,
      display: InterfaceDisplay,
      title: 'Interfaces'
    },
    {
      key: 'delete',
      on: 'node',
      type: ButtonView,
      label: 'Delete Node'
    },
    {
      key: 'delete',
      on: 'link',
      type: ButtonView,
      label: 'Delete Link'
    },
    {
      key: 'delete',
      on: 'site',
      type: ButtonView,
      label: 'Delete Site'
    },
  ];

  var Info = View.extend({

    // options {
    //   context: Overall context for Jacks
    //   data: Opaque object passed back on change
    // }
    initialize: function (options)
    {
      this.data = options.data;
      this.context = options.context;
      this.constraints = options.constraints;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	this.$el.addClass('nodeAttr fromLeft withButtonBar');
	var inner = $('<div/>')
	  .addClass('form-group')
	  .appendTo(this.$el);
	this._each(fieldOptions, function (option) {
	  var current = null;

	  if (option.type === ConstrainedField || option.type === SiteField)
	  {
	    var choices = this.context.canvasOptions[option.optionKey];
	    current = new option.type({
	      data: option,
	      choices: choices,
	      title: option.title,
	      constraints: this.constraints,
	      optionKey: option.optionKey,
	      placeholder: option.placeholder,
	      help: option.help
	    });
	    this.listenTo(current, 'change', this.change);
	  }
	  else if (option.type === ListManager)
	  {
	    current = new option.type({
	      data: option,
	      display: option.display,
	      title: option.title,
	      help: option.help
	    });
	    this.listenTo(current, 'addToList', this.addToList);
	    this.listenTo(current, 'removeFromList', this.removeFromList);
	    this.listenTo(current, 'changeList', this.changeList);
	  }
	  else if (option.type === SelectField)
	  {
	    current = new option.type({ data: option,
					title: option.title,
					help: option.help,
					choices: option.choices,
					labels: option.labels });
	    this.listenTo(current, 'change', this.change);
	  }
	  else if (option.type === ImageField)
	  {
	    current = new option.type({ data: option,
					title: option.title,
					help: option.help,
					dynamicImages: this.context.canvasOptions.dynamicImages,
					staticImages: this.context.canvasOptions.images
				      });
	    this.listenTo(current, 'change', this.change);
	  }
	  else
	  {
	    current = new option.type({ data: option,
					title: option.title,
					help: option.help });
	    this.listenTo(current, 'change', this.change);
	  }
	  inner.append(current.render());
	  if (option.key === 'delete')
	  {
	    current.$el.addClass('btn-danger');
	  }
	  this.children.push(current);
	});
      }
      return this.superRender();
    },

    // state {
    //   selection: array of selected items
    //   type: type of selected item
    //   isViewer: True if this is in viewer mode
    // }
    update:  function (state)
    {
      if (state)
      {
	this._each(this.children, function (field, index) {
	  var option = fieldOptions[index];
	  var disabled = (option.key === 'name' && state.type === 'site');
	  var fieldSelection = [];
	  this._each(state.selection, function (item) {
	    fieldSelection.push(item[option.key]);
	  });
	  // Lookup interfaces based on indexes if we are printing them.
	  if (option.key === 'interfaces' && state.selection.length === 1)
	  {
	    fieldSelection = [[]];
	    this._each(state.selection[0].interfaces, function (id) {
	      fieldSelection[0].push(state.model.interfaces[id]);
	    });
	  }
	  var freeform = false;
	  if (state.selection.length === 1 &&
	      state.selection[0].custom[option.key])
	  {
	    freeform = true;
	  }
	  var siteNodeCount = 1;
	  if (state.type === 'site' && state.selection.length === 1)
	  {
	    siteNodeCount = 0;
	    _.each(_.values(state.model.nodes), function (node) {
	      if (node.group === state.selection[0].id)
	      {
		siteNodeCount += 1;
	      }
	    });
	  }
	  var values = fieldSelection;

	  // If the image the user has selected was tagged nomac, then
	  // display the nomac checkbox as checked and disabled.
	  if (option.key === 'nomac' &&
	      state.selection.length === 1 &&
	      state.selection[0].image)
	  {
	    var image = state.selection[0].image;
	    var imageList = state.model.context.canvasOptions.images;
	    var imageObj = _.findWhere(imageList, { id: image });
	    if (imageObj && imageObj.nomac)
	    {
	      disabled = true;
	      values = [true];
	    }
	  }

	  var shownBecauseDataset = true;
	  var hiddenBecauseDataset = false;
	  if (option.datasetOnly || option.hideOnDataset)
	  {
	    _.each(state.selection, function (item) {
	      if (option.datasetOnly && item.type !== 'emulab-blockstore')
	      {
		shownBecauseDataset = false;
	      }
	      if (option.hideOnDataset && item.type === 'emulab-blockstore')
	      {
		hiddenBecauseDataset = true;
	      }
	    });
	  }
	  var datasetShown = shownBecauseDataset && ! hiddenBecauseDataset;
	  if ((option.on === 'any' ||
	       option.on === state.type) &&
	      (option.type !== ListDisplay ||
	       state.selection.length === 1) &&
	      !(option.type === ButtonView && option.key === 'delete' && state.isViewer) &&
	      !(option.type === ButtonView && option.key === 'delete' && option.on === 'site' &&
		siteNodeCount !== 0) &&
	     datasetShown)
	  {
	    field.update({
	      values: values,
	      isViewer: state.isViewer,
	      type: state.type,
	      disabled: disabled,
	      freeform: freeform,
	      model: state.model,
	      selection: state.selection,
	      label: option.label
	    });
	    field.show();
	  }
	  else
	  {
	    field.hide();
	  }
	});
      }
    },

    change: function (state)
    {
      var result = {
	key: state.data.key,
	value: state.value,
	data: this.data
      };
      if (state.data.type === ConstrainedField || state.data.type === SiteField)
      {
	result.hasFreeform = true;
	result.freeform = state.freeform;
	result.hasVersion = state.hasVersion;
	result.version = state.version;
      }
      this.trigger('change', result);
    },

    addToList: function (state)
    {
      this.trigger('addToList', {
	key: state.data.key,
	item: state.item,
	data: this.data
      });
    },

    removeFromList: function (state)
    {
      this.trigger('removeFromList', {
	key: state.data.key,
	index: state.index,
	data: this.data
      });
    },

    changeList: function (state)
    {
      this.trigger('changeList', {
	key: state.data.key,
	index: state.index,
	changed: state.changed,
	data: this.data
      });
    }

/*
    show: function (customFields)
    {
      if (this.context.show.selectInfo)
      {
	this.executeDisplay = new ListDisplay({
	  el: root.find('#execute-list'),
	  ItemDisplay: ExecuteDisplay
	});
	this.executeDisplay.on('add', _.bind(this.addExecute, this));
	this.executeDisplay.on('remove', _.bind(this.removeExecute, this));
	this.executeDisplay.on('change', _.bind(this.changeExecute, this));
	this.installDisplay = new ListDisplay({
	  el: root.find('#install-list'),
	  ItemDisplay: InstallDisplay
	});
	this.installDisplay.on('add', _.bind(this.addInstall, this));
	this.installDisplay.on('remove', _.bind(this.removeInstall, this));
	this.installDisplay.on('change', _.bind(this.changeInstall, this));
	this.interfaceDisplay = new ListDisplay({
	  el: root.find('#interface-list'),
	  ItemDisplay: InterfaceDisplay
	});
	this.interfaceDisplay.on('change', _.bind(this.changeInterface, this));
      }
    },

    update: function (selection, selectionType)
    {
    },


    handleOpenflowChange: function (event)
    {
      if (this.domRoot.find('#openflowControl').prop('checked'))
      {
	this.domRoot.find('#openflowField').show();
      }
      else
      {
	this.domRoot.find('#openflowField').hide();
	this.topoData.changeAttribute(this.highlights, 'openflow',
				      undefined, this.lastHighlightType);
      }
    },

    handleRoutableChange: function (event)
    {
      var isRoutable = false;
      if (this.domRoot.find('#routableControl').prop('checked'))
      {
	isRoutable = true;
      }
      this.topoData.changeAttribute(this.highlights, 'routable',
				    isRoutable, this.lastHighlightType);
    },

    bindAttribute: function (that, selector, field, optionList, allowCustom)
    {
      var dom = that.domRoot.find(selector);
      if (optionList)
      {
	if (allowCustom)
	{
	  dom = that.domRoot.find(selector).find('.dropdown');
	}
	dom.change(function (event) {
	  var value = undefined;
	  if (event.val !== '(any)')
	  {
	    value = _.where(optionList,
			    { name: event.val })[0].id;
	  }
	  that.topoData.changeAttribute(that.highlights, field, value,
					that.lastHighlightType);
	  if (field === 'image')
	  {
	    var versionValue = undefined;
	    if (event.val !== '(any)')
	    {
	      versionValue = _.findWhere(optionList,
					 { name: event.val }).version;
	    }
	    that.topoData.changeAttribute(that.highlights, 'imageVersion',
					  versionValue,
					  that.lastHighlightType);
	  }
	  that.showAttributes();
	});
      }
      if (allowCustom)
      {
	dom = that.domRoot.find(selector).find('.checkbox');
	
	dom.change(function () {
	  var isCustom = false;
	  if (that.domRoot.find(selector).find('.checkbox').prop('checked'))
	  {
	    isCustom = true;
	  }
	  var item = that.topoData.getType(that.lastHighlightType)[that.highlights[0]];
	  var custom = item.custom;
	  custom[field] = isCustom;
	  that.topoData.changeAttribute(that.highlights, 'custom',
					custom, that.lastHighlightType);
	  that.showAttributes();
	});
	
	dom = that.domRoot.find(selector).find('.custom');
      }
      if (! optionList || allowCustom)
      {
	dom.on('change keyup paste', function () {
	  var value = dom.val();
	  that.topoData.changeAttribute(that.highlights, field, value,
					that.lastHighlightType);
	  that.showAttributes();
	});
      }
    }
*/

  });

  return Info;
});

define('js/canvas/ErrorModal',['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  

  var ErrorModal = View.extend({

    initialize: function (options)
    {
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('#errorModal');
      }
      this.$('button').on('click', _.bind(this.close, this));
      return this.superRender();
    },

    // state {
    //   title: Text for title
    //   contents: Text for display
    //   verbatim: True if you don't want the contents and title escaped
    // }
    // Displays the modal on every update
    update: function (state)
    {
      if (state)
      {
	var title = state.title;
	var contents = state.contents;
	if (! state.verbatim)
	{
	  title = _.escape(title);
	  contents = _.escape(contents);
	}
	this.$('.error-title').html(title);
	console.log(this.$el.length);
	console.log(title, this.$('.error-title'));
	this.$('.error-contents').html(contents);
	this.$el.show();
      }
    },

    close: function ()
    {
      this.$el.hide();
    }

  });

  return ErrorModal;
});


define('text!html/Canvas.html',[],function () { return '<!--\r\n<div class="modal fade" id="error-modal">\r\n  <div class="modal-dialog">\r\n    <div class="modal-content">\r\n\r\n\r\n    </div>\r\n  </div>\r\n</div>\r\n-->\r\n<div id="errorModal" class="fullscreen">\r\n  <div class="panel panel-default">\r\n    <div class="panel-heading">\r\n      <button type="button" class="close" aria-label="Close">\r\n\t<span aria-hidden="true">&times;</span>\r\n      </button>\r\n      <h4 class="error-title"></h4>\r\n    </div>\r\n    <div class="panel-body">\r\n      <p class="error-contents"></p>\r\n    </div>\r\n    <div class="panel-footer">\r\n      <button type="button" class="btn btn-primary pull-right">Close</button>\r\n      <div class="clearfix"></div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div id="imageModal" class="fullscreen" style="display: none;">\r\n  <div class="panel panel-default">\r\n    <div class="panel-heading">\r\n      <button type="button" class="close" aria-label="Close">\r\n\t<span aria-hidden="true">&times;</span>\r\n      </button>\r\n      <h4>Pick Image</h4>\r\n    </div>\r\n    <div class="panel-body">\r\n      <div class="image-contents"></div>\r\n    </div>\r\n    <div class="panel-footer">\r\n      <button type="button" class="btn btn-primary pull-right">Close</button>\r\n      <div class="clearfix"></div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div id="topoButtons" class="viewer hidden">\r\n  <button id="fbTour" class="btn btn-default">\r\n    Slice Tour\r\n  </button>\r\n  <button id="fbSlice" class="btn btn-primary">\r\n    Switch Slice\r\n  </button>\r\n  <button id="fbRspec" class="btn btn-primary">\r\n    View Rspec\r\n  </button>\r\n</div>\r\n<div id="topoButtons" class="editor hidden">\r\n  <!--<button id="fbcTour" class="btn btn-primary pull-right">\r\n    Edit Tour\r\n  </button>-->\r\n  <div id="fbcSite" class="pull-right"></div>\r\n  <button id="fbcClear" class="btn btn-danger pull-right">\r\n    Delete All\r\n  </button>\r\n  <button id="fbcCleanup" class="btn btn-primary pull-right">\r\n    Tidy View\r\n  </button>\r\n  <button id="fbcRspec" class="btn btn-primary pull-right">\r\n    View Rspec\r\n  </button>\r\n</div>\r\n<div id="warning-field-container"></div>\r\n<div id="leftNav">\r\n  <div style="display: none; visibility: hidden" class="navSlices fromLeft withButtonBar">\r\n    <div class="panel panel-default">\r\n      <div class="panel-body">\r\n        <div class="panel-heading">\r\n          <h3 class="panel-title">Select Slice</h3>\r\n        </div>\r\n        <div class="panel-body">\r\n          <div class="form-group">\r\n            <div id="lnSliceList" class="list-group"></div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class="navRspec fromBottom">\r\n    <div class="panel panel-default">\r\n      <div class="panel-body">\r\n\t<button id="hideRspec" type="button" class="close">\r\n\t  <span aria-hidden="true">&times;</span>\r\n\t  <span class="sr-only">Close</span>\r\n\t</button>\r\n        <div class="form-group">\r\n          <div id="lnRspec">\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div style="display: none;  visibility: hidden" class="navTopo fromRight">\r\n    <div class="panel panel-default">\r\n      <div class="panel-body">\r\n        <div id="addNodeList" class="form-group">\r\n          <!--<div id="lnTopo" class="lnLink">\r\n            <image id="node-source" class="nodebox" x="-30px" y="-30px" width="70px" height="45px">\r\n            <span>Add VM</span>\r\n          </div>-->\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div id="viewer-default" class="navTopo" style="display: none">\r\n    <h1>Click Node to Select</h1>\r\n  </div>\r\n\r\n  <div class="closeContainer fromLeft withButtonBar">\r\n    <button id="hideLeftPane" class="close" type="button">\r\n      <span aria-hidden="true">x</span>\r\n      <span class="sr-only">Close</span>\r\n    </button>\r\n  </div>\r\n\r\n  <div class="nodeAttr fromLeft withButtonBar">\r\n<!--\r\n    <div class="panel panel-default">\r\n      <div class="panel-body">\r\n-->\r\n<!--\r\n          <div class="form-group">\r\n\t    <div class="naNameGroup">\r\n              <h4>Name</h4>\r\n              <input type="text" class="form-control" id="naName" value="">\r\n\t    </div>\r\n\t    <div class="naLoginGroup">\r\n\t      <strong>SSH to</strong> <span id="#loginHost"></span>\r\n\t    </div>\r\n\t    <div class="naNodeType">\r\n\t      <h4>Node Type</h4>\r\n\t      <input type="hidden" id="naTypes" class="form-control dropdown">\r\n\t      <div class="checkboxContainer"><input class="checkbox" type="checkbox"> <span class="checkboxLabel">Custom Type</span></div>\r\n\t      <input type="text" class="form-control custom">\r\n\t    </div>\r\n\t    <div class="naHardwareType">\r\n\t      <h4>Hardware Type</h4>\r\n\t      <input type="hidden" id="naHardwares" class="form-control dropdown">\r\n\t      <div class="checkboxContainer"><input class="checkbox" type="checkbox"> <span class="checkboxLabel">Custom Hardware</span></div>\r\n\t      <input type="text" class="form-control custom">\r\n\t    </div>\r\n            <div class="naImageGroup">\r\n              <h4>Disk Image</h4>\r\n              <input type="hidden" id="naImages" class="form-control dropdown">\r\n\t      <div class="checkboxContainer"><input class="checkbox" type="checkbox"> <span class="checkboxLabel">Custom Disk Image</span></div>\r\n\t      <input type="text" class="form-control custom">\r\n\t      <input type="text" class="form-control customVersion"\r\n\t\t     placeholder="Version (for ExoGENI Images)">\r\n            </div>\r\n\t    <div class="naInstallGroup">\r\n\t      <hr>\r\n\t      <h4>Install Scripts</h4>\r\n\t      <div id="install-list">\r\n\t\t<ul class="list"></ul>\r\n\t\t<button class="btn btn-success" id="add">Add</button>\r\n\t      </div>\r\n\t    </div>\r\n\t    <div class="naExecuteGroup">\r\n\t      <hr>\r\n\t      <h4>Execute Scripts</h4>\r\n\t      <div id="execute-list">\r\n\t\t<ul class="list"></ul>\r\n\t\t<button class="btn btn-success" id="add">Add</button>\r\n\t      </div>\r\n\t    </div>\r\n\t    <div class="naRoutableGroup">\r\n\t      <hr><div class="checkboxContainer">\r\n\t      <input id="routableControl" type="checkbox"> <span class="checkboxLabel">Publicly Routable IP</span></div>\r\n\t    </div>\r\n\t    <div class="naLinkTypeGroup">\r\n\t      <h4>Link Type</h4>\r\n              <input type="hidden" id="naLinkType" class="form-control dropdown">\r\n\t      <div class="checkboxContainer"><input class="checkbox" type="checkbox"> <span class="checkboxLabel">Custom Link Type</span></div>\r\n\t      <input type="text" class="form-control custom">\r\n\t    </div>\r\n\t    <div class="naOpenflowGroup">\r\n\t      <div class="checkboxContainer"><input id="openflowControl" type="checkbox"> <span class="checkboxLabel">Enable Openflow</span></div>\r\n\t      <div id="openflowField">\r\n\t\t<h4>Controller URL (required)</h4>\r\n\t\t<input type="text" class="form-control"\r\n\t\t       id="naOpenflow" value=""\r\n\t\t       placeholder="ex: tcp:10.11.12.13:8000">\r\n\t      </div>\r\n\t    </div>\r\n\t    <div class="naSharedVLanGroup">\r\n\t      <h4>Shared VLan</h4>\r\n\t      <input type="hidden" id="naSharedVLan" class="form-control dropdown">\r\n\t      <div class="checkboxContainer"><input class="checkbox" type="checkbox"> <span class="checkboxLabel">Custom Shared VLan</span></div>\r\n\t      <input type="text" class="form-control custom">\r\n\t    </div>\r\n\t    <div class="naAggregateUrnGroup">\r\n\t      <h4>Aggregate</h4>\r\n\t      <input type="hidden" id="naAggregateUrn" class="form-control dropdown">\r\n\t      <div class="checkboxContainer"><input class="checkbox" type="checkbox"> <span class="checkboxLabel">Custom Aggregate</span></div>\r\n\t      <input type="text" class="form-control custom">\r\n\t    </div>\r\n\t    <div class="naInterfaceGroup">\r\n\t      <hr>\r\n\t      <h4>Interfaces</h4>\r\n\t      <div id="interface-list">\r\n\t\t<ul class="list"></ul>\r\n\t      </div>\r\n\t    </div>\r\n\t    <div class="naIconGroup">\r\n\t      <h4>Icon</h4>\r\n\t      <input type="hidden" id="naIcon" class="form-control dropdown">\r\n\t      <div class="checkboxContainer"><input class="checkbox" type="checkbox"> <span class="checkboxLabel">Custom Icon</span></div>\r\n\t      <input type="text" class="form-control custom">\r\n\t    </div>\r\n-->\r\n<!--\r\n\t    <div class="naLatencyGroup">\r\n\t      <h4>Latency (ms)</h4>\r\n              <input type="text" class="form-control"\r\n\t\t     id="naLatency" value=""\r\n\t\t     placeholder="ex: 100"\r\n\t\t     title="Latency is the number of ms of delay"\r\n\t\t     pattern="[0-9]*[.]?[0-9]+">\r\n\t    </div>\r\n\t    <div class="naBandwidthGroup">\r\n\t      <h4>Bandwidth (kbps)</h4>\r\n              <input type="text" class="form-control"\r\n\t\t     id="naBandwidth" value=""\r\n\t\t     placeholder="ex: 10000"\r\n\t\t     title="Bandwidth is the number of kbps required"\r\n\t\t     pattern="[0-9]*[.]?[0-9]+">\r\n\t    </div>\r\n\t    <div class="naLossGroup">\r\n\t      <h4>Loss (0.0-1.0)</h4>\r\n              <input type="text" class="form-control"\r\n\t\t     id="naLoss" value=""\r\n\t\t     placeholder="ex: 0.03"\r\n\t\t     title="Loss should be a proportion between 0 and 1"\r\n\t\t     pattern="[0-9]*[.]?[0-9]+">\r\n\t    </div>\r\n-->\r\n<!--\r\n            <hr>\r\n            <button type="button" id="naDelete" class="btn btn-danger pull-right">\r\n              Delete\r\n            </button>\r\n          </div>\r\n-->\r\n<!--\r\n      </div>\r\n    </div>\r\n-->\r\n  </div>\r\n\r\n  <div id="versionNumber">v1.6</div>\r\n</div>\r\n<div id="topoContainer" class="withButtonBar showLeft"></div>\r\n';});


define('text!html/AddNode.html',[],function () { return '<div class="lnNode">\r\n  <image class="nodebox" x="-30px" y="-30px" width="70px" height="45px"\r\n\t src="<%- icon %>">\r\n  <span><%- name %></span>\r\n</div>\r\n';});

/*
Copyright 2012 Igor Vaynberg

Version: 3.5.0 Timestamp: Mon Jun 16 19:29:44 EDT 2014

This software is licensed under the Apache License, Version 2.0 (the "Apache License") or the GNU
General Public License version 2 (the "GPL License"). You may choose either license to govern your
use of this software only upon the condition that you accept all of the terms of either the Apache
License or the GPL License.

You may obtain a copy of the Apache License and the GPL License at:

    http://www.apache.org/licenses/LICENSE-2.0
    http://www.gnu.org/licenses/gpl-2.0.html

Unless required by applicable law or agreed to in writing, software distributed under the
Apache License or the GPL License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the Apache License and the GPL License for
the specific language governing permissions and limitations under the Apache License and the GPL License.
*/
(function ($) {
    if(typeof $.fn.each2 == "undefined") {
        $.extend($.fn, {
            /*
            * 4-10 times faster .each replacement
            * use it carefully, as it overrides jQuery context of element on each iteration
            */
            each2 : function (c) {
                var j = $([0]), i = -1, l = this.length;
                while (
                    ++i < l
                    && (j.context = j[0] = this[i])
                    && c.call(j[0], i, j) !== false //"this"=DOM, i=index, j=jQuery object
                );
                return this;
            }
        });
    }
})(jQuery);

(function ($, undefined) {
    
    /*global document, window, jQuery, console */

    if (window.Select2 !== undefined) {
        return;
    }

    var KEY, AbstractSelect2, SingleSelect2, MultiSelect2, nextUid, sizer,
        lastMousePosition={x:0,y:0}, $document, scrollBarDimensions,

    KEY = {
        TAB: 9,
        ENTER: 13,
        ESC: 27,
        SPACE: 32,
        LEFT: 37,
        UP: 38,
        RIGHT: 39,
        DOWN: 40,
        SHIFT: 16,
        CTRL: 17,
        ALT: 18,
        PAGE_UP: 33,
        PAGE_DOWN: 34,
        HOME: 36,
        END: 35,
        BACKSPACE: 8,
        DELETE: 46,
        isArrow: function (k) {
            k = k.which ? k.which : k;
            switch (k) {
            case KEY.LEFT:
            case KEY.RIGHT:
            case KEY.UP:
            case KEY.DOWN:
                return true;
            }
            return false;
        },
        isControl: function (e) {
            var k = e.which;
            switch (k) {
            case KEY.SHIFT:
            case KEY.CTRL:
            case KEY.ALT:
                return true;
            }

            if (e.metaKey) return true;

            return false;
        },
        isFunctionKey: function (k) {
            k = k.which ? k.which : k;
            return k >= 112 && k <= 123;
        }
    },
    MEASURE_SCROLLBAR_TEMPLATE = "<div class='select2-measure-scrollbar'></div>",

    DIACRITICS = {"\u24B6":"A","\uFF21":"A","\u00C0":"A","\u00C1":"A","\u00C2":"A","\u1EA6":"A","\u1EA4":"A","\u1EAA":"A","\u1EA8":"A","\u00C3":"A","\u0100":"A","\u0102":"A","\u1EB0":"A","\u1EAE":"A","\u1EB4":"A","\u1EB2":"A","\u0226":"A","\u01E0":"A","\u00C4":"A","\u01DE":"A","\u1EA2":"A","\u00C5":"A","\u01FA":"A","\u01CD":"A","\u0200":"A","\u0202":"A","\u1EA0":"A","\u1EAC":"A","\u1EB6":"A","\u1E00":"A","\u0104":"A","\u023A":"A","\u2C6F":"A","\uA732":"AA","\u00C6":"AE","\u01FC":"AE","\u01E2":"AE","\uA734":"AO","\uA736":"AU","\uA738":"AV","\uA73A":"AV","\uA73C":"AY","\u24B7":"B","\uFF22":"B","\u1E02":"B","\u1E04":"B","\u1E06":"B","\u0243":"B","\u0182":"B","\u0181":"B","\u24B8":"C","\uFF23":"C","\u0106":"C","\u0108":"C","\u010A":"C","\u010C":"C","\u00C7":"C","\u1E08":"C","\u0187":"C","\u023B":"C","\uA73E":"C","\u24B9":"D","\uFF24":"D","\u1E0A":"D","\u010E":"D","\u1E0C":"D","\u1E10":"D","\u1E12":"D","\u1E0E":"D","\u0110":"D","\u018B":"D","\u018A":"D","\u0189":"D","\uA779":"D","\u01F1":"DZ","\u01C4":"DZ","\u01F2":"Dz","\u01C5":"Dz","\u24BA":"E","\uFF25":"E","\u00C8":"E","\u00C9":"E","\u00CA":"E","\u1EC0":"E","\u1EBE":"E","\u1EC4":"E","\u1EC2":"E","\u1EBC":"E","\u0112":"E","\u1E14":"E","\u1E16":"E","\u0114":"E","\u0116":"E","\u00CB":"E","\u1EBA":"E","\u011A":"E","\u0204":"E","\u0206":"E","\u1EB8":"E","\u1EC6":"E","\u0228":"E","\u1E1C":"E","\u0118":"E","\u1E18":"E","\u1E1A":"E","\u0190":"E","\u018E":"E","\u24BB":"F","\uFF26":"F","\u1E1E":"F","\u0191":"F","\uA77B":"F","\u24BC":"G","\uFF27":"G","\u01F4":"G","\u011C":"G","\u1E20":"G","\u011E":"G","\u0120":"G","\u01E6":"G","\u0122":"G","\u01E4":"G","\u0193":"G","\uA7A0":"G","\uA77D":"G","\uA77E":"G","\u24BD":"H","\uFF28":"H","\u0124":"H","\u1E22":"H","\u1E26":"H","\u021E":"H","\u1E24":"H","\u1E28":"H","\u1E2A":"H","\u0126":"H","\u2C67":"H","\u2C75":"H","\uA78D":"H","\u24BE":"I","\uFF29":"I","\u00CC":"I","\u00CD":"I","\u00CE":"I","\u0128":"I","\u012A":"I","\u012C":"I","\u0130":"I","\u00CF":"I","\u1E2E":"I","\u1EC8":"I","\u01CF":"I","\u0208":"I","\u020A":"I","\u1ECA":"I","\u012E":"I","\u1E2C":"I","\u0197":"I","\u24BF":"J","\uFF2A":"J","\u0134":"J","\u0248":"J","\u24C0":"K","\uFF2B":"K","\u1E30":"K","\u01E8":"K","\u1E32":"K","\u0136":"K","\u1E34":"K","\u0198":"K","\u2C69":"K","\uA740":"K","\uA742":"K","\uA744":"K","\uA7A2":"K","\u24C1":"L","\uFF2C":"L","\u013F":"L","\u0139":"L","\u013D":"L","\u1E36":"L","\u1E38":"L","\u013B":"L","\u1E3C":"L","\u1E3A":"L","\u0141":"L","\u023D":"L","\u2C62":"L","\u2C60":"L","\uA748":"L","\uA746":"L","\uA780":"L","\u01C7":"LJ","\u01C8":"Lj","\u24C2":"M","\uFF2D":"M","\u1E3E":"M","\u1E40":"M","\u1E42":"M","\u2C6E":"M","\u019C":"M","\u24C3":"N","\uFF2E":"N","\u01F8":"N","\u0143":"N","\u00D1":"N","\u1E44":"N","\u0147":"N","\u1E46":"N","\u0145":"N","\u1E4A":"N","\u1E48":"N","\u0220":"N","\u019D":"N","\uA790":"N","\uA7A4":"N","\u01CA":"NJ","\u01CB":"Nj","\u24C4":"O","\uFF2F":"O","\u00D2":"O","\u00D3":"O","\u00D4":"O","\u1ED2":"O","\u1ED0":"O","\u1ED6":"O","\u1ED4":"O","\u00D5":"O","\u1E4C":"O","\u022C":"O","\u1E4E":"O","\u014C":"O","\u1E50":"O","\u1E52":"O","\u014E":"O","\u022E":"O","\u0230":"O","\u00D6":"O","\u022A":"O","\u1ECE":"O","\u0150":"O","\u01D1":"O","\u020C":"O","\u020E":"O","\u01A0":"O","\u1EDC":"O","\u1EDA":"O","\u1EE0":"O","\u1EDE":"O","\u1EE2":"O","\u1ECC":"O","\u1ED8":"O","\u01EA":"O","\u01EC":"O","\u00D8":"O","\u01FE":"O","\u0186":"O","\u019F":"O","\uA74A":"O","\uA74C":"O","\u01A2":"OI","\uA74E":"OO","\u0222":"OU","\u24C5":"P","\uFF30":"P","\u1E54":"P","\u1E56":"P","\u01A4":"P","\u2C63":"P","\uA750":"P","\uA752":"P","\uA754":"P","\u24C6":"Q","\uFF31":"Q","\uA756":"Q","\uA758":"Q","\u024A":"Q","\u24C7":"R","\uFF32":"R","\u0154":"R","\u1E58":"R","\u0158":"R","\u0210":"R","\u0212":"R","\u1E5A":"R","\u1E5C":"R","\u0156":"R","\u1E5E":"R","\u024C":"R","\u2C64":"R","\uA75A":"R","\uA7A6":"R","\uA782":"R","\u24C8":"S","\uFF33":"S","\u1E9E":"S","\u015A":"S","\u1E64":"S","\u015C":"S","\u1E60":"S","\u0160":"S","\u1E66":"S","\u1E62":"S","\u1E68":"S","\u0218":"S","\u015E":"S","\u2C7E":"S","\uA7A8":"S","\uA784":"S","\u24C9":"T","\uFF34":"T","\u1E6A":"T","\u0164":"T","\u1E6C":"T","\u021A":"T","\u0162":"T","\u1E70":"T","\u1E6E":"T","\u0166":"T","\u01AC":"T","\u01AE":"T","\u023E":"T","\uA786":"T","\uA728":"TZ","\u24CA":"U","\uFF35":"U","\u00D9":"U","\u00DA":"U","\u00DB":"U","\u0168":"U","\u1E78":"U","\u016A":"U","\u1E7A":"U","\u016C":"U","\u00DC":"U","\u01DB":"U","\u01D7":"U","\u01D5":"U","\u01D9":"U","\u1EE6":"U","\u016E":"U","\u0170":"U","\u01D3":"U","\u0214":"U","\u0216":"U","\u01AF":"U","\u1EEA":"U","\u1EE8":"U","\u1EEE":"U","\u1EEC":"U","\u1EF0":"U","\u1EE4":"U","\u1E72":"U","\u0172":"U","\u1E76":"U","\u1E74":"U","\u0244":"U","\u24CB":"V","\uFF36":"V","\u1E7C":"V","\u1E7E":"V","\u01B2":"V","\uA75E":"V","\u0245":"V","\uA760":"VY","\u24CC":"W","\uFF37":"W","\u1E80":"W","\u1E82":"W","\u0174":"W","\u1E86":"W","\u1E84":"W","\u1E88":"W","\u2C72":"W","\u24CD":"X","\uFF38":"X","\u1E8A":"X","\u1E8C":"X","\u24CE":"Y","\uFF39":"Y","\u1EF2":"Y","\u00DD":"Y","\u0176":"Y","\u1EF8":"Y","\u0232":"Y","\u1E8E":"Y","\u0178":"Y","\u1EF6":"Y","\u1EF4":"Y","\u01B3":"Y","\u024E":"Y","\u1EFE":"Y","\u24CF":"Z","\uFF3A":"Z","\u0179":"Z","\u1E90":"Z","\u017B":"Z","\u017D":"Z","\u1E92":"Z","\u1E94":"Z","\u01B5":"Z","\u0224":"Z","\u2C7F":"Z","\u2C6B":"Z","\uA762":"Z","\u24D0":"a","\uFF41":"a","\u1E9A":"a","\u00E0":"a","\u00E1":"a","\u00E2":"a","\u1EA7":"a","\u1EA5":"a","\u1EAB":"a","\u1EA9":"a","\u00E3":"a","\u0101":"a","\u0103":"a","\u1EB1":"a","\u1EAF":"a","\u1EB5":"a","\u1EB3":"a","\u0227":"a","\u01E1":"a","\u00E4":"a","\u01DF":"a","\u1EA3":"a","\u00E5":"a","\u01FB":"a","\u01CE":"a","\u0201":"a","\u0203":"a","\u1EA1":"a","\u1EAD":"a","\u1EB7":"a","\u1E01":"a","\u0105":"a","\u2C65":"a","\u0250":"a","\uA733":"aa","\u00E6":"ae","\u01FD":"ae","\u01E3":"ae","\uA735":"ao","\uA737":"au","\uA739":"av","\uA73B":"av","\uA73D":"ay","\u24D1":"b","\uFF42":"b","\u1E03":"b","\u1E05":"b","\u1E07":"b","\u0180":"b","\u0183":"b","\u0253":"b","\u24D2":"c","\uFF43":"c","\u0107":"c","\u0109":"c","\u010B":"c","\u010D":"c","\u00E7":"c","\u1E09":"c","\u0188":"c","\u023C":"c","\uA73F":"c","\u2184":"c","\u24D3":"d","\uFF44":"d","\u1E0B":"d","\u010F":"d","\u1E0D":"d","\u1E11":"d","\u1E13":"d","\u1E0F":"d","\u0111":"d","\u018C":"d","\u0256":"d","\u0257":"d","\uA77A":"d","\u01F3":"dz","\u01C6":"dz","\u24D4":"e","\uFF45":"e","\u00E8":"e","\u00E9":"e","\u00EA":"e","\u1EC1":"e","\u1EBF":"e","\u1EC5":"e","\u1EC3":"e","\u1EBD":"e","\u0113":"e","\u1E15":"e","\u1E17":"e","\u0115":"e","\u0117":"e","\u00EB":"e","\u1EBB":"e","\u011B":"e","\u0205":"e","\u0207":"e","\u1EB9":"e","\u1EC7":"e","\u0229":"e","\u1E1D":"e","\u0119":"e","\u1E19":"e","\u1E1B":"e","\u0247":"e","\u025B":"e","\u01DD":"e","\u24D5":"f","\uFF46":"f","\u1E1F":"f","\u0192":"f","\uA77C":"f","\u24D6":"g","\uFF47":"g","\u01F5":"g","\u011D":"g","\u1E21":"g","\u011F":"g","\u0121":"g","\u01E7":"g","\u0123":"g","\u01E5":"g","\u0260":"g","\uA7A1":"g","\u1D79":"g","\uA77F":"g","\u24D7":"h","\uFF48":"h","\u0125":"h","\u1E23":"h","\u1E27":"h","\u021F":"h","\u1E25":"h","\u1E29":"h","\u1E2B":"h","\u1E96":"h","\u0127":"h","\u2C68":"h","\u2C76":"h","\u0265":"h","\u0195":"hv","\u24D8":"i","\uFF49":"i","\u00EC":"i","\u00ED":"i","\u00EE":"i","\u0129":"i","\u012B":"i","\u012D":"i","\u00EF":"i","\u1E2F":"i","\u1EC9":"i","\u01D0":"i","\u0209":"i","\u020B":"i","\u1ECB":"i","\u012F":"i","\u1E2D":"i","\u0268":"i","\u0131":"i","\u24D9":"j","\uFF4A":"j","\u0135":"j","\u01F0":"j","\u0249":"j","\u24DA":"k","\uFF4B":"k","\u1E31":"k","\u01E9":"k","\u1E33":"k","\u0137":"k","\u1E35":"k","\u0199":"k","\u2C6A":"k","\uA741":"k","\uA743":"k","\uA745":"k","\uA7A3":"k","\u24DB":"l","\uFF4C":"l","\u0140":"l","\u013A":"l","\u013E":"l","\u1E37":"l","\u1E39":"l","\u013C":"l","\u1E3D":"l","\u1E3B":"l","\u017F":"l","\u0142":"l","\u019A":"l","\u026B":"l","\u2C61":"l","\uA749":"l","\uA781":"l","\uA747":"l","\u01C9":"lj","\u24DC":"m","\uFF4D":"m","\u1E3F":"m","\u1E41":"m","\u1E43":"m","\u0271":"m","\u026F":"m","\u24DD":"n","\uFF4E":"n","\u01F9":"n","\u0144":"n","\u00F1":"n","\u1E45":"n","\u0148":"n","\u1E47":"n","\u0146":"n","\u1E4B":"n","\u1E49":"n","\u019E":"n","\u0272":"n","\u0149":"n","\uA791":"n","\uA7A5":"n","\u01CC":"nj","\u24DE":"o","\uFF4F":"o","\u00F2":"o","\u00F3":"o","\u00F4":"o","\u1ED3":"o","\u1ED1":"o","\u1ED7":"o","\u1ED5":"o","\u00F5":"o","\u1E4D":"o","\u022D":"o","\u1E4F":"o","\u014D":"o","\u1E51":"o","\u1E53":"o","\u014F":"o","\u022F":"o","\u0231":"o","\u00F6":"o","\u022B":"o","\u1ECF":"o","\u0151":"o","\u01D2":"o","\u020D":"o","\u020F":"o","\u01A1":"o","\u1EDD":"o","\u1EDB":"o","\u1EE1":"o","\u1EDF":"o","\u1EE3":"o","\u1ECD":"o","\u1ED9":"o","\u01EB":"o","\u01ED":"o","\u00F8":"o","\u01FF":"o","\u0254":"o","\uA74B":"o","\uA74D":"o","\u0275":"o","\u01A3":"oi","\u0223":"ou","\uA74F":"oo","\u24DF":"p","\uFF50":"p","\u1E55":"p","\u1E57":"p","\u01A5":"p","\u1D7D":"p","\uA751":"p","\uA753":"p","\uA755":"p","\u24E0":"q","\uFF51":"q","\u024B":"q","\uA757":"q","\uA759":"q","\u24E1":"r","\uFF52":"r","\u0155":"r","\u1E59":"r","\u0159":"r","\u0211":"r","\u0213":"r","\u1E5B":"r","\u1E5D":"r","\u0157":"r","\u1E5F":"r","\u024D":"r","\u027D":"r","\uA75B":"r","\uA7A7":"r","\uA783":"r","\u24E2":"s","\uFF53":"s","\u00DF":"s","\u015B":"s","\u1E65":"s","\u015D":"s","\u1E61":"s","\u0161":"s","\u1E67":"s","\u1E63":"s","\u1E69":"s","\u0219":"s","\u015F":"s","\u023F":"s","\uA7A9":"s","\uA785":"s","\u1E9B":"s","\u24E3":"t","\uFF54":"t","\u1E6B":"t","\u1E97":"t","\u0165":"t","\u1E6D":"t","\u021B":"t","\u0163":"t","\u1E71":"t","\u1E6F":"t","\u0167":"t","\u01AD":"t","\u0288":"t","\u2C66":"t","\uA787":"t","\uA729":"tz","\u24E4":"u","\uFF55":"u","\u00F9":"u","\u00FA":"u","\u00FB":"u","\u0169":"u","\u1E79":"u","\u016B":"u","\u1E7B":"u","\u016D":"u","\u00FC":"u","\u01DC":"u","\u01D8":"u","\u01D6":"u","\u01DA":"u","\u1EE7":"u","\u016F":"u","\u0171":"u","\u01D4":"u","\u0215":"u","\u0217":"u","\u01B0":"u","\u1EEB":"u","\u1EE9":"u","\u1EEF":"u","\u1EED":"u","\u1EF1":"u","\u1EE5":"u","\u1E73":"u","\u0173":"u","\u1E77":"u","\u1E75":"u","\u0289":"u","\u24E5":"v","\uFF56":"v","\u1E7D":"v","\u1E7F":"v","\u028B":"v","\uA75F":"v","\u028C":"v","\uA761":"vy","\u24E6":"w","\uFF57":"w","\u1E81":"w","\u1E83":"w","\u0175":"w","\u1E87":"w","\u1E85":"w","\u1E98":"w","\u1E89":"w","\u2C73":"w","\u24E7":"x","\uFF58":"x","\u1E8B":"x","\u1E8D":"x","\u24E8":"y","\uFF59":"y","\u1EF3":"y","\u00FD":"y","\u0177":"y","\u1EF9":"y","\u0233":"y","\u1E8F":"y","\u00FF":"y","\u1EF7":"y","\u1E99":"y","\u1EF5":"y","\u01B4":"y","\u024F":"y","\u1EFF":"y","\u24E9":"z","\uFF5A":"z","\u017A":"z","\u1E91":"z","\u017C":"z","\u017E":"z","\u1E93":"z","\u1E95":"z","\u01B6":"z","\u0225":"z","\u0240":"z","\u2C6C":"z","\uA763":"z","\u0386":"\u0391","\u0388":"\u0395","\u0389":"\u0397","\u038A":"\u0399","\u03AA":"\u0399","\u038C":"\u039F","\u038E":"\u03A5","\u03AB":"\u03A5","\u038F":"\u03A9","\u03AC":"\u03B1","\u03AD":"\u03B5","\u03AE":"\u03B7","\u03AF":"\u03B9","\u03CA":"\u03B9","\u0390":"\u03B9","\u03CC":"\u03BF","\u03CD":"\u03C5","\u03CB":"\u03C5","\u03B0":"\u03C5","\u03C9":"\u03C9","\u03C2":"\u03C3"};

    $document = $(document);

    nextUid=(function() { var counter=1; return function() { return counter++; }; }());


    function reinsertElement(element) {
        var placeholder = $(document.createTextNode(''));

        element.before(placeholder);
        placeholder.before(element);
        placeholder.remove();
    }

    function stripDiacritics(str) {
        // Used 'uni range + named function' from http://jsperf.com/diacritics/18
        function match(a) {
            return DIACRITICS[a] || a;
        }

        return str.replace(/[^\u0000-\u007E]/g, match);
    }

    function indexOf(value, array) {
        var i = 0, l = array.length;
        for (; i < l; i = i + 1) {
            if (equal(value, array[i])) return i;
        }
        return -1;
    }

    function measureScrollbar () {
        var $template = $( MEASURE_SCROLLBAR_TEMPLATE );
        $template.appendTo('body');

        var dim = {
            width: $template.width() - $template[0].clientWidth,
            height: $template.height() - $template[0].clientHeight
        };
        $template.remove();

        return dim;
    }

    /**
     * Compares equality of a and b
     * @param a
     * @param b
     */
    function equal(a, b) {
        if (a === b) return true;
        if (a === undefined || b === undefined) return false;
        if (a === null || b === null) return false;
        // Check whether 'a' or 'b' is a string (primitive or object).
        // The concatenation of an empty string (+'') converts its argument to a string's primitive.
        if (a.constructor === String) return a+'' === b+''; // a+'' - in case 'a' is a String object
        if (b.constructor === String) return b+'' === a+''; // b+'' - in case 'b' is a String object
        return false;
    }

    /**
     * Splits the string into an array of values, trimming each value. An empty array is returned for nulls or empty
     * strings
     * @param string
     * @param separator
     */
    function splitVal(string, separator) {
        var val, i, l;
        if (string === null || string.length < 1) return [];
        val = string.split(separator);
        for (i = 0, l = val.length; i < l; i = i + 1) val[i] = $.trim(val[i]);
        return val;
    }

    function getSideBorderPadding(element) {
        return element.outerWidth(false) - element.width();
    }

    function installKeyUpChangeEvent(element) {
        var key="keyup-change-value";
        element.on("keydown", function () {
            if ($.data(element, key) === undefined) {
                $.data(element, key, element.val());
            }
        });
        element.on("keyup", function () {
            var val= $.data(element, key);
            if (val !== undefined && element.val() !== val) {
                $.removeData(element, key);
                element.trigger("keyup-change");
            }
        });
    }


    /**
     * filters mouse events so an event is fired only if the mouse moved.
     *
     * filters out mouse events that occur when mouse is stationary but
     * the elements under the pointer are scrolled.
     */
    function installFilteredMouseMove(element) {
        element.on("mousemove", function (e) {
            var lastpos = lastMousePosition;
            if (lastpos === undefined || lastpos.x !== e.pageX || lastpos.y !== e.pageY) {
                $(e.target).trigger("mousemove-filtered", e);
            }
        });
    }

    /**
     * Debounces a function. Returns a function that calls the original fn function only if no invocations have been made
     * within the last quietMillis milliseconds.
     *
     * @param quietMillis number of milliseconds to wait before invoking fn
     * @param fn function to be debounced
     * @param ctx object to be used as this reference within fn
     * @return debounced version of fn
     */
    function debounce(quietMillis, fn, ctx) {
        ctx = ctx || undefined;
        var timeout;
        return function () {
            var args = arguments;
            window.clearTimeout(timeout);
            timeout = window.setTimeout(function() {
                fn.apply(ctx, args);
            }, quietMillis);
        };
    }

    function installDebouncedScroll(threshold, element) {
        var notify = debounce(threshold, function (e) { element.trigger("scroll-debounced", e);});
        element.on("scroll", function (e) {
            if (indexOf(e.target, element.get()) >= 0) notify(e);
        });
    }

    function focus($el) {
        if ($el[0] === document.activeElement) return;

        /* set the focus in a 0 timeout - that way the focus is set after the processing
            of the current event has finished - which seems like the only reliable way
            to set focus */
        window.setTimeout(function() {
            var el=$el[0], pos=$el.val().length, range;

            $el.focus();

            /* make sure el received focus so we do not error out when trying to manipulate the caret.
                sometimes modals or others listeners may steal it after its set */
            var isVisible = (el.offsetWidth > 0 || el.offsetHeight > 0);
            if (isVisible && el === document.activeElement) {

                /* after the focus is set move the caret to the end, necessary when we val()
                    just before setting focus */
                if(el.setSelectionRange)
                {
                    el.setSelectionRange(pos, pos);
                }
                else if (el.createTextRange) {
                    range = el.createTextRange();
                    range.collapse(false);
                    range.select();
                }
            }
        }, 0);
    }

    function getCursorInfo(el) {
        el = $(el)[0];
        var offset = 0;
        var length = 0;
        if ('selectionStart' in el) {
            offset = el.selectionStart;
            length = el.selectionEnd - offset;
        } else if ('selection' in document) {
            el.focus();
            var sel = document.selection.createRange();
            length = document.selection.createRange().text.length;
            sel.moveStart('character', -el.value.length);
            offset = sel.text.length - length;
        }
        return { offset: offset, length: length };
    }

    function killEvent(event) {
        event.preventDefault();
        event.stopPropagation();
    }
    function killEventImmediately(event) {
        event.preventDefault();
        event.stopImmediatePropagation();
    }

    function measureTextWidth(e) {
        if (!sizer){
            var style = e[0].currentStyle || window.getComputedStyle(e[0], null);
            sizer = $(document.createElement("div")).css({
                position: "absolute",
                left: "-10000px",
                top: "-10000px",
                display: "none",
                fontSize: style.fontSize,
                fontFamily: style.fontFamily,
                fontStyle: style.fontStyle,
                fontWeight: style.fontWeight,
                letterSpacing: style.letterSpacing,
                textTransform: style.textTransform,
                whiteSpace: "nowrap"
            });
            sizer.attr("class","select2-sizer");
            $("body").append(sizer);
        }
        sizer.text(e.val());
        return sizer.width();
    }

    function syncCssClasses(dest, src, adapter) {
        var classes, replacements = [], adapted;

        classes = $.trim(dest.attr("class"));

        if (classes) {
            classes = '' + classes; // for IE which returns object

            $(classes.split(/\s+/)).each2(function() {
                if (this.indexOf("select2-") === 0) {
                    replacements.push(this);
                }
            });
        }

        classes = $.trim(src.attr("class"));

        if (classes) {
            classes = '' + classes; // for IE which returns object

            $(classes.split(/\s+/)).each2(function() {
                if (this.indexOf("select2-") !== 0) {
                    adapted = adapter(this);

                    if (adapted) {
                        replacements.push(adapted);
                    }
                }
            });
        }

        dest.attr("class", replacements.join(" "));
    }


    function markMatch(text, term, markup, escapeMarkup) {
        var match=stripDiacritics(text.toUpperCase()).indexOf(stripDiacritics(term.toUpperCase())),
            tl=term.length;

        if (match<0) {
            markup.push(escapeMarkup(text));
            return;
        }

        markup.push(escapeMarkup(text.substring(0, match)));
        markup.push("<span class='select2-match'>");
        markup.push(escapeMarkup(text.substring(match, match + tl)));
        markup.push("</span>");
        markup.push(escapeMarkup(text.substring(match + tl, text.length)));
    }

    function defaultEscapeMarkup(markup) {
        var replace_map = {
            '\\': '&#92;',
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#39;',
            "/": '&#47;'
        };

        return String(markup).replace(/[&<>"'\/\\]/g, function (match) {
            return replace_map[match];
        });
    }

    /**
     * Produces an ajax-based query function
     *
     * @param options object containing configuration parameters
     * @param options.params parameter map for the transport ajax call, can contain such options as cache, jsonpCallback, etc. see $.ajax
     * @param options.transport function that will be used to execute the ajax request. must be compatible with parameters supported by $.ajax
     * @param options.url url for the data
     * @param options.data a function(searchTerm, pageNumber, context) that should return an object containing query string parameters for the above url.
     * @param options.dataType request data type: ajax, jsonp, other datatypes supported by jQuery's $.ajax function or the transport function if specified
     * @param options.quietMillis (optional) milliseconds to wait before making the ajaxRequest, helps debounce the ajax function if invoked too often
     * @param options.results a function(remoteData, pageNumber, query) that converts data returned form the remote request to the format expected by Select2.
     *      The expected format is an object containing the following keys:
     *      results array of objects that will be used as choices
     *      more (optional) boolean indicating whether there are more results available
     *      Example: {results:[{id:1, text:'Red'},{id:2, text:'Blue'}], more:true}
     */
    function ajax(options) {
        var timeout, // current scheduled but not yet executed request
            handler = null,
            quietMillis = options.quietMillis || 100,
            ajaxUrl = options.url,
            self = this;

        return function (query) {
            window.clearTimeout(timeout);
            timeout = window.setTimeout(function () {
                var data = options.data, // ajax data function
                    url = ajaxUrl, // ajax url string or function
                    transport = options.transport || $.fn.select2.ajaxDefaults.transport,
                    // deprecated - to be removed in 4.0  - use params instead
                    deprecated = {
                        type: options.type || 'GET', // set type of request (GET or POST)
                        cache: options.cache || false,
                        jsonpCallback: options.jsonpCallback||undefined,
                        dataType: options.dataType||"json"
                    },
                    params = $.extend({}, $.fn.select2.ajaxDefaults.params, deprecated);

                data = data ? data.call(self, query.term, query.page, query.context) : null;
                url = (typeof url === 'function') ? url.call(self, query.term, query.page, query.context) : url;

                if (handler && typeof handler.abort === "function") { handler.abort(); }

                if (options.params) {
                    if ($.isFunction(options.params)) {
                        $.extend(params, options.params.call(self));
                    } else {
                        $.extend(params, options.params);
                    }
                }

                $.extend(params, {
                    url: url,
                    dataType: options.dataType,
                    data: data,
                    success: function (data) {
                        // TODO - replace query.page with query so users have access to term, page, etc.
                        // added query as third paramter to keep backwards compatibility
                        var results = options.results(data, query.page, query);
                        query.callback(results);
                    }
                });
                handler = transport.call(self, params);
            }, quietMillis);
        };
    }

    /**
     * Produces a query function that works with a local array
     *
     * @param options object containing configuration parameters. The options parameter can either be an array or an
     * object.
     *
     * If the array form is used it is assumed that it contains objects with 'id' and 'text' keys.
     *
     * If the object form is used it is assumed that it contains 'data' and 'text' keys. The 'data' key should contain
     * an array of objects that will be used as choices. These objects must contain at least an 'id' key. The 'text'
     * key can either be a String in which case it is expected that each element in the 'data' array has a key with the
     * value of 'text' which will be used to match choices. Alternatively, text can be a function(item) that can extract
     * the text.
     */
    function local(options) {
        var data = options, // data elements
            dataText,
            tmp,
            text = function (item) { return ""+item.text; }; // function used to retrieve the text portion of a data item that is matched against the search

         if ($.isArray(data)) {
            tmp = data;
            data = { results: tmp };
        }

         if ($.isFunction(data) === false) {
            tmp = data;
            data = function() { return tmp; };
        }

        var dataItem = data();
        if (dataItem.text) {
            text = dataItem.text;
            // if text is not a function we assume it to be a key name
            if (!$.isFunction(text)) {
                dataText = dataItem.text; // we need to store this in a separate variable because in the next step data gets reset and data.text is no longer available
                text = function (item) { return item[dataText]; };
            }
        }

        return function (query) {
            var t = query.term, filtered = { results: [] }, process;
            if (t === "") {
                query.callback(data());
                return;
            }

            process = function(datum, collection) {
                var group, attr;
                datum = datum[0];
                if (datum.children) {
                    group = {};
                    for (attr in datum) {
                        if (datum.hasOwnProperty(attr)) group[attr]=datum[attr];
                    }
                    group.children=[];
                    $(datum.children).each2(function(i, childDatum) { process(childDatum, group.children); });
                    if (group.children.length || query.matcher(t, text(group), datum)) {
                        collection.push(group);
                    }
                } else {
                    if (query.matcher(t, text(datum), datum)) {
                        collection.push(datum);
                    }
                }
            };

            $(data().results).each2(function(i, datum) { process(datum, filtered.results); });
            query.callback(filtered);
        };
    }

    // TODO javadoc
    function tags(data) {
        var isFunc = $.isFunction(data);
        return function (query) {
            var t = query.term, filtered = {results: []};
            var result = isFunc ? data(query) : data;
            if ($.isArray(result)) {
                $(result).each(function () {
                    var isObject = this.text !== undefined,
                        text = isObject ? this.text : this;
                    if (t === "" || query.matcher(t, text)) {
                        filtered.results.push(isObject ? this : {id: this, text: this});
                    }
                });
                query.callback(filtered);
            }
        };
    }

    /**
     * Checks if the formatter function should be used.
     *
     * Throws an error if it is not a function. Returns true if it should be used,
     * false if no formatting should be performed.
     *
     * @param formatter
     */
    function checkFormatter(formatter, formatterName) {
        if ($.isFunction(formatter)) return true;
        if (!formatter) return false;
        if (typeof(formatter) === 'string') return true;
        throw new Error(formatterName +" must be a string, function, or falsy value");
    }

  /**
   * Returns a given value
   * If given a function, returns its output
   *
   * @param val string|function
   * @param context value of "this" to be passed to function
   * @returns {*}
   */
    function evaluate(val, context) {
        if ($.isFunction(val)) {
            var args = Array.prototype.slice.call(arguments, 2);
            return val.apply(context, args);
        }
        return val;
    }

    function countResults(results) {
        var count = 0;
        $.each(results, function(i, item) {
            if (item.children) {
                count += countResults(item.children);
            } else {
                count++;
            }
        });
        return count;
    }

    /**
     * Default tokenizer. This function uses breaks the input on substring match of any string from the
     * opts.tokenSeparators array and uses opts.createSearchChoice to create the choice object. Both of those
     * two options have to be defined in order for the tokenizer to work.
     *
     * @param input text user has typed so far or pasted into the search field
     * @param selection currently selected choices
     * @param selectCallback function(choice) callback tho add the choice to selection
     * @param opts select2's opts
     * @return undefined/null to leave the current input unchanged, or a string to change the input to the returned value
     */
    function defaultTokenizer(input, selection, selectCallback, opts) {
        var original = input, // store the original so we can compare and know if we need to tell the search to update its text
            dupe = false, // check for whether a token we extracted represents a duplicate selected choice
            token, // token
            index, // position at which the separator was found
            i, l, // looping variables
            separator; // the matched separator

        if (!opts.createSearchChoice || !opts.tokenSeparators || opts.tokenSeparators.length < 1) return undefined;

        while (true) {
            index = -1;

            for (i = 0, l = opts.tokenSeparators.length; i < l; i++) {
                separator = opts.tokenSeparators[i];
                index = input.indexOf(separator);
                if (index >= 0) break;
            }

            if (index < 0) break; // did not find any token separator in the input string, bail

            token = input.substring(0, index);
            input = input.substring(index + separator.length);

            if (token.length > 0) {
                token = opts.createSearchChoice.call(this, token, selection);
                if (token !== undefined && token !== null && opts.id(token) !== undefined && opts.id(token) !== null) {
                    dupe = false;
                    for (i = 0, l = selection.length; i < l; i++) {
                        if (equal(opts.id(token), opts.id(selection[i]))) {
                            dupe = true; break;
                        }
                    }

                    if (!dupe) selectCallback(token);
                }
            }
        }

        if (original!==input) return input;
    }

    function cleanupJQueryElements() {
        var self = this;

        $.each(arguments, function (i, element) {
            self[element].remove();
            self[element] = null;
        });
    }

    /**
     * Creates a new class
     *
     * @param superClass
     * @param methods
     */
    function clazz(SuperClass, methods) {
        var constructor = function () {};
        constructor.prototype = new SuperClass;
        constructor.prototype.constructor = constructor;
        constructor.prototype.parent = SuperClass.prototype;
        constructor.prototype = $.extend(constructor.prototype, methods);
        return constructor;
    }

    AbstractSelect2 = clazz(Object, {

        // abstract
        bind: function (func) {
            var self = this;
            return function () {
                func.apply(self, arguments);
            };
        },

        // abstract
        init: function (opts) {
            var results, search, resultsSelector = ".select2-results";

            // prepare options
            this.opts = opts = this.prepareOpts(opts);

            this.id=opts.id;

            // destroy if called on an existing component
            if (opts.element.data("select2") !== undefined &&
                opts.element.data("select2") !== null) {
                opts.element.data("select2").destroy();
            }

            this.container = this.createContainer();

            this.liveRegion = $("<span>", {
                    role: "status",
                    "aria-live": "polite"
                })
                .addClass("select2-hidden-accessible")
                .appendTo(document.body);

            this.containerId="s2id_"+(opts.element.attr("id") || "autogen"+nextUid());
            this.containerEventName= this.containerId
                .replace(/([.])/g, '_')
                .replace(/([;&,\-\.\+\*\~':"\!\^#$%@\[\]\(\)=>\|])/g, '\\$1');
            this.container.attr("id", this.containerId);

            this.container.attr("title", opts.element.attr("title"));

            this.body = $("body");

            syncCssClasses(this.container, this.opts.element, this.opts.adaptContainerCssClass);

            this.container.attr("style", opts.element.attr("style"));
            this.container.css(evaluate(opts.containerCss, this.opts.element));
            this.container.addClass(evaluate(opts.containerCssClass, this.opts.element));

            this.elementTabIndex = this.opts.element.attr("tabindex");

            // swap container for the element
            this.opts.element
                .data("select2", this)
                .attr("tabindex", "-1")
                .before(this.container)
                .on("click.select2", killEvent); // do not leak click events

            this.container.data("select2", this);

            this.dropdown = this.container.find(".select2-drop");

            syncCssClasses(this.dropdown, this.opts.element, this.opts.adaptDropdownCssClass);

            this.dropdown.addClass(evaluate(opts.dropdownCssClass, this.opts.element));
            this.dropdown.data("select2", this);
            this.dropdown.on("click", killEvent);

            this.results = results = this.container.find(resultsSelector);
            this.search = search = this.container.find("input.select2-input");

            this.queryCount = 0;
            this.resultsPage = 0;
            this.context = null;

            // initialize the container
            this.initContainer();

            this.container.on("click", killEvent);

            installFilteredMouseMove(this.results);

            this.dropdown.on("mousemove-filtered", resultsSelector, this.bind(this.highlightUnderEvent));
            this.dropdown.on("touchstart touchmove touchend", resultsSelector, this.bind(function (event) {
                this._touchEvent = true;
                this.highlightUnderEvent(event);
            }));
            this.dropdown.on("touchmove", resultsSelector, this.bind(this.touchMoved));
            this.dropdown.on("touchstart touchend", resultsSelector, this.bind(this.clearTouchMoved));

            // Waiting for a click event on touch devices to select option and hide dropdown
            // otherwise click will be triggered on an underlying element
            this.dropdown.on('click', this.bind(function (event) {
                if (this._touchEvent) {
                    this._touchEvent = false;
                    this.selectHighlighted();
                }
            }));

            installDebouncedScroll(80, this.results);
            this.dropdown.on("scroll-debounced", resultsSelector, this.bind(this.loadMoreIfNeeded));

            // do not propagate change event from the search field out of the component
            $(this.container).on("change", ".select2-input", function(e) {e.stopPropagation();});
            $(this.dropdown).on("change", ".select2-input", function(e) {e.stopPropagation();});

            // if jquery.mousewheel plugin is installed we can prevent out-of-bounds scrolling of results via mousewheel
            if ($.fn.mousewheel) {
                results.mousewheel(function (e, delta, deltaX, deltaY) {
                    var top = results.scrollTop();
                    if (deltaY > 0 && top - deltaY <= 0) {
                        results.scrollTop(0);
                        killEvent(e);
                    } else if (deltaY < 0 && results.get(0).scrollHeight - results.scrollTop() + deltaY <= results.height()) {
                        results.scrollTop(results.get(0).scrollHeight - results.height());
                        killEvent(e);
                    }
                });
            }

            installKeyUpChangeEvent(search);
            search.on("keyup-change input paste", this.bind(this.updateResults));
            search.on("focus", function () { search.addClass("select2-focused"); });
            search.on("blur", function () { search.removeClass("select2-focused");});

            this.dropdown.on("mouseup", resultsSelector, this.bind(function (e) {
                if ($(e.target).closest(".select2-result-selectable").length > 0) {
                    this.highlightUnderEvent(e);
                    this.selectHighlighted(e);
                }
            }));

            // trap all mouse events from leaving the dropdown. sometimes there may be a modal that is listening
            // for mouse events outside of itself so it can close itself. since the dropdown is now outside the select2's
            // dom it will trigger the popup close, which is not what we want
            // focusin can cause focus wars between modals and select2 since the dropdown is outside the modal.
            this.dropdown.on("click mouseup mousedown touchstart touchend focusin", function (e) { e.stopPropagation(); });

            this.nextSearchTerm = undefined;

            if ($.isFunction(this.opts.initSelection)) {
                // initialize selection based on the current value of the source element
                this.initSelection();

                // if the user has provided a function that can set selection based on the value of the source element
                // we monitor the change event on the element and trigger it, allowing for two way synchronization
                this.monitorSource();
            }

            if (opts.maximumInputLength !== null) {
                this.search.attr("maxlength", opts.maximumInputLength);
            }

            var disabled = opts.element.prop("disabled");
            if (disabled === undefined) disabled = false;
            this.enable(!disabled);

            var readonly = opts.element.prop("readonly");
            if (readonly === undefined) readonly = false;
            this.readonly(readonly);

            // Calculate size of scrollbar
            scrollBarDimensions = scrollBarDimensions || measureScrollbar();

            this.autofocus = opts.element.prop("autofocus");
            opts.element.prop("autofocus", false);
            if (this.autofocus) this.focus();

            this.search.attr("placeholder", opts.searchInputPlaceholder);
        },

        // abstract
        destroy: function () {
            var element=this.opts.element, select2 = element.data("select2");

            this.close();

            if (element.length && element[0].detachEvent) {
                element.each(function () {
                    this.detachEvent("onpropertychange", this._sync);
                });
            }
            if (this.propertyObserver) {
                this.propertyObserver.disconnect();
                this.propertyObserver = null;
            }
            this._sync = null;

            if (select2 !== undefined) {
                select2.container.remove();
                select2.liveRegion.remove();
                select2.dropdown.remove();
                element
                    .removeClass("select2-offscreen")
                    .removeData("select2")
                    .off(".select2")
                    .prop("autofocus", this.autofocus || false);
                if (this.elementTabIndex) {
                    element.attr({tabindex: this.elementTabIndex});
                } else {
                    element.removeAttr("tabindex");
                }
                element.show();
            }

            cleanupJQueryElements.call(this,
                "container",
                "liveRegion",
                "dropdown",
                "results",
                "search"
            );
        },

        // abstract
        optionToData: function(element) {
            if (element.is("option")) {
                return {
                    id:element.prop("value"),
                    text:element.text(),
                    element: element.get(),
                    css: element.attr("class"),
                    disabled: element.prop("disabled"),
                    locked: equal(element.attr("locked"), "locked") || equal(element.data("locked"), true)
                };
            } else if (element.is("optgroup")) {
                return {
                    text:element.attr("label"),
                    children:[],
                    element: element.get(),
                    css: element.attr("class")
                };
            }
        },

        // abstract
        prepareOpts: function (opts) {
            var element, select, idKey, ajaxUrl, self = this;

            element = opts.element;

            if (element.get(0).tagName.toLowerCase() === "select") {
                this.select = select = opts.element;
            }

            if (select) {
                // these options are not allowed when attached to a select because they are picked up off the element itself
                $.each(["id", "multiple", "ajax", "query", "createSearchChoice", "initSelection", "data", "tags"], function () {
                    if (this in opts) {
                        throw new Error("Option '" + this + "' is not allowed for Select2 when attached to a <select> element.");
                    }
                });
            }

            opts = $.extend({}, {
                populateResults: function(container, results, query) {
                    var populate, id=this.opts.id, liveRegion=this.liveRegion;

                    populate=function(results, container, depth) {

                        var i, l, result, selectable, disabled, compound, node, label, innerContainer, formatted;

                        results = opts.sortResults(results, container, query);

                        // collect the created nodes for bulk append
                        var nodes = [];
                        for (i = 0, l = results.length; i < l; i = i + 1) {

                            result=results[i];

                            disabled = (result.disabled === true);
                            selectable = (!disabled) && (id(result) !== undefined);

                            compound=result.children && result.children.length > 0;

                            node=$("<li></li>");
                            node.addClass("select2-results-dept-"+depth);
                            node.addClass("select2-result");
                            node.addClass(selectable ? "select2-result-selectable" : "select2-result-unselectable");
                            if (disabled) { node.addClass("select2-disabled"); }
                            if (compound) { node.addClass("select2-result-with-children"); }
                            node.addClass(self.opts.formatResultCssClass(result));
                            node.attr("role", "presentation");

                            label=$(document.createElement("div"));
                            label.addClass("select2-result-label");
                            label.attr("id", "select2-result-label-" + nextUid());
                            label.attr("role", "option");

                            formatted=opts.formatResult(result, label, query, self.opts.escapeMarkup);
                            if (formatted!==undefined) {
                                label.html(formatted);
                                node.append(label);
                            }


                            if (compound) {

                                innerContainer=$("<ul></ul>");
                                innerContainer.addClass("select2-result-sub");
                                populate(result.children, innerContainer, depth+1);
                                node.append(innerContainer);
                            }

                            node.data("select2-data", result);
                            nodes.push(node[0]);
                        }

                        // bulk append the created nodes
                        container.append(nodes);
                        liveRegion.text(opts.formatMatches(results.length));
                    };

                    populate(results, container, 0);
                }
            }, $.fn.select2.defaults, opts);

            if (typeof(opts.id) !== "function") {
                idKey = opts.id;
                opts.id = function (e) { return e[idKey]; };
            }

            if ($.isArray(opts.element.data("select2Tags"))) {
                if ("tags" in opts) {
                    throw "tags specified as both an attribute 'data-select2-tags' and in options of Select2 " + opts.element.attr("id");
                }
                opts.tags=opts.element.data("select2Tags");
            }

            if (select) {
                opts.query = this.bind(function (query) {
                    var data = { results: [], more: false },
                        term = query.term,
                        children, placeholderOption, process;

                    process=function(element, collection) {
                        var group;
                        if (element.is("option")) {
                            if (query.matcher(term, element.text(), element)) {
                                collection.push(self.optionToData(element));
                            }
                        } else if (element.is("optgroup")) {
                            group=self.optionToData(element);
                            element.children().each2(function(i, elm) { process(elm, group.children); });
                            if (group.children.length>0) {
                                collection.push(group);
                            }
                        }
                    };

                    children=element.children();

                    // ignore the placeholder option if there is one
                    if (this.getPlaceholder() !== undefined && children.length > 0) {
                        placeholderOption = this.getPlaceholderOption();
                        if (placeholderOption) {
                            children=children.not(placeholderOption);
                        }
                    }

                    children.each2(function(i, elm) { process(elm, data.results); });

                    query.callback(data);
                });
                // this is needed because inside val() we construct choices from options and there id is hardcoded
                opts.id=function(e) { return e.id; };
            } else {
                if (!("query" in opts)) {

                    if ("ajax" in opts) {
                        ajaxUrl = opts.element.data("ajax-url");
                        if (ajaxUrl && ajaxUrl.length > 0) {
                            opts.ajax.url = ajaxUrl;
                        }
                        opts.query = ajax.call(opts.element, opts.ajax);
                    } else if ("data" in opts) {
                        opts.query = local(opts.data);
                    } else if ("tags" in opts) {
                        opts.query = tags(opts.tags);
                        if (opts.createSearchChoice === undefined) {
                            opts.createSearchChoice = function (term) { return {id: $.trim(term), text: $.trim(term)}; };
                        }
                        if (opts.initSelection === undefined) {
                            opts.initSelection = function (element, callback) {
                                var data = [];
                                $(splitVal(element.val(), opts.separator)).each(function () {
                                    var obj = { id: this, text: this },
                                        tags = opts.tags;
                                    if ($.isFunction(tags)) tags=tags();
                                    $(tags).each(function() { if (equal(this.id, obj.id)) { obj = this; return false; } });
                                    data.push(obj);
                                });

                                callback(data);
                            };
                        }
                    }
                }
            }
            if (typeof(opts.query) !== "function") {
                throw "query function not defined for Select2 " + opts.element.attr("id");
            }

            if (opts.createSearchChoicePosition === 'top') {
                opts.createSearchChoicePosition = function(list, item) { list.unshift(item); };
            }
            else if (opts.createSearchChoicePosition === 'bottom') {
                opts.createSearchChoicePosition = function(list, item) { list.push(item); };
            }
            else if (typeof(opts.createSearchChoicePosition) !== "function")  {
                throw "invalid createSearchChoicePosition option must be 'top', 'bottom' or a custom function";
            }

            return opts;
        },

        /**
         * Monitor the original element for changes and update select2 accordingly
         */
        // abstract
        monitorSource: function () {
            var el = this.opts.element, observer, self = this;

            el.on("change.select2", this.bind(function (e) {
                if (this.opts.element.data("select2-change-triggered") !== true) {
                    this.initSelection();
                }
            }));

            this._sync = this.bind(function () {

                // sync enabled state
                var disabled = el.prop("disabled");
                if (disabled === undefined) disabled = false;
                this.enable(!disabled);

                var readonly = el.prop("readonly");
                if (readonly === undefined) readonly = false;
                this.readonly(readonly);

                syncCssClasses(this.container, this.opts.element, this.opts.adaptContainerCssClass);
                this.container.addClass(evaluate(this.opts.containerCssClass, this.opts.element));

                syncCssClasses(this.dropdown, this.opts.element, this.opts.adaptDropdownCssClass);
                this.dropdown.addClass(evaluate(this.opts.dropdownCssClass, this.opts.element));

            });

            // IE8-10 (IE9/10 won't fire propertyChange via attachEventListener)
            if (el.length && el[0].attachEvent) {
                el.each(function() {
                    this.attachEvent("onpropertychange", self._sync);
                });
            }

            // safari, chrome, firefox, IE11
            observer = window.MutationObserver || window.WebKitMutationObserver|| window.MozMutationObserver;
            if (observer !== undefined) {
                if (this.propertyObserver) { delete this.propertyObserver; this.propertyObserver = null; }
                this.propertyObserver = new observer(function (mutations) {
                    $.each(mutations, self._sync);
                });
                this.propertyObserver.observe(el.get(0), { attributes:true, subtree:false });
            }
        },

        // abstract
        triggerSelect: function(data) {
            var evt = $.Event("select2-selecting", { val: this.id(data), object: data, choice: data });
            this.opts.element.trigger(evt);
            return !evt.isDefaultPrevented();
        },

        /**
         * Triggers the change event on the source element
         */
        // abstract
        triggerChange: function (details) {

            details = details || {};
            details= $.extend({}, details, { type: "change", val: this.val() });
            // prevents recursive triggering
            this.opts.element.data("select2-change-triggered", true);
            this.opts.element.trigger(details);
            this.opts.element.data("select2-change-triggered", false);

            // some validation frameworks ignore the change event and listen instead to keyup, click for selects
            // so here we trigger the click event manually
            this.opts.element.click();

            // ValidationEngine ignores the change event and listens instead to blur
            // so here we trigger the blur event manually if so desired
            if (this.opts.blurOnChange)
                this.opts.element.blur();
        },

        //abstract
        isInterfaceEnabled: function()
        {
            return this.enabledInterface === true;
        },

        // abstract
        enableInterface: function() {
            var enabled = this._enabled && !this._readonly,
                disabled = !enabled;

            if (enabled === this.enabledInterface) return false;

            this.container.toggleClass("select2-container-disabled", disabled);
            this.close();
            this.enabledInterface = enabled;

            return true;
        },

        // abstract
        enable: function(enabled) {
            if (enabled === undefined) enabled = true;
            if (this._enabled === enabled) return;
            this._enabled = enabled;

            this.opts.element.prop("disabled", !enabled);
            this.enableInterface();
        },

        // abstract
        disable: function() {
            this.enable(false);
        },

        // abstract
        readonly: function(enabled) {
            if (enabled === undefined) enabled = false;
            if (this._readonly === enabled) return;
            this._readonly = enabled;

            this.opts.element.prop("readonly", enabled);
            this.enableInterface();
        },

        // abstract
        opened: function () {
            return (this.container) ? this.container.hasClass("select2-dropdown-open") : false;
        },

        // abstract
        positionDropdown: function() {
            var $dropdown = this.dropdown,
                offset = this.container.offset(),
                height = this.container.outerHeight(false),
                width = this.container.outerWidth(false),
                dropHeight = $dropdown.outerHeight(false),
                $window = $(window),
                windowWidth = $window.width(),
                windowHeight = $window.height(),
                viewPortRight = $window.scrollLeft() + windowWidth,
                viewportBottom = $window.scrollTop() + windowHeight,
                dropTop = offset.top + height,
                dropLeft = offset.left,
                enoughRoomBelow = dropTop + dropHeight <= viewportBottom,
                enoughRoomAbove = (offset.top - dropHeight) >= $window.scrollTop(),
                dropWidth = $dropdown.outerWidth(false),
                enoughRoomOnRight = dropLeft + dropWidth <= viewPortRight,
                aboveNow = $dropdown.hasClass("select2-drop-above"),
                bodyOffset,
                above,
                changeDirection,
                css,
                resultsListNode;

            // always prefer the current above/below alignment, unless there is not enough room
            if (aboveNow) {
                above = true;
                if (!enoughRoomAbove && enoughRoomBelow) {
                    changeDirection = true;
                    above = false;
                }
            } else {
                above = false;
                if (!enoughRoomBelow && enoughRoomAbove) {
                    changeDirection = true;
                    above = true;
                }
            }

            //if we are changing direction we need to get positions when dropdown is hidden;
            if (changeDirection) {
                $dropdown.hide();
                offset = this.container.offset();
                height = this.container.outerHeight(false);
                width = this.container.outerWidth(false);
                dropHeight = $dropdown.outerHeight(false);
                viewPortRight = $window.scrollLeft() + windowWidth;
                viewportBottom = $window.scrollTop() + windowHeight;
                dropTop = offset.top + height;
                dropLeft = offset.left;
                dropWidth = $dropdown.outerWidth(false);
                enoughRoomOnRight = dropLeft + dropWidth <= viewPortRight;
                $dropdown.show();

                // fix so the cursor does not move to the left within the search-textbox in IE
                this.focusSearch();
            }

            if (this.opts.dropdownAutoWidth) {
                resultsListNode = $('.select2-results', $dropdown)[0];
                $dropdown.addClass('select2-drop-auto-width');
                $dropdown.css('width', '');
                // Add scrollbar width to dropdown if vertical scrollbar is present
                dropWidth = $dropdown.outerWidth(false) + (resultsListNode.scrollHeight === resultsListNode.clientHeight ? 0 : scrollBarDimensions.width);
                dropWidth > width ? width = dropWidth : dropWidth = width;
                dropHeight = $dropdown.outerHeight(false);
                enoughRoomOnRight = dropLeft + dropWidth <= viewPortRight;
            }
            else {
                this.container.removeClass('select2-drop-auto-width');
            }

            //console.log("below/ droptop:", dropTop, "dropHeight", dropHeight, "sum", (dropTop+dropHeight)+" viewport bottom", viewportBottom, "enough?", enoughRoomBelow);
            //console.log("above/ offset.top", offset.top, "dropHeight", dropHeight, "top", (offset.top-dropHeight), "scrollTop", this.body.scrollTop(), "enough?", enoughRoomAbove);

            // fix positioning when body has an offset and is not position: static
            if (this.body.css('position') !== 'static') {
                bodyOffset = this.body.offset();
                dropTop -= bodyOffset.top;
                dropLeft -= bodyOffset.left;
            }

            if (!enoughRoomOnRight) {
                dropLeft = offset.left + this.container.outerWidth(false) - dropWidth;
            }

            css =  {
                left: dropLeft,
                width: width
            };

            if (above) {
                css.top = offset.top - dropHeight;
                css.bottom = 'auto';
                this.container.addClass("select2-drop-above");
                $dropdown.addClass("select2-drop-above");
            }
            else {
                css.top = dropTop;
                css.bottom = 'auto';
                this.container.removeClass("select2-drop-above");
                $dropdown.removeClass("select2-drop-above");
            }
            css = $.extend(css, evaluate(this.opts.dropdownCss, this.opts.element));

            $dropdown.css(css);
        },

        // abstract
        shouldOpen: function() {
            var event;

            if (this.opened()) return false;

            if (this._enabled === false || this._readonly === true) return false;

            event = $.Event("select2-opening");
            this.opts.element.trigger(event);
            return !event.isDefaultPrevented();
        },

        // abstract
        clearDropdownAlignmentPreference: function() {
            // clear the classes used to figure out the preference of where the dropdown should be opened
            this.container.removeClass("select2-drop-above");
            this.dropdown.removeClass("select2-drop-above");
        },

        /**
         * Opens the dropdown
         *
         * @return {Boolean} whether or not dropdown was opened. This method will return false if, for example,
         * the dropdown is already open, or if the 'open' event listener on the element called preventDefault().
         */
        // abstract
        open: function () {

            if (!this.shouldOpen()) return false;

            this.opening();

            // Only bind the document mousemove when the dropdown is visible
            $document.on("mousemove.select2Event", function (e) {
                lastMousePosition.x = e.pageX;
                lastMousePosition.y = e.pageY;
            });

            return true;
        },

        /**
         * Performs the opening of the dropdown
         */
        // abstract
        opening: function() {
            var cid = this.containerEventName,
                scroll = "scroll." + cid,
                resize = "resize."+cid,
                orient = "orientationchange."+cid,
                mask;

            this.container.addClass("select2-dropdown-open").addClass("select2-container-active");

            this.clearDropdownAlignmentPreference();

            if(this.dropdown[0] !== this.body.children().last()[0]) {
                this.dropdown.detach().appendTo(this.body);
            }

            // create the dropdown mask if doesn't already exist
            mask = $("#select2-drop-mask");
            if (mask.length == 0) {
                mask = $(document.createElement("div"));
                mask.attr("id","select2-drop-mask").attr("class","select2-drop-mask");
                mask.hide();
                mask.appendTo(this.body);
                mask.on("mousedown touchstart click", function (e) {
                    // Prevent IE from generating a click event on the body
                    reinsertElement(mask);

                    var dropdown = $("#select2-drop"), self;
                    if (dropdown.length > 0) {
                        self=dropdown.data("select2");
                        if (self.opts.selectOnBlur) {
                            self.selectHighlighted({noFocus: true});
                        }
                        self.close();
                        e.preventDefault();
                        e.stopPropagation();
                    }
                });
            }

            // ensure the mask is always right before the dropdown
            if (this.dropdown.prev()[0] !== mask[0]) {
                this.dropdown.before(mask);
            }

            // move the global id to the correct dropdown
            $("#select2-drop").removeAttr("id");
            this.dropdown.attr("id", "select2-drop");

            // show the elements
            mask.show();

            this.positionDropdown();
            this.dropdown.show();
            this.positionDropdown();

            this.dropdown.addClass("select2-drop-active");

            // attach listeners to events that can change the position of the container and thus require
            // the position of the dropdown to be updated as well so it does not come unglued from the container
            var that = this;
            this.container.parents().add(window).each(function () {
                $(this).on(resize+" "+scroll+" "+orient, function (e) {
                    if (that.opened()) that.positionDropdown();
                });
            });


        },

        // abstract
        close: function () {
            if (!this.opened()) return;

            var cid = this.containerEventName,
                scroll = "scroll." + cid,
                resize = "resize."+cid,
                orient = "orientationchange."+cid;

            // unbind event listeners
            this.container.parents().add(window).each(function () { $(this).off(scroll).off(resize).off(orient); });

            this.clearDropdownAlignmentPreference();

            $("#select2-drop-mask").hide();
            this.dropdown.removeAttr("id"); // only the active dropdown has the select2-drop id
            this.dropdown.hide();
            this.container.removeClass("select2-dropdown-open").removeClass("select2-container-active");
            this.results.empty();

            // Now that the dropdown is closed, unbind the global document mousemove event
            $document.off("mousemove.select2Event");

            this.clearSearch();
            this.search.removeClass("select2-active");
            this.opts.element.trigger($.Event("select2-close"));
        },

        /**
         * Opens control, sets input value, and updates results.
         */
        // abstract
        externalSearch: function (term) {
            this.open();
            this.search.val(term);
            this.updateResults(false);
        },

        // abstract
        clearSearch: function () {

        },

        //abstract
        getMaximumSelectionSize: function() {
            return evaluate(this.opts.maximumSelectionSize, this.opts.element);
        },

        // abstract
        ensureHighlightVisible: function () {
            var results = this.results, children, index, child, hb, rb, y, more, topOffset;

            index = this.highlight();

            if (index < 0) return;

            if (index == 0) {

                // if the first element is highlighted scroll all the way to the top,
                // that way any unselectable headers above it will also be scrolled
                // into view

                results.scrollTop(0);
                return;
            }

            children = this.findHighlightableChoices().find('.select2-result-label');

            child = $(children[index]);

            topOffset = (child.offset() || {}).top || 0;

            hb = topOffset + child.outerHeight(true);

            // if this is the last child lets also make sure select2-more-results is visible
            if (index === children.length - 1) {
                more = results.find("li.select2-more-results");
                if (more.length > 0) {
                    hb = more.offset().top + more.outerHeight(true);
                }
            }

            rb = results.offset().top + results.outerHeight(true);
            if (hb > rb) {
                results.scrollTop(results.scrollTop() + (hb - rb));
            }
            y = topOffset - results.offset().top;

            // make sure the top of the element is visible
            if (y < 0 && child.css('display') != 'none' ) {
                results.scrollTop(results.scrollTop() + y); // y is negative
            }
        },

        // abstract
        findHighlightableChoices: function() {
            return this.results.find(".select2-result-selectable:not(.select2-disabled):not(.select2-selected)");
        },

        // abstract
        moveHighlight: function (delta) {
            var choices = this.findHighlightableChoices(),
                index = this.highlight();

            while (index > -1 && index < choices.length) {
                index += delta;
                var choice = $(choices[index]);
                if (choice.hasClass("select2-result-selectable") && !choice.hasClass("select2-disabled") && !choice.hasClass("select2-selected")) {
                    this.highlight(index);
                    break;
                }
            }
        },

        // abstract
        highlight: function (index) {
            var choices = this.findHighlightableChoices(),
                choice,
                data;

            if (arguments.length === 0) {
                return indexOf(choices.filter(".select2-highlighted")[0], choices.get());
            }

            if (index >= choices.length) index = choices.length - 1;
            if (index < 0) index = 0;

            this.removeHighlight();

            choice = $(choices[index]);
            choice.addClass("select2-highlighted");

            // ensure assistive technology can determine the active choice
            this.search.attr("aria-activedescendant", choice.find(".select2-result-label").attr("id"));

            this.ensureHighlightVisible();

            this.liveRegion.text(choice.text());

            data = choice.data("select2-data");
            if (data) {
                this.opts.element.trigger({ type: "select2-highlight", val: this.id(data), choice: data });
            }
        },

        removeHighlight: function() {
            this.results.find(".select2-highlighted").removeClass("select2-highlighted");
        },

        touchMoved: function() {
            this._touchMoved = true;
        },

        clearTouchMoved: function() {
          this._touchMoved = false;
        },

        // abstract
        countSelectableResults: function() {
            return this.findHighlightableChoices().length;
        },

        // abstract
        highlightUnderEvent: function (event) {
            var el = $(event.target).closest(".select2-result-selectable");
            if (el.length > 0 && !el.is(".select2-highlighted")) {
                var choices = this.findHighlightableChoices();
                this.highlight(choices.index(el));
            } else if (el.length == 0) {
                // if we are over an unselectable item remove all highlights
                this.removeHighlight();
            }
        },

        // abstract
        loadMoreIfNeeded: function () {
            var results = this.results,
                more = results.find("li.select2-more-results"),
                below, // pixels the element is below the scroll fold, below==0 is when the element is starting to be visible
                page = this.resultsPage + 1,
                self=this,
                term=this.search.val(),
                context=this.context;

            if (more.length === 0) return;
            below = more.offset().top - results.offset().top - results.height();

            if (below <= this.opts.loadMorePadding) {
                more.addClass("select2-active");
                this.opts.query({
                        element: this.opts.element,
                        term: term,
                        page: page,
                        context: context,
                        matcher: this.opts.matcher,
                        callback: this.bind(function (data) {

                    // ignore a response if the select2 has been closed before it was received
                    if (!self.opened()) return;


                    self.opts.populateResults.call(this, results, data.results, {term: term, page: page, context:context});
                    self.postprocessResults(data, false, false);

                    if (data.more===true) {
                        more.detach().appendTo(results).text(evaluate(self.opts.formatLoadMore, self.opts.element, page+1));
                        window.setTimeout(function() { self.loadMoreIfNeeded(); }, 10);
                    } else {
                        more.remove();
                    }
                    self.positionDropdown();
                    self.resultsPage = page;
                    self.context = data.context;
                    this.opts.element.trigger({ type: "select2-loaded", items: data });
                })});
            }
        },

        /**
         * Default tokenizer function which does nothing
         */
        tokenize: function() {

        },

        /**
         * @param initial whether or not this is the call to this method right after the dropdown has been opened
         */
        // abstract
        updateResults: function (initial) {
            var search = this.search,
                results = this.results,
                opts = this.opts,
                data,
                self = this,
                input,
                term = search.val(),
                lastTerm = $.data(this.container, "select2-last-term"),
                // sequence number used to drop out-of-order responses
                queryNumber;

            // prevent duplicate queries against the same term
            if (initial !== true && lastTerm && equal(term, lastTerm)) return;

            $.data(this.container, "select2-last-term", term);

            // if the search is currently hidden we do not alter the results
            if (initial !== true && (this.showSearchInput === false || !this.opened())) {
                return;
            }

            function postRender() {
                search.removeClass("select2-active");
                self.positionDropdown();
                if (results.find('.select2-no-results,.select2-selection-limit,.select2-searching').length) {
                    self.liveRegion.text(results.text());
                }
                else {
                    self.liveRegion.text(self.opts.formatMatches(results.find('.select2-result-selectable').length));
                }
            }

            function render(html) {
                results.html(html);
                postRender();
            }

            queryNumber = ++this.queryCount;

            var maxSelSize = this.getMaximumSelectionSize();
            if (maxSelSize >=1) {
                data = this.data();
                if ($.isArray(data) && data.length >= maxSelSize && checkFormatter(opts.formatSelectionTooBig, "formatSelectionTooBig")) {
                    render("<li class='select2-selection-limit'>" + evaluate(opts.formatSelectionTooBig, opts.element, maxSelSize) + "</li>");
                    return;
                }
            }

            if (search.val().length < opts.minimumInputLength) {
                if (checkFormatter(opts.formatInputTooShort, "formatInputTooShort")) {
                    render("<li class='select2-no-results'>" + evaluate(opts.formatInputTooShort, opts.element, search.val(), opts.minimumInputLength) + "</li>");
                } else {
                    render("");
                }
                if (initial && this.showSearch) this.showSearch(true);
                return;
            }

            if (opts.maximumInputLength && search.val().length > opts.maximumInputLength) {
                if (checkFormatter(opts.formatInputTooLong, "formatInputTooLong")) {
                    render("<li class='select2-no-results'>" + evaluate(opts.formatInputTooLong, opts.element, search.val(), opts.maximumInputLength) + "</li>");
                } else {
                    render("");
                }
                return;
            }

            if (opts.formatSearching && this.findHighlightableChoices().length === 0) {
                render("<li class='select2-searching'>" + evaluate(opts.formatSearching, opts.element) + "</li>");
            }

            search.addClass("select2-active");

            this.removeHighlight();

            // give the tokenizer a chance to pre-process the input
            input = this.tokenize();
            if (input != undefined && input != null) {
                search.val(input);
            }

            this.resultsPage = 1;

            opts.query({
                element: opts.element,
                    term: search.val(),
                    page: this.resultsPage,
                    context: null,
                    matcher: opts.matcher,
                    callback: this.bind(function (data) {
                var def; // default choice

                // ignore old responses
                if (queryNumber != this.queryCount) {
                  return;
                }

                // ignore a response if the select2 has been closed before it was received
                if (!this.opened()) {
                    this.search.removeClass("select2-active");
                    return;
                }

                // save context, if any
                this.context = (data.context===undefined) ? null : data.context;
                // create a default choice and prepend it to the list
                if (this.opts.createSearchChoice && search.val() !== "") {
                    def = this.opts.createSearchChoice.call(self, search.val(), data.results);
                    if (def !== undefined && def !== null && self.id(def) !== undefined && self.id(def) !== null) {
                        if ($(data.results).filter(
                            function () {
                                return equal(self.id(this), self.id(def));
                            }).length === 0) {
                            this.opts.createSearchChoicePosition(data.results, def);
                        }
                    }
                }

                if (data.results.length === 0 && checkFormatter(opts.formatNoMatches, "formatNoMatches")) {
                    render("<li class='select2-no-results'>" + evaluate(opts.formatNoMatches, opts.element, search.val()) + "</li>");
                    return;
                }

                results.empty();
                self.opts.populateResults.call(this, results, data.results, {term: search.val(), page: this.resultsPage, context:null});

                if (data.more === true && checkFormatter(opts.formatLoadMore, "formatLoadMore")) {
                    results.append("<li class='select2-more-results'>" + opts.escapeMarkup(evaluate(opts.formatLoadMore, opts.element, this.resultsPage)) + "</li>");
                    window.setTimeout(function() { self.loadMoreIfNeeded(); }, 10);
                }

                this.postprocessResults(data, initial);

                postRender();

                this.opts.element.trigger({ type: "select2-loaded", items: data });
            })});
        },

        // abstract
        cancel: function () {
            this.close();
        },

        // abstract
        blur: function () {
            // if selectOnBlur == true, select the currently highlighted option
            if (this.opts.selectOnBlur)
                this.selectHighlighted({noFocus: true});

            this.close();
            this.container.removeClass("select2-container-active");
            // synonymous to .is(':focus'), which is available in jquery >= 1.6
            if (this.search[0] === document.activeElement) { this.search.blur(); }
            this.clearSearch();
            this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus");
        },

        // abstract
        focusSearch: function () {
            focus(this.search);
        },

        // abstract
        selectHighlighted: function (options) {
            if (this._touchMoved) {
              this.clearTouchMoved();
              return;
            }
            var index=this.highlight(),
                highlighted=this.results.find(".select2-highlighted"),
                data = highlighted.closest('.select2-result').data("select2-data");

            if (data) {
                this.highlight(index);
                this.onSelect(data, options);
            } else if (options && options.noFocus) {
                this.close();
            }
        },

        // abstract
        getPlaceholder: function () {
            var placeholderOption;
            return this.opts.element.attr("placeholder") ||
                this.opts.element.attr("data-placeholder") || // jquery 1.4 compat
                this.opts.element.data("placeholder") ||
                this.opts.placeholder ||
                ((placeholderOption = this.getPlaceholderOption()) !== undefined ? placeholderOption.text() : undefined);
        },

        // abstract
        getPlaceholderOption: function() {
            if (this.select) {
                var firstOption = this.select.children('option').first();
                if (this.opts.placeholderOption !== undefined ) {
                    //Determine the placeholder option based on the specified placeholderOption setting
                    return (this.opts.placeholderOption === "first" && firstOption) ||
                           (typeof this.opts.placeholderOption === "function" && this.opts.placeholderOption(this.select));
                } else if ($.trim(firstOption.text()) === "" && firstOption.val() === "") {
                    //No explicit placeholder option specified, use the first if it's blank
                    return firstOption;
                }
            }
        },

        /**
         * Get the desired width for the container element.  This is
         * derived first from option `width` passed to select2, then
         * the inline 'style' on the original element, and finally
         * falls back to the jQuery calculated element width.
         */
        // abstract
        initContainerWidth: function () {
            function resolveContainerWidth() {
                var style, attrs, matches, i, l, attr;

                if (this.opts.width === "off") {
                    return null;
                } else if (this.opts.width === "element"){
                    return this.opts.element.outerWidth(false) === 0 ? 'auto' : this.opts.element.outerWidth(false) + 'px';
                } else if (this.opts.width === "copy" || this.opts.width === "resolve") {
                    // check if there is inline style on the element that contains width
                    style = this.opts.element.attr('style');
                    if (style !== undefined) {
                        attrs = style.split(';');
                        for (i = 0, l = attrs.length; i < l; i = i + 1) {
                            attr = attrs[i].replace(/\s/g, '');
                            matches = attr.match(/^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i);
                            if (matches !== null && matches.length >= 1)
                                return matches[1];
                        }
                    }

                    if (this.opts.width === "resolve") {
                        // next check if css('width') can resolve a width that is percent based, this is sometimes possible
                        // when attached to input type=hidden or elements hidden via css
                        style = this.opts.element.css('width');
                        if (style.indexOf("%") > 0) return style;

                        // finally, fallback on the calculated width of the element
                        return (this.opts.element.outerWidth(false) === 0 ? 'auto' : this.opts.element.outerWidth(false) + 'px');
                    }

                    return null;
                } else if ($.isFunction(this.opts.width)) {
                    return this.opts.width();
                } else {
                    return this.opts.width;
               }
            };

            var width = resolveContainerWidth.call(this);
            if (width !== null) {
                this.container.css("width", width);
            }
        }
    });

    SingleSelect2 = clazz(AbstractSelect2, {

        // single

        createContainer: function () {
            var container = $(document.createElement("div")).attr({
                "class": "select2-container"
            }).html([
                "<a href='javascript:void(0)' class='select2-choice' tabindex='-1'>",
                "   <span class='select2-chosen'>&#160;</span><abbr class='select2-search-choice-close'></abbr>",
                "   <span class='select2-arrow' role='presentation'><b role='presentation'></b></span>",
                "</a>",
                "<label for='' class='select2-offscreen'></label>",
                "<input class='select2-focusser select2-offscreen' type='text' aria-haspopup='true' role='button' />",
                "<div class='select2-drop select2-display-none'>",
                "   <div class='select2-search'>",
                "       <label for='' class='select2-offscreen'></label>",
                "       <input type='text' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' class='select2-input' role='combobox' aria-expanded='true'",
                "       aria-autocomplete='list' />",
                "   </div>",
                "   <ul class='select2-results' role='listbox'>",
                "   </ul>",
                "</div>"].join(""));
            return container;
        },

        // single
        enableInterface: function() {
            if (this.parent.enableInterface.apply(this, arguments)) {
                this.focusser.prop("disabled", !this.isInterfaceEnabled());
            }
        },

        // single
        opening: function () {
            var el, range, len;

            if (this.opts.minimumResultsForSearch >= 0) {
                this.showSearch(true);
            }

            this.parent.opening.apply(this, arguments);

            if (this.showSearchInput !== false) {
                // IE appends focusser.val() at the end of field :/ so we manually insert it at the beginning using a range
                // all other browsers handle this just fine

                this.search.val(this.focusser.val());
            }
            if (this.opts.shouldFocusInput(this)) {
                this.search.focus();
                // move the cursor to the end after focussing, otherwise it will be at the beginning and
                // new text will appear *before* focusser.val()
                el = this.search.get(0);
                if (el.createTextRange) {
                    range = el.createTextRange();
                    range.collapse(false);
                    range.select();
                } else if (el.setSelectionRange) {
                    len = this.search.val().length;
                    el.setSelectionRange(len, len);
                }
            }

            // initializes search's value with nextSearchTerm (if defined by user)
            // ignore nextSearchTerm if the dropdown is opened by the user pressing a letter
            if(this.search.val() === "") {
                if(this.nextSearchTerm != undefined){
                    this.search.val(this.nextSearchTerm);
                    this.search.select();
                }
            }

            this.focusser.prop("disabled", true).val("");
            this.updateResults(true);
            this.opts.element.trigger($.Event("select2-open"));
        },

        // single
        close: function () {
            if (!this.opened()) return;
            this.parent.close.apply(this, arguments);

            this.focusser.prop("disabled", false);

            if (this.opts.shouldFocusInput(this)) {
                this.focusser.focus();
            }
        },

        // single
        focus: function () {
            if (this.opened()) {
                this.close();
            } else {
                this.focusser.prop("disabled", false);
                if (this.opts.shouldFocusInput(this)) {
                    this.focusser.focus();
                }
            }
        },

        // single
        isFocused: function () {
            return this.container.hasClass("select2-container-active");
        },

        // single
        cancel: function () {
            this.parent.cancel.apply(this, arguments);
            this.focusser.prop("disabled", false);

            if (this.opts.shouldFocusInput(this)) {
                this.focusser.focus();
            }
        },

        // single
        destroy: function() {
            $("label[for='" + this.focusser.attr('id') + "']")
                .attr('for', this.opts.element.attr("id"));
            this.parent.destroy.apply(this, arguments);

            cleanupJQueryElements.call(this,
                "selection",
                "focusser"
            );
        },

        // single
        initContainer: function () {

            var selection,
                container = this.container,
                dropdown = this.dropdown,
                idSuffix = nextUid(),
                elementLabel;

            if (this.opts.minimumResultsForSearch < 0) {
                this.showSearch(false);
            } else {
                this.showSearch(true);
            }

            this.selection = selection = container.find(".select2-choice");

            this.focusser = container.find(".select2-focusser");

            // add aria associations
            selection.find(".select2-chosen").attr("id", "select2-chosen-"+idSuffix);
            this.focusser.attr("aria-labelledby", "select2-chosen-"+idSuffix);
            this.results.attr("id", "select2-results-"+idSuffix);
            this.search.attr("aria-owns", "select2-results-"+idSuffix);

            // rewrite labels from original element to focusser
            this.focusser.attr("id", "s2id_autogen"+idSuffix);

            elementLabel = $("label[for='" + this.opts.element.attr("id") + "']");

            this.focusser.prev()
                .text(elementLabel.text())
                .attr('for', this.focusser.attr('id'));

            // Ensure the original element retains an accessible name
            var originalTitle = this.opts.element.attr("title");
            this.opts.element.attr("title", (originalTitle || elementLabel.text()));

            this.focusser.attr("tabindex", this.elementTabIndex);

            // write label for search field using the label from the focusser element
            this.search.attr("id", this.focusser.attr('id') + '_search');

            this.search.prev()
                .text($("label[for='" + this.focusser.attr('id') + "']").text())
                .attr('for', this.search.attr('id'));

            this.search.on("keydown", this.bind(function (e) {
                if (!this.isInterfaceEnabled()) return;

                if (e.which === KEY.PAGE_UP || e.which === KEY.PAGE_DOWN) {
                    // prevent the page from scrolling
                    killEvent(e);
                    return;
                }

                switch (e.which) {
                    case KEY.UP:
                    case KEY.DOWN:
                        this.moveHighlight((e.which === KEY.UP) ? -1 : 1);
                        killEvent(e);
                        return;
                    case KEY.ENTER:
                        this.selectHighlighted();
                        killEvent(e);
                        return;
                    case KEY.TAB:
                        this.selectHighlighted({noFocus: true});
                        return;
                    case KEY.ESC:
                        this.cancel(e);
                        killEvent(e);
                        return;
                }
            }));

            this.search.on("blur", this.bind(function(e) {
                // a workaround for chrome to keep the search field focussed when the scroll bar is used to scroll the dropdown.
                // without this the search field loses focus which is annoying
                if (document.activeElement === this.body.get(0)) {
                    window.setTimeout(this.bind(function() {
                        if (this.opened()) {
                            this.search.focus();
                        }
                    }), 0);
                }
            }));

            this.focusser.on("keydown", this.bind(function (e) {
                if (!this.isInterfaceEnabled()) return;

                if (e.which === KEY.TAB || KEY.isControl(e) || KEY.isFunctionKey(e) || e.which === KEY.ESC) {
                    return;
                }

                if (this.opts.openOnEnter === false && e.which === KEY.ENTER) {
                    killEvent(e);
                    return;
                }

                if (e.which == KEY.DOWN || e.which == KEY.UP
                    || (e.which == KEY.ENTER && this.opts.openOnEnter)) {

                    if (e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) return;

                    this.open();
                    killEvent(e);
                    return;
                }

                if (e.which == KEY.DELETE || e.which == KEY.BACKSPACE) {
                    if (this.opts.allowClear) {
                        this.clear();
                    }
                    killEvent(e);
                    return;
                }
            }));


            installKeyUpChangeEvent(this.focusser);
            this.focusser.on("keyup-change input", this.bind(function(e) {
                if (this.opts.minimumResultsForSearch >= 0) {
                    e.stopPropagation();
                    if (this.opened()) return;
                    this.open();
                }
            }));

            selection.on("mousedown touchstart", "abbr", this.bind(function (e) {
                if (!this.isInterfaceEnabled()) return;
                this.selection.focus();
                this.clear();
                killEventImmediately(e);
                this.close();
            }));

            selection.on("mousedown touchstart", this.bind(function (e) {
                // Prevent IE from generating a click event on the body
                reinsertElement(selection);

                if (!this.container.hasClass("select2-container-active")) {
                    this.opts.element.trigger($.Event("select2-focus"));
                }

                if (this.opened()) {
                    this.close();
                } else if (this.isInterfaceEnabled()) {
                    this.open();
                }

                killEvent(e);
            }));

            dropdown.on("mousedown touchstart", this.bind(function() {
                if (this.opts.shouldFocusInput(this)) {
                    this.search.focus();
                }
            }));

            selection.on("focus", this.bind(function(e) {
                killEvent(e);
            }));

            this.focusser.on("focus", this.bind(function(){
                if (!this.container.hasClass("select2-container-active")) {
                    this.opts.element.trigger($.Event("select2-focus"));
                }
                this.container.addClass("select2-container-active");
            })).on("blur", this.bind(function() {
                if (!this.opened()) {
                    this.container.removeClass("select2-container-active");
                    this.opts.element.trigger($.Event("select2-blur"));
                }
            }));
            this.search.on("focus", this.bind(function(){
                if (!this.container.hasClass("select2-container-active")) {
                    this.opts.element.trigger($.Event("select2-focus"));
                }
                this.container.addClass("select2-container-active");
            }));

            this.initContainerWidth();
            this.opts.element.addClass("select2-offscreen");
            this.setPlaceholder();

        },

        // single
        clear: function(triggerChange) {
            var data=this.selection.data("select2-data");
            if (data) { // guard against queued quick consecutive clicks
                var evt = $.Event("select2-clearing");
                this.opts.element.trigger(evt);
                if (evt.isDefaultPrevented()) {
                    return;
                }
                var placeholderOption = this.getPlaceholderOption();
                this.opts.element.val(placeholderOption ? placeholderOption.val() : "");
                this.selection.find(".select2-chosen").empty();
                this.selection.removeData("select2-data");
                this.setPlaceholder();

                if (triggerChange !== false){
                    this.opts.element.trigger({ type: "select2-removed", val: this.id(data), choice: data });
                    this.triggerChange({removed:data});
                }
            }
        },

        /**
         * Sets selection based on source element's value
         */
        // single
        initSelection: function () {
            var selected;
            if (this.isPlaceholderOptionSelected()) {
                this.updateSelection(null);
                this.close();
                this.setPlaceholder();
            } else {
                var self = this;
                this.opts.initSelection.call(null, this.opts.element, function(selected){
                    if (selected !== undefined && selected !== null) {
                        self.updateSelection(selected);
                        self.close();
                        self.setPlaceholder();
                        self.nextSearchTerm = self.opts.nextSearchTerm(selected, self.search.val());
                    }
                });
            }
        },

        isPlaceholderOptionSelected: function() {
            var placeholderOption;
            if (this.getPlaceholder() === undefined) return false; // no placeholder specified so no option should be considered
            return ((placeholderOption = this.getPlaceholderOption()) !== undefined && placeholderOption.prop("selected"))
                || (this.opts.element.val() === "")
                || (this.opts.element.val() === undefined)
                || (this.opts.element.val() === null);
        },

        // single
        prepareOpts: function () {
            var opts = this.parent.prepareOpts.apply(this, arguments),
                self=this;

            if (opts.element.get(0).tagName.toLowerCase() === "select") {
                // install the selection initializer
                opts.initSelection = function (element, callback) {
                    var selected = element.find("option").filter(function() { return this.selected && !this.disabled });
                    // a single select box always has a value, no need to null check 'selected'
                    callback(self.optionToData(selected));
                };
            } else if ("data" in opts) {
                // install default initSelection when applied to hidden input and data is local
                opts.initSelection = opts.initSelection || function (element, callback) {
                    var id = element.val();
                    //search in data by id, storing the actual matching item
                    var match = null;
                    opts.query({
                        matcher: function(term, text, el){
                            var is_match = equal(id, opts.id(el));
                            if (is_match) {
                                match = el;
                            }
                            return is_match;
                        },
                        callback: !$.isFunction(callback) ? $.noop : function() {
                            callback(match);
                        }
                    });
                };
            }

            return opts;
        },

        // single
        getPlaceholder: function() {
            // if a placeholder is specified on a single select without a valid placeholder option ignore it
            if (this.select) {
                if (this.getPlaceholderOption() === undefined) {
                    return undefined;
                }
            }

            return this.parent.getPlaceholder.apply(this, arguments);
        },

        // single
        setPlaceholder: function () {
            var placeholder = this.getPlaceholder();

            if (this.isPlaceholderOptionSelected() && placeholder !== undefined) {

                // check for a placeholder option if attached to a select
                if (this.select && this.getPlaceholderOption() === undefined) return;

                this.selection.find(".select2-chosen").html(this.opts.escapeMarkup(placeholder));

                this.selection.addClass("select2-default");

                this.container.removeClass("select2-allowclear");
            }
        },

        // single
        postprocessResults: function (data, initial, noHighlightUpdate) {
            var selected = 0, self = this, showSearchInput = true;

            // find the selected element in the result list

            this.findHighlightableChoices().each2(function (i, elm) {
                if (equal(self.id(elm.data("select2-data")), self.opts.element.val())) {
                    selected = i;
                    return false;
                }
            });

            // and highlight it
            if (noHighlightUpdate !== false) {
                if (initial === true && selected >= 0) {
                    this.highlight(selected);
                } else {
                    this.highlight(0);
                }
            }

            // hide the search box if this is the first we got the results and there are enough of them for search

            if (initial === true) {
                var min = this.opts.minimumResultsForSearch;
                if (min >= 0) {
                    this.showSearch(countResults(data.results) >= min);
                }
            }
        },

        // single
        showSearch: function(showSearchInput) {
            if (this.showSearchInput === showSearchInput) return;

            this.showSearchInput = showSearchInput;

            this.dropdown.find(".select2-search").toggleClass("select2-search-hidden", !showSearchInput);
            this.dropdown.find(".select2-search").toggleClass("select2-offscreen", !showSearchInput);
            //add "select2-with-searchbox" to the container if search box is shown
            $(this.dropdown, this.container).toggleClass("select2-with-searchbox", showSearchInput);
        },

        // single
        onSelect: function (data, options) {

            if (!this.triggerSelect(data)) { return; }

            var old = this.opts.element.val(),
                oldData = this.data();

            this.opts.element.val(this.id(data));
            this.updateSelection(data);

            this.opts.element.trigger({ type: "select2-selected", val: this.id(data), choice: data });

            this.nextSearchTerm = this.opts.nextSearchTerm(data, this.search.val());
            this.close();

            if ((!options || !options.noFocus) && this.opts.shouldFocusInput(this)) {
                this.focusser.focus();
            }

            if (!equal(old, this.id(data))) {
                this.triggerChange({ added: data, removed: oldData });
            }
        },

        // single
        updateSelection: function (data) {

            var container=this.selection.find(".select2-chosen"), formatted, cssClass;

            this.selection.data("select2-data", data);

            container.empty();
            if (data !== null) {
                formatted=this.opts.formatSelection(data, container, this.opts.escapeMarkup);
            }
            if (formatted !== undefined) {
                container.append(formatted);
            }
            cssClass=this.opts.formatSelectionCssClass(data, container);
            if (cssClass !== undefined) {
                container.addClass(cssClass);
            }

            this.selection.removeClass("select2-default");

            if (this.opts.allowClear && this.getPlaceholder() !== undefined) {
                this.container.addClass("select2-allowclear");
            }
        },

        // single
        val: function () {
            var val,
                triggerChange = false,
                data = null,
                self = this,
                oldData = this.data();

            if (arguments.length === 0) {
                return this.opts.element.val();
            }

            val = arguments[0];

            if (arguments.length > 1) {
                triggerChange = arguments[1];
            }

            if (this.select) {
                this.select
                    .val(val)
                    .find("option").filter(function() { return this.selected }).each2(function (i, elm) {
                        data = self.optionToData(elm);
                        return false;
                    });
                this.updateSelection(data);
                this.setPlaceholder();
                if (triggerChange) {
                    this.triggerChange({added: data, removed:oldData});
                }
            } else {
                // val is an id. !val is true for [undefined,null,'',0] - 0 is legal
                if (!val && val !== 0) {
                    this.clear(triggerChange);
                    return;
                }
                if (this.opts.initSelection === undefined) {
                    throw new Error("cannot call val() if initSelection() is not defined");
                }
                this.opts.element.val(val);
                this.opts.initSelection(this.opts.element, function(data){
                    self.opts.element.val(!data ? "" : self.id(data));
                    self.updateSelection(data);
                    self.setPlaceholder();
                    if (triggerChange) {
                        self.triggerChange({added: data, removed:oldData});
                    }
                });
            }
        },

        // single
        clearSearch: function () {
            this.search.val("");
            this.focusser.val("");
        },

        // single
        data: function(value) {
            var data,
                triggerChange = false;

            if (arguments.length === 0) {
                data = this.selection.data("select2-data");
                if (data == undefined) data = null;
                return data;
            } else {
                if (arguments.length > 1) {
                    triggerChange = arguments[1];
                }
                if (!value) {
                    this.clear(triggerChange);
                } else {
                    data = this.data();
                    this.opts.element.val(!value ? "" : this.id(value));
                    this.updateSelection(value);
                    if (triggerChange) {
                        this.triggerChange({added: value, removed:data});
                    }
                }
            }
        }
    });

    MultiSelect2 = clazz(AbstractSelect2, {

        // multi
        createContainer: function () {
            var container = $(document.createElement("div")).attr({
                "class": "select2-container select2-container-multi"
            }).html([
                "<ul class='select2-choices'>",
                "  <li class='select2-search-field'>",
                "    <label for='' class='select2-offscreen'></label>",
                "    <input type='text' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' class='select2-input'>",
                "  </li>",
                "</ul>",
                "<div class='select2-drop select2-drop-multi select2-display-none'>",
                "   <ul class='select2-results'>",
                "   </ul>",
                "</div>"].join(""));
            return container;
        },

        // multi
        prepareOpts: function () {
            var opts = this.parent.prepareOpts.apply(this, arguments),
                self=this;

            // TODO validate placeholder is a string if specified

            if (opts.element.get(0).tagName.toLowerCase() === "select") {
                // install the selection initializer
                opts.initSelection = function (element, callback) {

                    var data = [];

                    element.find("option").filter(function() { return this.selected && !this.disabled }).each2(function (i, elm) {
                        data.push(self.optionToData(elm));
                    });
                    callback(data);
                };
            } else if ("data" in opts) {
                // install default initSelection when applied to hidden input and data is local
                opts.initSelection = opts.initSelection || function (element, callback) {
                    var ids = splitVal(element.val(), opts.separator);
                    //search in data by array of ids, storing matching items in a list
                    var matches = [];
                    opts.query({
                        matcher: function(term, text, el){
                            var is_match = $.grep(ids, function(id) {
                                return equal(id, opts.id(el));
                            }).length;
                            if (is_match) {
                                matches.push(el);
                            }
                            return is_match;
                        },
                        callback: !$.isFunction(callback) ? $.noop : function() {
                            // reorder matches based on the order they appear in the ids array because right now
                            // they are in the order in which they appear in data array
                            var ordered = [];
                            for (var i = 0; i < ids.length; i++) {
                                var id = ids[i];
                                for (var j = 0; j < matches.length; j++) {
                                    var match = matches[j];
                                    if (equal(id, opts.id(match))) {
                                        ordered.push(match);
                                        matches.splice(j, 1);
                                        break;
                                    }
                                }
                            }
                            callback(ordered);
                        }
                    });
                };
            }

            return opts;
        },

        // multi
        selectChoice: function (choice) {

            var selected = this.container.find(".select2-search-choice-focus");
            if (selected.length && choice && choice[0] == selected[0]) {

            } else {
                if (selected.length) {
                    this.opts.element.trigger("choice-deselected", selected);
                }
                selected.removeClass("select2-search-choice-focus");
                if (choice && choice.length) {
                    this.close();
                    choice.addClass("select2-search-choice-focus");
                    this.opts.element.trigger("choice-selected", choice);
                }
            }
        },

        // multi
        destroy: function() {
            $("label[for='" + this.search.attr('id') + "']")
                .attr('for', this.opts.element.attr("id"));
            this.parent.destroy.apply(this, arguments);

            cleanupJQueryElements.call(this,
                "searchContainer",
                "selection"
            );
        },

        // multi
        initContainer: function () {

            var selector = ".select2-choices", selection;

            this.searchContainer = this.container.find(".select2-search-field");
            this.selection = selection = this.container.find(selector);

            var _this = this;
            this.selection.on("click", ".select2-search-choice:not(.select2-locked)", function (e) {
                //killEvent(e);
                _this.search[0].focus();
                _this.selectChoice($(this));
            });

            // rewrite labels from original element to focusser
            this.search.attr("id", "s2id_autogen"+nextUid());

            this.search.prev()
                .text($("label[for='" + this.opts.element.attr("id") + "']").text())
                .attr('for', this.search.attr('id'));

            this.search.on("input paste", this.bind(function() {
                if (this.search.attr('placeholder') && this.search.val().length == 0) return;
                if (!this.isInterfaceEnabled()) return;
                if (!this.opened()) {
                    this.open();
                }
            }));

            this.search.attr("tabindex", this.elementTabIndex);

            this.keydowns = 0;
            this.search.on("keydown", this.bind(function (e) {
                if (!this.isInterfaceEnabled()) return;

                ++this.keydowns;
                var selected = selection.find(".select2-search-choice-focus");
                var prev = selected.prev(".select2-search-choice:not(.select2-locked)");
                var next = selected.next(".select2-search-choice:not(.select2-locked)");
                var pos = getCursorInfo(this.search);

                if (selected.length &&
                    (e.which == KEY.LEFT || e.which == KEY.RIGHT || e.which == KEY.BACKSPACE || e.which == KEY.DELETE || e.which == KEY.ENTER)) {
                    var selectedChoice = selected;
                    if (e.which == KEY.LEFT && prev.length) {
                        selectedChoice = prev;
                    }
                    else if (e.which == KEY.RIGHT) {
                        selectedChoice = next.length ? next : null;
                    }
                    else if (e.which === KEY.BACKSPACE) {
                        if (this.unselect(selected.first())) {
                            this.search.width(10);
                            selectedChoice = prev.length ? prev : next;
                        }
                    } else if (e.which == KEY.DELETE) {
                        if (this.unselect(selected.first())) {
                            this.search.width(10);
                            selectedChoice = next.length ? next : null;
                        }
                    } else if (e.which == KEY.ENTER) {
                        selectedChoice = null;
                    }

                    this.selectChoice(selectedChoice);
                    killEvent(e);
                    if (!selectedChoice || !selectedChoice.length) {
                        this.open();
                    }
                    return;
                } else if (((e.which === KEY.BACKSPACE && this.keydowns == 1)
                    || e.which == KEY.LEFT) && (pos.offset == 0 && !pos.length)) {

                    this.selectChoice(selection.find(".select2-search-choice:not(.select2-locked)").last());
                    killEvent(e);
                    return;
                } else {
                    this.selectChoice(null);
                }

                if (this.opened()) {
                    switch (e.which) {
                    case KEY.UP:
                    case KEY.DOWN:
                        this.moveHighlight((e.which === KEY.UP) ? -1 : 1);
                        killEvent(e);
                        return;
                    case KEY.ENTER:
                        this.selectHighlighted();
                        killEvent(e);
                        return;
                    case KEY.TAB:
                        this.selectHighlighted({noFocus:true});
                        this.close();
                        return;
                    case KEY.ESC:
                        this.cancel(e);
                        killEvent(e);
                        return;
                    }
                }

                if (e.which === KEY.TAB || KEY.isControl(e) || KEY.isFunctionKey(e)
                 || e.which === KEY.BACKSPACE || e.which === KEY.ESC) {
                    return;
                }

                if (e.which === KEY.ENTER) {
                    if (this.opts.openOnEnter === false) {
                        return;
                    } else if (e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) {
                        return;
                    }
                }

                this.open();

                if (e.which === KEY.PAGE_UP || e.which === KEY.PAGE_DOWN) {
                    // prevent the page from scrolling
                    killEvent(e);
                }

                if (e.which === KEY.ENTER) {
                    // prevent form from being submitted
                    killEvent(e);
                }

            }));

            this.search.on("keyup", this.bind(function (e) {
                this.keydowns = 0;
                this.resizeSearch();
            })
            );

            this.search.on("blur", this.bind(function(e) {
                this.container.removeClass("select2-container-active");
                this.search.removeClass("select2-focused");
                this.selectChoice(null);
                if (!this.opened()) this.clearSearch();
                e.stopImmediatePropagation();
                this.opts.element.trigger($.Event("select2-blur"));
            }));

            this.container.on("click", selector, this.bind(function (e) {
                if (!this.isInterfaceEnabled()) return;
                if ($(e.target).closest(".select2-search-choice").length > 0) {
                    // clicked inside a select2 search choice, do not open
                    return;
                }
                this.selectChoice(null);
                this.clearPlaceholder();
                if (!this.container.hasClass("select2-container-active")) {
                    this.opts.element.trigger($.Event("select2-focus"));
                }
                this.open();
                this.focusSearch();
                e.preventDefault();
            }));

            this.container.on("focus", selector, this.bind(function () {
                if (!this.isInterfaceEnabled()) return;
                if (!this.container.hasClass("select2-container-active")) {
                    this.opts.element.trigger($.Event("select2-focus"));
                }
                this.container.addClass("select2-container-active");
                this.dropdown.addClass("select2-drop-active");
                this.clearPlaceholder();
            }));

            this.initContainerWidth();
            this.opts.element.addClass("select2-offscreen");

            // set the placeholder if necessary
            this.clearSearch();
        },

        // multi
        enableInterface: function() {
            if (this.parent.enableInterface.apply(this, arguments)) {
                this.search.prop("disabled", !this.isInterfaceEnabled());
            }
        },

        // multi
        initSelection: function () {
            var data;
            if (this.opts.element.val() === "" && this.opts.element.text() === "") {
                this.updateSelection([]);
                this.close();
                // set the placeholder if necessary
                this.clearSearch();
            }
            if (this.select || this.opts.element.val() !== "") {
                var self = this;
                this.opts.initSelection.call(null, this.opts.element, function(data){
                    if (data !== undefined && data !== null) {
                        self.updateSelection(data);
                        self.close();
                        // set the placeholder if necessary
                        self.clearSearch();
                    }
                });
            }
        },

        // multi
        clearSearch: function () {
            var placeholder = this.getPlaceholder(),
                maxWidth = this.getMaxSearchWidth();

            if (placeholder !== undefined  && this.getVal().length === 0 && this.search.hasClass("select2-focused") === false) {
                this.search.val(placeholder).addClass("select2-default");
                // stretch the search box to full width of the container so as much of the placeholder is visible as possible
                // we could call this.resizeSearch(), but we do not because that requires a sizer and we do not want to create one so early because of a firefox bug, see #944
                this.search.width(maxWidth > 0 ? maxWidth : this.container.css("width"));
            } else {
                this.search.val("").width(10);
            }
        },

        // multi
        clearPlaceholder: function () {
            if (this.search.hasClass("select2-default")) {
                this.search.val("").removeClass("select2-default");
            }
        },

        // multi
        opening: function () {
            this.clearPlaceholder(); // should be done before super so placeholder is not used to search
            this.resizeSearch();

            this.parent.opening.apply(this, arguments);

            this.focusSearch();

            // initializes search's value with nextSearchTerm (if defined by user)
            // ignore nextSearchTerm if the dropdown is opened by the user pressing a letter
            if(this.search.val() === "") {
                if(this.nextSearchTerm != undefined){
                    this.search.val(this.nextSearchTerm);
                    this.search.select();
                }
            }

            this.updateResults(true);
            if (this.opts.shouldFocusInput(this)) {
                this.search.focus();
            }
            this.opts.element.trigger($.Event("select2-open"));
        },

        // multi
        close: function () {
            if (!this.opened()) return;
            this.parent.close.apply(this, arguments);
        },

        // multi
        focus: function () {
            this.close();
            this.search.focus();
        },

        // multi
        isFocused: function () {
            return this.search.hasClass("select2-focused");
        },

        // multi
        updateSelection: function (data) {
            var ids = [], filtered = [], self = this;

            // filter out duplicates
            $(data).each(function () {
                if (indexOf(self.id(this), ids) < 0) {
                    ids.push(self.id(this));
                    filtered.push(this);
                }
            });
            data = filtered;

            this.selection.find(".select2-search-choice").remove();
            $(data).each(function () {
                self.addSelectedChoice(this);
            });
            self.postprocessResults();
        },

        // multi
        tokenize: function() {
            var input = this.search.val();
            input = this.opts.tokenizer.call(this, input, this.data(), this.bind(this.onSelect), this.opts);
            if (input != null && input != undefined) {
                this.search.val(input);
                if (input.length > 0) {
                    this.open();
                }
            }

        },

        // multi
        onSelect: function (data, options) {

            if (!this.triggerSelect(data)) { return; }

            this.addSelectedChoice(data);

            this.opts.element.trigger({ type: "selected", val: this.id(data), choice: data });

            // keep track of the search's value before it gets cleared
            this.nextSearchTerm = this.opts.nextSearchTerm(data, this.search.val());

            this.clearSearch();
            this.updateResults();

            if (this.select || !this.opts.closeOnSelect) this.postprocessResults(data, false, this.opts.closeOnSelect===true);

            if (this.opts.closeOnSelect) {
                this.close();
                this.search.width(10);
            } else {
                if (this.countSelectableResults()>0) {
                    this.search.width(10);
                    this.resizeSearch();
                    if (this.getMaximumSelectionSize() > 0 && this.val().length >= this.getMaximumSelectionSize()) {
                        // if we reached max selection size repaint the results so choices
                        // are replaced with the max selection reached message
                        this.updateResults(true);
                    } else {
                        // initializes search's value with nextSearchTerm and update search result
                        if(this.nextSearchTerm != undefined){
                            this.search.val(this.nextSearchTerm);
                            this.updateResults();
                            this.search.select();
                        }
                    }
                    this.positionDropdown();
                } else {
                    // if nothing left to select close
                    this.close();
                    this.search.width(10);
                }
            }

            // since its not possible to select an element that has already been
            // added we do not need to check if this is a new element before firing change
            this.triggerChange({ added: data });

            if (!options || !options.noFocus)
                this.focusSearch();
        },

        // multi
        cancel: function () {
            this.close();
            this.focusSearch();
        },

        addSelectedChoice: function (data) {
            var enableChoice = !data.locked,
                enabledItem = $(
                    "<li class='select2-search-choice'>" +
                    "    <div></div>" +
                    "    <a href='#' class='select2-search-choice-close' tabindex='-1'></a>" +
                    "</li>"),
                disabledItem = $(
                    "<li class='select2-search-choice select2-locked'>" +
                    "<div></div>" +
                    "</li>");
            var choice = enableChoice ? enabledItem : disabledItem,
                id = this.id(data),
                val = this.getVal(),
                formatted,
                cssClass;

            formatted=this.opts.formatSelection(data, choice.find("div"), this.opts.escapeMarkup);
            if (formatted != undefined) {
                choice.find("div").replaceWith("<div>"+formatted+"</div>");
            }
            cssClass=this.opts.formatSelectionCssClass(data, choice.find("div"));
            if (cssClass != undefined) {
                choice.addClass(cssClass);
            }

            if(enableChoice){
              choice.find(".select2-search-choice-close")
                  .on("mousedown", killEvent)
                  .on("click dblclick", this.bind(function (e) {
                  if (!this.isInterfaceEnabled()) return;

                  this.unselect($(e.target));
                  this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus");
                  killEvent(e);
                  this.close();
                  this.focusSearch();
              })).on("focus", this.bind(function () {
                  if (!this.isInterfaceEnabled()) return;
                  this.container.addClass("select2-container-active");
                  this.dropdown.addClass("select2-drop-active");
              }));
            }

            choice.data("select2-data", data);
            choice.insertBefore(this.searchContainer);

            val.push(id);
            this.setVal(val);
        },

        // multi
        unselect: function (selected) {
            var val = this.getVal(),
                data,
                index;
            selected = selected.closest(".select2-search-choice");

            if (selected.length === 0) {
                throw "Invalid argument: " + selected + ". Must be .select2-search-choice";
            }

            data = selected.data("select2-data");

            if (!data) {
                // prevent a race condition when the 'x' is clicked really fast repeatedly the event can be queued
                // and invoked on an element already removed
                return;
            }

            var evt = $.Event("select2-removing");
            evt.val = this.id(data);
            evt.choice = data;
            this.opts.element.trigger(evt);

            if (evt.isDefaultPrevented()) {
                return false;
            }

            while((index = indexOf(this.id(data), val)) >= 0) {
                val.splice(index, 1);
                this.setVal(val);
                if (this.select) this.postprocessResults();
            }

            selected.remove();

            this.opts.element.trigger({ type: "select2-removed", val: this.id(data), choice: data });
            this.triggerChange({ removed: data });

            return true;
        },

        // multi
        postprocessResults: function (data, initial, noHighlightUpdate) {
            var val = this.getVal(),
                choices = this.results.find(".select2-result"),
                compound = this.results.find(".select2-result-with-children"),
                self = this;

            choices.each2(function (i, choice) {
                var id = self.id(choice.data("select2-data"));
                if (indexOf(id, val) >= 0) {
                    choice.addClass("select2-selected");
                    // mark all children of the selected parent as selected
                    choice.find(".select2-result-selectable").addClass("select2-selected");
                }
            });

            compound.each2(function(i, choice) {
                // hide an optgroup if it doesn't have any selectable children
                if (!choice.is('.select2-result-selectable')
                    && choice.find(".select2-result-selectable:not(.select2-selected)").length === 0) {
                    choice.addClass("select2-selected");
                }
            });

            if (this.highlight() == -1 && noHighlightUpdate !== false){
                self.highlight(0);
            }

            //If all results are chosen render formatNoMatches
            if(!this.opts.createSearchChoice && !choices.filter('.select2-result:not(.select2-selected)').length > 0){
                if(!data || data && !data.more && this.results.find(".select2-no-results").length === 0) {
                    if (checkFormatter(self.opts.formatNoMatches, "formatNoMatches")) {
                        this.results.append("<li class='select2-no-results'>" + evaluate(self.opts.formatNoMatches, self.opts.element, self.search.val()) + "</li>");
                    }
                }
            }

        },

        // multi
        getMaxSearchWidth: function() {
            return this.selection.width() - getSideBorderPadding(this.search);
        },

        // multi
        resizeSearch: function () {
            var minimumWidth, left, maxWidth, containerLeft, searchWidth,
                sideBorderPadding = getSideBorderPadding(this.search);

            minimumWidth = measureTextWidth(this.search) + 10;

            left = this.search.offset().left;

            maxWidth = this.selection.width();
            containerLeft = this.selection.offset().left;

            searchWidth = maxWidth - (left - containerLeft) - sideBorderPadding;

            if (searchWidth < minimumWidth) {
                searchWidth = maxWidth - sideBorderPadding;
            }

            if (searchWidth < 40) {
                searchWidth = maxWidth - sideBorderPadding;
            }

            if (searchWidth <= 0) {
              searchWidth = minimumWidth;
            }

            this.search.width(Math.floor(searchWidth));
        },

        // multi
        getVal: function () {
            var val;
            if (this.select) {
                val = this.select.val();
                return val === null ? [] : val;
            } else {
                val = this.opts.element.val();
                return splitVal(val, this.opts.separator);
            }
        },

        // multi
        setVal: function (val) {
            var unique;
            if (this.select) {
                this.select.val(val);
            } else {
                unique = [];
                // filter out duplicates
                $(val).each(function () {
                    if (indexOf(this, unique) < 0) unique.push(this);
                });
                this.opts.element.val(unique.length === 0 ? "" : unique.join(this.opts.separator));
            }
        },

        // multi
        buildChangeDetails: function (old, current) {
            var current = current.slice(0),
                old = old.slice(0);

            // remove intersection from each array
            for (var i = 0; i < current.length; i++) {
                for (var j = 0; j < old.length; j++) {
                    if (equal(this.opts.id(current[i]), this.opts.id(old[j]))) {
                        current.splice(i, 1);
                        if(i>0){
                        	i--;
                        }
                        old.splice(j, 1);
                        j--;
                    }
                }
            }

            return {added: current, removed: old};
        },


        // multi
        val: function (val, triggerChange) {
            var oldData, self=this;

            if (arguments.length === 0) {
                return this.getVal();
            }

            oldData=this.data();
            if (!oldData.length) oldData=[];

            // val is an id. !val is true for [undefined,null,'',0] - 0 is legal
            if (!val && val !== 0) {
                this.opts.element.val("");
                this.updateSelection([]);
                this.clearSearch();
                if (triggerChange) {
                    this.triggerChange({added: this.data(), removed: oldData});
                }
                return;
            }

            // val is a list of ids
            this.setVal(val);

            if (this.select) {
                this.opts.initSelection(this.select, this.bind(this.updateSelection));
                if (triggerChange) {
                    this.triggerChange(this.buildChangeDetails(oldData, this.data()));
                }
            } else {
                if (this.opts.initSelection === undefined) {
                    throw new Error("val() cannot be called if initSelection() is not defined");
                }

                this.opts.initSelection(this.opts.element, function(data){
                    var ids=$.map(data, self.id);
                    self.setVal(ids);
                    self.updateSelection(data);
                    self.clearSearch();
                    if (triggerChange) {
                        self.triggerChange(self.buildChangeDetails(oldData, self.data()));
                    }
                });
            }
            this.clearSearch();
        },

        // multi
        onSortStart: function() {
            if (this.select) {
                throw new Error("Sorting of elements is not supported when attached to <select>. Attach to <input type='hidden'/> instead.");
            }

            // collapse search field into 0 width so its container can be collapsed as well
            this.search.width(0);
            // hide the container
            this.searchContainer.hide();
        },

        // multi
        onSortEnd:function() {

            var val=[], self=this;

            // show search and move it to the end of the list
            this.searchContainer.show();
            // make sure the search container is the last item in the list
            this.searchContainer.appendTo(this.searchContainer.parent());
            // since we collapsed the width in dragStarted, we resize it here
            this.resizeSearch();

            // update selection
            this.selection.find(".select2-search-choice").each(function() {
                val.push(self.opts.id($(this).data("select2-data")));
            });
            this.setVal(val);
            this.triggerChange();
        },

        // multi
        data: function(values, triggerChange) {
            var self=this, ids, old;
            if (arguments.length === 0) {
                 return this.selection
                     .children(".select2-search-choice")
                     .map(function() { return $(this).data("select2-data"); })
                     .get();
            } else {
                old = this.data();
                if (!values) { values = []; }
                ids = $.map(values, function(e) { return self.opts.id(e); });
                this.setVal(ids);
                this.updateSelection(values);
                this.clearSearch();
                if (triggerChange) {
                    this.triggerChange(this.buildChangeDetails(old, this.data()));
                }
            }
        }
    });

    $.fn.select2 = function () {

        var args = Array.prototype.slice.call(arguments, 0),
            opts,
            select2,
            method, value, multiple,
            allowedMethods = ["val", "destroy", "opened", "open", "close", "focus", "isFocused", "container", "dropdown", "onSortStart", "onSortEnd", "enable", "disable", "readonly", "positionDropdown", "data", "search"],
            valueMethods = ["opened", "isFocused", "container", "dropdown"],
            propertyMethods = ["val", "data"],
            methodsMap = { search: "externalSearch" };

        this.each(function () {
            if (args.length === 0 || typeof(args[0]) === "object") {
                opts = args.length === 0 ? {} : $.extend({}, args[0]);
                opts.element = $(this);

                if (opts.element.get(0).tagName.toLowerCase() === "select") {
                    multiple = opts.element.prop("multiple");
                } else {
                    multiple = opts.multiple || false;
                    if ("tags" in opts) {opts.multiple = multiple = true;}
                }

                select2 = multiple ? new window.Select2["class"].multi() : new window.Select2["class"].single();
                select2.init(opts);
            } else if (typeof(args[0]) === "string") {

                if (indexOf(args[0], allowedMethods) < 0) {
                    throw "Unknown method: " + args[0];
                }

                value = undefined;
                select2 = $(this).data("select2");
                if (select2 === undefined) return;

                method=args[0];

                if (method === "container") {
                    value = select2.container;
                } else if (method === "dropdown") {
                    value = select2.dropdown;
                } else {
                    if (methodsMap[method]) method = methodsMap[method];

                    value = select2[method].apply(select2, args.slice(1));
                }
                if (indexOf(args[0], valueMethods) >= 0
                    || (indexOf(args[0], propertyMethods) >= 0 && args.length == 1)) {
                    return false; // abort the iteration, ready to return first matched value
                }
            } else {
                throw "Invalid arguments to select2 plugin: " + args;
            }
        });
        return (value === undefined) ? this : value;
    };

    // plugin defaults, accessible to users
    $.fn.select2.defaults = {
        width: "copy",
        loadMorePadding: 0,
        closeOnSelect: true,
        openOnEnter: true,
        containerCss: {},
        dropdownCss: {},
        containerCssClass: "",
        dropdownCssClass: "",
        formatResult: function(result, container, query, escapeMarkup) {
            var markup=[];
            markMatch(result.text, query.term, markup, escapeMarkup);
            return markup.join("");
        },
        formatSelection: function (data, container, escapeMarkup) {
            return data ? escapeMarkup(data.text) : undefined;
        },
        sortResults: function (results, container, query) {
            return results;
        },
        formatResultCssClass: function(data) {return data.css;},
        formatSelectionCssClass: function(data, container) {return undefined;},
        formatMatches: function (matches) { if (matches === 1) { return "One result is available, press enter to select it."; } return matches + " results are available, use up and down arrow keys to navigate."; },
        formatNoMatches: function () { return "No matches found"; },
        formatInputTooShort: function (input, min) { var n = min - input.length; return "Please enter " + n + " or more character" + (n == 1? "" : "s"); },
        formatInputTooLong: function (input, max) { var n = input.length - max; return "Please delete " + n + " character" + (n == 1? "" : "s"); },
        formatSelectionTooBig: function (limit) { return "You can only select " + limit + " item" + (limit == 1 ? "" : "s"); },
        formatLoadMore: function (pageNumber) { return "Loading more results…"; },
        formatSearching: function () { return "Searching…"; },
        minimumResultsForSearch: 0,
        minimumInputLength: 0,
        maximumInputLength: null,
        maximumSelectionSize: 0,
        id: function (e) { return e == undefined ? null : e.id; },
        matcher: function(term, text) {
            return stripDiacritics(''+text).toUpperCase().indexOf(stripDiacritics(''+term).toUpperCase()) >= 0;
        },
        separator: ",",
        tokenSeparators: [],
        tokenizer: defaultTokenizer,
        escapeMarkup: defaultEscapeMarkup,
        blurOnChange: false,
        selectOnBlur: false,
        adaptContainerCssClass: function(c) { return c; },
        adaptDropdownCssClass: function(c) { return null; },
        nextSearchTerm: function(selectedObject, currentSearchTerm) { return undefined; },
        searchInputPlaceholder: '',
        createSearchChoicePosition: 'top',
        shouldFocusInput: function (instance) {
            // Attempt to detect touch devices
            var supportsTouchEvents = (('ontouchstart' in window) ||
                                       (navigator.msMaxTouchPoints > 0));

            // Only devices which support touch events should be special cased
            if (!supportsTouchEvents) {
                return true;
            }

            // Never focus the input if search is disabled
            if (instance.opts.minimumResultsForSearch < 0) {
                return false;
            }

            return true;
        }
    };

    $.fn.select2.ajaxDefaults = {
        transport: $.ajax,
        params: {
            type: "GET",
            cache: false,
            dataType: "json"
        }
    };

    // exports
    window.Select2 = {
        query: {
            ajax: ajax,
            local: local,
            tags: tags
        }, util: {
            debounce: debounce,
            markMatch: markMatch,
            escapeMarkup: defaultEscapeMarkup,
            stripDiacritics: stripDiacritics
        }, "class": {
            "abstract": AbstractSelect2,
            "single": SingleSelect2,
            "multi": MultiSelect2
        }
    };

}(jQuery));

define("select2", function(){});

/*
 * Canvas.js
 *
 * Overall module for editing or viewing rspecs, profiles, and slices
 *
 */

define('js/canvas/Canvas',['underscore', 'backbone', 'lib/PrettyXML',
	'js/canvas/Constraints',
	'js/canvas/rspecParser', 'js/canvas/rspecGenerator',
	'js/canvas/TourView', 'js/canvas/TopologyView',
	'js/canvas/TopologyModel',
	'js/info/Info', 'js/info/ConstrainedField', 'js/canvas/ErrorModal',
	'text!html/Canvas.html', 'text!html/AddNode.html',
	'text!html/rspec.xml',
	'select2'],
function (_, Backbone, PrettyXML,
	  Constraints, rspecParser, rspecGenerator,
	  TourView, TopologyView, TopologyModel,
	  Info, ConstrainedField, ErrorModal,
	  canvasString, addNodeString, defaultRspecString)
{
  

  var addNodeTemplate = _.template(addNodeString);

  function Canvas(context, domRoot, tourContainer, updateIn, updateOut)
  {
    this.context = context;
    this.constraints = new Constraints(context);
    this.domRoot = domRoot;
    this.tourContainer = tourContainer;
    this.updateIn = updateIn;
    this.updateOut = updateOut;
    this.size = context.size;
    this.highlights = [];
    this.lastHighlightType = null;
    if (context.mixedSelection)
    {
      this.lastHighlightType = 'mixed';
    }
    this.lastHighlightEvent = null;
    this.rspecShowing = false;
    this.info = new Info({
      context: context,
      constraints: this.constraints
    });
    domRoot.html(canvasString);
    this.errorModal = new ErrorModal();
    this.errorModal.render($('#errorModal'));

    this.info.render(domRoot.find('.nodeAttr'));
    this.info.on('change', _.bind(this.changeAttribute, this));
    this.info.on('addToList', _.bind(this.addToList, this));
    this.info.on('removeFromList', _.bind(this.removeFromList, this));
    this.info.on('changeList', _.bind(this.changeList, this));

    this.singleSite = new ConstrainedField({
      choices: context.canvasOptions.aggregates,
      constraints: this.info.constraints,
      optionKey: 'aggregates',
    });
    this.singleSite.render(domRoot.find('#fbcSite'));
    this.singleSite.on('change', _.bind(this.changeSingleSite, this));

    this.clickUpdate = {};
    _.extend(this.clickUpdate, Backbone.Events);

    if (this.context.nodeSelect === false) {
      this.clickUpdate.on("click-event", function(data) {
	var out = {};
	if (data.type === 'node')
	{
	  out = {
	    'type': 'node',
	    'client_id': data.item.name,
	    'ssh': data.item.sshurl
	  };
	}
	else if (data.type === 'link')
	{
	  out = {
	    'type': 'link',
	    'client_id': data.item.name
	  };
	}
	else if (data.type === 'site')
	{
	  out = {
	    'type': 'site'
	  };
	}
	else if (data.type === 'host')
	{
	  out = {
	    'type': 'host',
	    'client_id': data.item.name
	  };
	}
	out.event = data.event;
        updateOut.trigger("click-event", out);
      });
    }
    else {
      this.clickUpdate.on("click-event", _.bind(this.handleClick, this));
      this.clickUpdate.on("click-outside", _.bind(this.handleClickOutside, this));
    }
    if (_.isString(context.size) && context.size === 'auto')
    {
      this.lastSize = {
	x: null,
	y: null
      };
      setInterval(_.bind(this.checkResize, this), 100);
    }
    this.updateIn.on('show-rspec', _.bind(this.viewRspec, this));
    this.updateIn.on('add-canvas-options', _.bind(this.addCanvasOptions, this));
    this.updateIn.on('set-selection', _.bind(this.handleSetSelection, this));
  }

  Canvas.prototype.hasNodes = function ()
  {
    return _.keys(this.topoData.nodes).length > 0;
  }

  Canvas.prototype.show = function ()
  {
    var that = this;
    var root = this.domRoot;
    this.rspec = defaultRspecString;
    if (! this.context.show.menu)
    {
      root.find('.withButtonBar').removeClass('withButtonBar');
      root.find('.editor#topoButtons, .viewer#topoButtons').hide();
    }

    if (_.isObject(this.context.size))
    {
      root.css('width', this.context.size.x + 'px');
      root.css('height', this.context.size.y + 'px');
    }

    showCommon(this.rspec, that, root);
    this.updateSingleSite();
    if (this.context.mode === 'viewer') {
      showViewer(this.rspec, that, root);
    }
    else if (this.context.mode === 'editor') {
      showEditor(this.rspec, that, root);
    }
  };

  Canvas.prototype.hide = function ()
  {
    this.domRoot.hide();
  };

  Canvas.prototype.clear = function ()
  {
    this.highlights = [];
    this.topology.setHighlights(this.highlights);
    this.showAttributes();
    this.topoData.removeLinks(_.keys(this.topoData.lans));
    this.topoData.removeNodes(_.keys(this.topoData.nodes));
    this.topoData.removeSites(_.keys(this.topoData.sites));
  };

  Canvas.prototype.checkResize = function (force)
  {
    var topoDom = this.domRoot.find('#topoContainer');
    var newSize = {
      x: topoDom.outerWidth(),
      y: topoDom.outerHeight()
    };
    if (! this.lastSize ||
	newSize.x !== this.lastSize.x ||
	newSize.y !== this.lastSize.y || force)
    {
      this.lastSize = newSize;
      this.resize(newSize);
    }
  };

  Canvas.prototype.calculateSize = function ()
  {
    var topoDom = this.domRoot.find('#topoContainer');
    var size = {
      x: topoDom.outerWidth(),
      y: topoDom.outerHeight()
    };
    if (_.isObject(this.context.size))
    {
      size = {
	x: this.context.size.x,
	y: this.context.size.y
      };
    }
    return size;
  };

  Canvas.prototype.resize = function (size)
  {
    var topoDom = this.domRoot.find('#topoContainer');
    this.size = size;
    this.topology.resize(size.x, size.y);
  };

  Canvas.prototype.generateRequest = function () {
    return rspecGenerator.request(this.topoData, this.context);
  };

  Canvas.prototype.viewRspec = function () {
    if (! this.rspecShowing)
    {
      var request = rspecGenerator.request(this.topoData, this.context);
      this.domRoot.find('#lnRspec').html('<pre>' +
					 _.escape(PrettyXML.format(request)) +
					 '</pre>');
      this.domRoot.find('.navRspec').show();
      this.rspecShowing = true;
      $('html').css('-webkit-user-select', '');
      $('html').css('-moz-user-select', '');
    }
    else
    {
      this.hideRspec();
    }
  };

  Canvas.prototype.hideRspec = function () {
    this.domRoot.find('.navRspec').hide();
    this.rspecShowing = false;
  };

  Canvas.prototype.addCanvasOptions = function (options) {
    if (options.canvasOptions)
    {
      this.constraints.addPossibles(options.canvasOptions);
      _.each(_.keys(options.canvasOptions), function (key) {
	var current = this.context.canvasOptions[key] || [];
	var newPossibles = options.canvasOptions[key];
	_.each(current, function (item) {
	  var dup = _.findWhere(newPossibles, { id: item.id });
	  if (! dup)
	  {
	    newPossibles.push(item);
	  }
	}.bind(this));
	this.context.canvasOptions[key] = current;
      }.bind(this));
    }
    if (options.constraints)
    {
      this.constraints.allowAllSets(options.constraints);
      var current = this.context.constraints || [];
      current = _.union(current, options.constraints);
      this.context.constraints = current;
    }
    this.showAttributes();
  };

  Canvas.prototype.addRspec = function (list) {
    var that = this;
    _.each(list, function (item) {
      var newGraph =
	rspecParser.parse({
	  rspec: item.rspec,
	  context: that.context,
	  errorModal: that.errorModal
	})
      that.topoData.addGraph(newGraph, item.sourceUrn);
    });
    if (list.length > 0)
    {
      this.topology.startForce();
    }
    this.updateOut.trigger('found-images', this.imageList());
    this.updateOut.trigger('found-types', this.typeList());
  };

  Canvas.prototype.imageList = function () {
    var result = [];
    _.each(this.topoData.nodes, function (node) {
      if (node.image)
      {
	result = _.union(result, [node.image]);
      }
    }.bind(this));
    return result;
  };

  Canvas.prototype.typeList = function () {
    var result = {};
    _.each(this.topoData.nodes, function (node) {
      var site = node.group;
      result[site] = result[site] || { name: '', types: {}, hardware: {} };
      result[site].name = this.topoData.sites[site].name;
      updateCounter(node.type, result[site].types);
      updateCounter(node.hardware, result[site].hardware);
    }.bind(this));
    return result;
  };

  function updateCounter(inName, map)
  {
    var name = inName;
    if (name === undefined)
    {
      name = '';
    }
    if (map[name] === undefined)
    {
      map[name] = 0;
    }
    map[name] += 1;
  }

  var canvasModified = false;
  Canvas.prototype.modifiedTopology = function () {
    canvasModified = true;
    _.defer(function () {
      if (canvasModified)
      {
        this.updateOut.trigger('modified-topology',
			       this.generateModifiedData(true));
	canvasModified = false;
      }
    }.bind(this));
  };

  Canvas.prototype.modifiedField = function () {
    if (! this.pendingModifiedField)
    {
      this.pendingModifiedField = true;
      _.defer(function () {
	this.updateOut.trigger('modified', this.generateModifiedData(false));
	this.pendingModifiedField = false;
      }.bind(this));
    }
  };

  Canvas.prototype.generateModifiedData = function (makeRspec) {
    var data = {};
    if (makeRspec)
    {
      data.rspec = PrettyXML.format(rspecGenerator.request(this.topoData,
							   this.context));
    }
    data.nodes = [];
    _.each(this.topoData.nodes, function (node) {
	var nodeData = {
	  id: node.id,
	  client_id: node.name,
	  sliver_id: node.sliverId,
	  sourceUrn: node.sourceUrn
	};
	if (node.group)
	{
	  var site = this.topoData.sites[node.group];
	  if (site.urn)
	  {
	    nodeData.aggregate_id = site.urn;
	  }
	  if (site.name)
	  {
	    nodeData.site_name = site.name;
	  }
	}
	data.nodes.push(nodeData);
    }.bind(this));
    data.links = [];
    _.each(this.topoData.lans, function (link) {
	data.links.push({
	  id: link.id,
	  client_id: link.name,
	  link_type: link.linkType
	});
    }.bind(this));
    data.sites = [];
    _.each(this.topoData.sites, function (site) {
	var siteData = {
	  id: site.id,
	};
	if (site.name)
	{
	  siteData.name = site.name;
	}
	if (site.urn)
	{
	  siteData.urn = site.urn;
	}
	data.sites.push(siteData);
    }.bind(this));
    return data;
  };

  function showCommon(rspec, that, root)
  {
    if (! that.context.show.rspec)
    {
      root.find('#fbRspec').hide();//css('visibility', 'hidden');
      root.find('#fbcRspec').hide();//css('visibility', 'hidden');
    }
    if (! that.context.show.tour)
    {
      root.find('#fbTour').css('visibility', 'hidden');
    }
    if (! that.context.show.version)
    {
      root.find('#versionNumber').css('visibility', 'hidden');
    }
    root.find('#fbRspec').click(_.bind(that.viewRspec, that));
    root.find('#fbcRspec').click(_.bind(that.viewRspec, that));
    root.find('#hideRspec').click(_.bind(that.hideRspec, that));
    root.find('#hideLeftPane').click(_.bind(that.handleClickOutside, that));
    root.find('.navRspec').blur(function() {
      setTimeout(function() {
	enableButton(root.find('#fbRspec'));
	enableButton(root.find('#fbcRspec'));
      }, 250);
    });
    root.find('#fbTour').click(function (event) {
      event.preventDefault();
      that.tour.tour.start();
    });
    root.find('#fbcCleanup').click(function (event) {
      that.topology.startForce();
    });

    var topoDom = that.domRoot.find('#topoContainer');
    var size = that.calculateSize();

    that.topoData = new TopologyModel(that.context, that.constraints, that.errorModal);
    that.topoData.on('addSites addNodes addLans addEndpoint ' +
		     'removeSites removeNodes removeLans removeEndpoint ' +
		     'changeAggregate ',
		     _.bind(that.modifiedTopology, that));
    that.topoData.on('change', _.bind(that.modifiedField, that));
    that.topoData.on('addSites removeSites changeAggregate',
		     _.bind(that.updateSingleSite, that));
    that.topoData.on('change', _.bind(that.showAttributes, that));
    that.tour = new TourView(topoDom, that.tourContainer, rspec);
    that.topology = new TopologyView({ el: topoDom,
				       model: that.topoData,
				       clickUpdate: that.clickUpdate,
				       defaults: that.context.canvasOptions.defaults,
				       tour: that.tour,
				       context: that.context,
				       constraints: that.constraints,
				       errorModal: that.errorModal });
    that.topoData.addGraph(rspecParser.parse({
      rspec: rspec,
      context: that.context,
      errorModal: that.errorModal
    }));
    if (_.isString(that.context.size) && that.context.size === 'auto')
    {
      that.checkResize(true);
    }
    else
    {
      that.resize(that.size);
    }

    if (that.context.mode === 'editor' ||
	(that.context.nodeSelect && that.context.show.selectInfo))
    {
      slideOutStatic(that.domRoot.find('.nodeAttr'), that.domRoot.find('.closeContainer'));
      that.showAttributes();
    }
  }

  function showViewer(rspec, that, root)
  {
    if (that.context.show.selectInfo)
    {
      root.find('#viewer-default').show();
    }
    root.find('#topoButtons.viewer').removeClass('hidden');

    root.find('#fbSlice').click(function() {
      slideOut(root.find('.navSlices'), root.find('#fbSlice'));
    });
    root.find('.navSlices').blur(function() {
      setTimeout(function() {
	enableButton(root.find('#fbSlice'));
      }, 250);
    });
    if (that.context.show.tour)
    {
      root.find('#fbTour').popover({
	title: 'Start Tour',
	content: '<p>This slice has a tour which describe how it works. ' +
	  'Click here to view it.</p>',
	html: true,
	placement: 'bottom',
	trigger: 'manual'
      });
    }

    if (that.tour.nonTrivial())
    {
      root.find('#fbTour').show();
      _.defer(function () { root.find('#fbTour').popover('show'); });
      $('body').one('click', function () {
	root.find('#fbTour').popover('hide');
      });
    }
    else
    {
      root.find('#fbTour').hide();
    }

    if (that.context.source === 'api')
    {
      root.find('#fbSlice').show();
    }
    else
    {
      root.find('#fbSlice').hide();
    }

    root.find('#naDelete').hide();
  }

  function showEditor(rspec, that, root)
  {
    if (that.context.show.clear)
    {
      root.find('#fbcClear').click(function () {
	this.clear();
	this.topoData.addGraph(rspecParser.parse({
	  rspec: defaultRspecString,
	  context: this.context,
	  errorModal: this.errorModal
	}));
      }.bind(that));
    }
    else
    {
      root.find('#fbcClear').hide();
    }

    root.find('#topoButtons.editor').removeClass('hidden');
  }

  Canvas.prototype.handleClick = function(data) 
  {
    if (this.lastHighlightType !== 'mixed')
    {
      if (this.lastHighlightType !== data.type)
      {
	this.highlights = [];
      }
      this.lastHighlightType = data.type;
    }
    this.lastHighlightEvent = data.event;

    if (data.modkey)
    {
      if (_.contains(this.highlights, data.item.id))
      {
        this.highlights = _.without(this.highlights, data.item.id)
      }
      else
      {
        this.highlights.push(data.item.id);
      }
    }
    else
    {
      if (this.highlights !== undefined &&
	  this.highlights.length === 1 &&
	  _.contains(this.highlights, data.item.id))
      {
        this.highlights = [];
      }
      else
      {
        this.highlights = [data.item.id];
      }
    }

    this.showAttributes();
    this.topology.setHighlights(this.highlights, data.type);
  };

  Canvas.prototype.handleClickOutside = function()
  {
    this.lastHighlightEvent = null;
    this.highlights = [];
    this.topology.setHighlights(this.highlights);
    this.showAttributes();
  };

  Canvas.prototype.handleSetSelection = function (newSelection)
  {
    this.lastHighlightEvent = null;
    this.lastHighlightType = 'mixed';
    this.highlights = [];
    if (_.isArray(newSelection))
    {
      _.each(newSelection, function (item) {
	if (_.isString(item) && this.topoData.hasId(item)) {
	  this.highlights.push(item);
	}
      }.bind(this));
    }
    this.topology.setHighlights(this.highlights);
    this.showAttributes();
  };

  function generateOutsideSelection(items, selectType, lastHighlightEvent)
  {
    var result = {
      type: selectType,
      items: [],
      event: lastHighlightEvent
    };
    _.each(items, function (item) {
      var out = {};
      out.key = item.id;
      if (item.sliverId)
      {
	out.sliver_id = item.sliverId;
      }
      if (selectType == 'node')
      {
	out.name = item.name;
	out.sshurl = item.ssh;
      }
      else if (selectType == 'link')
      {
	out.name = item.name;
      }
      else if (selectType == 'site')
      {
	out.urn = item.urn;
	out.id = item.name;
      }
      result.items.push(out);
    });
    return result;
  }

  Canvas.prototype.changeSingleSite = function (state)
  {
    _.defer(function () {
      var key = _.keys(this.topoData.sites)[0];
      this.topoData.changeAttribute([key], 'urn', state.value, 'site');
    }.bind(this));
  }

  Canvas.prototype.updateSingleSite = function ()
  {
/*
    var keys = _.keys(this.topoData.sites);
    if (keys.length === 1)
    {
      var site = this.topoData.sites[keys[0]];
      this.singleSite.show();
      this.singleSite.update({
	values: [site.urn],
	isViewer: this.context.mode === 'viewer',
	type: 'site',
	disabled: false,
	freeform: site.custom.urn,
	model: this.topoData,
	selection: 'site'
      });
    }
    else
*/
    {
      this.singleSite.hide();
    }
  }

  Canvas.prototype.changeAttribute = function (state)
  {
    if (state.hasFreeform)
    {
      var items = _.filter(this.topoData.getItems(this.lastHighlightType),
			   function(item) {
			     return _.contains(this.highlights, item.id);
			   }.bind(this));
      _.each(items, function (item) {
	item.custom[state.key] = state.freeform;
      });
    }
    if (state.key === 'delete')
    {
      this.topoData.removeItems(this.highlights, this.lastHighlightType);
      this.highlights = [];
    }
    else
    {
      this.topoData.changeAttribute(this.highlights, state.key, state.value,
				    this.lastHighlightType);
    }
    if (state.hasVersion)
    {
      this.topoData.changeAttribute(this.highlights, 'imageVersion', state.version,
				    this.lastHighlightType);
    }
    _.defer(_.bind(this.showAttributes, this));
  };

  Canvas.prototype.addToList = function (state)
  {
    if (this.highlights.length === 1)
    {
      var current = this.topoData.nodes[this.highlights[0]];
      current[state.key].push(state.item);
      this.showAttributes();
    }
  };

  Canvas.prototype.removeFromList = function (state)
  {
    if (this.highlights.length === 1)
    {
      if (state.key === 'interfaces')
      {
	var link = this.topoData.lans[this.highlights[0]];
	var iface = this.topoData.interfaces[link.interfaces[state.index]];
	var node = this.topoData.nodes[iface.nodeId];
	this.topoData.removeNodeFromLan(node, link);
      }
      else
      {
	var current = this.topoData.nodes[this.highlights[0]];
	current[state.key].splice(state.index, 1);
      }
      this.showAttributes();
    }
  };

  Canvas.prototype.changeList = function (state)
  {
    if (this.highlights.length === 1)
    {
      var current;
      if (state.key === 'interfaces')
      {
	var link = this.topoData.lans[this.highlights[0]];
	var id = link.interfaces[state.index];
	current = this.topoData.interfaces[id];
      }
      else
      {
	current = this.topoData.nodes[this.highlights[0]][state.key][state.index];
      }
      _.each(state.changed, function (value, key) {
	if (key === 'ip')
	{
	  var oldValue = current[key];
	  current[key] = value;
	  this.topoData.switchIp(current.id, oldValue, value);
	}
	else
	{
	  current[key] = value;
	}
      }.bind(this));
      this.showAttributes();
    }
  };

  Canvas.prototype.showAttributes = function()
  {
    if (! this.showingAttributes)
    {
      this.showingAttributes = true;
      _.defer(_.bind(this.finalShowAttributes, this));
    }
  }

  Canvas.prototype.finalShowAttributes = function ()
  {
    this.showingAttributes = false;
    $('html').css('-webkit-user-select', 'initial');
    $('html').css('-moz-user-select', 'initial');
    var isViewer = this.context.mode === 'viewer';
    var items = [];
    if (this.lastHighlightType === 'mixed')
    {
      items = this.topoData.findMixed(this.highlights);
    }
    else
    {
      items = _.filter(this.topoData.getItems(this.lastHighlightType),
		       function(item) {
			 return _.contains(this.highlights, item.id);
		       }.bind(this));
    }
    var selectType = this.lastHighlightType;
    var unselected = (items.length === 0);
    if (unselected)
    {
      selectType = 'none';
    }
    if (selectType !== 'none' && selectType !== 'mixed')
    {
      this.info.update({ selection: items, type: selectType,
			 isViewer: isViewer, model: this.topoData });
    }

    //var topo = this.domRoot.find('#topoContainer');
    var pane = this.domRoot.find('.nodeAttr');
    this.updateOut.trigger('selection',
			   generateOutsideSelection(items, selectType,
						    this.lastHighlightEvent));
    if (items.length === 0 || selectType === 'mixed')
    {
      slideInStatic(pane, this.domRoot.find('.closeContainer'));
    }
    else
    {
      slideOutStatic(pane, this.domRoot.find('.closeContainer'));
    }
  }

  function slideOut(goTo, thisButton)
  {
    if (!thisButton.hasClass('freeze'))
    {
      setTimeout(function() {
	      goTo.attr('tabindex', -1).focus();
      }, 1);
      thisButton.addClass('freeze');
    }
  }

  function enableButton(thisButton)
  {
    thisButton.removeClass('freeze');
  }

  function slideOutStatic(goTo, closeButton, thisButton)
  {
    goTo.addClass('show');
    closeButton.addClass('show');
    if (thisButton !== null && thisButton !== undefined) {
      thisButton.addClass('btn-warning');
      thisButton.removeClass('btn-primary');
    }
  }

  function slideInStatic(goFrom, closeButton, thisButton)
  {
    goFrom.removeClass('show');
    closeButton.removeClass('show');
    if (thisButton !== null && thisButton !== undefined) {
      thisButton.removeClass('btn-warning');
      thisButton.addClass('btn-primary');
    }
  }

  return Canvas;
});


define('text!html/Login.html',[],function () { return '<div class="row xmlrpc">\r\n  <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">\r\n\r\n    <!-- Wait spinner -->\r\n    <div id="waitContainer" class="text-center hides afterHide">\r\n      <span id="waitText">Doing stuff...</span>\r\n      <img src="images/ajax-loader.gif">\r\n    </div>\r\n\r\n    <!-- Cert window -->\r\n    <div id="certificate" class="panel panel-default">\r\n      <div class="panel-heading">\r\n        <h3 class="panel-title">Log In</h3>\r\n      </div>\r\n      <div class="panel-body">\r\n        <div class="form-group">\r\n          <ul class="nav nav-tabs sslCertNav">\r\n            <li class="active saTab"><a href="#">Identity Provider</a></li>\r\n            <li class="fileTab"><a href="#">Certificate</a></li>\r\n          </ul>\r\n\r\n          <div class="panel-body sa active tab">\r\n            <p>Sign in using your account at any one of GENI\'s federated\r\n              facilities:</p>\r\n\t    <div class="panel-group" id="accordion">\r\n              <div class="panel panel-default">\r\n                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">\r\n                  <div class="panel-heading">\r\n                    <h4 class="panel-title">\r\n                      Recommended Providers\r\n                    </h4>\r\n                  </div>\r\n                </a>\r\n                <div id="collapseOne" class="panel-collapse collapse in">\r\n                  <div class="panel-body">\r\n                    <div id="recommendedSA">\r\n                      <div class="recSAImages">\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class="panel panel-default">\r\n                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">\r\n                  <div class="panel-heading">\r\n                    <h4 class="panel-title">\r\n                      Other Providers\r\n                    </h4>\r\n                  </div>\r\n                </a>\r\n                <div id="collapseTwo" class="panel-collapse collapse">\r\n                  <div class="panel-body">\r\n                    <div id="otherSA">\r\n                      <select class="form-control sliceAuthorities"></select>\r\n                      <button id="other-sa-button" class="btn btn-primary btn-sm pull-right retrieve-cert">Retrieve Certificate</button>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class="panel-body fromFile tab">\r\n            <h5>\r\n              Enter SSL Certificate\r\n              <a class="info" data-toggle="tooltip" data-placement="right" data-html="true" title="SSL Certificate is in PEM format">\r\n                <span class="glyphicon glyphicon-question-sign"></span>\r\n              </a>\r\n            </h5>\r\n            <p>You may paste a certificate issued by any GENI identity provider below.</p>\r\n            <textarea id="clientKey" class="form-control" rows="5"></textarea>\r\n            <button id="manualCert" class="btn btn-primary btn-sm pull-right cert-form" type="button">Select Certificate</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <!-- Passphrase window -->\r\n    <div id="passphrase" class="panel panel-default hides afterHide">\r\n      <div class="panel-heading">\r\n        <h3 id="userNameContainer" class="panel-title" style="display: none;">Log In</h3>\r\n      </div>\r\n      <div class="panel-body">\r\n\t<div class="alert alert-info">Remember to use the passphrase for your certificate, not for your identity provider.</div>\r\n        <div class="form-group" id="userGroup">\r\n          <h5>Identity Provider</h5>\r\n          <input type="text" class="form-control" id="currentSA" disabled>\r\n          <h5>User Name</h5>\r\n          <input type="text" class="form-control pull-left" id="userName" disabled>\r\n          <button id="passBack" class="btn btn-primary btn-sm pull-right" type="button">Change User</button>\r\n        </div>\r\n        <div class="form-group" id="passGroup">\r\n          <h5>Enter Passphrase</h5>\r\n          <input type="password" class="form-control" id="clientPhrase">\r\n          <div id="passError" class="alert alert-danger hidden"></div>\r\n          <button id="passForm" class="btn btn-primary btn-sm pull-right" name="pass" type="button">Log In</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <!-- Slice select window -->\r\n    <div id="sliceListContainer" class="panel panel-default hides afterHide">\r\n      <div class="panel-heading">\r\n\t<h3 class="panel-title">Available Slices</h3>\r\n      </div>\r\n      <div class="panel-body">\r\n\t<div class="form-group">\r\n          <div id="slice-list" class="list-group"></div>\r\n\t  <!--              <button id="select-slice" class="btn btn-primary btn-sm pull-right">Select</button> -->\r\n\t</div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n';});

define('js/actor/LoginView',['underscore', 'backbone', 'js/cache',
	'text!html/Login.html'],
function (_, Backbone, cache,
	  loginString)
{
  

  var LoginView = Backbone.View.extend({

    events: {
      'keyup #clientPhrase': 'updatePassphrase',
      'click .fileTab': 'clickFileTab',
      'click .saTab': 'clickSaTab',
      'click #manualCert': 'clickManualCert',
      'click #passBack': 'clickPassphraseBack',
      'click #passForm': 'submitPassphraseForm',
      'click .suggested-button': 'clickSuggested',
      'click #other-sa-button': 'clickOtherSA'
    },

    initialize: function () {
      this.render();
    },

    render: function () {
      this.$el.html(loginString);
      this.renderSAList();
      this.$('.info').tooltip();
    },

    renderSAList: function () {
      var inputOptions = '';
      var isSelected = false;
      var list = this.options.saList;
      var suggested = [];

      for (var i = 0; i < list.length; ++i)
      {
	if (list[i]['web-cert-url'])
	{
	  var name = list[i]['name'];
	  if (! name || name === '')
	  {
	    name = list[i]['urn'].split('+')[1];
	  }
	  inputOptions += '<option value="' + i + '"';
	  if (list[i]['urn'] === 'urn:publicid:IDN+emulab.net+authority+sa' ||
	      list[i]['urn'] === 'urn:publicid:IDN+uky.emulab.net+authority+sa')
	  {
	    inputOptions += ' class="hidden"';
	    suggested.push({ index: i, item: list[i]});
	  }
	  else if (! isSelected)
	  {
	    inputOptions += ' selected="selected"';
	    isSelected = false;
	  }
	  inputOptions += '>' + name + '</option>';
	}
      }

      this.$('.sliceAuthorities').html(inputOptions);
      this.renderSuggestedSAList(suggested);
/*
      this.$('.sliceAuthorities').change(updateIdentityProvider);
      if (! cache.get('provider'))
      {
	updateIdentityProvider(0);
      }
*/
    },

    renderSuggestedSAList: function (suggested) {
      var imageHTML = '';
      _.each(suggested, function (value) {
	var icon = value.item.icon;
	var index = value.index;

	imageHTML += '<div class="SAImage pull-left"><a data-index="' + index + '" class="suggested-button">';
	if (icon && icon !== '')
	{
	  imageHTML += '<img style="margin-top: 10px;" src="' +
	    _.escape(icon) + '" />';
	}
	else
	{
	  imageHTML += value.item.name;
	}
	imageHTML += '</a></div>';
      });

      this.$('.recSAImages').html(imageHTML);
    },

    fillPassphraseForm: function (name, saName)
    {
      this.$('#userName').val(_.escape(name));
      this.$('#userNameContainer').show();
      this.$('#currentSA').val(saName);
      this.options.transition.page(this.$('#certificate'),
				   this.$('#passphrase'));
    },

    updatePassphrase: function (event) {
      if (event.keyCode === 13)
      {
	this.submitPassphraseForm();
      }
    },

    clickFileTab: function () {
      switchTab(this.$('.fileTab'), this.$('.fromFile'));
    },

    clickSaTab: function () {
      switchTab(this.$('.saTab'), this.$('.sa'));
    },

    clickManualCert: function () {
      var that = this;
      this.options.login.parseCert(this.$('#clientKey').val());
      this.options.transition.page(this.$('#certificate'),
				   this.$('#passphrase'));
      this.$('#passphrase').one('transitionend', function() {
	that.$('#clientPhrase').focus();
      });
    },

    clickPassphraseBack: function () {
      this.options.transition.page(this.$('#passphrase'),
				   this.$('#certificate'))
    },

    submitPassphraseForm: function () {
      this.options.login.tryPassphrase(this.$('#clientPhrase').val());
    },

    clickSuggested: function (event) {
      event.preventDefault();
      var index = event.currentTarget.dataset['index'];
      cache.set('provider', this.options.saList[index]);
      this.options.login.getCertFromSA();
    },

    clickOtherSA: function (event) {
      event.preventDefault();
      var index = parseInt(this.$('.sliceAuthorities').val());
      cache.set('provider', this.options.saList[indx]);
      this.options.login.getCertFromSA();
    }

  });

  function switchTab(switchTo, panel) {
    switchTo.siblings('.active').removeClass('active');
    panel.siblings('.active').removeClass('active');

    switchTo.addClass('active');
    panel.addClass('active');
  }

  return LoginView;
});

!function(a){if("function"==typeof bootstrap)bootstrap("promise",a);else if("object"==typeof exports)module.exports=a();else if("function"==typeof define&&define.amd)define('q',a);else if("undefined"!=typeof ses){if(!ses.ok())return;ses.makeQ=a}else Q=a()}(function(){function a(a){return function(){return V.apply(a,arguments)}}function b(a){return a===Object(a)}function c(a){return"[object StopIteration]"===bb(a)||a instanceof R}function d(a,b){if(O&&b.stack&&"object"==typeof a&&null!==a&&a.stack&&-1===a.stack.indexOf(cb)){for(var c=[],d=b;d;d=d.source)d.stack&&c.unshift(d.stack);c.unshift(a.stack);var f=c.join("\n"+cb+"\n");a.stack=e(f)}}function e(a){for(var b=a.split("\n"),c=[],d=0;d<b.length;++d){var e=b[d];h(e)||f(e)||!e||c.push(e)}return c.join("\n")}function f(a){return-1!==a.indexOf("(module.js:")||-1!==a.indexOf("(node.js:")}function g(a){var b=/at .+ \((.+):(\d+):(?:\d+)\)$/.exec(a);if(b)return[b[1],Number(b[2])];var c=/at ([^ ]+):(\d+):(?:\d+)$/.exec(a);if(c)return[c[1],Number(c[2])];var d=/.*@(.+):(\d+)$/.exec(a);return d?[d[1],Number(d[2])]:void 0}function h(a){var b=g(a);if(!b)return!1;var c=b[0],d=b[1];return c===Q&&d>=S&&gb>=d}function i(){if(O)try{throw new Error}catch(a){var b=a.stack.split("\n"),c=b[0].indexOf("@")>0?b[1]:b[2],d=g(c);if(!d)return;return Q=d[0],d[1]}}function j(a,b,c){return function(){return"undefined"!=typeof console&&"function"==typeof console.warn&&console.warn(b+" is deprecated, use "+c+" instead.",new Error("").stack),a.apply(a,arguments)}}function k(a){return r(a)?a:s(a)?B(a):A(a)}function l(){function a(a){b=a,f.source=a,X(c,function(b,c){U(function(){a.promiseDispatch.apply(a,c)})},void 0),c=void 0,d=void 0}var b,c=[],d=[],e=$(l.prototype),f=$(o.prototype);if(f.promiseDispatch=function(a,e,f){var g=W(arguments);c?(c.push(g),"when"===e&&f[1]&&d.push(f[1])):U(function(){b.promiseDispatch.apply(b,g)})},f.valueOf=function(){if(c)return f;var a=q(b);return r(a)&&(b=a),a},f.inspect=function(){return b?b.inspect():{state:"pending"}},k.longStackSupport&&O)try{throw new Error}catch(g){f.stack=g.stack.substring(g.stack.indexOf("\n")+1)}return e.promise=f,e.resolve=function(c){b||a(k(c))},e.fulfill=function(c){b||a(A(c))},e.reject=function(c){b||a(z(c))},e.notify=function(a){b||X(d,function(b,c){U(function(){c(a)})},void 0)},e}function m(a){if("function"!=typeof a)throw new TypeError("resolver must be a function.");var b=l();try{a(b.resolve,b.reject,b.notify)}catch(c){b.reject(c)}return b.promise}function n(a){return m(function(b,c){for(var d=0,e=a.length;e>d;d++)k(a[d]).then(b,c)})}function o(a,b,c){void 0===b&&(b=function(a){return z(new Error("Promise does not support operation: "+a))}),void 0===c&&(c=function(){return{state:"unknown"}});var d=$(o.prototype);if(d.promiseDispatch=function(c,e,f){var g;try{g=a[e]?a[e].apply(d,f):b.call(d,e,f)}catch(h){g=z(h)}c&&c(g)},d.inspect=c,c){var e=c();"rejected"===e.state&&(d.exception=e.reason),d.valueOf=function(){var a=c();return"pending"===a.state||"rejected"===a.state?d:a.value}}return d}function p(a,b,c,d){return k(a).then(b,c,d)}function q(a){if(r(a)){var b=a.inspect();if("fulfilled"===b.state)return b.value}return a}function r(a){return b(a)&&"function"==typeof a.promiseDispatch&&"function"==typeof a.inspect}function s(a){return b(a)&&"function"==typeof a.then}function t(a){return r(a)&&"pending"===a.inspect().state}function u(a){return!r(a)||"fulfilled"===a.inspect().state}function v(a){return r(a)&&"rejected"===a.inspect().state}function w(){db.length=0,eb.length=0,fb||(fb=!0)}function x(a,b){fb&&(eb.push(a),db.push(b&&"undefined"!=typeof b.stack?b.stack:"(no stack) "+b))}function y(a){if(fb){var b=Y(eb,a);-1!==b&&(eb.splice(b,1),db.splice(b,1))}}function z(a){var b=o({when:function(b){return b&&y(this),b?b(a):this}},function(){return this},function(){return{state:"rejected",reason:a}});return x(b,a),b}function A(a){return o({when:function(){return a},get:function(b){return a[b]},set:function(b,c){a[b]=c},"delete":function(b){delete a[b]},post:function(b,c){return null===b||void 0===b?a.apply(void 0,c):a[b].apply(a,c)},apply:function(b,c){return a.apply(b,c)},keys:function(){return ab(a)}},void 0,function(){return{state:"fulfilled",value:a}})}function B(a){var b=l();return U(function(){try{a.then(b.resolve,b.reject,b.notify)}catch(c){b.reject(c)}}),b.promise}function C(a){return o({isDef:function(){}},function(b,c){return I(a,b,c)},function(){return k(a).inspect()})}function D(a,b,c){return k(a).spread(b,c)}function E(a){return function(){function b(a,b){var g;if("undefined"==typeof StopIteration){try{g=d[a](b)}catch(h){return z(h)}return g.done?g.value:p(g.value,e,f)}try{g=d[a](b)}catch(h){return c(h)?h.value:z(h)}return p(g,e,f)}var d=a.apply(this,arguments),e=b.bind(b,"next"),f=b.bind(b,"throw");return e()}}function F(a){k.done(k.async(a)())}function G(a){throw new R(a)}function H(a){return function(){return D([this,J(arguments)],function(b,c){return a.apply(b,c)})}}function I(a,b,c){return k(a).dispatch(b,c)}function J(a){return p(a,function(a){var b=0,c=l();return X(a,function(d,e,f){var g;r(e)&&"fulfilled"===(g=e.inspect()).state?a[f]=g.value:(++b,p(e,function(d){a[f]=d,0===--b&&c.resolve(a)},c.reject,function(a){c.notify({index:f,value:a})}))},void 0),0===b&&c.resolve(a),c.promise})}function K(a){return p(a,function(a){return a=Z(a,k),p(J(Z(a,function(a){return p(a,T,T)})),function(){return a})})}function L(a){return k(a).allSettled()}function M(a,b){return k(a).then(void 0,void 0,b)}function N(a,b){return k(a).nodeify(b)}var O=!1;try{throw new Error}catch(P){O=!!P.stack}var Q,R,S=i(),T=function(){},U=function(){function a(){for(;b.next;){b=b.next;var c=b.task;b.task=void 0;var e=b.domain;e&&(b.domain=void 0,e.enter());try{c()}catch(g){if(f)throw e&&e.exit(),setTimeout(a,0),e&&e.enter(),g;setTimeout(function(){throw g},0)}e&&e.exit()}d=!1}var b={task:void 0,next:null},c=b,d=!1,e=void 0,f=!1;if(U=function(a){c=c.next={task:a,domain:f&&process.domain,next:null},d||(d=!0,e())},"undefined"!=typeof process&&process.nextTick)f=!0,e=function(){process.nextTick(a)};else if("function"==typeof setImmediate)e="undefined"!=typeof window?setImmediate.bind(window,a):function(){setImmediate(a)};else if("undefined"!=typeof MessageChannel){var g=new MessageChannel;g.port1.onmessage=function(){e=h,g.port1.onmessage=a,a()};var h=function(){g.port2.postMessage(0)};e=function(){setTimeout(a,0),h()}}else e=function(){setTimeout(a,0)};return U}(),V=Function.call,W=a(Array.prototype.slice),X=a(Array.prototype.reduce||function(a,b){var c=0,d=this.length;if(1===arguments.length)for(;;){if(c in this){b=this[c++];break}if(++c>=d)throw new TypeError}for(;d>c;c++)c in this&&(b=a(b,this[c],c));return b}),Y=a(Array.prototype.indexOf||function(a){for(var b=0;b<this.length;b++)if(this[b]===a)return b;return-1}),Z=a(Array.prototype.map||function(a,b){var c=this,d=[];return X(c,function(e,f,g){d.push(a.call(b,f,g,c))},void 0),d}),$=Object.create||function(a){function b(){}return b.prototype=a,new b},_=a(Object.prototype.hasOwnProperty),ab=Object.keys||function(a){var b=[];for(var c in a)_(a,c)&&b.push(c);return b},bb=a(Object.prototype.toString);R="undefined"!=typeof ReturnValue?ReturnValue:function(a){this.value=a};var cb="From previous event:";k.resolve=k,k.nextTick=U,k.longStackSupport=!1,k.defer=l,l.prototype.makeNodeResolver=function(){var a=this;return function(b,c){b?a.reject(b):a.resolve(arguments.length>2?W(arguments,1):c)}},k.Promise=m,k.promise=m,m.race=n,m.all=J,m.reject=z,m.resolve=k,k.passByCopy=function(a){return a},o.prototype.passByCopy=function(){return this},k.join=function(a,b){return k(a).join(b)},o.prototype.join=function(a){return k([this,a]).spread(function(a,b){if(a===b)return a;throw new Error("Can't join: not the same: "+a+" "+b)})},k.race=n,o.prototype.race=function(){return this.then(k.race)},k.makePromise=o,o.prototype.toString=function(){return"[object Promise]"},o.prototype.then=function(a,b,c){function e(b){try{return"function"==typeof a?a(b):b}catch(c){return z(c)}}function f(a){if("function"==typeof b){d(a,h);try{return b(a)}catch(c){return z(c)}}return z(a)}function g(a){return"function"==typeof c?c(a):a}var h=this,i=l(),j=!1;return U(function(){h.promiseDispatch(function(a){j||(j=!0,i.resolve(e(a)))},"when",[function(a){j||(j=!0,i.resolve(f(a)))}])}),h.promiseDispatch(void 0,"when",[void 0,function(a){var b,c=!1;try{b=g(a)}catch(d){if(c=!0,!k.onerror)throw d;k.onerror(d)}c||i.notify(b)}]),i.promise},k.when=p,o.prototype.thenResolve=function(a){return this.then(function(){return a})},k.thenResolve=function(a,b){return k(a).thenResolve(b)},o.prototype.thenReject=function(a){return this.then(function(){throw a})},k.thenReject=function(a,b){return k(a).thenReject(b)},k.nearer=q,k.isPromise=r,k.isPromiseAlike=s,k.isPending=t,o.prototype.isPending=function(){return"pending"===this.inspect().state},k.isFulfilled=u,o.prototype.isFulfilled=function(){return"fulfilled"===this.inspect().state},k.isRejected=v,o.prototype.isRejected=function(){return"rejected"===this.inspect().state};var db=[],eb=[],fb=!0;k.resetUnhandledRejections=w,k.getUnhandledReasons=function(){return db.slice()},k.stopUnhandledRejectionTracking=function(){w(),fb=!1},w(),k.reject=z,k.fulfill=A,k.master=C,k.spread=D,o.prototype.spread=function(a,b){return this.all().then(function(b){return a.apply(void 0,b)},b)},k.async=E,k.spawn=F,k["return"]=G,k.promised=H,k.dispatch=I,o.prototype.dispatch=function(a,b){var c=this,d=l();return U(function(){c.promiseDispatch(d.resolve,a,b)}),d.promise},k.get=function(a,b){return k(a).dispatch("get",[b])},o.prototype.get=function(a){return this.dispatch("get",[a])},k.set=function(a,b,c){return k(a).dispatch("set",[b,c])},o.prototype.set=function(a,b){return this.dispatch("set",[a,b])},k.del=k["delete"]=function(a,b){return k(a).dispatch("delete",[b])},o.prototype.del=o.prototype["delete"]=function(a){return this.dispatch("delete",[a])},k.mapply=k.post=function(a,b,c){return k(a).dispatch("post",[b,c])},o.prototype.mapply=o.prototype.post=function(a,b){return this.dispatch("post",[a,b])},k.send=k.mcall=k.invoke=function(a,b){return k(a).dispatch("post",[b,W(arguments,2)])},o.prototype.send=o.prototype.mcall=o.prototype.invoke=function(a){return this.dispatch("post",[a,W(arguments,1)])},k.fapply=function(a,b){return k(a).dispatch("apply",[void 0,b])},o.prototype.fapply=function(a){return this.dispatch("apply",[void 0,a])},k["try"]=k.fcall=function(a){return k(a).dispatch("apply",[void 0,W(arguments,1)])},o.prototype.fcall=function(){return this.dispatch("apply",[void 0,W(arguments)])},k.fbind=function(a){var b=k(a),c=W(arguments,1);return function(){return b.dispatch("apply",[this,c.concat(W(arguments))])}},o.prototype.fbind=function(){var a=this,b=W(arguments);return function(){return a.dispatch("apply",[this,b.concat(W(arguments))])}},k.keys=function(a){return k(a).dispatch("keys",[])},o.prototype.keys=function(){return this.dispatch("keys",[])},k.all=J,o.prototype.all=function(){return J(this)},k.allResolved=j(K,"allResolved","allSettled"),o.prototype.allResolved=function(){return K(this)},k.allSettled=L,o.prototype.allSettled=function(){return this.then(function(a){return J(Z(a,function(a){function b(){return a.inspect()}return a=k(a),a.then(b,b)}))})},k.fail=k["catch"]=function(a,b){return k(a).then(void 0,b)},o.prototype.fail=o.prototype["catch"]=function(a){return this.then(void 0,a)},k.progress=M,o.prototype.progress=function(a){return this.then(void 0,void 0,a)},k.fin=k["finally"]=function(a,b){return k(a)["finally"](b)},o.prototype.fin=o.prototype["finally"]=function(a){return a=k(a),this.then(function(b){return a.fcall().then(function(){return b})},function(b){return a.fcall().then(function(){throw b})})},k.done=function(a,b,c,d){return k(a).done(b,c,d)},o.prototype.done=function(a,b,c){var e=function(a){U(function(){if(d(a,f),!k.onerror)throw a;k.onerror(a)})},f=a||b||c?this.then(a,b,c):this;"object"==typeof process&&process&&process.domain&&(e=process.domain.bind(e)),f.then(void 0,e)},k.timeout=function(a,b,c){return k(a).timeout(b,c)},o.prototype.timeout=function(a,b){var c=l(),d=setTimeout(function(){c.reject(new Error(b||"Timed out after "+a+" ms"))},a);return this.then(function(a){clearTimeout(d),c.resolve(a)},function(a){clearTimeout(d),c.reject(a)},c.notify),c.promise},k.delay=function(a,b){return void 0===b&&(b=a,a=void 0),k(a).delay(b)},o.prototype.delay=function(a){return this.then(function(b){var c=l();return setTimeout(function(){c.resolve(b)},a),c.promise})},k.nfapply=function(a,b){return k(a).nfapply(b)},o.prototype.nfapply=function(a){var b=l(),c=W(a);return c.push(b.makeNodeResolver()),this.fapply(c).fail(b.reject),b.promise},k.nfcall=function(a){var b=W(arguments,1);return k(a).nfapply(b)},o.prototype.nfcall=function(){var a=W(arguments),b=l();return a.push(b.makeNodeResolver()),this.fapply(a).fail(b.reject),b.promise},k.nfbind=k.denodeify=function(a){var b=W(arguments,1);return function(){var c=b.concat(W(arguments)),d=l();return c.push(d.makeNodeResolver()),k(a).fapply(c).fail(d.reject),d.promise}},o.prototype.nfbind=o.prototype.denodeify=function(){var a=W(arguments);return a.unshift(this),k.denodeify.apply(void 0,a)},k.nbind=function(a,b){var c=W(arguments,2);return function(){function d(){return a.apply(b,arguments)}var e=c.concat(W(arguments)),f=l();return e.push(f.makeNodeResolver()),k(d).fapply(e).fail(f.reject),f.promise}},o.prototype.nbind=function(){var a=W(arguments,0);return a.unshift(this),k.nbind.apply(void 0,a)},k.nmapply=k.npost=function(a,b,c){return k(a).npost(b,c)},o.prototype.nmapply=o.prototype.npost=function(a,b){var c=W(b||[]),d=l();return c.push(d.makeNodeResolver()),this.dispatch("post",[a,c]).fail(d.reject),d.promise},k.nsend=k.nmcall=k.ninvoke=function(a,b){var c=W(arguments,2),d=l();return c.push(d.makeNodeResolver()),k(a).dispatch("post",[b,c]).fail(d.reject),d.promise},o.prototype.nsend=o.prototype.nmcall=o.prototype.ninvoke=function(a){var b=W(arguments,1),c=l();return b.push(c.makeNodeResolver()),this.dispatch("post",[a,b]).fail(c.reject),c.promise},k.nodeify=N,o.prototype.nodeify=function(a){return a?void this.then(function(b){U(function(){a(null,b)})},function(b){U(function(){a(b)})}):this};var gb=i();return k});
define('js/ajaxforge',['q'],
function (Q)
{
  

  var isInitialized = false;
  var socketpool = null;
  var socketCounter = 0;
  var clientPool = {};
  var settingsPool = {};

  var serverCerts = [];
  var clientCerts = [];
  var clientKey = [];

  var defaultPoolUrl = 'SocketPool.swf';
  var defaultPoolId = 'socketPool';

  // $.ajaxforge(url, settings)
  // $.ajaxforge(settings)
  $.ajaxforge = function (arg1, arg2)
  {
//    $.ajaxforge.initialize(defaultPoolUrl, defaultPoolId);
    var settings;
    if (arguments.length === 2)
    {
      settings = arg2;
      settings.url = arg1;
    }
    else
    {
      settings = arg1;
    }

    settings.deferred = Q.defer();

    var url = $('<a>', { href: settings.url } )[0];
    var host = url.hostname;
    var path = url.pathname + url.search + url.hash;
    var sendData = settings.data;

    var newClient = initClient(url.protocol + '//' + host + ':' + url.port,
                               socketCounter);
    clientPool[socketCounter] = newClient;
    settingsPool[socketCounter] = settings;
    sendClient(newClient, path, sendData, socketCounter);

    socketCounter += 1;

    return settings.deferred.promise.then(function (response) {
      return settings.converters['xml json']($.parseXML(response.body));
    });
  };

  $.ajaxforge.initialize = function (flashUrl, flashId, serverCertText,
                                     clientKeyText, clientCertText)
  {
    setServerCert(serverCertText);
    setClientKey(clientKeyText);
    setClientCert(clientCertText);
    if (! isInitialized)
    {
//      swfobject.embedSWF(flashUrl, flashId, '0', '0', '9.0.0',
//                         false, {}, {allowscriptaccess: 'always'}, {});
      socketpool = forge.net.createSocketPool({
        flashId: flashId,
        policyPort: 843,
        msie: false
        });

      isInitialized = true;
    }
  };

  function initClient(host, socketId)
  {
    var result = null;
    try
    {
//      console.log('initClient -- ' + host);
      var arg = {
        url: host,
        socketPool: socketpool,
        connections: 1,
        // optional cipher suites in order of preference
        caCerts : serverCerts,
        cipherSuites: [
          forge.tls.CipherSuites.TLS_RSA_WITH_AES_128_CBC_SHA,
          forge.tls.CipherSuites.TLS_RSA_WITH_AES_256_CBC_SHA],
        verify: function(c, verified, depth, certs)
        {
          return verified;
        },
        primeTlsSockets: false
      };
      if (clientCerts.length > 0)
      {
        arg.getCertificate = function(c, request) { return clientCerts; };
        arg.getPrivateKey = function(c, cert) { return clientKey; };
      }
      result = forge.http.createClient(arg);
    }
    catch(ex)
    {
      console.log('ERROR: client_init', host);
      console.dir(ex);
    }
  
    return result;
  }


  function sendClient(client, path, data, socketId)
  {
    var requestArg = {
      path: path,
      method: 'GET'
    };
    if (data != "")
    {
      requestArg.method = 'POST';
      requestArg.headers = [{'Content-Type': 'text/xml'}];
      requestArg.body = data;
    }
    var request = forge.http.createRequest(requestArg);
    console.log('sendClient', request);
    try {
    client.send({
      request: request,
      connected: function(e)
      {
//        console.log('forge.tests.tls', 'connected', e);
      },
      headerReady: function(e)
      {
//        console.log('forge.tests.tls', 'header ready', e);
      },
      bodyReady: function(e)
      {
//        console.log('bodyReady', e);
        settingsPool[socketId].deferred.resolve(e.response);
      },
      error: function(e)
      {
        console.log('error', e);
        settingsPool[socketId].deferred.reject([e.type, e.message,
                                                e.cause]);
        e.socket.close();
      }
    });
    } catch (e) {
      console.log('sendError', e);
    }
    return false;
  }

  function setServerCert(newCert)
  {
    try
    {
      serverCerts = [];
      var list = newCert.split("-----END CERTIFICATE-----");
      for (var i = 0; i < list.length - 1; ++i)
      {
        serverCerts.push(list[i] + "-----END CERTIFICATE-----\n");
      }
    }
    catch(ex)
    {
      console.log('ERROR setServerCert:');
      console.dir(ex);
    }
  }

  function setClientCert(newCert)
  {
    try
    {
      clientCerts = [];
      if (typeof(newCert) === "string")
      {
        var list = newCert.split("-----END CERTIFICATE-----");
        for (var i = 0; i < list.length - 1; ++i)
        {
          clientCerts.push(list[i] + "-----END CERTIFICATE-----\n");
        }
      }
      else
      {
        clientCerts = newCert;
      }
    }
    catch (ex)
    {
      console.log('ERROR setting client cert:');
      console.dir(ex);
    }
  }

  function setClientKey(newKey)
  {
    try
    {
      clientKey = newKey;
    }
    catch(ex)
    {
      console.log('ERROR setting client key:');
      console.dir(ex);
    }
  }

  return $.ajaxforge;
});

define('js/actor/Login',['underscore', 'backbone',
	'js/cache', 'js/actor/LoginView', 'js/ajaxforge'],
function (_, Backbone, cache, LoginView)
{
  

  var Login = function (rootDom, canvas, transition, saList)
  {
    _.extend(this, Backbone.Events);
    this.root = rootDom;
    this.certWindow = null;
    this.canvas = canvas;
    this.transition = transition;
    this.saList = saList;
    this.view = new LoginView({
      el: rootDom.find('#loginContainer'),
      login: this,
      transition: transition,
      saList: saList
    });
    rootDom.find('button.logout').click(logout);
    window.addEventListener('message', _.bind(this.certFromMessage, this));
    // This cannot run directly in the initializer because we need to
    // give others a chance to bind to the events this might invoke.
    _.defer(_.bind(this.checkCache, this));
  }

  Login.prototype.checkCache = function ()
  {
    this.transition.page($('#loadingContainer'), $('#loginContainer'));
    $('#loginContainer').one('transitionend', function() {
      $('#clientPhrase').focus();
    });
    if (cache.get('userKey') && cache.get('userCert'))
    {
      var combined = cache.get('userKey') + '\n' + cache.get('userCert');
      $('#clientKey').val(combined);
      this.parseCert(combined);
      if (cache.get('passphrase') !== null &&
	  cache.get('passphrase') !== undefined)
      {
	this.tryPassphrase(cache.get('passphrase'));
      }
/*
      {
      	pageTransition($('#certificate'), $('#passphrase'));
	$('#passphrase').one('transitionend', function() {
	  $('#clientPhrase').focus();
	});
      }
*/
    }
  };

  Login.prototype.getCertFromSA = function ()
  {
    // The confirmation event is a message that is caught and handled
    // by LoginView
    var that = this;
    if (this.certWindow)
    {
      this.certWindow.close();
    }

    var url = cache.get('provider')['web-cert-url'];
    this.certWindow = window.open(url, 'Slice Authority Credential',
				  'height=400,width=600,toolbar=yes');
    $(this.certWindow.document).ready(function() {
      that.root.find('.windowOpen').removeClass('hidden');
      $(that.certWindow).focus();

      var interval = window.setInterval(function() {
    	if (that.certWindow.closed)
	{
    	  window.clearInterval(interval);
    	  that.root.find('.windowOpen').addClass('hidden');
    	}
      }, 250);
    });
  };

  Login.prototype.certFromMessage = function (event)
  {
    if (event.source === this.certWindow && event.data &&
	event.data.certificate && event.data.authority)
    {
      this.root.find('#clientKey').val(event.data.certificate);
      this.view.clickManualCert();
    }
  };

  Login.prototype.parseCert = function (source)
  {
    var inKey = false;
    var inCert = false;
    var cert = "";
    var key = "";
    var lines = source.split('\n');
    var i = 0;
    for (i = 0; i < lines.length; i += 1)
    {
      if (lines[i] === "-----BEGIN RSA PRIVATE KEY-----")
      {
	key = lines[i] + '\n';
	inKey = true;
      }
      else if (lines[i] === "-----END RSA PRIVATE KEY-----")
      {
	key += lines[i] + '\n';
	inKey = false;
      }
      else if (inKey)
      {
	key += lines[i] + '\n';
      }
      else if (lines[i] === "-----BEGIN CERTIFICATE-----")
      {
	cert += lines[i] + '\n';
	inCert = true;
      }
      else if (lines[i] === "-----END CERTIFICATE-----")
      {
	cert += lines[i] + '\n';
	inCert = false;
      }
      else if (inCert)
      {
	cert += lines[i] + '\n';
      }
    }

    cache.set('userKey', key);
    cache.set('userCert', cert);

    this.getCertFields(cert);
  };

  Login.prototype.getCertFields = function (certificate)
  {
    var cert = forge.pki.certificateFromPem(certificate);
    var found = false;
    var urn;
    var name;

    var altField;
    _.each(cert.extensions, function (field) {
      if (field.name === 'subjectAltName')
      {
	altField = field;
      }
    });
    if (altField)
    {
      _.each(altField.altNames, function (item) {
	if (item.type === 6 && item.value.substr(0, 12) === 'urn:publicid')
	{
	  urn = item.value;
	}
      });
    }
    if (urn)
    {
      name = urn.split('+')[3];
      cache.set('userUrn', urn);
      cache.set('userName', name);
      found = true;
    }

    if (found)
    {
      var authorityUrn = 'urn:publicid:IDN+' + urn.split('+')[1] +
	'+authority+sa';
      var index = 0;
      _.each(this.saList, function (provider, i) {
	if (provider.urn === authorityUrn)
	{
	  index = i;
	}
      });
      cache.set('provider', this.saList[index]);

      var currentSAName;
      if (cache.get('provider').name)
      {
	currentSAName = _.escape(cache.get('provider').name);
      }
      else
      {
	currentSAName = _.escape(cache.get('provider')['urn'].split('+')[1]);
      }
      this.view.fillPassphraseForm(name, currentSAName);
    }
  };

  Login.prototype.tryPassphrase = function (passphrase)
  {
    try
    {
      var decrypted = forge.pki.decryptRsaPrivateKey(cache.get('userKey'),
						     passphrase);
      var keyText = forge.pki.privateKeyToPem(decrypted);
      $.ajaxforge.initialize('SocketPool.swf', 'socketPool',
			     cache.get('cabundle'), keyText,
			     cache.get('userCert'));
      this.root.find('#passError').addClass('hidden');
      this.root.find('#passGroup').removeClass('has-error');
      cache.set('passphrase', passphrase);
      this.trigger('login-complete');
    }
    catch (e)
    {
      console.log('Error: ', e);
      this.root.find('#passError').html('Could not decrypt private key. Please check your passphrase.');
      this.root.find('#passError').removeClass('hidden');
      this.root.find('#passGroup').addClass('has-error');
    }
  };

  function logout(event)
  {
    event.preventDefault();
    cache.remove('passphrase');
    cache.remove('userName');
    cache.remove('userUrn');
    cache.remove('userCert');
    cache.remove('userKey');
    cache.remove('userCredential');
    cache.remove('provider');
    cache.remove('salist');
    cache.remove('amlist');
    cache.remove('cabundle');
    window.location.reload();
  }

  return Login;
});

/*jshint browser:true */
/*global jQuery */
(function($) {
	

	var XmlRpcFault = function() {
		Error.apply(this, arguments);
	};
	XmlRpcFault.prototype = new Error();
	XmlRpcFault.prototype.type = 'XML-RPC fault';

	var xmlrpc = $.xmlrpc = function(url, settings) {

		if (arguments.length === 2) {
			settings.url = url;
		} else {
			settings = url;
			url = settings.url;
		}

		settings.dataType = 'xml json';
		settings.type = 'POST';
		settings.contentType = 'text/xml';
		settings.converters = {'xml json': xmlrpc.parseDocument};

		var xmlDoc = xmlrpc.document(settings.methodName, settings.params || []);

		if ("XMLSerializer" in window) {
			settings.data = new window.XMLSerializer().serializeToString(xmlDoc);
		} else {
			// IE does not have XMLSerializer
			settings.data = xmlDoc.xml;
		}

		return $.ajaxforge(settings);
	};

	/**
	* Make an XML document node.
	*/
	xmlrpc.createXMLDocument = function () {

		if (document.implementation && "createDocument" in document.implementation) {
			// Most browsers support createDocument
			return document.implementation.createDocument(null, null, null);

		} else {
			// IE uses ActiveXObject instead of the above.
			var i, length, activeX = [
				"MSXML6.DomDocument", "MSXML3.DomDocument",
				"MSXML2.DomDocument", "MSXML.DomDocument", "Microsoft.XmlDom"
			];
			for (i = 0, length = activeX.length; i < length; i++) {
				try {
					return new ActiveXObject(activeX[i]);
				} catch(_) {}
			}
		}
	};

	/**
	* Make an XML-RPC document from a method name and a set of parameters
	*/
	xmlrpc.document = function(name, params) {
		var doc = xmlrpc.createXMLDocument();


		var $xml = function(name) {
			return $(doc.createElement(name));
		};

		var $methodName = $xml('methodName').text(name);
		var $params = $xml('params').append($.map(params, function(param) {
			var $value = $xml('value').append(xmlrpc.toXmlRpc(param, $xml));
			return $xml('param').append($value);
		}));
		var $methodCall = $xml('methodCall').append($methodName, $params);
		doc.appendChild($methodCall.get(0));
		return doc;
	};

	var _isInt = function(x) {
		return (x === parseInt(x, 10)) && !isNaN(x);
	};

	/**
	* Take a JavaScript value, and return an XML node representing the value
	* in XML-RPC style. If the value is one of the `XmlRpcType`s, that type is
	* used. Otherwise, a best guess is made as to its type. The best guess is
	* good enough in the vast majority of cases.
	*/
	xmlrpc.toXmlRpc = function(item, $xml) {

		if (item instanceof XmlRpcType) {
			return item.toXmlRpc($xml);
		}

		var types = $.xmlrpc.types;
		var type = $.type(item);

		switch (type) {
			case "undefined":
			case "null":
				return types.nil.encode(item, $xml);

			case "date":
				return types['datetime.iso8601'].encode(item, $xml);

			case "object":
				if (item instanceof ArrayBuffer) {
					return types.base64.encode(item, $xml);
				} else {
					return types.struct.encode(item, $xml);
				}
				break;


			case "number":
				// Ints and Floats encode differently
				if (_isInt(item)) {
					return types['int'].encode(item, $xml);
				} else {
					return types['double'].encode(item, $xml);
				}
				break;

			case "array":
			case "boolean":
			case "string":
				return types[type].encode(item, $xml);

			default:
				throw new Error("Unknown type", item);
		}
	};

	/**
	* Take an XML-RPC document and decode it to an equivalent JavaScript
	* representation.
	*
	* If the XML-RPC document represents a fault, then an equivalent
	* XmlRpcFault will be thrown instead
	*/
	xmlrpc.parseDocument = function(doc) {
		var $doc = $(doc);
		var $response = $doc.children('methodresponse');

		var $fault = $response.find('> fault');
		if ($fault.length === 0) {
			var $params = $response.find('> params > param > value > *');
			var json = $params.toArray().map(xmlrpc.parseNode);
			return json;
		} else {
			var fault = xmlrpc.parseNode($fault.find('> value > *').get(0));
			var err = new XmlRpcFault(fault.faultString);
			err.msg = err.message = fault.faultString;
			err.type = err.code = fault.faultCode;
		  console.log('err', err);
			throw err;
		}
	};

	/*
	* Take an XML-RPC node, and return the JavaScript equivalent
	*/
	xmlrpc.parseNode = function(node) {

		// Some XML-RPC services return empty <value /> elements. This is not
		// legal XML-RPC, but we may as well handle it.
		if (node === undefined) {
			return null;
		}
		var nodename = node.nodeName.toLowerCase();
		if (nodename in xmlrpc.types) {
			return xmlrpc.types[nodename].decode(node);
		} else {
			throw new Error('Unknown type ' + nodename);
		}
	};

	/*
	* Take a <value> node, and return the JavaScript equivalent.
	*/
	xmlrpc.parseValue = function(value) {
		var child = $(value).children()[0];
		if (child) {
			// Child nodes should be decoded.
			return xmlrpc.parseNode(child);
		} else {
			// If no child nodes, the value is a plain text node.
			return $(value).text();
		}
	};

	var XmlRpcType = function() { };

	$.xmlrpc.types = {};

	/**
	* Make a XML-RPC type. We use these to encode and decode values. You can
	* also force a values type using this. See `$.xmlrpc.force()`
	*/
	xmlrpc.makeType = function(tagName, simple, encode, decode) {
		var Type;

		Type = function(value) {
			this.value = value;
		};
		Type.prototype = new XmlRpcType();
		Type.prototype.tagName = tagName;

		if (simple) {
			var simpleEncode = encode, simpleDecode = decode;
			encode = function(value, $xml) {
				var text = simpleEncode(value);
				return $xml(Type.tagName).text(text);
			};
			decode = function(node) {
				return simpleDecode($(node).text(), node);
			};
		}
		Type.prototype.toXmlRpc = function($xml) {
			return Type.encode(this.value, $xml);
		};

		Type.tagName = tagName;
		Type.encode = encode;
		Type.decode = decode;

		xmlrpc.types[tagName.toLowerCase()] = Type;
	};


	// Number types
	var _fromInt = function(value) { return '' + Math.floor(value); };
	var _toInt = function(text, _) { return parseInt(text, 10); };

	xmlrpc.makeType('int', true, _fromInt, _toInt);
	xmlrpc.makeType('i4', true, _fromInt, _toInt);
	xmlrpc.makeType('i8', true, _fromInt, _toInt);
	xmlrpc.makeType('i16', true, _fromInt, _toInt);
	xmlrpc.makeType('i32', true, _fromInt, _toInt);

	xmlrpc.makeType('double', true, String, function(text) {
		return parseFloat(text, 10);
	});

	// String type. Fairly simple
	xmlrpc.makeType('string', true, String, String);

	// Boolean type. True == '1', False == '0'
	xmlrpc.makeType('boolean', true, function(value) {
		return value ? '1' : '0';
	}, function(text) {
		return text === '1';
	});

	// Dates are a little trickier
	var _pad = function(n) { return n<10 ? '0'+n : n; };

	xmlrpc.makeType('dateTime.iso8601', true, function(d) {
		return [
			d.getUTCFullYear(), '-', _pad(d.getUTCMonth()+1), '-',
			_pad(d.getUTCDate()), 'T', _pad(d.getUTCHours()), ':',
			_pad(d.getUTCMinutes()), ':', _pad(d.getUTCSeconds()), 'Z'
		].join('');
	}, function(text) {
		return new Date(text);
	});

	// Go between a base64 string and an ArrayBuffer
	xmlrpc.binary = (function() {
		var pad = '=';
		var toChars = ('ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
			'abcdefghijklmnopqrstuvwxyz0123456789+/').split("");
		var fromChars = toChars.reduce(function(acc, chr, i) {
			acc[chr] = i;
			return acc;
		}, {});

		/*
		* In the following, three bytes are added together into a 24-bit
		* number, which is then split up in to 4 6-bit numbers - or vice versa.
		* That is why there is lots of shifting by multiples of 6 and 8, and
		* the magic numbers 3 and 4.
		*
		* The modulo 64 is for converting to base 64, and the modulo 256 is for
		* converting to 8-bit numbers.
		*/
		return {
			toBase64: function(ab) {
				var acc = [];

				var int8View = new Uint8Array(ab);
				var int8Index = 0, int24;
				for (; int8Index < int8View.length; int8Index += 3) {

					// Grab three bytes
					int24 =
						(int8View[int8Index + 0] << 16) +
						(int8View[int8Index + 1] << 8) +
						(int8View[int8Index + 2] << 0);

					// Push four chars
					acc.push(toChars[(int24 >> 18) % 64]);
					acc.push(toChars[(int24 >> 12) % 64]);
					acc.push(toChars[(int24 >> 6) % 64]);
					acc.push(toChars[(int24 >> 0)% 64]);
				}

				// Set the last few characters to the padding character
				var padChars = 3 - ((ab.byteLength % 3) || 3);
				while (padChars--) {
					acc[acc.length - padChars - 1] = pad;
				}

				return acc.join('');
			},

			fromBase64: function(base64) {
				var base64Len = base64.length;

				// Work out the length of the data, accommodating for padding
				var abLen = (base64Len / 4) * 3;
				if (base64.charAt(base64Len - 1) === pad) { abLen--; }
				if (base64.charAt(base64Len - 2) === pad) { abLen--; }

				// Make the ArrayBuffer, and an Int8Array to work with it
				var ab = new ArrayBuffer(abLen);
				var int8View = new Uint8Array(ab);

				var base64Index = 0, int8Index = 0, int24;
				for (; base64Index < base64Len; base64Index += 4, int8Index += 3) {

					// Grab four chars
					int24 =
						(fromChars[base64[base64Index + 0]] << 18) +
						(fromChars[base64[base64Index + 1]] << 12) +
						(fromChars[base64[base64Index + 2]] << 6) +
						(fromChars[base64[base64Index + 3]] << 0);

					// Push three bytes
					int8View[int8Index + 0] = (int24 >> 16) % 256;
					int8View[int8Index + 1] = (int24 >> 8) % 256;
					int8View[int8Index + 2] = (int24 >> 0) % 256;

				}

				return ab;
			}
		};
	})();

	xmlrpc.makeType('base64', true, function(ab) {
		return xmlrpc.binary.toBase64(ab);
	}, function(text) {
		return xmlrpc.binary.fromBase64(text);
	});

	// Nil/null
	xmlrpc.makeType('nil', false,
		function(val, $xml) { return $xml('nil'); },
		function(_) { return null; }
	);

	// Structs/Objects
	xmlrpc.makeType('struct', false, function(value, $xml) {
		var $struct = $xml('struct');

		$.each(value, function(name, value) {
			var $name = $xml('name').text(name);
			var $value = $xml('value').append(xmlrpc.toXmlRpc(value, $xml));
			$struct.append($xml('member').append($name, $value));
		});

		return $struct;

	}, function(node) {
		return $(node)
			.find('> member')
			.toArray()
			.reduce(function(struct, el) {
				var $el = $(el);
				var key = $el.find('> name').text();
				var value = xmlrpc.parseValue($el.find('> value'));

				struct[key] = value;
				return struct;
			}, {});

	});

	// Arrays
	xmlrpc.makeType('array', false, function(value, $xml) {
		var $array = $xml('array');
		var $data = $xml('data');
		$.each(value, function(i, val) {
			$data.append($xml('value').append(xmlrpc.toXmlRpc(val, $xml)));
		});
		$array.append($data);
		return $array;
	}, function(node) {
		return $(node).find('> data > value').toArray()
			.map(xmlrpc.parseValue);
	});


	/**
	* Force a value to an XML-RPC type. All the usual XML-RPC types are
	* supported
	*/
	xmlrpc.force = function(type, value) {
		return new xmlrpc.types[type](value);
	};

})(jQuery);

define("lib/jquery.xmlrpc", function(){});

define('js/actor/FetchSlices',['underscore', 'backbone', 'q', 'js/cache',
	'lib/jquery.xmlrpc'],
function (_, Backbone, Q, cache)
{
  

  function FetchSlices (root, transition, amList, canvas)
  {
    this.root = root;
    this.transition = transition;
    this.amList = amList;
    this.canvas = canvas;
    this.userSlices = null;
  }

  FetchSlices.prototype.checkCode = function (response, action, isGeni)
  {
    if ((! isGeni && response[0].code !== 0) ||
	(isGeni && response[0].code.geni_code !== 0))
    {
      this.transition.startFail($('#loginContainer'), 'Failed: ' + action,
				response);
      throw 'Failed: ' + action + JSON.stringify(response);
    }
  };

  FetchSlices.prototype.fetchXmlrpc = function ()
  {
    var that = this;
    this.transition.startWaiting($('#passphrase'), null);
    var deferred = Q.defer();
    var promise = deferred.promise;
    if (!cache.get('userCredential'))
    {
      promise = promise.then(function () {
        that.transition.startWaiting(null, 'Fetching Your Credential...');
        return $.xmlrpc({
	  url: cache.get('provider')['pg-sa-url'],
	  methodName: 'GetCredential',
	  params: [{}]
        });
      }).then(function (response) {
	that.checkCode(response, 'Fetch Credential');
        cache.set('userCredential', response[0].value);
      });
    }
    promise.then(function () {
      that.transition.startWaiting(null, 'Finding Slices...');
      return $.xmlrpc({
        url: cache.get('provider')['pg-sa-url'],
        methodName: 'Resolve',
        params: [{credential: cache.get('userCredential'),
		  urn: cache.get('userUrn'),
		  type: 'User' }]
      });
    }).then(function (response) {
      that.checkCode(response, 'Find Slices');
      that.userSlices = response[0].value.slices;
      if (! that.userSlices)
      {
	that.transition.startFail($('#loginContainer'), 'Found no slices',
				  response);
	throw 'Found no slices: ' + JSON.stringify(response);
      }
      that.updateSliceList(_.bind(that.selectSlice, that));
      that.transition.stopWaiting($('#sliceListContainer'));
    }).fail(function (error) {
      that.transition.startFail($('#loginContainer'), 'Error during setup',
				error);
      console.log('ERROR: ', error);
    });
    deferred.resolve();
  };

  FetchSlices.prototype.selectSlice = function (sliceUrn)
  {
    var that = this;
    if ($('#loginContainer').hasClass('afterHide')) {
      this.transition.page($('#canvasContainer'), $('#loginContainer'));
      $('#waitContainer').removeClass('hides afterHide');
    }
    this.transition.startWaiting($('#sliceListContainer'),
				 'Resolving Slice...');

    var sliceCredential;
    var amUrl;
    var promise = $.xmlrpc({
      url: cache.get('provider')['pg-sa-url'],
      methodName: 'Resolve',
      params: [{credential: cache.get('userCredential'),
		urn: sliceUrn,
		type: 'Slice' }]
    });
    promise.then(function (response) {
      that.checkCode(response, 'Resolving Slice');
      that.transition.startWaiting(null, 'Fetching Credential...');
      var urn = response[0].value.component_managers[0];
      if (! urn)
      {
	that.transition.startFail($('#loginContainer'),
				  'Could not find AM URN for this slice',
				  null);
	throw 'Could not find URN for this slice';
      }
      amUrl = that.amList[urn];
      if (! amUrl)
      {
	that.transition.startFail($('#loginContainer'),
				  'Could not convert AM URN ' + urn 
				  + ' to URL', null);
	throw 'Could not convert AM URN to URL';
      }
      return $.xmlrpc({
	url: cache.get('provider')['pg-sa-url'],
	methodName: 'GetCredential',
	params: [{credential: cache.get('userCredential'),
		  urn: sliceUrn,
		  type: 'Slice' }]
      });
    }).then(function (response) {
      that.checkCode(response, 'Fetching Credential');
      that.transition.startWaiting(null, 'Fetching Manifest...');

      //  sliceCredential = { 'geni_type': 'geni_sfa',
      //          'geni_version': '3',
      //          'geni_value': response[0].value };

      sliceCredential = response[0].value;
      var options = {
	'geni_rspec_version': {
	  'type': 'ProtoGENI',
	  'version': '3'
	},
	'geni_slice_urn': sliceUrn
      };
      return $.xmlrpc({
        url: amUrl,
	methodName: 'ListResources',
        params: [[sliceCredential], options]
      });
    }).then(function (response) {
      that.checkCode(response, 'Fetching Manifest', true);
      that.transition.switchNav($('#small'));
      $('#canvasContainer').removeClass('afterHide');
      that.transition.page($('#loginContainer'), that.canvas.domRoot);
      that.canvas.show(response[0].value);
      that.transition.stopWaiting($('#sliceListContainer'));
      //      initJacks($('#canvasContainer'), response[0].value);
      var name = sliceUrn.split('+')[3];
      $('#lnSliceList .active').removeClass('active');
      $('.name' + name).addClass('active');
    }).fail(function (error) {
      that.transition.startFail($('#loginContainer'), 'Error during setup',
				error);
      console.log('ERROR: ', error);
    });
  };


  FetchSlices.prototype.updateSliceList = function (selectSlice)
  {
    var that = this;
    var options = $('<div/>');
    var options2 = $('<div/>');
    _.each(that.userSlices, function (urn) {
      var name = urn.split('+')[3];
      var newOption = $('<a class="list-group-item" href="">' + name + 
			'<span class="glyphicon glyphicon-chevron-right pull-right"></span>' +
			'</a>');
      newOption.click(function (event) {
	event.preventDefault();
	selectSlice(urn);
      });
      options.append(newOption);
      var newOption2 = $('<a class="list-group-item name' + name + '" href="">' + name + 
			 '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' +
			 '</a>');
      newOption2.click(function (event) {
	event.preventDefault();
	selectSlice(urn);
	$('#leftNav > .active').removeClass('active');
      });
      options2.append(newOption2);
    });
    $('#slice-list').html(options);
    $('#lnSliceList').html(options2);
  }

  return FetchSlices;
});

define('js/actor/Actor.js',['underscore', 'backbone', 'js/cache',
	'js/actor/Login', 'js/actor/FetchSlices'],
function (_, Backbone, cache, Login, FetchSlices)
{
  

  function Actor(rootDom, canvas, transition)
  {
    this.rootDom = rootDom;
    this.canvas = canvas;
    this.transition = transition;
    this.saList = [];
    this.amList = {};
    this.bundle = '';
    this.login = null;
    this.fetchSlices = null;

    swfobject.embedSWF(
      'SocketPool.swf', 'socketPool',
      '0', '0',
      '9.0.0', false,
      {}, {allowscriptaccess: 'always'}, {}, _.bind(this.swfComplete, this));
    swfobject.createCSS("#socketPool", "opacity: 1;");
  }

  Actor.prototype.swfComplete = function (event)
  {
    if (event.success)
    {
      var sa, am, caBundle;

      if (cache.get('salist'))
      {
	this.parseSAList(cache.get('salist'));
      }
      else
      {
	sa = $.ajax({
	  url: 'https://www.emulab.net/protogeni/boot/salist.json',
	  dataType: 'text'
	}).then(_.bind(this.parseSAList, this));
      }

      if (cache.get('amlist'))
      {
	this.parseAMList(cache.get('amlist'));
      }
      else
      {
	am = $.ajax({
	  url: 'https://www.emulab.net/protogeni/boot/amlist.json',
	  dataType: 'text'
	}).then(_.bind(this.parseAMList, this));
      }

      if (cache.get('cabundle'))
      {
	this.parseCABundle(cache.get('cabundle'));
      }
      else
      {
	caBundle = $.ajax({
	  url: 'https://www.emulab.net/protogeni/boot/genica.bundle',
	  dataType: 'text'
	}).then(_.bind(this.parseCABundle, this));
      }

      var waitForSwf = $.Deferred();
      setTimeout(function () {
	waitForSwf.resolve();
      }, 500);

      var that = this;
      $.when(sa, am, caBundle, waitForSwf.promise()).then(function () {
	that.transition.page(that.rootDom.find('#loadingContainer'),
			     that.rootDom.find('#loginContainer'));
	that.login = new Login(that.rootDom, that.canvas, that.transition,
			       that.saList);
	that.login.on('login-complete', _.bind(that.loginComplete, that));
      });
    }
    else
    {
      this.transition.startFail(this.rootDom.find('#loadingContainer'),
				'Failed to load socket flash file', event);
    }
  };

  Actor.prototype.parseSAList = function (response)
  {
    cache.set('salist', response);
    var inputOptions = '';
    var data = JSON.parse(response);
    var list = data['identity-providers'];
    this.saList = list;
  };

  Actor.prototype.parseAMList = function (response)
  {
    cache.set('amlist', response);
    var data = JSON.parse(response);
    var list = data['aggregate-managers'];

    var that = this;
    _.each(list, function (item) {
      that.amList[item.urn] = item['am-url'];
    });
  };

  Actor.prototype.parseCABundle = function (response)
  {
    cache.set('cabundle', response);
  };

  Actor.prototype.loginComplete = function ()
  {
    this.fetchSlices = new FetchSlices(this.rootDom, this.transition,
				       this.amList, this.canvas);
    this.fetchSlices.fetchXmlrpc();
  };

  return Actor;
});


define('text!html/Viewer.html',[],function () { return '<!-- Error Mode -->\n<div id="failContainer" class="container hides afterHide">\n  <div class="row">\n    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">\n      <div class="alert alert-danger">\n\t<h1 id="failTitle"></h1>\n\t<pre id="failMessage"></pre>\n      </div>\n    </div>\n  </div>\n</div>\n\n<!-- Main Jacks page -->\n<div id="canvasContainer" class="container"></div>\n<!-- End Main Jacks page -->\n\n<!-- Loading containers -->\n<div id="loadingContainer" class="container">\n  <div class="row loading">\n    <h1 id="title">Loading...</h1>\n    <noscript>\n      <h1>Jacks requires that JavaScript be turned on</h1>\n    </noscript>\n  </div>\n</div>\n<!-- End Loading containers -->\n';});


define('text!html/Actor.html',[],function () { return '<div id="wrap">\r\n\r\n<!-- Top nav -->\r\n\r\n  <div id="large" class="navbar navbar-static-top" role="navigation">\r\n    <button class="btn btn-danger logout pull-right">Logout</button>\r\n    <div class="brand">\r\n      <div class="logo-container">\r\n\t<img height="45px" src="images/jacks-logo.svg">\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div id="small" class="navbar navbar-static-top hidden" role="navigation">\r\n    <button class="btn btn-danger logout pull-right">Logout</button>\r\n    <div class="brand">\r\n      <div class="logo-container">\r\n\t<img height="25px" src="images/jacks-logo.svg">\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n<!-- End Top nav -->\r\n\r\n<!-- Error Mode -->\r\n  <div id="failContainer" class="container hides afterHide">\r\n    <div class="row">\r\n      <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">\r\n\t<div class="alert alert-danger">\r\n\t  <h1 id="failTitle"></h1>\r\n\t  <pre id="failMessage"></pre>\r\n\t</div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n<!-- Main Jacks page -->\r\n  <div id="canvasContainer" class="container hides afterHide"></div>\r\n<!-- End Main Jacks page -->\r\n\r\n<!-- Loading containers -->\r\n\r\n  <div id="loadingContainer" class="container">\r\n    <div class="row loading">\r\n      <h1 id="title">Loading...</h1>\r\n      <noscript>\r\n        <h1>Jacks requires that JavaScript be turned on</h1>\r\n      </noscript>\r\n    </div>\r\n  </div>\r\n\r\n<!-- End Loading containers -->\r\n\r\n  <div class="windowOpen hidden">\r\n    <div class="alert alert-success"><h5 class="panel-title">Please continue in popup window.</h5></div>\r\n  </div>\r\n\r\n<!-- Login pages -->\r\n\r\n  <div id="loginContainer" class="container hides"></div></div>\r\n\r\n\r\n\r\n<!-- End Login pages -->\r\n</div>\r\n\r\n<div id="socketPool" class="row hides">\r\n  <p>Could not load the Flash SocketPool for Jacks. Make sure you have Flash installed.</p>\r\n</div>\r\n';});


define('text!html/Editor.html',[],function () { return '<!-- Error Mode -->\n<div id="failContainer" class="container hides afterHide">\n  <div class="row">\n    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">\n      <div class="alert alert-danger">\n\t<h1 id="failTitle"></h1>\n\t<pre id="failMessage"></pre>\n      </div>\n    </div>\n  </div>\n</div>\n<!-- End Error Mode -->\n\n<!-- Main Jacks page -->\n<div id="canvasContainer" class="container"></div>\n<!-- End Main Jacks page -->\n\n<!-- Loading containers -->\n<div id="loadingContainer" class="container">\n  <div class="row loading">\n    <h1 id="title">Loading...</h1>\n    <noscript>\n      <h1>Jacks requires that JavaScript be turned on</h1>\n    </noscript>\n  </div>\n</div>\n<!-- End Loading containers -->\n';});

/*
 * main.js
 *
 */

/*global require: true */
/*
require.config({
  baseUrl: '.',
  paths: {
    'jquery-xmlrpc': 'lib/jquery.xmlrpc',
    'backbone': 'lib/backbone-min',
    'underscore': 'lib/underscore-min',
    'demo': 'js/demo',
    'tourist': 'lib/tourist',
    'q': 'lib/q.min',
    'highlight': 'lib/highlight.pack',
    'text': 'lib/text'
  },
  shim: {
    'jquery-xmlrpc': { deps: ['js/ajaxforge'] },
    'backbone': { deps: ['underscore'], exports: 'Backbone' },
    'tourist': { deps: ['backbone'], exports: 'Tourist' },
    'underscore': { exports: '_' },
    'highlight': { exports: 'hljs' }
  }
});
*/
require({
    baseUrl: '.',
    paths: {
      'jquery-xmlrpc': 'lib/jquery.xmlrpc',
      'backbone': 'lib/backbone-min',
      'underscore': 'lib/underscore-min',
      'demo': 'js/demo',
      'tourist': 'lib/tourist',
      'q': 'lib/q.min',
      'highlight': 'lib/highlight.pack',
      'select2': 'lib/select2.min',
      'text': 'lib/text'
    },
    shim: {
      'jquery-xmlrpc': { deps: ['js/ajaxforge'] },
      'backbone': { deps: ['underscore'], exports: 'Backbone' },
      'tourist': { deps: ['backbone'], exports: 'Tourist' },
      'underscore': { exports: '_' },
      'highlight': { exports: 'hljs' }
    }
  },
  ['underscore', 'backbone', 'js/Transition', 'js/cache',
   'js/canvas/Canvas', 'js/actor/Actor.js', 'js/canvas/ValidList',
   'js/canvas/Constraints',
   'text!html/Viewer.html', 'text!html/Actor.html',
   'text!html/Editor.html'],
function (_, Backbone, Transition, cache,
	  Canvas, Actor, ValidList, Constraints,
	  viewerString, actorString, editorString)
{
  

  window.JACKS_LOADER.version = '2018-01-10-A';
  window.JACKS_LOADER.Constraints = Constraints;
  window.JACKS_LOADER.ValidList = ValidList;

  var defaults = {
    mode: 'viewer',
    multiSite: false,
    source: 'rspec',
    root: 'body',
    nodeSelect: true,
    size: 'auto',
    canvasOptions: {}
  };

  var defaultDefault = {
    name: 'Add Node'
  };

  var defaultShow = {
    rspec: true,
    tour: true,
    version: true,
    menu: true,
    selectInfo: false,
    clear: true
  };

  function MainClass(context)
  {
    this.context = _.defaults(context, defaults);
    if (context.mode === 'viewer')
    {
      this.context.multiSite = true;
    }
    if (this.context.show)
    {
      var show = _.clone(defaultShow);
      if (context.nodeSelect)
      {
	show.selectInfo = true;
      }
      this.context.show = _.defaults(this.context.show, defaultShow);
    }
    else
    {
      this.context.show = defaultShow;
    }
    if (! this.context.canvasOptions.defaults)
    {
      this.context.canvasOptions.defaults = [defaultDefault];
    }
    if (this.context.mode === 'viewer' && ! this.context.nodeSelect)
    {
      this.context.show.selectInfo = false;
    }
    this.updateIn = {};
    _.extend(this.updateIn, Backbone.Events);
    this.updateOut = {};
    _.extend(this.updateOut, Backbone.Events);

    this.root = $(context.root);
    this.root.show();
    this.transition = new Transition(this.root);

    if (context.mode === 'viewer' && context.source === 'api')
    {
      this.root.html(actorString);
      this.canvas = new Canvas(context, this.root.find('#canvasContainer'),
			       this.root, this.updateIn, this.updateOut);
      this.canvas.show();
      this.actor = new Actor(this.root, this.canvas, this.transition);
    }
    else if (context.mode === 'viewer' && context.source === 'rspec')
    {
      this.root.html(viewerString);
      this.canvas = new Canvas(context, this.root.find('#canvasContainer'),
			       this.root, this.updateIn, this.updateOut);
      this.canvas.show();
      this.root.find('#loadingContainer').hide();
      this.root.find('#canvasContainer').show();
    }
    else if (context.mode === 'editor' && context.source === 'rspec')
    {
      this.root.html(editorString);
      this.canvas = new Canvas(context, this.root.find('#canvasContainer'),
			       this.root, this.updateIn, this.updateOut);
      this.canvas.show();
      this.root.find('#loadingContainer').hide();
      this.root.find('#canvasContainer').show();
    }

    this.updateIn.on('change-topology', _.bind(this.changeTopologyHandler,
					       this));
    this.updateIn.on('add-topology', _.bind(this.addTopologyHandler,
					    this));
    this.updateIn.on('resize', _.bind(this.resizeHandler, this));
    this.updateIn.on('fetch-topology', _.bind(this.fetchTopologyHandler,
					      this));
    this.lastRspec = [];
  }

  MainClass.prototype.changeTopologyHandler = function (list, options)
  {
    var isEqual = true;
    if ((options && ! options.constrainedFields) || list.length !== this.lastRspec.length)
    {
      isEqual = false;
    }
    else
    {
      for (var i = 0; i < list.length; i++)
      {
	if (this.lastRspec[i] !== list[i].rspec)
	{
	  isEqual = false;
	  break;
	}
      }
    }
    if (! isEqual)
    {
      this.lastRspec = _.pluck(list, 'rspec');
      this.canvas.clear();
      if (list.length > 0)
      {
	this.canvas.addRspec(list);
      }
    }
    if (options && options.constrainedFields)
    {
      var validList = new ValidList(this.canvas.topoData, this.canvas.constraints);
      var clauses = validList.getNodeCandidates(true);
      var sites = validList.getNodeCandidatesBySite(true);
      console.log('constrainedFields', clauses, sites);
      options.constrainedFields(clauses, sites);
    }
  };

  MainClass.prototype.addTopologyHandler = function (list)
  {
    this.lastRspec = this.lastRspec.concat(_.pluck(list, 'rspec'));
    if (this.canvas.hasNodes())
    {
      this.canvas.addRspec(list);
    }
    else
    {
      this.changeTopologyHandler(list);
    }
  };

  MainClass.prototype.resizeHandler = function (size)
  {
    this.canvas.resize(size);
  };

  MainClass.prototype.fetchTopologyHandler = function ()
  {
    var rspec = this.canvas.generateRequest();
    this.updateOut.trigger('fetch-topology', [{ rspec: rspec }]);
  };

  function initialize ()
  {
    window.JACKS_LOADER.isReady = true;
    window.JACKS_LOADER.MainClass = MainClass;
    if (window.JACKS_LOADER.onReady)
    {
      _.defer(_.bind(window.JACKS_LOADER.onReady, window.JACKS_LOADER));
    }
  }

  $(document).ready(initialize);
});

define("js/main", function(){});


//# sourceMappingURL=main.js.map