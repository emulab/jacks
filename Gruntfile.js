/*global module: true */
module.exports = function (grunt) {
  'use strict';

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
//    jshint: {
//      files: ['gruntfile.js', 'src/js/*.js', 'src/js/*/*.js', 'src/js/*/*/*.js'],
//      options: grunt.file.readJSON('.jshintrc')
//    },
//    shell: {
//      'test': {
//        command: 'mocha-phantomjs test/test.html',
//        options: {
//          stdout: true,
//          stderr: true
//        }
//      }
//    },
    clean: ['build'],
    requirejs: {
      compile: {
        options: {
          appDir: 'src',
          baseUrl: '.',
          dir: 'build',
	  optimize: 'none',
//          optimize: 'uglify2',
          skipDirOptimize: true,
          generateSourceMaps: true,
	  useSourceUrl: false,
          preserveLicenseComments: false,
          fileExclusionRegExp: /(^\.)|(~$)/,
          findNestedDependencies: true,
//          optimizeAllPluginResources: true,

          text: {
          },

          paths: {
            'jquery-xmlrpc': 'lib/jquery.xmlrpc',
            'backbone': 'lib/backbone-min',
//            'd3': 'lib/d3.v3',
            'underscore': 'lib/underscore-min',
            'demo': 'js/demo',
            'tourist': 'lib/tourist',
            'q': 'lib/q.min',
            'highlight': 'lib/highlight.pack',
            'cache': 'js/cache',
	    'select2': 'lib/select2.min',
	    'text': 'lib/text',
	    'requireLib': 'lib/require'
          },

          shim: {
            'jquery-xmlrpc': { deps: ['js/ajaxforge'] },
            'backbone': { deps: ['underscore'], exports: 'Backbone' },
            'tourist': { deps: ['backbone'], exports: 'Tourist' },
            'underscore': { exports: '_' },
//            'd3': { exports: 'd3' },
            'highlight': { exports: 'hljs' }
          },
/*
	  namespace: 'JACKS_REQUIRE',

          modules: [
            {
	      name: 'JACKS_REQUIRE',
              include: ['requireLib', 'js/main'],
	      create: true
            }
          ]
*/
	  name: 'js/main'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-contrib-requirejs');

  grunt.registerTask('ensure_folders', function () {
    var folders = ['./dist', './build'];
    folders.forEach(function (folder) {
      grunt.file.mkdir(folder);
    });
  });

  grunt.registerTask('dist', ['ensure_folders', //'jshint',
                              'requirejs'/*, 'shell' */]);
};
