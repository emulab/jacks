define(['underscore', 'backbone'],
function (_, Backbone)
{
  'use strict';

  var View = function(options) {
    options = options || {};
    bindUnderscore(this);
    this.options = options;
    this.cid = _.uniqueId('view');
    this.$el = $([]);
    this.children = [];
    this.rendered = false;
    this.shown = true;
    this.initialize.apply(this, arguments);
  };

  _.extend(View.prototype, Backbone.Events, {

    $: function(selector)
    {
      return this.$el.find(selector);
    },

    // $on(eventName, method)
    // $on(eventName, selector, method)
    // $on(eventName, jQuery, method)
    $on: function(eventName, selector, method)
    {
      var base = this.$el;
      if (! method)
      {
	method = selector;
	selector = undefined;
      }
      if (selector !== undefined && ! _.isString(selector))
      {
	base = selector;
	selector = undefined;
      }

      var boundMethod = _.bind(method, this);
      var fullName = '';
      var names = eventName.split(' ');
      this._each(names, function (name) {
	if (name !== '')
	{
	  if (fullName !== '')
	  {
	    fullName += ' ';
	  }
	  fullName += name + '.selectBind' + this.cid;
	}
      });
      if (selector === undefined)
      {
	base.on(fullName, boundMethod);
      }
      else
      {
	base.on(fullName, selector, boundMethod);
      }
    },

    $off: function()
    {
      this.$el.off('.selectBind' + this.cid);
    },

    superCleanup: function ()
    {
      this.stopListening();
      this.$off();
      this._each(this.children, function (child) {
	child.$el.remove();
	child.cleanup();
      });
    },

    superRender: function (el)
    {
      this.$el = el || this.$el;
      this.rendered = true;
      this.update(this.options.state);
      return this.$el;
    },

    superShow: function ()
    {
      if (! this.shown)
      {
	this.$el.show();
	this.shown = true;
      }
    },

    superHide: function ()
    {
      if (this.shown)
      {
	this.$el.hide();
	this.shown = false;
      }
    },

    initialize: function (options) {},

    cleanup: function () { this.superCleanup() },

    render: function (el) { this.superRender(el) },

    update: function (state) {},

    show: function () { this.superShow() },

    hide: function () { this.superHide() }

  });

  View.extend = function(protoProps, staticProps)
  {
    var parent = this;
    var child;

    if (protoProps && _.has(protoProps, 'constructor'))
    {
      child = protoProps.constructor;
    }
    else
    {
      child = function(){ return parent.apply(this, arguments); };
    }

   _.extend(child, parent, staticProps);

    var Surrogate = function(){ this.constructor = child; };
    Surrogate.prototype = parent.prototype;
    child.prototype = new Surrogate;

    if (protoProps) _.extend(child.prototype, protoProps);

    child.__super__ = parent.prototype;

    return child;
  };

  var underscoreMethods = ['each', 'forEach',
			   'map', 'collect',
			   'reduce', 'inject', 'foldl',
			   'reduceRight', 'foldr',
			   'find', 'detect',
			   'filter', 'select',
			   'reject', 'every', 'all',
			   'some', 'any',
			   'max', 'min',
			   'sortBy', 'groupBy', 'indexBy', 'countBy'];

  function bindUnderscore(that)
  {
    that._ = {};
    _.each(underscoreMethods, function (method) {
      that['_' + method] = function()
      {
	var args = _.toArray(arguments);
	if (args.length >= 2 && _.isFunction(args[1]) &&
	    args[1] !== undefined && args[1] !== null)
	{
	  args[1] = _.bind(args[1], that);
	}
	return _[method].apply(_, args);
      };
    });
  }

  return View;
});
