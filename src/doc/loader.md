# Loader #

As I mentioned, I've pulled in the loader from Flack. So if you use 
source=devel or source=stable or source=local or source=none, you should 
see files loading from those respective locations. There are still two 
rough edges in the loader.

First, forge itself (in the api-viewer) is not yet loaded by the Jacks 
loader so has to be included separately.

Second, assets (images, swf) are still pulled in relative to the html 
file. I've still got to decide exactly how I want to fix this. Probably 
some kind of utility function to escape them consistently.

All loading is done from jacks.js and that is the only file you have to 
include (aside from the forge libraries for using the API):

    <script src="js/jacks.js"></script>

If you want to set a default load location (the GET parameters override 
this), then you should set the JACKS_LOADER variable before loading 
jacks.js:

    <script>
      window.JACKS_LOADER = {
        basePath: 'https://example.org/path/to/files'
      };
    </script>
    <script src="js/jacks.js"></script>

The basePath should be wherever you have installed the Jacks tarball 
(there is no tarball yet, but there will be).

BTW, you may notice that jquery itself is also loaded from the example 
files. This is just for convenience in the embedding page. It need not be 
included to make Jacks work. (Currently untested and probably will break: 
including a non 2.0.3 version of jQuery in the embedding page).
