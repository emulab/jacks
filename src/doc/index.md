# Jacks #

Jacks is an embeddable JavaScript client for viewing, manipulating,
and interacting with rspecs and GENI slivers. This documentation
describes how to embed instances of Jacks in your page and the API it
exports.

 * [Jacks Loader](loader.html)
 * [Embedding API](embed.html)
 * [Constraints](constraints.html)

There are three example pages which provide different kinds of Jacks embeddings:

- Simple rspec [topology viewer](https://www.emulab.net/protogeni/jacks-stable/viewer.html)
- WIP [rspec editor](https://www.emulab.net/protogeni/jacks-stable/editor.html) for editing an rspec
- A [sliver viewer](https://www.emulab.net/protogeni/jacks-stable/api-viewer.html) that provides a login interface and allows you to view an existing slice.

## Editor ##

The rspec editor has a couple of basic UI primitives that let you add 
nodes/links to the topology. Once you open this, click 'edit topology' and 
a pane slides out. Click add node to add a node onto the canvas (no 
dragging for now). While in the edit topology mode, drag between a pair of 
nodes to create a link between them. Everything is still brittle, so you 
might have to refresh if you get into a weird state.

Lots more work to do here, but let us know if you have any feedback about 
how topology generation should work.

