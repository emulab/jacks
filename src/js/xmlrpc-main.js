require.config({
  baseUrl: '.',
  paths: {
    'jquery': 'lib/jquery-2.0.3.min',
    'jquery-xmlrpc': 'lib/jquery.xmlrpc',
    'underscore': 'lib/underscore-min'
  },
  shim: {
      'jquery-xmlrpc': { deps: ['jquery', 'js/ajaxforge'] },
      'underscore': { exports: '_' }
  },
});

define(['jquery', 'underscore', 'js/ajaxforge', 'jquery-xmlrpc'],
function ($, _)
{
  'use strict';

  function initialize()
  {
    var tag = document.getElementById('url-parameter');
    if (tag)
    {
      $('#url').val(tag.textContent);
    }
    tag = document.getElementById('client-key-parameter');
    if (tag)
    {
      $('#client-key').val(tag.textContent);
    }
    tag = document.getElementById('client-cert-parameter');
    if (tag)
    {
      $('#client-cert').val(tag.textContent);
    }
    tag = document.getElementById('server-cert-parameter');
    if (tag)
    {
      $('#server').val(tag.textContent);
    }

    swfobject.embedSWF(
      'SocketPool.swf', 'socketPool',
      '0', '0',
      '9.0.0', false,
      {}, {allowscriptaccess: 'always'}, {}, swfLoaded);
    swfobject.createCSS("#socketPool", "visibility:visible;");
  }

  function swfLoaded(e)
  {
    if (e.success)
    {
      setTimeout(function () {
          $('#title').html('Ready');
          $('#try-xmlrpc').show();
          $('#try-xmlrpc').click(tryXmlrpc);
        }, 500);
    }
    else
    {
      $('#title').html('Failed to load SocketPool.swf');
    }
  }

  function tryXmlrpc(event)
  {
    event.preventDefault();

    try
    {
      var decrypted = forge.pki.decryptRsaPrivateKey($('#client-key').val(),
                                                     $('#client-phrase').val());
      var keyText = forge.pki.privateKeyToPem(decrypted);

      $.ajaxforge.initialize('SocketPool.swf', 'socketPool', $('#server').val(),
                             keyText, $('#client-cert').val());

      var promise = $.xmlrpc({
        url: $('#url').val(),
        methodName: 'GetVersion',
        params: [{}]
//       methodName: 'ListResources',
//        params: [[], { 'geni_rspec_version': { 'type': 'ProtoGENI', 'version': '3'}}],//{ 'options': {} }],
        });
      promise.then(success, failure);
      $('#title').html('Sending');
      $('#body').html('');
      $('#try-xmlrpc').hide();
    }
    catch (e)
    {
      console.log('Error: ', e);
      $('#title').html('Could not decrypt private key. Is password correct?');
      $('#body').html('<pre>' + _.escape(vkbeautify.json(JSON.stringify(e))) +
                      '</pre>');
    }
  }

  function success(response)
  {
    $('#title').html('Success');
    $('#body').html('<pre>' + _.escape(vkbeautify.xml(response.body)) +
                    '</pre>');
    $('#try-xmlrpc').show();
  }

  function failure(type, message, cause)
  {
    $('#title').html('Failure');
    $('#body').html('Status: ' + status + '<hr>' + JSON.stringify(jqXHR));
    $('#try-xmlrpc').show();
  }

  $(document).ready(initialize);
});
