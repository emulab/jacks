define(['underscore', 'backbone'],
function (_, Backbone)
{
  'use strict';

  // arguments
  // {
  //   root: jQuery node
  //   options: defaults item
  //   pos: { x: int, y: int } to place it on paletter
  //   toInternalPosition: function to get internal topology coordinates
  function NodeOption(args)
  {
    this.root = args.root;
    this.options = args.options;
    this.pos = args.pos;
    this.toInternalPosition = args.toInternalPosition;
    this.force = args.force;
    this.validList = args.validList;
    this.lazy = null;
    this.marker = null;
    this.lastPos = null;
    _.extend(this, Backbone.Events);
  }

  NodeOption.prototype = {

    render: function ()
    {
      this.lazy = this.makeNode(this.root);
      this.lazy.style('visibility', 'hidden');
      this.marker = this.makeNode(this.root);
      var drag = d3v4.drag()
	.on('start', _.bind(this.dragStart, this))
	.on('drag', _.bind(this.dragMove, this))
	.on('end', _.bind(this.dragEnd, this));
      this.marker.call(drag);
    },

    makeNode: function (root)
    {
      var result = root.append('svg:g')
	.attr('transform', 'translate(' + this.pos.x + ', ' +
	      this.pos.y + ')')
        .attr("class", "node");

      result.append('svg:rect')
	.attr('class', 'nodebox')
	.attr('x', '-30px')
	.attr('y', '-25px')
	.attr('width', '60px')
	.attr('height', '65px');

      var iconUrl = window.JACKS_LOADER.basePath + 'images/default.svg';
      if (this.options.icon)
      {
	iconUrl = this.options.icon;
      }
      result.append('svg:image')
	.attr("class", "nodebox")
	.attr("x", "-20px")
	.attr("y", "-20px")
	.attr("width", "40px")
	.attr("height", "40px")
	.attr("xlink:href", iconUrl);

      var name = 'New Node';
      if (this.options.name)
      {
	name = this.options.name;
      }
      result.append('svg:text')
	.attr("class", "nodetextpalette")
	.attr("x", "0px")
	.attr("y", "35px")
	.text(name);
      return result;
    },

    dragStart: function ()
    {
      this.lazy.style('visibility', 'visible');
      this.lastPos = null;
      this.force.stop();
    },

    dragMove: function ()
    {
      this.marker.attr('transform', 'translate(' + (d3v4.event.x - 20) +
		       ',' + (d3v4.event.y - 10) + ')');
      var oldClosest = null;
      if (this.lastPos)
      {
	oldClosest = this.lastPos.closestSite;
      }
      this.lastPos = this.toInternalPosition({ x: d3v4.event.x,
					       y: d3v4.event.y });
      if (this.lastPos.closestSite !== null)
      {
	this.lastPos.closestSite.update(true, undefined,
					[this.lastPos.topX,
					 this.lastPos.topY]);
      }
      if (this.lastPos.closestSite !== oldClosest &&
	  oldClosest !== null)
      {
	oldClosest.update(true);
      }
      if (this.lastPos.closestSite !== oldClosest &&
	  this.lastPos.closestSite !== null)
      {
	var node = _.clone(this.options);
	node.custom = {};
	if (this.validList.isValidNode(node,
				       this.lastPos.closestSite.model))
	{
	  this.marker.classed('invalid', false);
	}
	else
	{
	  this.marker.classed('invalid', true);
	}
      }
    },

    dragEnd: function ()
    {
      this.lazy.style('visibility', 'hidden');
      if (this.lastPos && this.lastPos.closestSite !== null)
      {
	this.trigger('create-node', this.lastPos.topX, this.lastPos.topY,
		     this.lastPos.closestSite.model.id);
      }
      this.marker.attr('transform', 'translate(' + this.pos.x + ', ' +
		       this.pos.y + ')')
      this.marker.classed('invalid', false);
    },

  };

  return NodeOption;
});
