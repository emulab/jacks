/*
 * Canvas.js
 *
 * Overall module for editing or viewing rspecs, profiles, and slices
 *
 */

define(['underscore', 'backbone', 'lib/PrettyXML',
	'js/canvas/Constraints',
	'js/canvas/rspecParser', 'js/canvas/rspecGenerator',
	'js/canvas/TourView', 'js/canvas/TopologyView',
	'js/canvas/TopologyModel',
	'js/info/Info', 'js/info/ConstrainedField', 'js/canvas/ErrorModal',
	'text!html/Canvas.html', 'text!html/AddNode.html',
	'text!html/rspec.xml',
	'select2'],
function (_, Backbone, PrettyXML,
	  Constraints, rspecParser, rspecGenerator,
	  TourView, TopologyView, TopologyModel,
	  Info, ConstrainedField, ErrorModal,
	  canvasString, addNodeString, defaultRspecString)
{
  'use strict';

  var addNodeTemplate = _.template(addNodeString);

  function Canvas(context, domRoot, tourContainer, updateIn, updateOut)
  {
    this.context = context;
    this.constraints = new Constraints(context);
    this.domRoot = domRoot;
    this.tourContainer = tourContainer;
    this.updateIn = updateIn;
    this.updateOut = updateOut;
    this.size = context.size;
    this.highlights = [];
    this.lastHighlightType = null;
    if (context.mixedSelection)
    {
      this.lastHighlightType = 'mixed';
    }
    this.lastHighlightEvent = null;
    this.rspecShowing = false;
    this.info = new Info({
      context: context,
      constraints: this.constraints
    });
    domRoot.html(canvasString);
    this.errorModal = new ErrorModal();
    this.errorModal.render($('#errorModal'));

    this.info.render(domRoot.find('.nodeAttr'));
    this.info.on('change', _.bind(this.changeAttribute, this));
    this.info.on('addToList', _.bind(this.addToList, this));
    this.info.on('removeFromList', _.bind(this.removeFromList, this));
    this.info.on('changeList', _.bind(this.changeList, this));

    this.singleSite = new ConstrainedField({
      choices: context.canvasOptions.aggregates,
      constraints: this.info.constraints,
      optionKey: 'aggregates',
    });
    this.singleSite.render(domRoot.find('#fbcSite'));
    this.singleSite.on('change', _.bind(this.changeSingleSite, this));

    this.clickUpdate = {};
    _.extend(this.clickUpdate, Backbone.Events);

    if (this.context.nodeSelect === false) {
      this.clickUpdate.on("click-event", function(data) {
	var out = {};
	if (data.type === 'node')
	{
	  out = {
	    'type': 'node',
	    'client_id': data.item.name,
	    'ssh': data.item.sshurl
	  };
	}
	else if (data.type === 'link')
	{
	  out = {
	    'type': 'link',
	    'client_id': data.item.name
	  };
	}
	else if (data.type === 'site')
	{
	  out = {
	    'type': 'site'
	  };
	}
	else if (data.type === 'host')
	{
	  out = {
	    'type': 'host',
	    'client_id': data.item.name
	  };
	}
	out.event = data.event;
        updateOut.trigger("click-event", out);
      });
    }
    else {
      this.clickUpdate.on("click-event", _.bind(this.handleClick, this));
      this.clickUpdate.on("click-outside", _.bind(this.handleClickOutside, this));
    }
    if (_.isString(context.size) && context.size === 'auto')
    {
      this.lastSize = {
	x: null,
	y: null
      };
      setInterval(_.bind(this.checkResize, this), 100);
    }
    this.updateIn.on('show-rspec', _.bind(this.viewRspec, this));
    this.updateIn.on('add-canvas-options', _.bind(this.addCanvasOptions, this));
    this.updateIn.on('set-selection', _.bind(this.handleSetSelection, this));
  }

  Canvas.prototype.hasNodes = function ()
  {
    return _.keys(this.topoData.nodes).length > 0;
  }

  Canvas.prototype.show = function ()
  {
    var that = this;
    var root = this.domRoot;
    this.rspec = defaultRspecString;
    if (! this.context.show.menu)
    {
      root.find('.withButtonBar').removeClass('withButtonBar');
      root.find('.editor#topoButtons, .viewer#topoButtons').hide();
    }

    if (_.isObject(this.context.size))
    {
      root.css('width', this.context.size.x + 'px');
      root.css('height', this.context.size.y + 'px');
    }

    showCommon(this.rspec, that, root);
    this.updateSingleSite();
    if (this.context.mode === 'viewer') {
      showViewer(this.rspec, that, root);
    }
    else if (this.context.mode === 'editor') {
      showEditor(this.rspec, that, root);
    }
  };

  Canvas.prototype.hide = function ()
  {
    this.domRoot.hide();
  };

  Canvas.prototype.clear = function ()
  {
    this.highlights = [];
    this.topology.setHighlights(this.highlights);
    this.showAttributes();
    this.topoData.removeLinks(_.keys(this.topoData.lans));
    this.topoData.removeNodes(_.keys(this.topoData.nodes));
    this.topoData.removeSites(_.keys(this.topoData.sites));
  };

  Canvas.prototype.checkResize = function (force)
  {
    var topoDom = this.domRoot.find('#topoContainer');
    var newSize = {
      x: topoDom.outerWidth(),
      y: topoDom.outerHeight()
    };
    if (! this.lastSize ||
	newSize.x !== this.lastSize.x ||
	newSize.y !== this.lastSize.y || force)
    {
      this.lastSize = newSize;
      this.resize(newSize);
    }
  };

  Canvas.prototype.calculateSize = function ()
  {
    var topoDom = this.domRoot.find('#topoContainer');
    var size = {
      x: topoDom.outerWidth(),
      y: topoDom.outerHeight()
    };
    if (_.isObject(this.context.size))
    {
      size = {
	x: this.context.size.x,
	y: this.context.size.y
      };
    }
    return size;
  };

  Canvas.prototype.resize = function (size)
  {
    var topoDom = this.domRoot.find('#topoContainer');
    this.size = size;
    this.topology.resize(size.x, size.y);
  };

  Canvas.prototype.generateRequest = function () {
    return rspecGenerator.request(this.topoData, this.context);
  };

  Canvas.prototype.viewRspec = function () {
    if (! this.rspecShowing)
    {
      var request = rspecGenerator.request(this.topoData, this.context);
      this.domRoot.find('#lnRspec').html('<pre>' +
					 _.escape(PrettyXML.format(request)) +
					 '</pre>');
      this.domRoot.find('.navRspec').show();
      this.rspecShowing = true;
      $('html').css('-webkit-user-select', '');
      $('html').css('-moz-user-select', '');
    }
    else
    {
      this.hideRspec();
    }
  };

  Canvas.prototype.hideRspec = function () {
    this.domRoot.find('.navRspec').hide();
    this.rspecShowing = false;
  };

  Canvas.prototype.addCanvasOptions = function (options) {
    if (options.canvasOptions)
    {
      this.constraints.addPossibles(options.canvasOptions);
      _.each(_.keys(options.canvasOptions), function (key) {
	var current = this.context.canvasOptions[key] || [];
	var newPossibles = options.canvasOptions[key];
	_.each(current, function (item) {
	  var dup = _.findWhere(newPossibles, { id: item.id });
	  if (! dup)
	  {
	    newPossibles.push(item);
	  }
	}.bind(this));
	this.context.canvasOptions[key] = current;
      }.bind(this));
    }
    if (options.constraints)
    {
      this.constraints.allowAllSets(options.constraints);
      var current = this.context.constraints || [];
      current = _.union(current, options.constraints);
      this.context.constraints = current;
    }
    this.showAttributes();
  };

  Canvas.prototype.addRspec = function (list) {
    var that = this;
    _.each(list, function (item) {
      var newGraph =
	rspecParser.parse({
	  rspec: item.rspec,
	  context: that.context,
	  errorModal: that.errorModal
	})
      that.topoData.addGraph(newGraph, item.sourceUrn);
    });
    if (list.length > 0)
    {
      this.topology.startForce();
    }
    this.updateOut.trigger('found-images', this.imageList());
    this.updateOut.trigger('found-types', this.typeList());
  };

  Canvas.prototype.imageList = function () {
    var result = [];
    _.each(this.topoData.nodes, function (node) {
      if (node.image)
      {
	result = _.union(result, [node.image]);
      }
    }.bind(this));
    return result;
  };

  Canvas.prototype.typeList = function () {
    var result = {};
    _.each(this.topoData.nodes, function (node) {
      var site = node.group;
      result[site] = result[site] || { name: '', types: {}, hardware: {} };
      result[site].name = this.topoData.sites[site].name;
      updateCounter(node.type, result[site].types);
      updateCounter(node.hardware, result[site].hardware);
    }.bind(this));
    return result;
  };

  function updateCounter(inName, map)
  {
    var name = inName;
    if (name === undefined)
    {
      name = '';
    }
    if (map[name] === undefined)
    {
      map[name] = 0;
    }
    map[name] += 1;
  }

  var canvasModified = false;
  Canvas.prototype.modifiedTopology = function () {
    canvasModified = true;
    _.defer(function () {
      if (canvasModified)
      {
        this.updateOut.trigger('modified-topology',
			       this.generateModifiedData(true));
	canvasModified = false;
      }
    }.bind(this));
  };

  Canvas.prototype.modifiedField = function () {
    if (! this.pendingModifiedField)
    {
      this.pendingModifiedField = true;
      _.defer(function () {
	this.updateOut.trigger('modified', this.generateModifiedData(false));
	this.pendingModifiedField = false;
      }.bind(this));
    }
  };

  Canvas.prototype.generateModifiedData = function (makeRspec) {
    var data = {};
    if (makeRspec)
    {
      data.rspec = PrettyXML.format(rspecGenerator.request(this.topoData,
							   this.context));
    }
    data.nodes = [];
    _.each(this.topoData.nodes, function (node) {
	var nodeData = {
	  id: node.id,
	  client_id: node.name,
	  sliver_id: node.sliverId,
	  sourceUrn: node.sourceUrn
	};
	if (node.group)
	{
	  var site = this.topoData.sites[node.group];
	  if (site.urn)
	  {
	    nodeData.aggregate_id = site.urn;
	  }
	  if (site.name)
	  {
	    nodeData.site_name = site.name;
	  }
	}
	data.nodes.push(nodeData);
    }.bind(this));
    data.links = [];
    _.each(this.topoData.lans, function (link) {
	data.links.push({
	  id: link.id,
	  client_id: link.name,
	  link_type: link.linkType
	});
    }.bind(this));
    data.sites = [];
    _.each(this.topoData.sites, function (site) {
	var siteData = {
	  id: site.id,
	};
	if (site.name)
	{
	  siteData.name = site.name;
	}
	if (site.urn)
	{
	  siteData.urn = site.urn;
	}
	data.sites.push(siteData);
    }.bind(this));
    return data;
  };

  function showCommon(rspec, that, root)
  {
    if (! that.context.show.rspec)
    {
      root.find('#fbRspec').hide();//css('visibility', 'hidden');
      root.find('#fbcRspec').hide();//css('visibility', 'hidden');
    }
    if (! that.context.show.tour)
    {
      root.find('#fbTour').css('visibility', 'hidden');
    }
    if (! that.context.show.version)
    {
      root.find('#versionNumber').css('visibility', 'hidden');
    }
    root.find('#fbRspec').click(_.bind(that.viewRspec, that));
    root.find('#fbcRspec').click(_.bind(that.viewRspec, that));
    root.find('#hideRspec').click(_.bind(that.hideRspec, that));
    root.find('#hideLeftPane').click(_.bind(that.handleClickOutside, that));
    root.find('.navRspec').blur(function() {
      setTimeout(function() {
	enableButton(root.find('#fbRspec'));
	enableButton(root.find('#fbcRspec'));
      }, 250);
    });
    root.find('#fbTour').click(function (event) {
      event.preventDefault();
      that.tour.tour.start();
    });
    root.find('#fbcCleanup').click(function (event) {
      that.topology.startForce();
    });

    var topoDom = that.domRoot.find('#topoContainer');
    var size = that.calculateSize();

    that.topoData = new TopologyModel(that.context, that.constraints, that.errorModal);
    that.topoData.on('addSites addNodes addLans addEndpoint ' +
		     'removeSites removeNodes removeLans removeEndpoint ' +
		     'changeAggregate ',
		     _.bind(that.modifiedTopology, that));
    that.topoData.on('change', _.bind(that.modifiedField, that));
    that.topoData.on('addSites removeSites changeAggregate',
		     _.bind(that.updateSingleSite, that));
    that.topoData.on('change', _.bind(that.showAttributes, that));
    that.tour = new TourView(topoDom, that.tourContainer, rspec);
    that.topology = new TopologyView({ el: topoDom,
				       model: that.topoData,
				       clickUpdate: that.clickUpdate,
				       defaults: that.context.canvasOptions.defaults,
				       tour: that.tour,
				       context: that.context,
				       constraints: that.constraints,
				       errorModal: that.errorModal });
    that.topoData.addGraph(rspecParser.parse({
      rspec: rspec,
      context: that.context,
      errorModal: that.errorModal
    }));
    if (_.isString(that.context.size) && that.context.size === 'auto')
    {
      that.checkResize(true);
    }
    else
    {
      that.resize(that.size);
    }

    if (that.context.mode === 'editor' ||
	(that.context.nodeSelect && that.context.show.selectInfo))
    {
      slideOutStatic(that.domRoot.find('.nodeAttr'), that.domRoot.find('.closeContainer'));
      that.showAttributes();
    }
  }

  function showViewer(rspec, that, root)
  {
    if (that.context.show.selectInfo)
    {
      root.find('#viewer-default').show();
    }
    root.find('#topoButtons.viewer').removeClass('hidden');

    root.find('#fbSlice').click(function() {
      slideOut(root.find('.navSlices'), root.find('#fbSlice'));
    });
    root.find('.navSlices').blur(function() {
      setTimeout(function() {
	enableButton(root.find('#fbSlice'));
      }, 250);
    });
    if (that.context.show.tour)
    {
      root.find('#fbTour').popover({
	title: 'Start Tour',
	content: '<p>This slice has a tour which describe how it works. ' +
	  'Click here to view it.</p>',
	html: true,
	placement: 'bottom',
	trigger: 'manual'
      });
    }

    if (that.tour.nonTrivial())
    {
      root.find('#fbTour').show();
      _.defer(function () { root.find('#fbTour').popover('show'); });
      $('body').one('click', function () {
	root.find('#fbTour').popover('hide');
      });
    }
    else
    {
      root.find('#fbTour').hide();
    }

    if (that.context.source === 'api')
    {
      root.find('#fbSlice').show();
    }
    else
    {
      root.find('#fbSlice').hide();
    }

    root.find('#naDelete').hide();
  }

  function showEditor(rspec, that, root)
  {
    if (that.context.show.clear)
    {
      root.find('#fbcClear').click(function () {
	this.clear();
	this.topoData.addGraph(rspecParser.parse({
	  rspec: defaultRspecString,
	  context: this.context,
	  errorModal: this.errorModal
	}));
      }.bind(that));
    }
    else
    {
      root.find('#fbcClear').hide();
    }

    root.find('#topoButtons.editor').removeClass('hidden');
  }

  Canvas.prototype.handleClick = function(data) 
  {
    if (this.lastHighlightType !== 'mixed')
    {
      if (this.lastHighlightType !== data.type)
      {
	this.highlights = [];
      }
      this.lastHighlightType = data.type;
    }
    this.lastHighlightEvent = data.event;

    if (data.modkey)
    {
      if (_.contains(this.highlights, data.item.id))
      {
        this.highlights = _.without(this.highlights, data.item.id)
      }
      else
      {
        this.highlights.push(data.item.id);
      }
    }
    else
    {
      if (this.highlights !== undefined &&
	  this.highlights.length === 1 &&
	  _.contains(this.highlights, data.item.id))
      {
        this.highlights = [];
      }
      else
      {
        this.highlights = [data.item.id];
      }
    }

    this.showAttributes();
    this.topology.setHighlights(this.highlights, data.type);
  };

  Canvas.prototype.handleClickOutside = function()
  {
    this.lastHighlightEvent = null;
    this.highlights = [];
    this.topology.setHighlights(this.highlights);
    this.showAttributes();
  };

  Canvas.prototype.handleSetSelection = function (newSelection)
  {
    this.lastHighlightEvent = null;
    this.lastHighlightType = 'mixed';
    this.highlights = [];
    if (_.isArray(newSelection))
    {
      _.each(newSelection, function (item) {
	if (_.isString(item) && this.topoData.hasId(item)) {
	  this.highlights.push(item);
	}
      }.bind(this));
    }
    this.topology.setHighlights(this.highlights);
    this.showAttributes();
  };

  function generateOutsideSelection(items, selectType, lastHighlightEvent)
  {
    var result = {
      type: selectType,
      items: [],
      event: lastHighlightEvent
    };
    _.each(items, function (item) {
      var out = {};
      out.key = item.id;
      if (item.sliverId)
      {
	out.sliver_id = item.sliverId;
      }
      if (selectType == 'node')
      {
	out.name = item.name;
	out.sshurl = item.ssh;
      }
      else if (selectType == 'link')
      {
	out.name = item.name;
      }
      else if (selectType == 'site')
      {
	out.urn = item.urn;
	out.id = item.name;
      }
      result.items.push(out);
    });
    return result;
  }

  Canvas.prototype.changeSingleSite = function (state)
  {
    _.defer(function () {
      var key = _.keys(this.topoData.sites)[0];
      this.topoData.changeAttribute([key], 'urn', state.value, 'site');
    }.bind(this));
  }

  Canvas.prototype.updateSingleSite = function ()
  {
/*
    var keys = _.keys(this.topoData.sites);
    if (keys.length === 1)
    {
      var site = this.topoData.sites[keys[0]];
      this.singleSite.show();
      this.singleSite.update({
	values: [site.urn],
	isViewer: this.context.mode === 'viewer',
	type: 'site',
	disabled: false,
	freeform: site.custom.urn,
	model: this.topoData,
	selection: 'site'
      });
    }
    else
*/
    {
      this.singleSite.hide();
    }
  }

  Canvas.prototype.changeAttribute = function (state)
  {
    if (state.hasFreeform)
    {
      var items = _.filter(this.topoData.getItems(this.lastHighlightType),
			   function(item) {
			     return _.contains(this.highlights, item.id);
			   }.bind(this));
      _.each(items, function (item) {
	item.custom[state.key] = state.freeform;
      });
    }
    if (state.key === 'delete')
    {
      this.topoData.removeItems(this.highlights, this.lastHighlightType);
      this.highlights = [];
    }
    else
    {
      this.topoData.changeAttribute(this.highlights, state.key, state.value,
				    this.lastHighlightType);
    }
    if (state.hasVersion)
    {
      this.topoData.changeAttribute(this.highlights, 'imageVersion', state.version,
				    this.lastHighlightType);
    }
    _.defer(_.bind(this.showAttributes, this));
  };

  Canvas.prototype.addToList = function (state)
  {
    if (this.highlights.length === 1)
    {
      var current = this.topoData.nodes[this.highlights[0]];
      current[state.key].push(state.item);
      this.showAttributes();
    }
  };

  Canvas.prototype.removeFromList = function (state)
  {
    if (this.highlights.length === 1)
    {
      if (state.key === 'interfaces')
      {
	var link = this.topoData.lans[this.highlights[0]];
	var iface = this.topoData.interfaces[link.interfaces[state.index]];
	var node = this.topoData.nodes[iface.nodeId];
	this.topoData.removeNodeFromLan(node, link);
      }
      else
      {
	var current = this.topoData.nodes[this.highlights[0]];
	current[state.key].splice(state.index, 1);
      }
      this.showAttributes();
    }
  };

  Canvas.prototype.changeList = function (state)
  {
    if (this.highlights.length === 1)
    {
      var current;
      if (state.key === 'interfaces')
      {
	var link = this.topoData.lans[this.highlights[0]];
	var id = link.interfaces[state.index];
	current = this.topoData.interfaces[id];
      }
      else
      {
	current = this.topoData.nodes[this.highlights[0]][state.key][state.index];
      }
      _.each(state.changed, function (value, key) {
	if (key === 'ip')
	{
	  var oldValue = current[key];
	  current[key] = value;
	  this.topoData.switchIp(current.id, oldValue, value);
	}
	else
	{
	  current[key] = value;
	}
      }.bind(this));
      this.showAttributes();
    }
  };

  Canvas.prototype.showAttributes = function()
  {
    if (! this.showingAttributes)
    {
      this.showingAttributes = true;
      _.defer(_.bind(this.finalShowAttributes, this));
    }
  }

  Canvas.prototype.finalShowAttributes = function ()
  {
    this.showingAttributes = false;
    $('html').css('-webkit-user-select', 'initial');
    $('html').css('-moz-user-select', 'initial');
    var isViewer = this.context.mode === 'viewer';
    var items = [];
    if (this.lastHighlightType === 'mixed')
    {
      items = this.topoData.findMixed(this.highlights);
    }
    else
    {
      items = _.filter(this.topoData.getItems(this.lastHighlightType),
		       function(item) {
			 return _.contains(this.highlights, item.id);
		       }.bind(this));
    }
    var selectType = this.lastHighlightType;
    var unselected = (items.length === 0);
    if (unselected)
    {
      selectType = 'none';
    }
    if (selectType !== 'none' && selectType !== 'mixed')
    {
      this.info.update({ selection: items, type: selectType,
			 isViewer: isViewer, model: this.topoData });
    }

    //var topo = this.domRoot.find('#topoContainer');
    var pane = this.domRoot.find('.nodeAttr');
    this.updateOut.trigger('selection',
			   generateOutsideSelection(items, selectType,
						    this.lastHighlightEvent));
    if (items.length === 0 || selectType === 'mixed')
    {
      slideInStatic(pane, this.domRoot.find('.closeContainer'));
    }
    else
    {
      slideOutStatic(pane, this.domRoot.find('.closeContainer'));
    }
  }

  function slideOut(goTo, thisButton)
  {
    if (!thisButton.hasClass('freeze'))
    {
      setTimeout(function() {
	      goTo.attr('tabindex', -1).focus();
      }, 1);
      thisButton.addClass('freeze');
    }
  }

  function enableButton(thisButton)
  {
    thisButton.removeClass('freeze');
  }

  function slideOutStatic(goTo, closeButton, thisButton)
  {
    goTo.addClass('show');
    closeButton.addClass('show');
    if (thisButton !== null && thisButton !== undefined) {
      thisButton.addClass('btn-warning');
      thisButton.removeClass('btn-primary');
    }
  }

  function slideInStatic(goFrom, closeButton, thisButton)
  {
    goFrom.removeClass('show');
    closeButton.removeClass('show');
    if (thisButton !== null && thisButton !== undefined) {
      thisButton.removeClass('btn-warning');
      thisButton.addClass('btn-primary');
    }
  }

  return Canvas;
});
