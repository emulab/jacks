define(['underscore', 'backbone'],
function (_, Backbone)
{
  'use strict';

  var NodeView = Backbone.View.extend({

    initialize: function () {
      this.node = {
	model: this.model,
	view: this
      };
      if (this.model.x !== undefined &&
	  this.model.y !== undefined)
      {
	this.node.x = this.model.x;
	this.node.y = this.model.y;
      }
      this.old = {};
      this.oldPos = { x: this.node.x, y: this.node.y };
      this.options.force.add([this.node], []);
      this.render();
      this.groupChanged = null;
      this.linkDragging = false;
    },

    cleanup: function () {
      this.options.force.remove([this.node], []);
      this.container.remove();
    },

    render: function () {
      var dragnode = d3v4.drag()
	.on('start', _.bind(this.handleDragStart, this))
	.on('drag', _.bind(this.handleDragMove, this))
	.on('end', _.bind(this.handleDragEnd, this));

      var draglink = d3v4.drag()
	.on('start', _.bind(this.linkDragStart, this))
	.on('drag', _.bind(this.linkDragMove, this))
	.on('end', _.bind(this.linkDragEnd, this));

      this.container = this.options.background.nodeBase.append('svg:g')
	.attr("id", this.model.id);

      if (this.options.context.mode === 'editor')
      {
	this.background = this.container.append('svg:g');
	this.backRect = this.background.append('svg:rect')
	  .attr('class', 'nodebackground')
	  .attr('x', '-70px')
	  .attr('y', '-65px')
	  .attr('width', '140px')
	  .attr('height', '145px')
	  .call(draglink)
	  .on('click', _.bind(this.handleBackgroundClick, this))
	  .on('mouseover', _.bind(this.linkHoverStart, this))
	  .on('mousemove', _.bind(this.linkHoverMove, this))
	  .on('mouseout', _.bind(this.linkHoverEnd, this));
      }

      this.foreground = this.container.append('svg:g')
        .attr("class", "node")
	.call(dragnode)
	//.on('click', _.bind(this.handleClick, this))
	.on('mouseover', _.bind(this.foregroundHover, this))
	.on('mouseout', _.bind(this.foregroundHoverEnd, this));

      var nodebox = 'nodebox';
      if (this.model.isHost !== undefined)
      {
	nodebox = 'nodehostbox';
      }

      this.rect = this.foreground.append('svg:rect')
	.attr('class', nodebox)
	.attr('x', '-30px')
	.attr('y', '-25px')
	.attr('width', '60px')
	.attr('height', '65px');

      this.highlight = this.foreground.append('svg:rect')
	.attr("class", "checkbox")
	.attr('x', '-30px')
	.attr('y', '-25px')
	.attr('width', '60px')
	.attr('height', '65px')
	.attr("style", "visibility:hidden;");

      this.status = this.foreground.append('svg:g')
        .attr('class', 'node-status')
        .attr('style', 'visibility: hidden');

      this.status.append('svg:rect')
	.attr('x', '10px')
	.attr('y', '-25px')
	.attr('class', 'node-status-box')
	.attr('width', '20px')
	.attr('height', '20px');

      this.status.append('svg:text')
        .attr('class', 'node-status-text')
	.attr('x', '15px')
	.attr('y', '-10px')
	.text('');

      this.warning = this.foreground.append('svg:g')
	.attr('style', 'visibility:hidden');

      this.warning.append('svg:rect')
        .attr('x', '-40px')
	.attr('y', '-35px')
        .attr('class', 'warningbox')
	.attr('width', '20px')
	.attr('height', '20px');

      this.warning.append('svg:text')
        .attr('class', 'warningboxtext')
        .attr('x', '-32px')
	.attr('y', '-20px')
	.text('!');

      this.icon = this.foreground.append('svg:image')
	.attr("class", "nodebox")
	.attr("x", "-20px")
	.attr("y", "-20px")
	.attr("width", "40px")
	.attr("height", "40px");

      this.label = this.foreground.append('svg:text')
	.attr("class", "nodetext")
	.attr("x", "0px")
	.attr("y", "35px");
    },

    update: function (shouldDisplay, highlightList) {
      if (shouldDisplay)
      {
	if (this.node.x && this.node.y &&
	    (this.node.x !== this.old.x ||
	     this.node.y !== this.old.y))
	{
	  this.container.attr('transform', 'translate(' +
			      this.node.x + ',' +
			      this.node.y + ')');
	  this.model.x = this.node.x;
	  this.model.y = this.node.y;
	}
	if (this.oldPos.x !== this.node.x ||
	    this.oldPos.y !== this.node.y)
	{
	  this.options.background.hideDragLine();
	  this.oldPos = { x: this.node.x, y: this.node.y };
	}
	if (this.model.name !== this.old.name)
	{
	  this.label.text(this.model.name);
	  var textWidth = 50;
	  try
	  {
	    this.label.each(function () {
	      textWidth = this.getBBox().width + 10;
	    });
	  } catch (e) {}
	  textWidth = Math.max(textWidth, 60)

	  this.rect.attr('width', textWidth + 'px');
	  this.rect.attr('x', (-textWidth/2) + 'px');
	  this.highlight.attr('width', textWidth + 'px');
	  this.highlight.attr('x', (-textWidth/2) + 'px');
	}
	var iconUrl = window.JACKS_LOADER.basePath + 'images/default.svg';
	if (this.model.icon)
	{
	  iconUrl = this.model.icon;
	}
	if (iconUrl !== this.old.icon)
	{
	  this.icon.attr("xlink:href", iconUrl);
	}

	this.old = {
	  x: this.node.x,
	  y: this.node.y,
	  name: this.model.name,
	  icon: iconUrl
	};
	if (highlightList)
	{
	  if (_.contains(highlightList, this.model.id))
	  {
	    this.highlight.style('visibility', 'visible')
	      .classed('selected', true);
	  }
	  else
	  {
	    this.highlight.style('visibility', 'hidden')
	      .classed('selected', false);
	  }
	}

	var shouldWarn = false;
	_.each(_.keys(this.model.warnings), function(key) {
	  if (this.model.warnings[key])
	  {
	    shouldWarn = true;
	  }
	}.bind(this));
	if (this.options.context.mode === 'viewer')
	{
	  shouldWarn = false;
	}
	if (shouldWarn !== this.shouldWarn)
	{
	  this.shouldWarn = shouldWarn;
	  if (shouldWarn)
	  {
	    this.warning.style('visibility', 'visible');
	  }
	  else
	  {
	    this.warning.style('visibility', 'hidden');
	  }
	}

      }
    },

    handleClick: function () {
      if (! d3v4.event.defaultPrevented)
      {
	//d3v4.event.preventDefault();
	var data = {
	  type: 'node',
	  item: this.model,
	  modkey: (d3v4.event.ctrlKey || d3v4.event.shiftKey),
	  event: null
	};
	if (d3v4.event.sourceEvent)
	{
	  data.event = _.clone(d3v4.event.sourceEvent);
	}
	else
	{
	  data.event = _.clone(d3v4.event);
	}
	this.options.clickUpdate.trigger('click-event', data);
      }
    },

    handleBackgroundClick: function () {
      if (! d3v4.event.defaultPrevented)
      {
	this.options.clickUpdate.trigger('click-outside');
      }
    },

    foregroundHover: function () {
      this.options.setHoverId(this.model.id);
    },

    foregroundHoverEnd: function () {
      this.options.setHoverId(null);
    },

    handleDragStart: function () {
      this.dragMoveCount = 0;
      this.groupChanged = null;
      this.options.force.stop();
      $('body > .popover').hide();
    },

    handleDragMove: function () {
      this.dragMoveCount += 1;
      var pos = d3v4.mouse(this.options.background.inner.node());
      this.node.vx = 0;
      this.node.vy = 0;
      this.node.x  = pos[0];
      this.node.y  = pos[1];
      var newGroup = this.options.checkGroup(this.node.x, this.node.y,
					     this.model.group);
      if (this.options.context.mode === 'editor' &&
	  newGroup !== this.model.group &&
	  (! this.groupChanged ||
	   Math.abs(this.node.x - this.groupChanged.x) > 60 ||
	   Math.abs(this.node.y - this.groupChanged.y) > 60))
      {
	this.groupChanged = { x: this.node.x, y: this.node.y };
	this.model.group = newGroup;
      }
      this.trigger('update');
    },

    handleDragEnd: function () {
      if (this.dragMoveCount && this.dragMoveCount > 1)
      {
	this.trigger('update-move');
	_.defer(function () {
	    $('html').css('-webkit-user-select', 'initial');
	    $('html').css('-moz-user-select', 'initial');
	  });
      }
      else
      {
	this.handleClick();
      }
    },

    linkHoverStart: function () {
      var pos = d3v4.mouse(this.options.background.inner.node());
      if (! this.linkDragging)
      {
	this.options.background.showDragLine(this.node,
					     { x: pos[0], y: pos[1] });
      }
    },

    linkHoverMove: function () {
      var pos = d3v4.mouse(this.options.background.inner.node());
      if (! this.linkDragging)
      {
	this.options.background.showDragLine(this.node,
					     { x: pos[0], y: pos[1] });
      }
    },

    linkHoverEnd: function () {
      if (! this.linkDragging)
      {
	this.options.background.hideDragLine();
      }
    },

    linkDragStart: function () {
      var pos = d3v4.mouse(this.options.background.inner.node());
      this.linkDragging = true;
      this.options.background.showDragLine(this.node,
					   { x: pos[0], y: pos[1] });
    },

    linkDragMove: function () {
      var pos = d3v4.mouse(this.options.background.inner.node());
      this.options.background.showDragLine(this.node,
					   { x: pos[0], y: pos[1] });
    },

    linkDragEnd: function () {
      this.linkDragging = false;
      this.options.background.hideDragLine();
      this.trigger('create-link', this.model.id);
      _.defer(function () {
	$('html').css('-webkit-user-select', 'initial')
	$('html').css('-moz-user-select', 'initial')
      });
    }

  });

  return NodeView;
});
