/*
 * rspecGenerator.js
 *
 * Generate an request rspec, maintaining original annotations
 *
 */

define (['underscore', 'backbone', 'js/canvas/rspecCommon',
	'text!html/rspec.xml'],
function (_, Backbone, rs, requestString)
{
  'use strict';

  var rspecGenerator = {};

  rspecGenerator.request = function(data, context)
  {
    var root = $($.parseXML(requestString)).find('rspec');
    root.attr('type', 'request');
    _.each(data.nodes, function (item) {
      var nodeXml;
      if (item.xml)
      {
	nodeXml = item.xml;
      }
      else
      {
	nodeXml = $($.parseXML('<node xmlns="http://www.geni.net/resources/rspec/3"/>')).find('node');
      }
      updateNode(item, data.interfaces, data.sites, data.lans, nodeXml,
		context);
      root.append(nodeXml);
    });
    _.each(data.lans, function (item) {
      var linkXml;
      if (item.xml)
      {
	linkXml = item.xml;
      }
      else
      {
	linkXml = $($.parseXML('<link xmlns="http://www.geni.net/resources/rspec/3"/>')).find('link');
      }
      updateLink(item, data.interfaces, data.sites, data.nodes, linkXml,
		 context);
      root.append(linkXml);
    });
    _.each(data.extraChildren, function (item) {
      root.append(item);
    });
    var s = new XMLSerializer();
    return s.serializeToString(root[0]);
  }

  function updateNode(node, interfaces, sites, links, xml, context)
  {
    xml.attr('client_id', node.name);
    if (node.sliverId)
    {
      xml.attr('sliver_id', node.sliverId);
    }
    else
    {
      xml.removeAttr('sliver_id');
    }

    if (node.icon)
    {
      var icon = rs.findOrMakeNS(xml, 'icon', rs.jacksNamespace);
      $(icon).attr('url', node.icon);
    }
    else
    {
      rs.removeNS(xml, 'icon', rs.jacksNamespace);
    }

    if (node.nomac)
    {
      rs.findOrMakeNS(xml, 'nomac', rs.jacksNamespace);
    }
    else
    {
      rs.removeNS(xml, 'nomac', rs.jacksNamespace);
    }

    updateNodeSite(node, sites, xml);

    var routable = rs.findOrMakeNS(xml, 'routable_control_ip',
				   rs.emulabNamespace);
    if (! node.routable)
    {
      routable.remove();
    }

    if (node.type || node.image)
    {
      var sliver = rs.findOrMakeNS(xml, 'sliver_type', rs.rspecNamespace);
      if (node.type)
      {
	sliver.attr('name', node.type);
      }
      else
      {
	sliver.removeAttr('name');
      }
      if (node.image)
      {
	var image = rs.findOrMakeNS(sliver, 'disk_image', rs.rspecNamespace);
	if (node.image.substring(0,3) === 'urn')
	{
	  image.attr('name', node.image);
	  image.removeAttr('url');
	}
	else
	{
	  image.attr('url', node.image);
	  image.removeAttr('name');
	}
	if (node.imageVersion)
	{
	  image.attr('version', node.imageVersion);
	}
	else
	{
	  image.removeAttr('version', node.imageVersion);
	}
      }
      else
      {
	rs.removeNS(sliver, 'disk_image', rs.rspecNamespace);
      }
    }
    if (node.hardware)
    {
      var hardware = rs.findOrMakeNS(xml, 'hardware_type', rs.rspecNamespace);
      hardware.attr('name', node.hardware);
    }
    else
    {
      rs.removeNS(xml, 'hardware_type', rs.rspecNamespace);
    }
    var services = rs.findOrMakeNS(xml, 'services', rs.rspecNamespace);
    rs.removeList(services, 'execute', rs.rspecNamespace);
    rs.removeList(services, 'install', rs.rspecNamespace);
    if (node.execute.length > 0 || node.install.length > 0)
    {
      rs.replaceList(services, node.execute, 'execute',
		     ['command', 'shell'], rs.rspecNamespace);
      rs.replaceList(services, node.install, 'install',
		     ['url', 'install_path'], rs.rspecNamespace);
    }

    if (node.type === 'emulab-blockstore')
    {
      var blockstore = rs.findOrMakeNS(xml, 'blockstore', rs.emulabNamespace);
      blockstore.attr('name', node.name + '-bs');
      if (node.dataset_mount)
      {
	blockstore.attr('mountpoint', node.dataset_mount)
      }
      else
      {
	blockstore.removeAttr('mountpoint');
      }
      blockstore.attr('class', 'remote');
      blockstore.attr('placement', 'any');
      if (node.dataset_option === 'remote')
      {
	blockstore.attr('readonly', 'false');
	blockstore.removeAttr('rwclone');
	blockstore.removeAttr('roclone');
      }
      else if (node.dataset_option === 'rwclone')
      {
	blockstore.attr('readonly', 'false');
	blockstore.attr('rwclone', 'true');
	blockstore.removeAttr('roclone');
      }
      else // node.dataset_option === 'readonly'
      {
	blockstore.attr('readonly', 'true');
	blockstore.removeAttr('rwclone');
	blockstore.removeAttr('roclone');
      }
      if (node.dataset_id)
      {
	blockstore.attr('dataset', node.dataset_id);
      }
      else
      {
	blockstore.removeAttr('dataset');
      }
    }
    //else
    //{
    //  rs.removeNS(xml, 'blockstore', rs.emulabNamespace);
    //}

    var xmlIface = {};
    xml.find('interface').each(function () {
      var clientId = $(this).attr('client_id');
      xmlIface[clientId] = $(this);
    });
    var internalIface = {};
    _.each(node.interfaces, function (ifaceId) {
      var iface = interfaces[ifaceId];
      internalIface[iface.name] = iface;
      var ifaceDom = xmlIface[iface.name];
      if (! ifaceDom)
      {
	ifaceDom = $($.parseXML('<interface xmlns="http://www.geni.net/resources/rspec/3"/>')).find('interface');
	xml.append(ifaceDom);
	ifaceDom.attr('client_id', iface.name);
      }
      var ip = rs.findOrMakeNS(ifaceDom, 'ip', rs.rspecNamespace);
      var chosenIp = iface.ip;
      var chosenNetmask = iface.netmask;
      var ifaceLink = links[iface.linkId];
      var linkContext = _.findWhere(context.canvasOptions.linkTypes,
				    { id: ifaceLink.linkType });
      if (! chosenIp && linkContext && linkContext.ip === 'auto')
      {
	chosenIp = '10.';
	chosenIp += Math.floor(iface.linkId / 256) + '.';
	chosenIp += (iface.linkId % 256) + '.';
	chosenIp += (ifaceLink.interfaces.indexOf(iface.id) + 1);
	chosenNetmask = '255.255.255.0';
      }
      if (chosenIp)
      {
	ip.attr('address', chosenIp);
	if (iface.ipType)
	{
	  ip.attr('type', iface.ipType);
	}
	else
	{
	  ip.attr('type', 'ipv4');
	}
	if (chosenNetmask)
	{
	  ip.attr('netmask', chosenNetmask);
	}
	else
	{
	  ip.removeAttr('netmask');
	}
      }
      else
      {
	ip.remove();
      }
    });
    _.each(_.keys(xmlIface), function (iface) {
      var ifaceDom = xmlIface[iface];
      if (! internalIface[iface])
      {
	ifaceDom.remove();
      }
    });
  }

  function updateNodeSite(node, sites, xml)
  {
      if (node.group)
      {
	  var nodeSite = sites[node.group];
	  if (nodeSite.type === 'site')
	  {
	      if (nodeSite.urn)
	      {
		  if (xml.attr('component_manager_id') !== nodeSite.urn)
		  {
		      xml.attr('component_manager_id', nodeSite.urn);
		  }
		  rs.removeNS(xml, 'site', rs.jacksNamespace);
	      }
	      else
	      {
		  xml.removeAttr('component_manager_id');
		  var group = rs.findOrMakeNS(xml, 'site', rs.jacksNamespace);
		  $(group).attr('id', sites[node.group].name);
	      }
	  }
      }
      else
      {
	  xml.removeAttr('component_manager_id');
	  rs.removeNS(xml, 'site', rs.jacksNamespace);
      }
  }

  //----------------------------------------------------------------------

  function updateLink(link, interfaces, sites, nodes, xml, context)
  {
    xml.attr('client_id', link.name);
    var type = link.linkType;
    if (type === 'vlan' || type === 'lan')
    {
      if (linkStitched(link))
      {
	type = 'vlan';
      }
      else
      {
	type = 'lan';
      }
    }
    var linkType = rs.findOrMakeNS(xml, 'link_type', rs.rspecNamespace);
    if (type)
    {
      linkType.attr('name', type);
    }
    else
    {
      linkType.remove();
    }

    var tagged = rs.findOrMakeNS(xml, 'vlan_tagging',
				 rs.emulabNamespace);
    if (link.nontrivial || containsBlockstore(nodes))
    {
      tagged.attr('enabled', 'true');
    }
    else
    {
      tagged.remove();
    }

    var interswitch = rs.findOrMakeNS(xml, 'interswitch',
				      rs.emulabNamespace);
    if (link.interswitch)
    {
      interswitch.remove();
    }
    else
    {
      interswitch.attr('allow', 'no');
    }

    updateOpenflow(xml, link.openflow);

    var sharedvlan = rs.findOrMakeNS(xml, 'link_shared_vlan',
				     rs.vlanNamespace);
    if (link.sharedvlan)
    {
      sharedvlan.attr('name', link.sharedvlan);
    }
    else
    {
      sharedvlan.remove();
    }

    var xmlIface = {};
    xml.find('interface_ref').each(function () {
      var clientId = $(this).attr('client_id');
      xmlIface[clientId] = $(this);
    });
    var internalIface = {};
    _.each(link.interfaces, function (ifaceId) {
      var iface = interfaces[ifaceId];
      internalIface[iface.name] = iface;
      if (! xmlIface[iface.name])
      {
	var ifaceDom = $($.parseXML('<interface_ref xmlns="http://www.geni.net/resources/rspec/3"/>')).find('interface_ref');
	xml.append(ifaceDom);
	ifaceDom.attr('client_id', iface.name);
      }
    });
    _.each(_.keys(xmlIface), function (iface) {
      var ifaceDom = xmlIface[iface];
      if (! internalIface[iface])
      {
	ifaceDom.remove();
      }
    });

    var properties = $(xml[0].getElementsByTagNameNS(rs.rspecNamespace,
						     'property'));
    //if (properties.length == 0)
    //{
      properties.remove();
      _.each(link.interfaces, function (ifaceId) {
	var iface = interfaces[ifaceId];
	var bw = iface.bandwidth;
	var name = iface.name;
	if (bw !== undefined && bw !== null && bw !== '')
	{
	  _.each(link.interfaces, function (destId) {
	    if (destId !== ifaceId)
	    {
	      var destName = interfaces[destId].name;
	      var dom = $($.parseXML('<property xmlns="http://www.geni.net/resources/rspec/3" source_id="' + _.escape(name) + '" dest_id="' + _.escape(destName) + '" capacity="' + _.escape(bw) + '"/>')).find('property');
	      xml.append(dom);
	    }
	  });
	}
      });
    //}
    //else
    //{
    //}

    var linkSites = {};
    _.each(link.endpoints, function (endpoint) {
      if (endpoint.group !== undefined)
      {
	var name = sites[endpoint.group].urn;
	var siteId = sites[endpoint.group].id;
	if (name)
	{
	  linkSites[name] = 'bound';
	}
	else
	{
	  linkSites[name] = 'unbound';
	}
      }
    });
    var cmList = $(xml[0].getElementsByTagNameNS(rs.rspecNamespace,
						 'component_manager'));
    cmList.each(function () {
      var name = $(this).attr('name');
      if (linkSites[name] === 'bound')
      {
	delete linkSites[name];
      }
      else if (! link.transitSites[name])
      {
	$(this).remove();
      }
    });
    var cmList = $(xml[0].getElementsByTagNameNS(rs.jacksNamespace,
						 'site'));
    cmList.each(function () {
      var name = $(this).attr('id');
      if (linkSites[name] === 'unbound')
      {
	delete linkSites[name];
      }
      else if (! link.transitSites[name])
      {
	$(this).remove();
      }
    });
    _.each(_.keys(linkSites), function (newSite) {
      var value = linkSites[newSite];
      var siteXml;
      if (value === 'bound')
      {
	siteXml = rs.makeNode(xml, 'component_manager',
			      rs.rspecNamespace);
	siteXml.attr('name', newSite);
      }
      else
      {
	siteXml = rs.makeNode(xml, 'site',
			      rs.jacksNamespace);
	siteXml.attr('id', newSite);
      }
    });
    var isNomac = findNomac(link, interfaces, nodes, context);
    var nomac = rs.findOrMakeNS(xml, 'link_attribute',
				rs.emulabNamespace);
    if (isNomac)
    {
      nomac.attr('key', 'nomac_learning');
      nomac.attr('value', 'yep');
    }
    else
    {
      nomac.remove();
    }
  }

  function containsBlockstore(nodes)
  {
    var result = false;
    _.each(nodes, function (node) {
      if (node.type === 'emulab-blockstore')
      {
	result = true;
      }
    });
    return result;
  }

  function linkStitched(link)
  {
    var result = false;
    var found;
    _.each(link.endpoints, function (endpoint) {
      if (found === undefined)
      {
	found = endpoint.group;
      }
      else if (found !== endpoint.group)
      {
	result = true;
      }
    });
    return result;
  }

  function updateOpenflow(xml, openflow)
  {
    var openflowList = rs.findOrMakeNS(xml, 'openflow_controller', rs.emulabNamespace);
    if (openflow)
    {
      openflowList.attr('url', openflow);
      openflowList.attr('type', 'primary');
    }
    else
    {
      openflowList.remove();
    }

    openflowList = rs.findOrMakeNS(xml, 'controller', rs.openflow3Namespace);
    if (openflow)
    {
      openflowList.attr('url', openflow);
      openflowList.attr('type', 'primary');
    }
    else
    {
      openflowList.remove();
    }

    openflowList = rs.findOrMakeNS(xml, 'controller', rs.openflow4Namespace);
    if (openflow)
    {
      openflowList.attr('url', openflow);
      openflowList.attr('role', 'primary');
    }
    else
    {
      openflowList.remove();
    }
  }

  function findNomac(link, interfaces, nodes, context)
  {
    var result = false;
    _.each(link.interfaces, function (ifaceId) {
      var iface = interfaces[ifaceId];
      var node = nodes[iface.nodeId];
      var image = node.image;
      var imageContext = _.findWhere(context.canvasOptions.images,
				     { id: image });
      if (node.nomac || (imageContext && imageContext.nomac))
      {
	result = true;
      }
    });
    return result ||
      (link.openflow !== null && link.openflow !== undefined);
  }

  return rspecGenerator;
});
