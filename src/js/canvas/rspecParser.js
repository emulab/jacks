/*
 * rspecParser.js
 *
 * Parse Requests, Manifests, and Advertisements
 *
 */

define(['underscore', 'backbone', 'js/canvas/rspecCommon'],
function (_, Backbone, rs)
{
  'use strict';

  var testGroup = 0;
  var rspecParser = {};

  // arg {
  //  rspec: String to parse
  //  context: Overall Jacks context
  //  errorModal: Handle to error modal for error display
  // }
  rspecParser.parse = function (arg)
  {
    var xmlString = arg.rspec;
    var context = arg.context;
    var errorModal = arg.errorModal;
    var result = null;

    if (! result)
    {
      result = {
	namespace: null,
	remainder: [],
	nodes: [],
	links: [],
	interfaces: {},
	interfaceList: [],
	sites: {},
	hosts: {}
      };
    }
    var nodeCount = 0;
    var defaultSite = {
      urn: undefined,
      name: undefined,
      custom: {},
      id: _.uniqueId()
    };

    try
    {
      var findEncoding = RegExp('^\\s*<\\?[^?]*\\?>');
      var match = findEncoding.exec(xmlString);
      if (match)
      {
	xmlString = xmlString.slice(match[0].length);
      }
      var xml = $.parseXML(xmlString);
      var xmlRoot = $(xml.documentElement);
      result.namespace = xmlRoot.namespaceURI;
      rs.findSameNS(xmlRoot, 'node').each(function(){
	  parseNode($(this), result, nodeCount, defaultSite, context);
	  $(this).remove();
	  nodeCount += 1;
	});

      var linkCount = 0;
      var used_iface = {};
      rs.findSameNS(xmlRoot, 'link').each(function() {
	  var repeatIface =
	    parseLink($(this), result, linkCount, context, used_iface);
	if (repeatIface)
	{
	  throw "An interface was used in more than one link";
	}
	  $(this).remove();
	  linkCount += 1;
	});

      result.remainder = xmlRoot.children();

      _.each(result.interfaceList, function (iface) {
	  if (! iface.linkId) {
	    console.log('Failed to find link for interface:', iface);
	  }
	});

      _.each(result.links, function (link) {
	_.each(link.nodeIndices, function (nodeIndex) {
	  link.endpoints.push(result.nodes[nodeIndex]);
	});
      });
      $(xmlRoot[0].getElementsByTagNameNS(rs.emulabNamespace, 'vhost')).each(function() {
	parseHost($(this), result);
      });
    }
    catch (e)
    {
      errorModal.update({
	verbatim: true,
	title: 'Cannot Parse Rspec',
	contents: 'Jacks cannot parse your rspec. Please send the rspec you tried to load and the following error message to jacks-support@protogeni.net <h1>Error:</h1><textarea class="form-control" rows="10">' + _.escape(e.toString()) + '</textarea><h1>Rspec:</h1><textarea class="form-control" rows="10">' + _.escape(xmlString) + '</textarea>'});
      console.log(e);
      result = {
	namespace: null,
	remainder: [],
	nodes: [],
	links: [],
	interfaces: {},
	interfaceList: [],
	sites: {}
      };
      nodeCount = 0;
    }

    if (nodeCount === 0)
    {
      result.sites[defaultSite.id] = defaultSite;
    }

    return result;
  };

  function parseNode(xml, result, count, defaultSite, context)
  {
    var node = {
      custom: {},
      warnings: {},
      execute: [],
      group: defaultSite.id,
      hardware: rs.findChildAttribute(xml, 'hardware_type', 'name'),
      hostport: undefined,
      icon: undefined,
      id: _.uniqueId(),
      image: undefined,
      imageVersion: rs.findChildAttribute(xml, 'disk_image', 'version'),
      index: count,
      install: [],
      interfaces: [],
      logins: undefined,
      name: xml.attr('client_id'),
      routable: false,
      routable_ip: undefined,
      sliverId: xml.attr('sliver_id'),
      sshurl: undefined,
      type: rs.findChildAttribute(xml, 'sliver_type', 'name'),
      xml: xml,
      dataset_id: undefined,
      dataset_mount: undefined,
      dataset_mount_option: 'remote'
    };

    node.image = rs.findChildAttribute(xml, 'disk_image', 'name');
    if (! node.image)
    {
      node.image = rs.findChildAttribute(xml, 'disk_image', 'url');
    }

    var iconParent = $(xml[0].getElementsByTagNameNS(rs.jacksNamespace, 'icon'));
    if (iconParent.length > 0)
    {
      node.icon = iconParent.attr('url');
    }

    var nomacParent = $(xml[0].getElementsByTagNameNS(rs.jacksNamespace, 'nomac'));
    if (nomacParent.length > 0)
    {
      node.nomac = true;
    }

    var taggingParent = $(xml[0].getElementsByTagNameNS(rs.emulabNamespace, 'vlan_tagging'));
    if (taggingParent.length > 0)
    {
      node.nontrivial = (taggingParent.attr('enabled') === 'true');
    }

    var foundIcon = _.findWhere(context.canvasOptions.icons,
				{ id: node.icon });
    if (node.icon && ! foundIcon)
    {
      node.custom.icon = true;
    }

    var foundImage = _.findWhere(context.canvasOptions.images,
				 { id: node.image });
    if (node.image && ! foundImage)
    {
      node.custom.image = true;
    }

    var foundHardware = _.findWhere(context.canvasOptions.hardware,
				    { id: node.hardware });
    if (node.hardware && ! foundHardware)
    {
      node.custom.hardware = true;
    }

    var foundType = _.findWhere(context.canvasOptions.types,
				{ id: node.type });
    if (node.type && ! foundType)
    {
      node.custom.type = true;
    }

    if (node.type && node.type === 'emulab-blockstore')
    {
      var blockstore = $(xml[0].getElementsByTagNameNS(rs.emulabNamespace, 'blockstore'));
      if (blockstore.length > 0)
      {
	this.dataset_mount = blockstore.attr('mountpoint');
	this.dataset_option = 'remote';
	if (blockstore.attr('rwclone') === 'true')
	{
	  this.dataset_option = 'rwclone';
	}
	else if (blockstore.attr('readonly') === 'true')
	{
	  this.dataset_option = 'readonly';
	}
	this.dataset_id = blockstore.attr('dataset');
      }
    }
    
    var services = rs.findSameNS(xml, 'services');
    services.each(function () {
      var executeItems = rs.findSameNS($(this), 'execute');
      executeItems.each(function () {
	node.execute.push({
	  'rspec': $(this),
	  'command': $(this).attr('command'),
	  'shell': $(this).attr('shell')
	});
      });
      var installItems = rs.findSameNS($(this), 'install');
      installItems.each(function () {
	node.install.push({
	  'rspec': $(this),
	  'url': $(this).attr('url'),
	  'install_path': $(this).attr('install_path')
	});
      });
    });

    var routableItems = xml[0].getElementsByTagNameNS(rs.emulabNamespace, 'routable_control_ip');
    if (routableItems.length > 0)
    {
      node.routable = true;
      var hostItems = rs.findSameNS(xml, 'host');
      if (hostItems.length > 0)
      {
	node.routable_ip = $(hostItems[0]).attr('ipv4');
      }
    }
    var cmUrn = xml.attr('component_manager_id');
    var wirelessElement = $(xml[0].getElementsByTagNameNS(rs.emulabNamespace, 'wireless-site'));
    var wirelessName = null;
    var wirelessType = null;
      if (wirelessElement.length > 0)
      {
	  wirelessName = wirelessElement.attr('id');
	  wirelessType = wirelessElement.attr('type');
	  wirelessUrn = wirelessElement.attr('urn');
	  if (wirelessName)
	  {
	      node.group = findOrMakeSite(wirelessUrn, wirelessName, result.sites, context, wirelessType).id;
	  }
      }
    var siteElement = $(xml[0].getElementsByTagNameNS(rs.jacksNamespace, 'site'));
    var siteName = null;
    if (siteElement.length > 0)
    {
      siteName = siteElement.attr('id');
    }
    if (cmUrn || siteName)
    {
      node.group = findOrMakeSite(cmUrn, siteName, result.sites, context, 'site').id;
    }
    else if (! result.sites[defaultSite.id])
    {
      // This is already part of the default site. Make sure the
      // default site is in the sites list.
      result.sites[defaultSite.id] = defaultSite;
    }

    rs.findSameNS(xml, 'interface').each(function() {
      var iface = {
	name: $(this).attr('client_id'),
	nodeName: node.name,
	node_index: count,
	id: _.uniqueId(),
	node: node,
	nodeId: node.id,
	mac: $(this).attr('mac_address'),
	bandwidth: undefined,
      };
      var ip = $(this.getElementsByTagNameNS(rs.rspecNamespace, 'ip'));
      if (ip.length > 0)
      {
	iface.ip = ip.attr('address');
	iface.ipType = ip.attr('type');
	iface.netmask = ip.attr('netmask');
      }
      node.interfaces.push(iface.id);
      result.interfaces[iface.id] = iface;
      result.interfaceList.push(iface);
    });

    if (services.length > 0)
    {
      var login  = rs.findSameNS(services, 'login');
      login.each(function () {
	var user   = $(this).attr('username');
	var host   = $(this).attr('hostname');
	var port   = $(this).attr('port');

	if (host !== undefined && port !== undefined)
	{
	  node.hostport  = host + ':' + port;
	  if (user !== undefined)
	  {
	    node.sshurl = 'ssh://' + user + '@' + host + ':' + port + '/';
	  }
	}
	if (user !== undefined &&
	    host !== undefined &&
	    port !== undefined)
	{
	  var newLogin = user + '@' + host + ':' + port;
	  if (node.logins)
	  {
	    node.logins += ', ' + newLogin;
	  }
	  else
	  {
	    node.logins = newLogin;
	  }
	}
      });
    }

    nodeHost(xml, result, node);

    //mergeNode(node, result.nodes);
    result.nodes.push(node);
    return node;
  }
/*
  function mergeNode(node, nodeList)
  {
    var found = null;
    var clashes = _.where(nodeList, { name: node.name });
    var i = 0;
    var index = 0;
    for (var i = 0; i < clashes.length; i += 1)
    {
      if (clashes[i].group === node.group)
      {
	found = clashes[i];
	index = i;
	break;
      }
    }
  }
*/
  function parseLink(xml, result, count, context, used_iface)
  {
    var link = {
      custom: {},
      warnings: {},
      index: count,
      interfaces: [],
      linkType: rs.findChildAttribute(xml, 'link_type', 'name'),
      name: xml.attr('client_id'),
      openflow: undefined,
      nodeIndices: [],
      endpoints: [],
      endpointNames: [],
      id: _.uniqueId(),
      sliverId: xml.attr('sliver_id'),
      sharedvlan: undefined,
      transitSites: {},
      xml: xml,
      interswitch: true
    };
    var foundType = _.findWhere(context.canvasOptions.linkTypes,
				{ id: link.linkType });
    if (link.linkType && ! foundType)
    {
      link.custom.linkType = true;
    }

    var shared = $(xml[0].getElementsByTagNameNS(rs.vlanNamespace,
						 'link_shared_vlan'));
    if (shared.length > 0)
    {
      link.sharedvlan = shared.attr('name');
    }

    var interswitch = $(xml[0].getElementsByTagNameNS(rs.emulabNamespace,
						      'interswitch'));
    if (interswitch.length > 0)
    {
      link.interswitch = (interswitch.attr('allow') === 'yes');
    }

    var foundShared = _.findWhere(context.canvasOptions.sharedvlans,
				  { id: link.sharedvlan });
    if (link.sharedvlan && ! foundShared)
    {
      link.custom.sharedvlan = true;
    }

    var openflow = parseOpenflow(xml);
    if (openflow)
    {
      link.openflow = openflow;
    }

    var repeatIface = false;
    var ifacerefs = rs.findSameNS(xml, 'interface_ref');
    _.each(ifacerefs, function (ref) {
      var targetName = $(ref).attr('client_id');
      /*
       * First we have map the client_ids to the node by
       * searching all of the interfaces we put into the
       * list above.
       *
       */
      _.each(result.interfaceList, function (iface) {
      	if (iface.name == targetName)
	{
	  if (used_iface[iface.name])
	  {
	    repeatIface = true;
	  }
	  else
	  {
	    used_iface[iface.name] = 1;
	  }
	  link.nodeIndices.push(iface.node_index);
	  link.endpointNames.push(iface.nodeName);
	  link.interfaces.push(iface.id);
	  iface.linkId = link.id;
	}
      });
    });

    var sites = $(xml[0].getElementsByTagNameNS(rs.rspecNamespace,
						'component_manager'));
    sites.each(function () {
      var name = $(this).attr('name');
      var found = false;
      _.each(link.nodeIndices, function (nodeId) {
	var node = result.nodes[nodeId];
	var currentSite = result.sites[node.group];
	if (currentSite.urn && currentSite.urn === name)
	{
	  found = true;
	}
      });
      if (! found)
      {
	link.transitSites[name] = 1;
      }
    });

    var properties = $(xml[0].getElementsByTagNameNS(rs.rspecNamespace,
						     'property'));
    properties.each(function () {
      var sourceName = $(this).attr('source_id');
      var bw = $(this).attr('capacity');
      if (bw !== null && bw !== undefined && bw !== '')
      {
	_.each(result.interfaceList, function (iface) {
      	  if (iface.name == sourceName)
	  {
	    iface.bandwidth = bw;
	  }
	});
      }
    });

    if (! repeatIface)
    {
      result.links.push(link);
    }
    return repeatIface;
  }

  function findOrMakeSite(cmUrn, name, sites, context, type)
  {
    var result;
    var siteList;
    // First try to match by Component Manager URN
    if (cmUrn)
    {
      siteList = _.where(_.values(sites), { urn: cmUrn });
      if (siteList.length > 0)
      {
	result = siteList[0];
	if (! result.name)
	{
	  result.name = name;
	}
      }
    }
    // If that doesn't work, try to match by Jacks site ID
    if (! result && name)
    {
      siteList = _.where(_.values(sites), { name: name });
      if (siteList.length > 0)
      {
	result = siteList[0];
	if (! result.urn)
	{
	  result.urn = cmUrn;
	}
      }
    }
    // If all else fails, just create a new one
    if (! result)
    {
      result = {
	custom: {},
	urn: cmUrn,
        id: _.uniqueId(),
        type: type
      };
      if (name)
      {
	result.name = name;
      }

      var foundUrn = _.findWhere(context.canvasOptions.aggregates,
				 { id: result.urn });
      if (result.urn && ! foundUrn)
      {
	result.custom.urn = true;
      }

      sites[result.id] = result;
    }
    return result;
  }

  function parseOpenflow(xml)
  {
    var result;
    var openflowList =
      $(xml[0].getElementsByTagNameNS(rs.emulabNamespace, 'openflow_controller'));
    if (openflowList.length > 0)
    {
      result = openflowList.attr('url');
    }

    if (! result)
    {
      openflowList = $(xml[0].getElementsByTagNameNS(rs.openflow3Namespace, 'controller'));
      if (openflowList.length > 0)
      {
	result = openflowList.attr('url');
      }
    }

    if (! result)
    {
      openflowList = $(xml[0].getElementsByTagNameNS(rs.openflow4Namespace, 'controller'));
      if (openflowList.length > 0)
      {
	result = openflowList.attr('url');
      }
    }

    return result;
  }

  function nodeHost(xml, result, node)
  {
    var vmlist = $(xml[0].getElementsByTagNameNS(rs.emulabNamespace, 'vmlist'));
    if (vmlist.length > 0)
    {
      vmlist = rs.findSameNS(vmlist, 'vm');
      var newHost = {
	id: _.uniqueId(),
	name: xml.attr('client_id'),
	attachedNode: xml.attr('client_id'),
	nodes: []
      };
      node.isHost = xml.attr('client_id');
      vmlist.each(function () {
	newHost.nodes.push($(this).attr('client_id'));
      });
      result.hosts[newHost.id] = newHost;
    }
  }

  function parseHost(xml, result)
  {
    var newHost = {
      id: _.uniqueId(),
      name: xml.attr('client_id'),
      attachedNode: undefined,
      nodes: []
    };
    var vmlist = rs.findSameNS(xml, 'vm');
    vmlist.each(function () {
      newHost.nodes.push($(this).attr('client_id'));
    });
    result.hosts[newHost.id] = newHost;
  }

  return rspecParser;
});
