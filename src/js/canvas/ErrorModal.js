define(['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  'use strict';

  var ErrorModal = View.extend({

    initialize: function (options)
    {
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('#errorModal');
      }
      this.$('button').on('click', _.bind(this.close, this));
      return this.superRender();
    },

    // state {
    //   title: Text for title
    //   contents: Text for display
    //   verbatim: True if you don't want the contents and title escaped
    // }
    // Displays the modal on every update
    update: function (state)
    {
      if (state)
      {
	var title = state.title;
	var contents = state.contents;
	if (! state.verbatim)
	{
	  title = _.escape(title);
	  contents = _.escape(contents);
	}
	this.$('.error-title').html(title);
	console.log(this.$el.length);
	console.log(title, this.$('.error-title'));
	this.$('.error-contents').html(contents);
	this.$el.show();
      }
    },

    close: function ()
    {
      this.$el.hide();
    }

  });

  return ErrorModal;
});
