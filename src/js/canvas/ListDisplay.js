define(['underscore', 'backbone'],
function (_, Backbone)
{
  'use strict';

  var ListDisplay = Backbone.View.extend({

    events: {
      'click #add': 'addItem'
    },

    initialize: function () {
      this.domItems = [];
    },

    cleanup: function () {
      this.stopListening();
      _.each(this.domItems, function (item) {
	item.cleanup();
      });
    },

    render: function () {
    },

    update: function (list) {
      for (var i = 0; i < this.domItems.length; i += 1)
      {
	if (i < list.length)
	{
	  this.domItems[i].update(list[i]);
	}
	else
	{
	  this.stopListening(this.domItems[i]);
	  this.domItems[i].cleanup();
	}
      }
      for (var i = this.domItems.length; i < list.length; i += 1)
      {
	var newElement = $('<div class="info-list-item panel panel-default"/>');
	var newItem = new this.options.ItemDisplay({ el: newElement });
	this.listenTo(newItem, 'change', this.changeItem);
	this.listenTo(newItem, 'remove', this.removeItem);
	newItem.update(list[i]);
	this.domItems.push(newItem);
	this.$('.list').append(newElement);
      }
      this.domItems.splice(list.length, this.domItems.length);
    },

    addItem: function () {
      this.trigger('add');
    },

    removeItem: function (changed) {
      var index = this.domItems.indexOf(changed.item)
      this.trigger('remove', { index: index });
    },

    changeItem: function (changed) {
      var index = this.domItems.indexOf(changed.item);
      this.trigger('change', { index: index,
			       changed: changed });
    }

  });

  return ListDisplay;
});
