define(['underscore', 'backbone'],
function (_, Backbone)
{
  'use strict';

  var template = '<div class="panel-body"><label>URL:</label><input type="text" class="form-control" id="url" value="" placeholder="ex: http://example.com/mystuff.tar.gz"><label>Install Path:</label><input type="text" class="form-control" id="path" value="" placeholder="ex: /local"><button id="remove" class="btn btn-danger">Remove</button></div>';

  var InstallDisplay = Backbone.View.extend({

    events: {
      'click #remove': 'removeItem'
    },

    initialize: function () {
      this.render();
      this.$el.on('change keyup paste', 'input',
		  _.bind(this.changeItem, this));
    },

    cleanup: function () {
      this.$el.off('change keyup paste', 'input');
      this.$el.remove();
    },

    render: function () {
      this.$el.html(template);
    },

    update: function (install) {
      if (this.$('#url').val() !== install.url)
      {
	this.$('#url').val(install.url);
      }
      if (this.$('#path').val() !== install.install_path)
      {
	this.$('#path').val(install.install_path);
      }
    },

    removeItem: function () {
      this.trigger('remove', { item: this });
    },

    changeItem: function () {
      _.defer(function () {
	this.trigger('change', { item: this,
				 url: this.$('#url').val(),
				 install_path: this.$('#path').val() });
      }.bind(this));
    }

  });

  return InstallDisplay;
});
