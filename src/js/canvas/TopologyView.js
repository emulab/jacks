define(['underscore', 'backbone',
	'js/canvas/TopologyBackground', 'js/canvas/ForceGraph',
	'js/canvas/NodeView', 'js/canvas/LanView', 'js/canvas/SiteView',
	'js/canvas/HostView', 'js/canvas/ValidList'],
function (_, Backbone,
	 TopologyBackground, ForceGraph, NodeView, LanView, SiteView,
	 HostView, ValidList)
{
  'use strict';

  var TopologyView = Backbone.View.extend({

    events: {
    },

    initialize: function () {
      this.force = new ForceGraph();
      this.nodes = [];
      this.lans = [];
      this.hosts = [];
      this.sites = [];
      this.siteFill = d3v4.schemeCategory10;
      this.validList = new ValidList(this.model, this.options.constraints);
      this.background = new TopologyBackground(this.$el,
					       this.force.nodes,
					       this.sites,
					       this.options.defaults,
					       this.options.clickUpdate,
					       this.options.context,
					       this.force,
					       this.validList);
      this.hoverId = null;

      this.listenTo(this.model, 'addNodes', this.addNodes);
      this.listenTo(this.model, 'addLans', this.addLans);
      this.listenTo(this.model, 'addSites', this.addSites);
      this.listenTo(this.model, 'addHosts', this.addHosts);
      this.listenTo(this.model, 'removeNodes', this.removeNodes);
      this.listenTo(this.model, 'removeLans', this.removeLans);
      this.listenTo(this.model, 'removeSites', this.removeSites);
      this.listenTo(this.model, 'firstGraph', this.firstGraph);
      this.listenTo(this.model, 'change', this.update);
      this.listenTo(this.force, 'tick', this.updateGraph);
      this.listenTo(this.background, 'create-site', this.createSite);
      this.listenTo(this.background, 'create-node', this.createNode);

      this.render();
    },

    render: function () {
      var that = this;
      this.background.render();
/*
      _.each(this.nodes, function (node) {
	node.render();
      });
      _.each(this.lans, function (lan) {
	lan.render();
      });
      _.each(this.sites, function (site) {
	site.render();
      });
*/
      this.updateGraph();
    },

    show: function () {
      this.$el.show();
    },

    hide: function () {
      this.$el.hide();
    },

    resize: function (width, height) {
      this.background.resize(width, height);
    },

    startForce: function () {
      this.force.start();
    },

    update: function () {
      this.updateNoScale(false, true);
    },

    updateNoScale: function (stopped, shouldDisplay, highlightList) {
      _.each(this.nodes, function (node) {
	node.update(shouldDisplay, highlightList);
      });
      _.each(this.lans, function (lan) {
	lan.update(shouldDisplay, highlightList);
      });
      _.each(this.sites, function (site) {
	site.update(shouldDisplay, highlightList);
      });
      _.each(this.hosts, function (host) {
	host.update(shouldDisplay, highlightList);
      });
    },

    updateGraph: function (stopped, shouldDisplay, highlightList) {
      this.updateNoScale(stopped, shouldDisplay, highlightList);
      this.background.centerScale(true);
    },

    firstGraph: function () {
      this.force.skipAhead();
      this.updateGraph(true, true);
    },

    addNodes: function (newNodes) {
      this.add(newNodes, NodeView, this.nodes);
      this.updateGraph(false, true);
    },

    addLans: function (newLans) {
      this.add(newLans, LanView, this.lans);
      this.updateGraph(false, true);
    },

    addSites: function (newSites) {
      this.add(newSites, SiteView, this.sites);
      this.updateGraph(false, true);
    },

    addHosts: function (newHosts) {
      this.add(newHosts, HostView, this.hosts);
      this.updateGraph(false, true);
    },

    removeNodes: function (oldIds) {
      this.nodes = this.remove(oldIds, this.nodes);
      this.updateGraph(false, true);
    },

    removeLans: function (oldIds) {
      this.lans = this.remove(oldIds, this.lans);
      this.updateGraph(false, true);
    },

    removeSites: function (oldIds) {
      this.sites = this.remove(oldIds, this.sites);
      this.updateGraph(false, true);
    },

    add: function (items, ItemType, list)
    {
      var that = this;
      _.each(items, function (item) {
	var fillColor = list.length % that.siteFill.length;
	var newView = new ItemType({
	  model: item,
	  force: that.force,
	  background: that.background,
	  fill: that.siteFill[fillColor],
	  clickUpdate: that.options.clickUpdate,
	  checkGroup: _.bind(that.checkGroup, that),
	  setHoverId: _.bind(that.setHoverId, that),
	  siteCount: _.bind(that.siteCount, that),
	  context: that.options.context
	});
	that.listenTo(newView, 'update', that.update);
	that.listenTo(newView, 'update-move', that.updateGraph);
	that.listenTo(newView, 'create-link', that.createLink);
	list.push(newView);
      });
    },

    remove: function (ids, list)
    {
      var result = list;
      _.each(ids, function (id) {
	var found;
	var foundIndex;
	_.each (list, function(candidate, index) {
	  if (id === candidate.options.model.id)
	  {
	    found = candidate;
	    foundIndex = index;
	  }
	}.bind(this));
	if (found)
	{
	  found.cleanup();
	  list.splice(foundIndex, 1);
	}
      });
      return result;
    },

    checkGroup: function (x, y, oldGroup)
    {
      var result = oldGroup;
      _.each(this.sites, function (site) {
	if (site.contains(x, y, oldGroup))
	{
	  result = site.model.id;
	}
      });
      return result;
    },

    setHoverId: function (newId)
    {
      this.hoverId = newId;
    },

    siteCount: function ()
    {
      return this.sites.length;
    },

    setHighlights: function (list, type)
    {
      this.updateGraph(true, true, list);
    },

    createNode: function (x, y, siteId, options)
    {
      this.model.addEmptyNode(x, y, siteId, options);
      this.force.start();
      this.force.stop();
    },

    createSite: function (x, y)
    {
      this.model.addEmptySite(x, y);
      this.force.start();
      this.force.stop();
    },

    createLink: function (sourceId)
    {
      this.model.attemptConnection(sourceId, this.hoverId);
      this.force.start();
      this.force.stop();
    }

  });


  return TopologyView;
});
