define(['underscore', 'backbone', 'js/canvas/ValidList'],
function (_, Backbone, ValidList)
{
  'use strict';

  function TopologyModel(context, constraints, errorModal)
  {
    this.context = context;
    this.validList = new ValidList(this, constraints);
    this.errorModal = errorModal;
    this.nodes = {};
    this.lans = {};
    this.interfaces = {};
    this.sites = {};
    this.hosts = {};
    this.idList = {};
    this.extraChildren = [];
    // A hash of arrays. Key is IP address. Array is a list of interface IDs. There should only ever be 1 interface ID at most for each key. Otherwise the user has assigned a duplicate address.
    this.ipUsage = {};
    _.extend(this, Backbone.Events);
  }

  TopologyModel.prototype = {

    addGraph: function (newGraph, sourceUrn) {
      var firstGraph = (_.keys(this.nodes).length === 0);

      var bornSites = [];
      _.each(newGraph.sites, function (site) {
	this.insertSite(site, newGraph, bornSites);
      }.bind(this));

      var bornNodes = [];
      // Reaper nodes are old nodes which are not kept
      var reaperNodes = [];
      // Ghosts are new nodes which are not kept
      var ghostToOld = {};
      var bornToOld = {};
      _.each(newGraph.nodes, function (node) {
	this.insertNode(node, newGraph, reaperNodes, bornNodes,
			bornToOld, ghostToOld, sourceUrn);
      }.bind(this));

      var reaperLans = [];
      _.each(newGraph.links, function (lan) {
	this.insertLan(lan, newGraph, bornToOld, ghostToOld,
		       newGraph.interfaces, this.interfaces, reaperLans);
      }.bind(this));

      _.each(newGraph.interfaces, function (iface) {
	this.interfaces[iface.id] = iface;
      }.bind(this));

      var bornHosts = [];
      _.each(newGraph.hosts, function (host) {
	this.insertHost(host, newGraph, bornHosts);
      }.bind(this));

      this.extraChildren = newGraph.remainder;
      this.removeNodes(reaperNodes);
      this.removeLinks(reaperLans);
      _.each(_.keys(this.interfaces), function (ifaceId) {
	var iface = this.interfaces[ifaceId];
	var node;
	if (! iface.linkId) {
	  node = this.nodes[iface.nodeId];
	  node.interfaces = _.without(node.interfaces, ifaceId);
	  delete this.interfaces[ifaceId];
	} else if (! this.nodes[iface.nodeId]) {
	  if (iface.ip)
	  {
	    this.switchIp(ifaceId, null, iface.ip);
	  }
	  var link = this.lans[iface.linkId];
	  node = _.findWhere(link.endpoints, { id: iface.nodeId });
	  link.interfaces = _.without(link.interfaces, ifaceId);
	  link.endpoints = _.without(link.endpoints, node);
	  delete this.interfaces[ifaceId];
	}
      }.bind(this));
      if (this.context.mode !== 'viewer')
      {
	_.each(bornNodes, function (node) {
	  this.checkNode(node);
	}.bind(this));
	_.each(newGraph.links, function (link) {
	  this.checkLan(link);
	}.bind(this));
      }
      
      _.each(newGraph.sites, function (site) {
	if (this.sites[site.id])// && ! this.sites[site.id].name)
	{
	  this.sites[site.id].name = this.generateSiteName(this.sites[site.id]);
	}
      }.bind(this));
      var reaperSites = [];
      _.each(bornSites, function (site) {
	if (this.countNodes(site.id, bornNodes) === 0 &&
	    (_.keys(this.nodes).length > 0 || bornNodes.length > 0))
	{
	  reaperSites.push(site.id);
	}
      }.bind(this));
      _.each(reaperSites, function (siteId) {
	bornSites = _.without(bornSites, _.findWhere(bornSites, {id: siteId}));
      }.bind(this));
      this.removeSites(reaperSites);

      this.trigger('addSites', bornSites);
      this.trigger('addHosts', bornHosts);
      this.trigger('addNodes', bornNodes);
      this.trigger('addLans', newGraph.links);
      this.trigger('change');
      if (firstGraph)
      {
	this.trigger('firstGraph');
      }
    },

    nodesMatch: function (oldNode, newNode) {
      var oldSite = this.sites[oldNode.group];
      var newSite = this.sites[newNode.group];
      var siteMatch = (oldSite.urn && newSite.urn &&
		       oldSite.urn === newSite.urn &&
		       oldNode.name === newNode.name);
      var sliverMatch = (oldNode.sliverId && newNode.sliverId &&
			 oldNode.sliverId === newNode.sliverId);
      return siteMatch || sliverMatch;
    },

    insertNode: function (newNode, newGraph,
			  reaperList, bornList, bornToOld, ghostToOld, sourceUrn) {
      newNode.sourceUrn = sourceUrn;
      // New nodes replace old nodes
      var newSite = this.sites[newNode.group];
      var oldSite, oldNode;
      _.each(_.values(this.nodes), function (candidate) {
	var site = this.sites[candidate.group];
	if (this.nodesMatch(candidate, newNode))
	{
	  oldNode = candidate;
	  oldSite = site;
	}
      }.bind(this));

      // Destroy old node if there is an old node and if either the
      // new node has a sliver id or the old node lacks a sliver id
//      var destroyOld = (oldNode &&
//			(! newNode.sliverId ||
//			 oldNode.sliverId));
//      var destroyOld = true;
//      if (destroyOld && oldNode)
      var hasReplaced = false;
      if (oldNode)
      {
	newNode.x = oldNode.x;
	newNode.y = oldNode.y;
	reaperList.push(oldNode.id);
	if (oldNode.sliverId && ! newNode.sliverId)
	{
	  _.each(_.keys(oldNode), function (key) {
	    if (key !== 'id' && key !== 'interfaces')
	    {
	      newNode[key] = oldNode[key];
	    }
	  }.bind(this));
	}
      }
      var isManifest = (this.context.isManifest === true);
      var hasSliver = (newNode.sliverId !== null && newNode.sliverId !== undefined)
      // Add the new node if there is no old node, or if we destroyed it
//      if (destroyOld || ! oldNode)
//      {
      if (! isManifest || hasSliver)
      {
	this.nodes[newNode.id] = newNode;
	bornList.push(newNode);
	bornToOld[newNode.id] = oldNode;
      }
//      }
//      else
//      {
//	ghostToOld[newNode.id] = oldNode;
//      }
    },

    insertLan: function (newLan, newGraph, bornToOld, ghostToOld, newInterfaces, oldInterfaces, reaperLans) {
/*
      _.each(_.values(this.lans), function (candidate) {
	if (candidate.name === newLan.name)
	{
	  var allMatch = true;
	  _.each(newLan.interfaces, function (ifaceId) {
	    var newId = newInterfaces[ifaceId].nodeId;
	    var mapped = ghostToOld[newId];
	    if (mapped === undefined || mapped === null)
	    {
	      mapped = bornToOld[newId];
	    }
	    var found = false;
	    if (mapped !== undefined && mapped !== null)
	    {
	      _.each(candidate.interfaces, function (oldIfaceId) {
		var oldId = this.interfaces[oldIfaceId].nodeId;
		if (oldId === mapped.id)
		{
		  found = true;
		}
	      });
	    }
	    if (! found)
	    {
	      allMatch = false;
	    }
	  });
	  if (allMatch)
	  {
	    reaperLans.push(candidate.id);
	  }
	}
      });
*/
      _.extend(newLan, Backbone.Events);
      this.lans[newLan.id] = newLan;
    },

    insertSite: function (newSite, newGraph, bornList) {
      // New sites are ignored and not added
      var foundId = undefined;
      _.each(_.values(this.sites), function (candidate) {
	if (candidate.urn && newSite.urn &&
	    candidate.urn === newSite.urn)
	{
	  foundId = candidate.id;
	}
      }.bind(this));
      if (foundId === undefined)
      {
	bornList.push(newSite);
	this.sites[newSite.id] = newSite;
      }
      else
      {
	_.each(_.values(newGraph.nodes), function (node) {
	  if (node.group === newSite.id)
	  {
	    node.group = foundId;
	  }
	}.bind(this));
      }
    },

    insertHost: function (newHost, newGraph, bornList) {
      bornList.push(newHost);
      this.hosts[newHost.id] = newHost;
      _.each(newHost.nodes, function (client_id) {
	var id = null;
	_.each(_.values(this.nodes), function (candidate) {
	  if (candidate.name === client_id)
	  {
	    id = candidate.id;
	  }
	});
	if (id && this.nodes[id])
	{
	  this.nodes[id].host = newHost.id;
	}
      }.bind(this));
    },

    countNodes: function (siteId, nodeList) {
      var result = 0;
      _.each(nodeList, function (node) {
	if (node.group === siteId)
	{
	  result += 1;
	}
      }.bind(this));
      return result;
    },

    addEmptyNode: function (x, y, siteId, option) {
      var name = this.generateNodeName();
      var newNode = {
	custom: {},
	warnings: {},
	id: _.uniqueId(),
	interfaces: [],
	name: name,
	x: x,
	y: y,
	group: siteId,
	type: option.type,
	image: option.image,
	imageVersion: undefined,
	hardware: option.hardware,
	icon: option.icon,
	execute: [],
	install: [],
	routable: false,
	dataset_option: 'remote'
      };
      if (option.execute)
      {
	newNode.execute = option.execute;
      }
      if (option.install)
      {
	newNode.install = option.install;
      }
      if (option.routable)
      {
	newNode.routable = option.routable;
      }
      var image = _.findWhere(this.context.canvasOptions.images,
			      { id: option.image });
      if (image)
      {
	newNode.imageVersion = image.version;
      }
      if (! this.validList.isValidNode(newNode, this.sites[siteId]))
      {
	this.errorModal.update({ title: 'Incompatible Node Warning',
				 contents: 'This node is not listed as compatible with the site you have placed it in. Your topology may be impossible to allocate.' });
      }
      this.nodes[newNode.id] = newNode;
      this.checkNode(newNode);
      this.trigger('addNodes', [newNode]);
    },

    addEmptySite: function (x, y) {
      var newSite = {
	custom: {},
	id: _.uniqueId(),
	x: x,
	y: y,
	type: 'site'
      };
      newSite.name = this.generateSiteName(newSite);
      this.sites[newSite.id] = newSite;
      this.trigger('addSites', [newSite]);
    },

    attemptConnection: function (sourceId, targetId) {
      if (sourceId !== null && targetId !== null && targetId !== sourceId)
      {
	if (this.nodes[sourceId] &&
	    this.nodes[targetId])
	{
	  this.addEmptyLink(sourceId, targetId);
	}
	else if (this.nodes[sourceId] && this.lans[targetId])
	{
	  this.addNodeToLan(sourceId, targetId);
	}
	else if (this.nodes[targetId] && this.lans[sourceId])
	{
	  this.addNodeToLan(targetId, sourceId);
	}
      }
    },

    addEmptyLink: function (sourceId, targetId) {
      var newLan = {
	custom: {},
	warnings: {},
	id: _.uniqueId(),
	name: this.generateLinkName(),
	interfaces: [],
	nodeIndices: [],
	endpoints: [],
	endpointNames: [],
	transitSites: {},
	interswitch: true
      };
      _.extend(newLan, Backbone.Events);
      this.lans[newLan.id] = newLan;
      this.addNodeToLan(sourceId, newLan.id);
      this.addNodeToLan(targetId, newLan.id);
      var source = this.nodes[sourceId];
      var target = this.nodes[targetId];
      if (! this.validList.isValidLink(source, this.sites[source.group],
				       target, this.sites[target.group]))
      {
	this.errorModal.update({ title: 'Incompatible Link Warning',
				 contents: 'This link is not listed as compatible with the nodes you have connected. Your topology may be impossible to allocate.' });
      }
      this.checkLan(newLan);
      this.trigger('addLans', [newLan]);
    },

    addNodeToLan: function (nodeId, lanId) {
      var node = this.nodes[nodeId];
      var lan = this.lans[lanId];
      if (! _.contains(lan.endpoints, node) &&
	  ! this.wouldThreeSite(node, lan))
      {
	lan.nodeIndices.push(nodeId);
	lan.endpoints.push(node);
	lan.endpointNames.push(node.name);
	var iface = {
	  id: _.uniqueId(),
	  name: this.generateInterfaceName(),
	  nodeName: node.name,
	  node: node,
	  linkId: lanId,
	  nodeId: nodeId,
	  mac: undefined,
	  bandwidth: undefined,
	};
	this.interfaces[iface.id] = iface;
	node.interfaces.push(iface.id);
	lan.interfaces.push(iface.id);
	this.checkLan(lan);
	this.checkNode(node);
	lan.trigger('addEndpoint', node);
	this.trigger('change');
      }
    },

    wouldThreeSite: function (node, lan) {
      var sites = {};
      sites[node.group] = true;
      _.each(lan.endpoints, function (endpoint) {
	sites[endpoint.group] = true;
      });
      var result = false;
      if (_.keys(sites).length > 2)
      {
	result = true;
      }
      return result;
    },

    getItems: function (type) {
      return _.values(this.getType(type));
    },

    getType: function (type) {
      if (type === 'node')
      {
	return this.nodes;
      }
      else if (type === 'link')
      {
	return this.lans;
      }
      else if (type === 'site')
      {
	return this.sites;
      }
      else
      {
	return {};
      }
    },

    hasId: function (id) {
      return (this.nodes[id] || this.lans[id] || this.sites[id]);
    },

    findMixed: function (list) {
      var result = [];
      _.each(list, function (id) {
	if (this.nodes[id])
	{
	  result.push(this.nodes[id]);
	}
	else if (this.lans[id])
	{
	  result.push(this.lans[id]);
	}
	else if (this.sites[id])
	{
	  result.push(this.sites[id]);
	}
      }.bind(this));
      return result;
    },

    uniqueName: function(inPrefix)
    {
      var prefix = inPrefix;
      if (! prefix)
      {
	prefix = '';
      }
      if (this.idList[prefix] === undefined)
      {
	this.idList[prefix] = 0;
      }
      var id = prefix + this.idList[prefix];
      this.idList[prefix] += 1;
      return id;
    },

    generateNodeName: function ()
    {
      var id = this.uniqueName('node-');
      while (_.where(this.nodes, { name: id }).length > 0)
      {
	id = this.uniqueName('node-');
      }
      return id;
    },

    generateLinkName: function ()
    {
      var id = this.uniqueName('link-');
      while (_.where(this.lans, { name: id }).length > 0)
      {
	id = this.uniqueName('link-');
      }
      return id;
    },

    generateInterfaceName: function ()
    {
      var id = this.uniqueName('interface-');
      while (_.where(this.interfaces, { name: id }).length > 0)
      {
	id = this.uniqueName('interface-');
      }
      return id;
    },

    generateSiteName: function (site)
    {
      var result;
      if (site.urn && site.type === 'site')
      {
	if (this.context.canvasOptions.aggregates)
	{
	  var found = _.findWhere(this.context.canvasOptions.aggregates,
				  { id: site.urn });
	  if (found)
	  {
	    result = found.name;
	  }
	}
	if (result === undefined)
	{
	  var split = site.urn.split('+');
	  if (split.length === 4)
	  {
	    result = site.urn.split('+')[1];
	  }
	  else
	  {
	    result = site.urn;
	  }
	}
      }
      if (result === undefined && site.name)
      {
	result = site.name;
      }
      if (result === undefined)
      {
	//result = this.uniqueName('Site '); // Rob likes small consecutive integers
	var attempt = 1; // Start at 1 and not zero. *sob*
	result = 'Site ' + attempt;
	while (_.where(this.sites, { name: result }).length > 0)
	{
	  ++attempt;
	  //result = this.uniqueName('Site ');
	  result = 'Site ' + attempt;
	}
      }
      return result;
    },

    changeAttribute: function (itemsToChange, key, value, type)
    {
      var that = this;
      var items;
      if (type === 'node')
      {
	items = this.nodes;
      }
      else if (type === 'link')
      {
	items = this.lans;
      }
      else
      {
	items = this.sites;
      }
      _.each(itemsToChange, _.bind(function(d) {
	var item = _.where(items, {id: d})[0];
	item[key] = value;
	if (key === 'name' && type === 'node')
	{
	  _.each(item.interfaces, function (iface) {
	    iface.nodeName = item.name;
	  });
	}
      }, this));
      if (key === 'urn' && type === 'site')
      {
	_.each(itemsToChange, _.bind(function(d) {
	  var item = _.where(items, {id: d})[0];
	  item.name = undefined;
	  item.name = this.generateSiteName(item);
	}, this));
	this.trigger('changeAggregate');
      }
      _.each(itemsToChange, _.bind(function(d) {
	var item = _.where(items, {id: d})[0];
	if (type === 'node')
	{
	  this.checkNode(item);
	  _.each(item.interfaces, function (ifaceId) {
	    var iface = this.interfaces[ifaceId];
	    var linkId = iface.linkId;
	    var link = this.lans[linkId];
	    this.checkLan(link);
	  }.bind(this));
	}
	else if (type === 'link')
	{
	  this.checkLan(item);
	  _.each(item.endpoints, function (endpoint) {
	    this.checkNode(endpoint);
	  }.bind(this));
	}
      }, this));
      this.trigger('change');
    },

    removeItems: function (idList, type)
    {
      if (type === 'node')
      {
	this.removeNodes(idList);
      }
      else if (type === 'link')
      {
	this.removeLinks(idList);
      }
      else if (type === 'site')
      {
	this.removeSites(idList);
      }
    },

    removeNodes: function (idList)
    {
      var linkReaper = [];
      _.each(idList, function (id) {
	var node = this.nodes[id];
	while (node.interfaces.length > 0)
	{
	  var iface = this.interfaces[node.interfaces[0]];
	  var link = this.lans[iface.linkId];
	  this.removeNodeFromLan(node, link);
	  if (link.interfaces.length < 2)
	  {
	    linkReaper.push(link.id);
	  }
	}
	delete this.nodes[id];
      }.bind(this));
      this.trigger('removeNodes', idList);
      this.trigger('change');
      this.removeLinks(_.unique(linkReaper));
    },

    removeLinks: function (idList)
    {
      _.each(idList, function (id) {
	var link = this.lans[id];
	while (link.interfaces.length > 0)
	{
	  var iface = this.interfaces[link.interfaces[0]];
	  var node = this.nodes[iface.nodeId];
	  this.removeNodeFromLan(node, link);
	}
	delete this.lans[id];
      }.bind(this));
      this.trigger('removeLans', idList);
      this.trigger('change');
    },

    removeNodeFromLan: function (node, link)
    {
      var iface;
      _.each(node.interfaces, function (id) {
	var candidate = this.interfaces[id];
	if (candidate.linkId === link.id)
	{
	  iface = candidate;
	}
      }.bind(this));
      if (iface)
      {
	this.switchIp(iface.id, iface.ip, null);
	delete this.interfaces[iface.id];
	node.interfaces = _.without(node.interfaces, iface.id);
	link.interfaces = _.without(link.interfaces, iface.id);
	link.endpoints = _.without(link.endpoints, node);
	link.trigger('removeEndpoint', node);
	this.trigger('change');
      }
    },

    removeSites: function (idList)
    {
      var reaperList = [];
      _.each(idList, function (id) {
	var membership = _.where(this.nodes, { group: id});
	if (membership.length === 0)
	{
	  reaperList.push(id);
	  delete this.sites[id];
	}
      }.bind(this));
      if (reaperList.length > 0)
      {
	this.trigger('removeSites', reaperList);
	this.trigger('change');
      }
    },

    // Do bookkeeping when an IP address changes.
    switchIp: function (ifaceId, oldValue, newValue)
    {
      if (oldValue)
      {
	this.ipUsage[oldValue] = _.without(this.ipUsage[oldValue], ifaceId);
	if (this.ipUsage[oldValue].length === 0)
	{
	  delete this.ipUsage[oldValue];
	}
      }
      if (newValue)
      {
	if (! this.ipUsage[newValue])
	{
	  this.ipUsage[newValue] = [];
	}
	this.ipUsage[newValue] = _.union(this.ipUsage[newValue], [ifaceId]);
      }
      var iface = this.interfaces[ifaceId];
      this.checkLan(this.lans[iface.linkId]);
      this.trigger('change');
    },

    checkNode: function (node)
    {
      node.warnings.constraintNode = (! this.validList.isValidNode(node, this.sites[node.group]));
      node.warnings.adjacentLink = (! this.validList.isValidNodeNeighbor(node));
    },

    checkLan: function (lan)
    {
      lan.warnings.openflow = (lan.openflow === '');
      lan.warnings.adjacentNode = (! this.validList.isValidLan(lan));

      var linkTypes = this.validList.findLinks(lan, 'linkTypes');
      var allowed = _.without(linkTypes.allowed, 'gre-tunnel', 'egre-tunnel');
      lan.warnings.defaultLinkType = (allowed.length === 0);

      var foundDuplicate = false;
      _.each(lan.interfaces, function (ifaceId) {
	var iface = this.interfaces[ifaceId];
	if (iface.ip && this.ipUsage[iface.ip] &&
	    this.ipUsage[iface.ip].length > 1)
	{
	  foundDuplicate = true;
	}
      }.bind(this));
      lan.warnings.duplicateAddress = foundDuplicate;
    },

  };

  return TopologyModel;
});
