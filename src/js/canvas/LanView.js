define(['underscore', 'backbone'],
function (_, Backbone)
{
  'use strict';

  var LanView = Backbone.View.extend({

    initialize: function () {
      var that = this;
      this.node = {
	model: this.model,
	view: this,
	fixed: true,
	x: Math.random() * 500,
	y: Math.random() * 500
      };
      this.randOffset = {
	x: Math.floor(Math.random() * 100 - 50),
	y: Math.floor(Math.random() * 100 - 50)
      };
      this.render();

      this.links = [];
      _.each(this.model.endpoints, function (endpoint) {
	that.addEndpoint(endpoint);
      });
      this.options.force.add([this.node], this.links);
      this.listenTo(this.model, 'addEndpoint', this.addEndpoint);
      this.listenTo(this.model, 'removeEndpoint', this.removeEndpoint);
    },

    cleanup: function () {
      this.options.force.remove([this.node], this.links);
      this.lineContainer.remove();
      this.center.remove();
    },

    render: function () {
      var that = this;
      this.lineContainer = this.options.background.linkBase.append('svg:g')
        .attr("class", "link");
      this.lines = [];

      this.center = this.options.background.nodeBase.append('svg:g')
	.attr("class", "boxgroup")
	.on('click', _.bind(this.handleClick, this))
	.on('mouseover', _.bind(this.handleHover, this))
	.on('mouseout', _.bind(this.handleHoverEnd, this))
	.attr("id", this.model.id);

      this.centerRect = this.center.append('svg:rect')
	.attr("class", "linkbox")
	.attr('x', '-10px')
	.attr('y', '-10px')
	.attr('width', '20px')
	.attr('height', '20px');

      this.centerHighlight = this.center.append('svg:rect')
	.attr("class", "checkbox")
	.attr('x', '-10px')
	.attr('y', '-10px')
	.attr('width', '20px')
	.attr('height', '20px')
	.attr("style", "visibility:hidden;");

      this.warning = this.center.append('svg:g')
	.attr('style', 'visibility:hidden');

      this.warning.append('svg:rect')
        .attr('x', '-17px')
	.attr('y', '-17px')
        .attr('class', 'warningbox')
	.attr('width', '14px')
	.attr('height', '14px');

      this.warning.append('svg:text')
        .attr('class', 'warningboxtext')
        .attr('x', '-12px')
	.attr('y', '-5px')
	.text('!');
    },

    update: function (shouldDisplay, highlightList) {
      if (this.links.length > 1)
      {
	var sum = { x: 0, y: 0 };
	_.each(this.links, function (link) {
	  if (link.target.x && link.target.y)
	  {
	    sum.x += link.target.x;
	    sum.y += link.target.y;
	  }
	});
	this.node.fixed = true;
	this.node.fx = sum.x / this.links.length + this.randOffset.x;
	this.node.fy = sum.y / this.links.length + this.randOffset.y;
	this.node.x = this.node.fx;
	this.node.y = this.node.fy;
      }
      else if (this.links.length == 1)
      {
	if (! this.node.x || ! this.node.y)
	{
	  this.node.x = this.links[0].target.x + 50;
	  this.node.y = this.links[0].target.y - 50;
	  delete this.node.fx;
	  delete this.node.fy;
	}
	this.node.fixed = false;
      }
      this.updateForceLinks();

      if (shouldDisplay)
      {
	this.center.attr('transform',
			 'translate(' + this.node.x +
			 ',' + this.node.y + ')');
	for (var i = 0; i < this.links.length; i += 1)
	{
	  if (this.links[i].source.x && this.links[i].source.y &&
	      this.links[i].target.x && this.links[i].target.y)
	  {
	    this.lines[i].attr('x1', this.links[i].source.x)
	      .attr('y1', this.links[i].source.y)
	      .attr('x2', this.links[i].target.x)
	      .attr('y2', this.links[i].target.y);
	  }
	}
	if (highlightList)
	{
	  if (_.contains(highlightList, this.model.id))
	  {
	    this.centerHighlight.style('visibility', 'visible')
	      .classed('selected', true);
	  }
	  else
	  {
	    this.centerHighlight.style('visibility', 'hidden')
	      .classed('selected', false);
	  }
	}

	var shouldWarn = false;
	_.each(_.keys(this.model.warnings), function(key) {
	  if (this.model.warnings[key])
	  {
	    shouldWarn = true;
	  }
	}.bind(this));
	if (this.options.context.mode === 'viewer')
	{
	  shouldWarn = false;
	}
	if (shouldWarn !== this.shouldWarn)
	{
	  this.shouldWarn = shouldWarn;
	  if (shouldWarn)
	  {
	    this.warning.style('visibility', 'visible');
	  }
	  else
	  {
	    this.warning.style('visibility', 'hidden');
	  }
	}

      }
    },

    updateForceLinks: function () {
      var distance = 300;
      var strength = 0.5;
      if (this.withinSite())
      {
	distance = 100;
	strength = 1.5;
      }
      _.each(this.links, function (link) {
	link.distance = distance;
	link.strength = strength;
      });
    },

    withinSite: function ()
    {
      var result = true;
      if (this.model.endpoints.length > 0)
      {
	var first = this.model.endpoints[0].group;
	_.each(this.model.endpoints, function (endpoint) {
	  if (first !== endpoint.group)
	  {
	    result = false;
	  }
	});
      }
      return result;
    },

    addEndpoint: function (endpoint) {
      var link = {};
      link.source = this.node;
      link.target = this.options.force.findEndpoint(endpoint);
      this.links.push(link);
      var line = this.lineContainer.append('svg:line')
	.attr('class', 'linkline')
	.attr("id", 'link-' + this.model.id + '-node-' + endpoint.id);
      this.lines.push(line);
      this.updateForceLinks();
      this.options.force.add([], [link]);
    },

    removeEndpoint: function (endpoint) {
      var link;
      var linkIndex;
      var target = this.options.force.findEndpoint(endpoint);
      _.each(this.links, function (candidate, index) {
	if (candidate.source === this.node && candidate.target === target)
	{
	  link = candidate;
	  linkIndex = index;
	}
      }.bind(this));
      if (link)
      {
	this.lines[linkIndex].remove();
	this.links.splice(linkIndex, 1);
	this.lines.splice(linkIndex, 1);
	this.options.force.remove([], [link]);
      }
    },

    handleClick: function () {
      if (! d3v4.event.defaultPrevented)
      {
	var data = {
	  type: 'link',
	  item: this.model,
	  modkey: (d3v4.event.ctrlKey || d3v4.event.shiftKey),
	  event: _.clone(d3v4.event)
	};
	this.options.clickUpdate.trigger('click-event', data);
      }
    },

    handleHover: function () {
      this.options.setHoverId(this.model.id);
    },

    handleHoverEnd: function () {
      this.options.setHoverId(null);
    }

  });

  return LanView;
});
