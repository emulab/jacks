define(['underscore', 'backbone'],
function (_, Backbone)
{
  'use strict';

  var SiteView = Backbone.View.extend({

    initialize: function () {
      this.justClickedOutside = false;
      this.vertices = [];
      this.links = [];
      this.label = {
	model: this.model,
	view: this
      };
      if (this.model.x !== undefined &&
	  this.model.y !== undefined)
      {
	this.label.x = this.model.x;
	this.label.y = this.model.y;
      }
      this.options.force.add([this.label], []);
      this.render();
    },

    cleanup: function () {
      this.options.force.remove([this.label], this.links);
      if (this.hull)
      {
	this.hull.remove();
      }
      if (this.hullLabel)
      {
	this.hullLabel.remove();
      }
    },

    render: function () {
      var drag = d3v4.drag()
	.on('start', _.bind(this.handleDragStart, this))
	.on('drag', _.bind(this.handleDragMove, this))
	.on('end', _.bind(this.handleDragEnd, this));

      var background = this.options.background;
      this.hull = background.hullBase.append('svg:path')
        .style("fill", this.options.fill)
        .style("stroke", this.options.fill)
        .style("stroke-width", 150)
        .style("stroke-linejoin", "round")
        .style("opacity", .6)
	.on("click", _.bind(this.handleClickOutside, this))
	.on("mousedown", _.bind(this.handleMouseDown, this));

      if (this.options.context.multiSite)
      {
	this.hullLabel = background.hullLabelBase.append('svg:g')
	  .attr('class', 'sitelabelgroup')
          .attr('style', 'cursor:pointer')
	  .call(drag)
	  .on('click', _.bind(this.handleClick, this))
	  .attr("id", this.model.id);

	this.labelRect = this.hullLabel.append('svg:rect')
          .attr('class', 'labelbox')
          .attr('x', '-40px')
          .attr('y', '-14px')
          .attr('width', '80px')
          .attr('height', '20px');

	this.labelSelect = this.hullLabel.append('svg:rect')
          .attr('class', 'checkbox')
          .attr('x', '-40px')
          .attr('y', '-14px')
          .attr('width', '80px')
          .attr('height', '20px')
          .style('visibility', 'hidden');

	this.hullText = this.hullLabel.append('svg:text')
          .attr('x', '0px')
          .attr("class", "sitetext");
      }
    },

    update: function (shouldDisplay, highlightList, dragPoint) {
      this.findVertices();
      this.connectVertices();
      if (shouldDisplay)
      {
	if (this.options.context.multiSite && this.options.siteCount() > 1)
	{
	  this.hull.style("fill", this.options.fill)
            .style("stroke", this.options.fill);
//	  this.hullLabel.attr('class', 'sitelabelgroup');
	}
	else
	{
	  this.hull.style("fill", "#fff")
            .style("stroke", "#fff");
//	  if (this.hullLabel)
//	  {
//	    this.hullLabel.attr('class', 'hidden sitelabelgroup');
//	  }
	}
	this.hull.attr("d", this.groupPath(dragPoint));
	if (this.options.context.multiSite &&
	    (this.options.siteCount() > 1 ||
	     this.options.context.mode === 'editor'))
	{
	    if (this.links.length > 1)
	    {
		this.hullLabel.attr('transform',
				    'translate(' + this.label.x + ',' +
				    this.label.y + ')');
	    }
	    else
	    {
		var that = this;
		var x = this.label.x;
		var y = this.label.y;
		_.each(this.options.force.nodes, function (node) {
		    if (node.model && node.model.group === that.model.id &&
			node.x && node.y)
		    {
			x = node.x;
			y = node.y - 50;
		    }
		});
		this.hullLabel.attr('transform',
				    'translate(' + x + ',' +
				    y + ')');
	    }
	  var name = this.model.name;
	  var re = new RegExp('^[0-9]+$');
	  if (re.test(name))
	  {
	    name = 'Site ' + name;
	  }
	  this.hullText.text(name);
	  var textWidth = 50;
	  try
	  {
	    this.hullText.each(function (d, i) {
	      textWidth = this.getBBox().width;
	    });
	  }
	  catch (e) {}
	  var textHeight = 20;
	  if (this.options.siteCount() === 1)
	  {
	    textWidth += 20;
	    textHeight += 20;
	  }
	  this.labelRect.attr('width', (textWidth + 10) + 'px')
	    .attr('height', textHeight + 'px');
	  this.labelRect.attr('x', (-textWidth/2 - 5) + 'px')
	    .attr('y', (-textHeight/2 - 5) + 'px');
	  this.labelSelect.attr('width', (textWidth + 10) + 'px')
	    .attr('height', textHeight + 'px');
	  this.labelSelect.attr('x', (-textWidth/2 - 5) + 'px')
	    .attr('y', (-textHeight/2 - 5) + 'px');
	  if (highlightList)
	  {
	    if (_.contains(highlightList, this.model.id))
	    {
	      this.hullLabel.selectAll('.checkbox')
		.style('visibility', 'visible')
		.classed('selected', true);
	    }
	    else
	    {
	      this.hullLabel.selectAll('.checkbox')
		.style('visibility', 'hidden')
		.classed('selected', false);
	    }
	  }
	  this.hullLabel.style('visibility', 'visible');
	}
	else
	{
	  if (this.hullLabel)
	  {
	    this.hullLabel.style('visibility', 'hidden');
	  }
	}
      }
    },

    contains: function (x, y, oldGroup) {
      return (this.model.id !== oldGroup &&
	      intersects([x, y, 150], this.vertices));
    },

    distance: function (x, y) {
      return minDistance([x, y], this.vertices);
    },

    connectVertices: function () {
      var freshLinks = []; // Links that need to be added
      var staleLinks = []; // Links that need to be removed
      _.each(this.options.force.nodes, function (node) {
	var link = _.findWhere(this.links, { target: node });
	if (node.model && node.model.group === this.model.id)
	{
	  // Ensure that it is connected to the label node
	  if (! link)
	  {
	    freshLinks.push({
	      source: this.label,
	      target: node,
	      distance: 100,
	      strength: 0.05
	    });
	  }
	}
	else
	{
	  // Ensure that it is not connected to the label node
	  if (link)
	  {
	    staleLinks.push(link);
	  }
	}
      }.bind(this));
      if (freshLinks.length > 0)
      {
	this.links = this.links.concat(freshLinks);
	this.options.force.add([], freshLinks);
      }
      if (staleLinks.length > 0)
      {
	this.links = _.difference(this.links, staleLinks);
	this.options.force.remove([], staleLinks);
      }
    },

    findVertices: function () {
      var that = this;
      this.vertices = [];

      _.each(this.options.force.nodes, function (node) {
	if (node.model && node.model.group === that.model.id &&
	    node.x && node.y)
	{
	  that.vertices.push([node.x, node.y]);
	}
      });

      if (this.label.x === undefined && this.label.y === undefined)
      {
	if (this.vertices.length > 0)
	{
	  this.label.x = (_.max(this.vertices, function (a) { return a[0]; })[0] + _.min(this.vertices, function (a) { return a[0]; })[0])/2;
	  this.label.y = (_.max(this.vertices, function (a) { return a[1]; })[1] + _.min(this.vertices, function (a) { return a[1]; })[1])/2;
	  this.label.vx = 0;//this.label.x;
	  this.label.vy = 0;//this.label.y;
	}
	else
	{
	  this.label.x = 0;
	  this.label.y = 0;
	  this.label.vx = 0;
	  this.label.vy = 0;
	}
      }
/*
      if (this.vertices.length > 1)
      {
	this.label.x = (_.max(this.vertices, function (a) { return a[0]; })[0] + _.min(this.vertices, function (a) { return a[0]; })[0])/2;
	this.label.y = _.min(this.vertices, function (a) { return a[1]; })[1] - 40;
      }
      else if (this.vertices.length === 1)
      {
	var distance = Math.sqrt(Math.pow(this.vertices[0][0] - this.label.x, 2) +
				 Math.pow(this.vertices[0][1] - this.label.y, 2));
	if (distance > 200)
	{
	  var angle = Math.atan2(this.vertices[0][1] - this.label.y,
				 this.vertices[0][0] - this.label.x);
	  this.label.x = this.vertices[0][0] - Math.cos(angle)*200;
	  this.label.y = this.vertices[0][1] - Math.sin(angle)*200;
	}
      }
*/

      if (this.vertices.length !== 1)
      {
        this.vertices.push([this.label.x, this.label.y]);
      }
      if (this.vertices.length === 1)
      {
        this.vertices.push([this.vertices[0][0] - 25, this.vertices[0][1] - 25]);
        this.vertices.push([this.vertices[0][0] + 25, this.vertices[0][1] - 25]);
        this.vertices.push([this.vertices[0][0] + 25, this.vertices[0][1] + 25]);
        this.vertices.push([this.vertices[0][0] - 25, this.vertices[0][1] + 25]);
      }
    },

    groupPath: function(dragPoint) {
      var vertices = this.vertices;
      if (dragPoint)
      {
	vertices = _.clone(vertices);
	vertices.push(dragPoint);
      }
      return 'M' + d3v4.polygonHull(vertices).join('L') + 'Z';
    },

    handleClick: function () {
      if (! d3v4.event.defaultPrevented)
      {
	var data = {
	  type: 'site',
	  item: this.model,
	  modkey: (d3v4.event.ctrlKey || d3v4.event.shiftKey),
	  event: _.clone(d3v4.event)
	};
	this.options.clickUpdate.trigger('click-event', data);
      }
    },

    handleDragStart: function () {
      this.dragMoveCount = 0;
      this.options.force.stop();
    },

    handleDragMove: function () {
      this.dragMoveCount += 1;
      var that = this;
      var found = 0;
      _.each(this.options.force.nodes, function (node) {
	if (node.model && node.model.group === that.model.id)
	{
	  node.vx += 0;
	  node.vy += 0;
	  node.x += d3v4.event.dx;
	  node.y += d3v4.event.dy;
	  found += 1;
	}
      });
      this.label.x += d3v4.event.dx;
      this.label.y += d3v4.event.dy;
      this.label.vx = 0;//this.label.x;
      this.label.vy = 0;//this.label.y;
      this.trigger('update');
    },

    handleDragEnd: function () {
      if (this.dragMoveCount && this.dragMoveCount > 1)
      {
        this.trigger('update-move');
        _.defer(function () {
	  $('html').css('-webkit-user-select', 'initial')
	  $('html').css('-moz-user-select', 'initial')
        });
      }
      else
      {
	this.handleClick();
      }
    },

    handleMouseDown: function ()
    {
      var that = this;
      this.justClickedOutside = true;
      _.delay(function () { that.justClickedOutside = false; }, 200);
    },

    handleClickOutside: function () {
      if (this.justClickedOutside)
      {
	this.options.clickUpdate.trigger('click-outside');
      }
    },

  });

  function minDistance(point, polygon)
  {
    var result = null;
    _.each(polygonEdges(polygon), function (edge) {
      var distance = pointLineSegmentDistance(point, edge);
      if (result === null ||
	  distance < result)
      {
	result = distance;
      }
    });
    return result;
  }

  function intersects(circle, polygon)
  {
    return pointInPolygon(circle, polygon)
        || polygonEdges(polygon).some(function(line) { return pointLineSegmentDistance(circle, line) < circle[2]; });
  }

  function polygonEdges(polygon)
  {
    return polygon.map(function(p, i) {
      return i ? [polygon[i - 1], p] : [polygon[polygon.length - 1], p];
    });
  }

  function pointInPolygon(point, polygon)
  {
    for (var n = polygon.length, i = 0, j = n - 1, x = point[0], y = point[1], inside = false; i < n; j = i++)
    {
      var xi = polygon[i][0], yi = polygon[i][1],
          xj = polygon[j][0], yj = polygon[j][1];
      if ((yi > y ^ yj > y) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi)) inside = !inside;
    }
    return inside;
  }

  function pointLineSegmentDistance(point, line)
  {
    var v = line[0], w = line[1], d, t;
    return Math.sqrt(pointPointSquaredDistance(point, (d = pointPointSquaredDistance(v, w))
        ? ((t = ((point[0] - v[0]) * (w[0] - v[0]) + (point[1] - v[1]) * (w[1] - v[1])) / d) < 0 ? v
        : t > 1 ? w
        : [v[0] + t * (w[0] - v[0]), v[1] + t * (w[1] - v[1])])
        : v));
  }

  function pointPointSquaredDistance(v, w)
  {
    var dx = v[0] - w[0], dy = v[1] - w[1];
    return dx * dx + dy * dy;
  }

  return SiteView;
});
