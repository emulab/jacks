define(['underscore', 'backbone'],
function (_, Backbone)
{
  'use strict';

  var HostView = Backbone.View.extend({

    initialize: function () {
      this.justClickedOutside = false;
      this.links = [];
      this.label = {
	model: this.model,
	view: this
      };
      this.center = {
	model: this.model,
	view: this
      };
      if (this.model.x !== undefined &&
	  this.model.y !== undefined)
      {
	this.label.x = this.model.x;
	this.label.y = this.model.y;
	this.center.x = this.model.x;
	this.center.y = this.model.y;
      }
      this.options.force.add([this.center], []);
      this.render();
    },

    cleanup: function () {
      this.options.force.remove([this.label], this.links);
      if (this.hull)
      {
	this.hull.remove();
      }
      if (this.hullLabel)
      {
	this.hullLabel.remove();
      }
    },

    render: function () {
/*
      var drag = d3v4.drag()
	.on('start', _.bind(this.handleDragStart, this))
	.on('drag', _.bind(this.handleDragMove, this))
	.on('end', _.bind(this.handleDragEnd, this));
*/
      var background = this.options.background;
      this.hull = background.hostBase.append('svg:path')
//        .style("fill", this.options.fill)
        .style("fill-opacity", 0)
        .style("stroke", this.options.fill)
        .style("stroke-width", 10)
        .style("stroke-linejoin", "round")
        .style("opacity", 1)
	.on("click", _.bind(this.handleClickOutside, this))
	.on("mousedown", _.bind(this.handleMouseDown, this));

      this.hullLabel = background.hostLabelBase.append('svg:g')
	.attr('class', 'hostlabelgroup')
        .attr('style', 'cursor:pointer')
//	  .call(drag)
	.on('click', _.bind(this.handleClick, this))
	.attr("id", this.model.id)
	.on('mouseover', _.bind(this.hoverStart, this))
	.on('mouseout', _.bind(this.hoverEnd, this));

      this.labelRect = this.hullLabel.append('svg:rect')
//          .attr('class', 'labelbox')
	.style('fill', "rgb(255,255,255)")
	.style('fill-opacity', 1)
        .attr('x', '0px')
        .attr('y', '-14px')
        .attr('width', '80px')
        .attr('height', '20px');

      this.labelSelect = this.hullLabel.append('svg:rect')
        .attr('class', 'checkbox')
        .attr('x', '-40px')
        .attr('y', '-14px')
        .attr('width', '80px')
        .attr('height', '20px')
        .style('visibility', 'hidden');

      this.hullText = this.hullLabel.append('svg:text')
        .attr('x', '0px')
	//.style('stroke', this.options.fill)
	//.style('fill', this.options.fill)
	.style('text-anchor', 'left')
        .attr("class", "hosttext");
    },

    update: function (shouldDisplay, highlightList, dragPoint) {
      if (shouldDisplay)
      {
	var groupPath = this.findVertices();
	this.connectVertices();
/*
	if (this.options.context.multiSite && this.options.siteCount() > 1)
	{
	  this.hull.style("fill", this.options.fill)
            .style("stroke", this.options.fill);
//	  this.hullLabel.attr('class', 'sitelabelgroup');
	}
	else
	{
	  this.hull.style("fill", "#fff")
            .style("stroke", "#fff");
//	  if (this.hullLabel)
//	  {
//	    this.hullLabel.attr('class', 'hidden sitelabelgroup');
//	  }
	}
*/
	if (groupPath !== "")
	{
	  this.hull.attr("d", groupPath);
	  this.hullLabel.attr('transform',
			      'translate(' + this.label.x + ',' +
			      this.label.y + ')');
	}
	var name = this.model.name;
	var re = new RegExp('^[0-9]+$');
	if (re.test(name))
	{
	  name = 'Host ' + name;
	}
	this.hullText.text(name);
	var textWidth = 50;
	try
	{
	  this.hullText.each(function (d, i) {
	    textWidth = this.getBBox().width;
	  });
	}
	catch (e) {}
	if (this.model.attachedNode !== undefined)
	{
	  _.each(this.options.force.nodes, function (node) {
	    if (node.model && node.model.name === this.model.attachedNode)
	    {
	      node.fx = this.label.x + textWidth/2;
	      node.fy = this.label.y + 30;
	      node.x = node.fx;
	      node.y = node.fy;
	    }
	  }.bind(this));
	}
	var textHeight = 20;
	this.labelRect.attr('width', (textWidth + 20) + 'px')
	  .attr('height', textHeight + 'px');

	this.labelRect.attr('x', '-10px')
	  .attr('y', (-textHeight/2 - 5) + 'px');
	this.labelSelect.attr('width', (textWidth + 10) + 'px')
	  .attr('height', textHeight + 'px');
	this.labelSelect.attr('x', (-textWidth/2 - 5) + 'px')
	  .attr('y', (-textHeight/2 - 5) + 'px');
	if (highlightList)
	{
	  if (_.contains(highlightList, this.model.id))
	  {
	    this.hullLabel.selectAll('.checkbox')
	      .style('visibility', 'visible')
	      .classed('selected', true);
	  }
	  else
	  {
	    this.hullLabel.selectAll('.checkbox')
	      .style('visibility', 'hidden')
	      .classed('selected', false);
	  }
	}
	this.hullLabel.style('visibility', 'visible');
      }
/*
      else
      {
	if (this.hullLabel)
	{
	  this.hullLabel.style('visibility', 'hidden');
	}
      }
*/
    },
/*
    contains: function (x, y, oldGroup) {
      return (this.model.id !== oldGroup &&
	      intersects([x, y, 150], this.vertices));
    },

    distance: function (x, y) {
      return minDistance([x, y], this.vertices);
    },
*/
    connectVertices: function () {
      var freshLinks = []; // Links that need to be added
      var staleLinks = []; // Links that need to be removed
      var nodeCount = 0;
      _.each(this.options.force.nodes, function (node) {
	if (node.model && node.model.host === this.model.id)
	{
	  nodeCount += 1;
	}
      }.bind(this));
      _.each(this.options.force.nodes, function (node) {
	var link = _.findWhere(this.links, { target: node });
	if (node.model && node.model.host === this.model.id &&
	    nodeCount > 1)
	{
	  // Ensure that it is connected to the center node
	  if (! link)
	  {
	    freshLinks.push({
	      source: this.center,
	      target: node,
	      distance: 50,
	      strength: 0.1
	    });
	  }
	}
	else
	{
	  // Ensure that it is not connected to the center node
	  if (link)
	  {
	    staleLinks.push(link);
	  }
	}
      }.bind(this));
      if (freshLinks.length > 0)
      {
	this.links = this.links.concat(freshLinks);
	this.options.force.add([], freshLinks);
      }
      if (staleLinks.length > 0)
      {
	this.links = _.difference(this.links, staleLinks);
	this.options.force.remove([], staleLinks);
      }
    },

    findVertices: function () {
      var xList = [];
      var yList = [];

      _.each(this.options.force.nodes, function (node) {
	if (node.model && node.model.host === this.model.id &&
	    node.x && node.y)
	{
	  xList.push(node.x);
	  yList.push(node.y);
	}
      }.bind(this));

      var result = "";
      if (xList.length > 0)
      {
	var minX = _.min(xList);
	var maxX = _.max(xList);
	var minY = _.min(yList);
	var maxY = _.max(yList);
	if (xList.length > 1 && yList.length > 1)
	{
//	  this.center.fx = (minX + maxX)/2;
//	  this.center.fy = (minY + maxY)/2;
//	  this.center.x = (minX + maxX)/2;
//	  this.center.y = (minY + maxY)/2;
	}

	if (this.lastMinX && Math.abs(minX - this.lastMinX) < 30)
	{
	  minX = this.lastMinX;
	}
	if (this.lastMaxX && Math.abs(maxX - this.lastMaxX) < 30)
	{
	  maxX = this.lastMaxX;
	}
	if (this.lastMinY && Math.abs(minY - this.lastMinY) < 30)
	{
	  minY = this.lastMinY;
	}
	if (this.lastMaxY && Math.abs(maxY - this.lastMaxY) < 30)
	{
	  maxY = this.lastMaxY;
	}
	this.lastMinX = minX;
	this.lastMaxX = maxX;
	this.lastMinY = minY;
	this.lastMaxY = maxY;
	
	this.label.x = minX - 40;
	this.label.y = minY - 55;
	var offset = 60;
	result = "M" + (minX-offset) + "," + (minY-offset) + "L" +
	  (minX-offset) + "," + (maxY+offset) + "L" +
	  (maxX+offset) + "," + (maxY+offset) + "L" +
	  (maxX+offset) + "," + (minY-offset) + "L" +
	  (minX-offset) + "," + (minY-offset) + "Z";
      }
      return result;
    },
/*
    groupPath: function(dragPoint) {
      var vertices = this.vertices;
      if (dragPoint)
      {
	vertices = _.clone(vertices);
	vertices.push(dragPoint);
      }
      return 'M' + d3v4.polygonHull(vertices).join('L') + 'Z';
    },
*/
    handleClick: function () {
      if (! d3v4.event.defaultPrevented)
      {
	var data = {
	  type: 'host',
	  item: this.model,
	  modkey: (d3v4.event.ctrlKey || d3v4.event.shiftKey),
	  event: _.clone(d3v4.event)
	};
	this.options.clickUpdate.trigger('click-event', data);
      }
    },

    hoverStart: function () {
      this.options.setHoverId(this.model.id);
    },

    hoverEnd: function () {
      this.options.setHoverId(null);
    },

    handleMouseDown: function ()
    {
      var that = this;
      this.justClickedOutside = true;
      _.delay(function () { that.justClickedOutside = false; }, 200);
    },

    handleClickOutside: function () {
      if (this.justClickedOutside)
      {
	this.options.clickUpdate.trigger('click-outside');
      }
    },

  });

  return HostView;
});
