define(['underscore', 'backbone'],
function (_, Backbone)
{
  'use strict';

  var template = '<div class="panel-body"><h5>Interface to <strong id="nodeName"></strong></h5><label>Name:</label><input type="text" class="form-control" id="name" value="" placeholder="ex: interface-3"><label>Bandwidth (in kbps):</label><input type="text" class="form-control" id="bandwidth" value="" placeholder="ex: 100000"><label>IP:</label><input type="text" class="form-control" id="ip" value="" placeholder="ex: 192.168.6.7"><label>Netmask:</label><input type="text" class="form-control" id="netmask" value="" placeholder="ex: 255.255.255.0"><div id="mac-container"><strong>MAC:</strong> <span id="mac"></span></div><button id="remove" class="btn btn-danger">Remove</button></div>';

  var InterfaceDisplay = Backbone.View.extend({

    events: {
      'click #remove': 'removeItem'
    },

    initialize: function () {
      this.render();
      this.$el.on('change keyup paste', 'input',
		  _.bind(this.changeItem, this));
    },

    cleanup: function () {
      this.$el.off('change keyup paste', 'input');
      this.$el.remove();
    },

    render: function () {
      this.$el.html(template);
    },

    update: function (iface) {
      this.$('#nodeName').html(_.escape(iface.node.name));
      if (this.$('#name').val() !== iface.name)
      {
	this.$('#name').val(iface.name);
      }
      if (this.$('#bandwidth').val() !== iface.bandwidth)
      {
	this.$('#bandwidth').val(iface.bandwidth);
      }
      if (this.$('#ip').val() !== iface.ip)
      {
	this.$('#ip').val(iface.ip);
      }
      if (this.$('#netmask').val() !== iface.netmask)
      {
	this.$('#netmask').val(iface.netmask);
      }
      if (iface.mac)
      {
	this.$('#mac-container').show();
	this.$('#mac').html(_.escape(iface.mac));
      }
      else
      {
	this.$('#mac-container').hide();
      }
    },

    removeItem: function () {
      this.trigger('remove', { item: this });
    },

    changeItem: function () {
      _.defer(function () {
	this.trigger('change', { item: this,
				 name: this.$('#name').val(),
				 bandwidth: this.$('#bandwidth').val(),
				 ip: this.$('#ip').val(),
				 netmask: this.$('#netmask').val() });
      }.bind(this));
    }

  });

  return InterfaceDisplay;
});
