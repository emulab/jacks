define(['underscore'],
function (_)
{
  'use strict';

  function ValidList(model, constraints)
  {
    this.model = model;
    this.constraints = constraints;
  }

  ValidList.prototype = {

    findNodes: function (node, key)
    {
      var result = {
	allowed: [],
	rejected: []
      };
      var bound = makeAllNodeCandidates(node, this.model);
      result.allowed = this.constraints.getValidList(bound, 'node', key,
						     result.rejected);
      return result;
    },

    findLinks: function (link, key)
    {
      var result = {
	allowed: [],
	rejected: []
      };
      var bound = makeAllLinkCandidates(link, this.model.sites);
      result.allowed = this.constraints.getValidList(bound, 'link', key,
						     result.rejected);
      return result;
    },

    findSites: function (site, key)
    {
      var result = {
	allowed: [],
	rejected: []
      };
      var bound = makeAllSiteCandidates(site, this.model.nodes, this.model.interfaces, this.model.lans, this.model.sites, false);
      result.allowed = this.constraints.getValidList(bound, 'node', key,
						     result.rejected);
      return result;
    },

    isValidNode: function (node, site)
    {
      var candidate = { node: makeSimpleNode(node, site) };
      return this.constraints.isValid(candidate);
    },

    isValidNodeNeighbor: function (node)
    {
      var candidates = makeNodeNeighborCandidates(node, this.model);
      return this.constraints.allValid(candidates);
    },

    isValidLan: function (lan)
    {
      var candidates = makeAllLinkCandidates(lan, this.model.sites);
      return this.constraints.allValid(candidates);
    },

    isValidLink: function (node1, site1, node2, site2)
    {
      var candidate = {
	node: makeSimpleNode(node1, site1),
	link: {},
	node2: makeSimpleNode(node2, site2)
      };
      return this.constraints.isValid(candidate);
    },

    getNodeCandidates: function (skipLinks)
    {
      var result = [];
      _.each(_.values(this.model.sites), function (site) {
	result = result.concat(makeAllSiteCandidates(site, this.model.nodes, this.model.interfaces, this.model.lans, this.model.sites, skipLinks));
      }.bind(this));
      return result;
    },

    getNodeCandidatesBySite: function (skipLinks)
    {
      var result = {};
      _.each(_.values(this.model.sites), function (site) {
	result[site.name] = makeAllSiteCandidates(site, this.model.nodes, this.model.interfaces, this.model.lans, this.model.sites, skipLinks);
      }.bind(this));
      return result;
    },
  };

  function makeNodeNeighborCandidates(node, topo)
  {
    var result = [];
    var nodeSite = topo.sites[node.group];
    _.each(node.interfaces, function (ifaceUnique) {
      var iface = topo.interfaces[ifaceUnique];
      var link = _.where(topo.lans,
			 { id: iface.linkId })[0]; 
      _.each(link.endpoints, function (endpoint) {
	if (endpoint.id !== node.id)
	{
	  var endpointSite = topo.sites[endpoint.group];
	  result.push({ node: makeSimpleNode(node, nodeSite),
			link: makeSimpleLink(link, topo.sites),
			node2: makeSimpleNode(endpoint, endpointSite) });
	}
      });
    });
    return result;
  }

  function makeAllNodeCandidates(node, topo)
  {
    var result = makeNodeNeighborCandidates(node, topo);
    var nodeSite = topo.sites[node.group];
    result.push({ node: makeSimpleNode(node, nodeSite) });
    return result;
  }

  function makeAllLinkCandidates(link, sites, defaultSite)
  {
    var result = [];
    for (var i = 0; i < link.endpoints.length; i += 1)
    {
      var site1 = sites[link.endpoints[i].group];
      for (var j = i + 1; j < link.endpoints.length; j += 1)
      {
	var site2 = sites[link.endpoints[j].group];
	if (defaultSite === site2)
	{
	  var temp = site1;
	  site1 = site2;
	  site2 = temp;
	}
	result.push({ node: makeSimpleNode(link.endpoints[i], site1),
		      link: makeSimpleLink(link),
		      node2: makeSimpleNode(link.endpoints[j], site2) });
      }
    }
    return result;
  }

  function makeAllSiteCandidates(site, nodes, ifaces, links, sites, skipLinks)
  {
    var used = {};
    var result = [];
    var linkMap = {};
    _.each(nodes, function (node) {
      if (node.group === site.id)
      {
	var newNode = { node: makeSimpleNode(node, site) };
	var newName = JSON.stringify(newNode, undefined, 0);
	if (used[newName] === undefined)
	{
	  used[newName] = 1;
	  result.push(newNode);
	  _.each(node.interfaces, function (ifaceId) {
	    var linkId = ifaces[ifaceId].linkId;
	    linkMap[linkId] = links[linkId];
	  });
	}
      }
    });
    if (! skipLinks)
    {
      _.each(_.values(linkMap), function(link) {
	var links = makeAllLinkCandidates(link, sites, site);
	result = result.concat(links);
      });
    }
    return result;
  }

  function makeSimpleNode(node, site)
  {
    var result =  {
      'images': node.image,
      'types': node.type,
      'hardware': node.hardware
    };
    if (node.custom.image)
    {
      result.images = undefined;
    }
    if (node.custom.type)
    {
      result.types = undefined;
    }
    if (node.custom.hardware)
    {
      result.hardware = undefined;
    }
    if (site.urn && ! site.custom.urn)
    {
      result.aggregates = site.urn;
    }
    return result;
  }

  function makeSimpleLink(link)
  {
    var result = {
      'linkTypes': link.linkType,
      'sharedvlan': link.sharedvlan
    };
    if (link.custom.linkType)
    {
      result.linkTypes = undefined;
    }
    if (link.custom.shardvlan)
    {
      result.sharedvlan = undefined;
    }
    return result;
  }

  function getIncidentLinkTypes(node, topoData)
  {
    var types = {};
    _.each(node.interfaces, function (ifaceUnique) {
      var iface = topoData.interfaces[ifaceUnique];
      var link = _.where(topoData.links, { id: iface.linkId })[0];
      if (link.linkType)
      {
	types[link.linkType] = 1;
      }
    });
    return _.keys(types);
  }

  return ValidList;
});

