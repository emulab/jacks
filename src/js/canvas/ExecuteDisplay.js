define(['underscore', 'backbone'],
function (_, Backbone)
{
  'use strict';

  var template = '<div class="panel-body"><label>Command:</label><input type="text" class="form-control" id="command" value="" placeholder="ex: sh /local/myscript.sh"><input type="text" class="form-control" id="shell" disabled="true" value="/bin/sh"><button id="remove" class="btn btn-danger">Remove</button></div>';

  var ExecuteDisplay = Backbone.View.extend({

    events: {
      'click #remove': 'removeItem'
    },

    initialize: function () {
      this.render();
      this.$el.on('change keyup paste', 'input',
		  _.bind(this.changeItem, this));
    },

    cleanup: function () {
      this.$el.off('change keyup paste', 'input');
      this.$el.remove();
    },

    render: function () {
      this.$el.html(template);
    },

    update: function (execute) {
      if (this.$('#command').val() !== execute.command)
      {
	this.$('#command').val(execute.command);
      }
      if (this.$('#shell').val() !== execute.shell)
      {
	this.$('#shell').val(execute.shell);
      }
    },

    removeItem: function () {
      this.trigger('remove', { item: this });
    },

    changeItem: function () {
      _.defer(function () {
	this.trigger('change', { item: this,
				 command: this.$('#command').val(),
				 shell: this.$('#shell').val() });
      }.bind(this));
    }

  });

  return ExecuteDisplay;
});
