define(['underscore', 'backbone', 'js/canvas/NodeOption'],
function (_, Backbone, NodeOption)
{
  'use strict';

  function TopologyBackground(domRoot, forceNodes, sites, defaults,
			      clickUpdate, context, force, validList)
  {
    this.root = d3v4.select(domRoot[0]);
    this.forceNodes = forceNodes;
    this.sites = sites;
    this.defaults = defaults;
    this.clickUpdate = clickUpdate;
    this.context = context;
    this.force = force;
    this.validList = validList;
    this.zoomScale = 1.0;
    this.zoomX = 0;
    this.zoomY = 0;
    this.width = 1000;
    this.height = 1000;
    this.lastCenter = null;
    this.justClickedOutside = false;
    this.paletteOffset = 300;
    if (context.mode !== 'editor' && ! context.show.selectInfo)
    {
      this.paletteOffset = 0;
    }

    _.extend(this, Backbone.Events);
  }

  TopologyBackground.prototype = {

    render: function ()
    {
      this.outer = this.root.append("svg:svg")
	.attr("class", "topomap")
//	.style("visibility", "hidden")
	.attr("width", this.width)
	.attr("height", this.height)
	.attr("pointer-events", "all");

      this.viewport = this.outer.append('svg:g')
	.attr('width', this.width - this.paletteOffset)
	.attr('height', this.height)
	.attr('transform', 'translate(' + this.paletteOffset + ', 0)');

      this.rect = this.viewport.append("svg:rect")
	.attr("class", "rect")
	.attr("width", this.width - this.paletteOffset)
	.attr("height", this.height)
	.style("fill", "#fff")
	.on("click", _.bind(this.handleClickOutside, this))
	.on("mousedown", _.bind(this.handleMouseDown, this))
	.on("mouseup", _.bind(this.handleMouseUp, this));

      this.inner = this.viewport
	.append('svg:g')
	.attr("class", "vis");

      this.hullBase = this.inner.append('svg:g');
      this.hostBase = this.inner.append('svg:g');
      this.linkBase = this.inner.append('svg:g');

      this.dragLine = this.inner.append('svg:line')
	.attr('class', 'linkline dragline hidden')
	.attr('x1', '0')
	.attr('y1', '0')
	.attr('x2', '0')
	.attr('y2', '0');

      this.nodeBase = this.inner.append('svg:g');
      this.hullLabelBase = this.inner.append('svg:g');
      this.hostLabelBase = this.inner.append('svg:g');

      this.palette = this.outer.append('svg:g')
	.attr('width', this.paletteOffset)
	.attr('height', this.height);
      this.paletteBackground = this.palette.append('svg:rect')
	.attr('fill', '#ddd')
	.attr('width', this.paletteOffset)
	.attr('height', this.height);

      if (this.context.mode === 'editor')
      {
	this.palette.append('svg:text')
          .attr('x', '150px')
	  .attr('y', '30px')
          .attr('class', 'dragtext')
	  .text('Drag to Add');

	this.nodeMarkers = [];
	var i = 0;
	_.each(this.defaults, function (options) {
	  var marker = new NodeOption({
	    root: this.outer,
	    options: options,
	    pos: {
	      x: 75 + (i%3) * 75,
	      y: 70 + 75 * Math.floor(i/3)
	    },
	    toInternalPosition: _.bind(this.toInternalPosition, this),
	    force: this.force,
	    validList: this.validList
	  });
	  marker.on('create-node', function (x, y, site) {
	    this.trigger('create-node', x, y, site, options);
	  }.bind(this));
	  marker.render();
	  this.nodeMarkers.push(marker);
	  i += 1;
	}.bind(this));

	if (this.context.multiSite)
	{
	  this.siteOrigin = { x: 125,
			      y: 100 + 75 * Math.ceil(i/3) };

	  this.lazySite = this.makeSite(this.outer);
	  this.lazySite.style('visibility', 'hidden');
	  this.siteMarker = this.makeSite(this.outer);
	  var siteDrag = d3v4.drag()
	    .on('start', _.bind(this.siteDragStart, this))
	    .on('drag', _.bind(this.siteDragMove, this))
	    .on('end', _.bind(this.siteDragEnd, this));
	  this.siteMarker.call(siteDrag);
	}
      }

      //this.rescaleToZoom();
      this.centerScale();
    },

    makeSite: function (root)
    {
      var result = root.append('svg:g')
	.attr('transform', 'translate(' + this.siteOrigin.x + ', ' +
	      this.siteOrigin.y + ')')
	.attr('class', 'site-marker')
	.attr('style', 'cursor: pointer');

      result.append('svg:path')
        .style("fill", 'rgb(255, 127, 14)')
        .style("stroke", 'rgb(255, 127, 14)')
        .style("stroke-width", 80)
        .style("stroke-linejoin", "round")
        .style("opacity", .6)
	.attr('d', 'M-20,0L70,0Z');
      var label = result.append('svg:g')
	.attr('class', 'sitelabelgroup')
	.attr('transform', 'translate(25, 5)');

      label.append('svg:rect')
        .attr('class', 'labelbox')
        .attr('x', '-40px')
        .attr('y', '-14px')
        .attr('width', '80px')
        .attr('height', '20px');

      label.append('svg:text')
        .attr('x', '0px')
        .attr("class", "sitetext")
	.text('New Site');

      return result;
    },

    resize: function (width, height)
    {
      if (width > 0 && height > 0)
      {
	this.width = width - this.paletteOffset;
	this.height = height;
	this.outer.attr('width', width).attr('height', height);
	this.palette.attr('height', height);
	this.paletteBackground.attr('height', height);
	this.viewport.attr('width', width - this.paletteOffset).attr('height', height);
	this.rect.attr('width', width - this.paletteOffset).attr('height', height);
	//this.rescaleToZoom();
	this.centerScale(true);
      }
    },

    centerScale: function (alwaysRecenter)
    {
      var nodes = _.clone(this.forceNodes);
      _.each(this.sites, function (site) {
	nodes.push(site.label);
      });
      var minX = getExtreme(Math.min, nodes, 'x');
      var maxX = getExtreme(Math.max, nodes, 'x');
      var minY = getExtreme(Math.min, nodes, 'y');
      var maxY = getExtreme(Math.max, nodes, 'y');
      var center = {
	tx: 0,
	ty: 0,
	scale: 1
      };
      if (minX !== null && minY !== null && maxX !== null && maxY !== null)
      {
	var width = maxX - minX + 200;
	var height = maxY - minY + 200;
	center.scale = Math.min(this.width / width,
				this.height / height);
	center.scale = Math.min(center.scale, 1);
//      center.scale *= 1.2; 
	var marginX = (this.width/center.scale - (maxX - minX))/2;
	var marginY = (this.height/center.scale - (maxY - minY))/2;
	center.tx = -(minX*center.scale) + marginX*center.scale;
	center.ty = -(minY*center.scale) + marginY*center.scale;
      }
      if (alwaysRecenter ||
	  ! this.lastCenter ||
	  Math.abs(this.lastCenter.tx - center.tx) > 100 ||
	  Math.abs(this.lastCenter.ty - center.ty) > 100 ||
	  center.scale / this.lastCenter.scale > 1.2 ||
	  center.scale / this.lastCenter.scale < 1.0)
      {
	this.lastCenter = center;
	this.inner.attr('transform',
			'translate(' + center.tx + ',' + center.ty + ')' +
			'scale(' + center.scale + ')');
      }
      this.lastCenter.scale = center.scale;
      this.zoomX = center.tx;
      this.zoomY = center.ty;
      this.zoomScale = center.scale;
    },

    rescaleToZoom: function ()
    {
      this.rescaleTo(this.zoomX, this.zoomY, this.zoomScale);
    },

    rescaleTo: function (x, y, scale)
    {
      var baseWidth = 1000;
      var baseHeight = 1000;
      var minScale = Math.max(this.width / baseWidth,
			      this.height / baseHeight);
      var xScale = scale;
      var yScale = scale;
      if (baseWidth * scale <= this.width)
      {
	xScale = this.width / baseWidth;
      }
      if (baseHeight * scale <= this.height)
      {
	yScale = this.height / baseHeight;
      }
      var finalScale = Math.max(xScale, yScale);

      var tx = Math.min(0, Math.max(this.width - baseWidth * finalScale, x));
      var ty = Math.min(0, Math.max(this.height - baseHeight * finalScale, y));
      this.zoomX = tx;
      this.zoomY = ty;
      this.zoomScale = finalScale;
      this.inner.attr('transform',
		      'translate(' + tx + ',' + ty + ')' +
		      'scale(' + finalScale + ')');
//      this.tour.update();
    },

    showDragLine: function (start, end)
    {
      this.dragLine.classed('hidden', false)
	.attr('x1', start.x)
	.attr('y1', start.y)
	.attr('x2', end.x)
	.attr('y2', end.y);
    },

    hideDragLine: function ()
    {
      this.dragLine.classed('hidden', true);
    },

    handleRescale: function ()
    {
//    //if (! this.isMouseDown)
//    {
//      this.rescaleTo(d3v4.event.translate[0], d3v4.event.translate[1],
//		     d3v4.event.scale);
//    }
    },

    handleMouseDown: function (event)
    {
      var that = this;
      this.justClickedOutside = true;
      _.delay(function () { that.justClickedOutside = false; }, 200);
//      this.domRoot.addClass("unselectable");
    },

    handleMouseUp: function (event)
    {
//      this.domRoot.removeClass("unselectable");
    },

    handleClickOutside: function (event)
    {
      if (this.justClickedOutside)
      {
	this.clickUpdate.trigger('click-outside',
				 { event: _.clone(d3v4.event) });
      }
    },

    siteDragStart: function ()
    {
      this.lazySite.style('visibility', 'visible');
      this.lastSite = null;
      this.force.stop();
    },

    siteDragMove: function ()
    {
      this.siteMarker.attr('transform', 'translate(' + (d3v4.event.x - 20) + ',' + (d3v4.event.y - 10) + ')');
//      this.lastSiteMouse = d3v4.mouse(this.inner);
      var scale = this.zoomScale;
      var tx = this.zoomX;
      var ty = this.zoomY;
      this.lastSite = {
	x: d3v4.event.x,
	y: d3v4.event.y,
	topX: (d3v4.event.x - tx - this.paletteOffset)/scale,
	topY: (d3v4.event.y - ty)/scale
      };
    },

    siteDragEnd: function ()
    {
      this.lazySite.style('visibility', 'hidden');
      if (this.lastSite &&
	  this.lastSite.x > this.paletteOffset &&
	  this.lastSite.y > 0 &&
	  this.lastSite.x < this.width + this.paletteOffset &&
	  this.lastSite.y < this.height)
      {
	this.trigger('create-site', this.lastSite.topX, this.lastSite.topY);
      }
      this.siteMarker.attr('transform', 'translate(' + this.siteOrigin.x +
			   ', ' + this.siteOrigin.y + ')');
    },

    closestSite: function (x, y)
    {
      var closest = null;
      var closestDistance = null;
      _.each(this.sites, function (site) {
	var distance = site.distance(x, y);
	if (closest === null || distance < closestDistance)
	{
	  closest = site;
	  closestDistance = distance;
	}
      });
      return closest;
    },

    toInternalPosition: function (pos)
    {
      var scale = this.zoomScale;
      var tx = this.zoomX;
      var ty = this.zoomY;
      var result = {
	x: pos.x,
	y: pos.y,
	topX: (pos.x - tx - this.paletteOffset)/scale,
	topY: (pos.y - ty)/scale
      };
      if (result.x > this.paletteOffset &&
	  result.y > 0 &&
	  result.x < this.width + this.paletteOffset &&
	  result.y < this.height)
      {
	result.closestSite = this.closestSite(result.topX, result.topY);
      }
      else
      {
	result.closestSite = null;
      }
      return result;
    }

  };

  function getExtreme(f, list, key)
  {
    var result = null;
    _.each(list, function (item) {
      if (item[key] !== null && item[key] !== undefined)
      {
	if (result === null)
	{
	  result = item[key];
	}
	else
	{
	  result = f(result, item[key]);
	}
      }
    });
    return result;
  }

  return TopologyBackground;
});
