/*
 * TourView.js
 *
 * Manager for rspec Tours
 *
 */

define(['underscore', 'backbone', 'tourist'],
function (_, Backbone, Tourist)
{
  'use strict';

  function TourView (rootDom, tourContainer, rspec)
  {
    this.rootDom = rootDom;
    this.tourContainer = tourContainer;
    this.rspec = rspec;
    this.tour = null;
    this.trivial = true;
    this.startTour();
  }

  TourView.prototype.startTour = function ()
  {
    var that = this;
    var root = $($.parseXML(this.rspec));
    var stepsNode = root.find('step');
    var steps = [];
    _.each(stepsNode, function (item) {
      var type = $(item).attr('point_type');
      var id = $(item).attr('point_id');
      var target = '#' + type + '-' + id;
      steps.push({
	content: '<p>' + _.escape($(item).find('description').text()) + '</p>',
	highlightTarget: true,
	nextButton: true,
	closeButton: true,
	container: that.tourContainer,
	setup: function(tourArg, options) {
	  return { target: that.rootDom.find(target) };
	},
	my: 'top center',
	at: 'bottom center'
      });
    });

    this.tour = new Tourist.Tour({
      steps: steps,
      tipClass: 'Bootstrap',
      tipOptions:{ showEffect: 'slidein' }
    });

    if (steps.length > 0)
    {
      this.trivial = false;
    }
  };

  TourView.prototype.nonTrivial = function ()
  {
    return ! this.trivial;
  };

  TourView.prototype.update = function ()
  {
    this.tour.view.render(this.tour.model.get('current_step'));
  };

  return TourView;
});
