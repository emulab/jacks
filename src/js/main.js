/*
 * main.js
 *
 */

/*global require: true */
/*
require.config({
  baseUrl: '.',
  paths: {
    'jquery-xmlrpc': 'lib/jquery.xmlrpc',
    'backbone': 'lib/backbone-min',
    'underscore': 'lib/underscore-min',
    'demo': 'js/demo',
    'tourist': 'lib/tourist',
    'q': 'lib/q.min',
    'highlight': 'lib/highlight.pack',
    'text': 'lib/text'
  },
  shim: {
    'jquery-xmlrpc': { deps: ['js/ajaxforge'] },
    'backbone': { deps: ['underscore'], exports: 'Backbone' },
    'tourist': { deps: ['backbone'], exports: 'Tourist' },
    'underscore': { exports: '_' },
    'highlight': { exports: 'hljs' }
  }
});
*/
require({
    baseUrl: '.',
    paths: {
      'jquery-xmlrpc': 'lib/jquery.xmlrpc',
      'backbone': 'lib/backbone-min',
      'underscore': 'lib/underscore-min',
      'demo': 'js/demo',
      'tourist': 'lib/tourist',
      'q': 'lib/q.min',
      'highlight': 'lib/highlight.pack',
      'select2': 'lib/select2.min',
      'text': 'lib/text'
    },
    shim: {
      'jquery-xmlrpc': { deps: ['js/ajaxforge'] },
      'backbone': { deps: ['underscore'], exports: 'Backbone' },
      'tourist': { deps: ['backbone'], exports: 'Tourist' },
      'underscore': { exports: '_' },
      'highlight': { exports: 'hljs' }
    }
  },
  ['underscore', 'backbone', 'js/Transition', 'js/cache',
   'js/canvas/Canvas', 'js/actor/Actor.js', 'js/canvas/ValidList',
   'js/canvas/Constraints',
   'text!html/Viewer.html', 'text!html/Actor.html',
   'text!html/Editor.html'],
function (_, Backbone, Transition, cache,
	  Canvas, Actor, ValidList, Constraints,
	  viewerString, actorString, editorString)
{
  'use strict';

  window.JACKS_LOADER.version = '2018-01-10-A';
  window.JACKS_LOADER.Constraints = Constraints;
  window.JACKS_LOADER.ValidList = ValidList;

  var defaults = {
    mode: 'viewer',
    multiSite: false,
    source: 'rspec',
    root: 'body',
    nodeSelect: true,
    size: 'auto',
    canvasOptions: {}
  };

  var defaultDefault = {
    name: 'Add Node'
  };

  var defaultShow = {
    rspec: true,
    tour: true,
    version: true,
    menu: true,
    selectInfo: false,
    clear: true
  };

  function MainClass(context)
  {
    this.context = _.defaults(context, defaults);
    if (context.mode === 'viewer')
    {
      this.context.multiSite = true;
    }
    if (this.context.show)
    {
      var show = _.clone(defaultShow);
      if (context.nodeSelect)
      {
	show.selectInfo = true;
      }
      this.context.show = _.defaults(this.context.show, defaultShow);
    }
    else
    {
      this.context.show = defaultShow;
    }
    if (! this.context.canvasOptions.defaults)
    {
      this.context.canvasOptions.defaults = [defaultDefault];
    }
    if (this.context.mode === 'viewer' && ! this.context.nodeSelect)
    {
      this.context.show.selectInfo = false;
    }
    this.updateIn = {};
    _.extend(this.updateIn, Backbone.Events);
    this.updateOut = {};
    _.extend(this.updateOut, Backbone.Events);

    this.root = $(context.root);
    this.root.show();
    this.transition = new Transition(this.root);

    if (context.mode === 'viewer' && context.source === 'api')
    {
      this.root.html(actorString);
      this.canvas = new Canvas(context, this.root.find('#canvasContainer'),
			       this.root, this.updateIn, this.updateOut);
      this.canvas.show();
      this.actor = new Actor(this.root, this.canvas, this.transition);
    }
    else if (context.mode === 'viewer' && context.source === 'rspec')
    {
      this.root.html(viewerString);
      this.canvas = new Canvas(context, this.root.find('#canvasContainer'),
			       this.root, this.updateIn, this.updateOut);
      this.canvas.show();
      this.root.find('#loadingContainer').hide();
      this.root.find('#canvasContainer').show();
    }
    else if (context.mode === 'editor' && context.source === 'rspec')
    {
      this.root.html(editorString);
      this.canvas = new Canvas(context, this.root.find('#canvasContainer'),
			       this.root, this.updateIn, this.updateOut);
      this.canvas.show();
      this.root.find('#loadingContainer').hide();
      this.root.find('#canvasContainer').show();
    }

    this.updateIn.on('change-topology', _.bind(this.changeTopologyHandler,
					       this));
    this.updateIn.on('add-topology', _.bind(this.addTopologyHandler,
					    this));
    this.updateIn.on('resize', _.bind(this.resizeHandler, this));
    this.updateIn.on('fetch-topology', _.bind(this.fetchTopologyHandler,
					      this));
    this.lastRspec = [];
  }

  MainClass.prototype.changeTopologyHandler = function (list, options)
  {
    var isEqual = true;
    if ((options && ! options.constrainedFields) || list.length !== this.lastRspec.length)
    {
      isEqual = false;
    }
    else
    {
      for (var i = 0; i < list.length; i++)
      {
	if (this.lastRspec[i] !== list[i].rspec)
	{
	  isEqual = false;
	  break;
	}
      }
    }
    if (! isEqual)
    {
      this.lastRspec = _.pluck(list, 'rspec');
      this.canvas.clear();
      if (list.length > 0)
      {
	this.canvas.addRspec(list);
      }
    }
    if (options && options.constrainedFields)
    {
      var validList = new ValidList(this.canvas.topoData, this.canvas.constraints);
      var clauses = validList.getNodeCandidates(true);
      var sites = validList.getNodeCandidatesBySite(true);
      console.log('constrainedFields', clauses, sites);
      options.constrainedFields(clauses, sites);
    }
  };

  MainClass.prototype.addTopologyHandler = function (list)
  {
    this.lastRspec = this.lastRspec.concat(_.pluck(list, 'rspec'));
    if (this.canvas.hasNodes())
    {
      this.canvas.addRspec(list);
    }
    else
    {
      this.changeTopologyHandler(list);
    }
  };

  MainClass.prototype.resizeHandler = function (size)
  {
    this.canvas.resize(size);
  };

  MainClass.prototype.fetchTopologyHandler = function ()
  {
    var rspec = this.canvas.generateRequest();
    this.updateOut.trigger('fetch-topology', [{ rspec: rspec }]);
  };

  function initialize ()
  {
    window.JACKS_LOADER.isReady = true;
    window.JACKS_LOADER.MainClass = MainClass;
    if (window.JACKS_LOADER.onReady)
    {
      _.defer(_.bind(window.JACKS_LOADER.onReady, window.JACKS_LOADER));
    }
  }

  $(document).ready(initialize);
});
