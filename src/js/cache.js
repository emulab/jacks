define(['underscore'],
function (_)
{
  'use strict';

  var cache = {};

  var localStorageParams = {};
  var sessionStorageParams = {};

  var initialized = false;

  var sessionStorageItems = [
    "passphrase"
  ];

  var localStorageItems = [
    "userName",
    "userUrn",
    "userCert",
    "userKey",
    "userCredential",
    "provider",
    "cabundle",
    "salist",
    "amlist"
  ];

  function initializeCache()
  {
    initialized = true;
    try {
      sessionStorageParams = JSON.parse(window.sessionStorage.params);
      localStorageParams = JSON.parse(window.localStorage.params);
    } catch (e) { }
  }

  cache.get = function(cachedItem)
  {
    var result = null;
    if (!initialized)
    {
      initializeCache();
    }
    if (_.contains(sessionStorageItems, cachedItem))
    {
      try {
	result = sessionStorageParams[cachedItem];
      } catch (e) { }
    }
    else if (_.contains(localStorageItems, cachedItem))
    {
      try {
	result = localStorageParams[cachedItem];
      } catch (e) { }
    }
    return result;
  };

  cache.set = function(cacheName, cacheValue)
  {
    if (!initialized)
    {
      initializeCache();
    }
    if (_.contains(sessionStorageItems, cacheName))
    {
      try {
	sessionStorageParams[cacheName] = cacheValue;
	sessionStorage.params = JSON.stringify(sessionStorageParams);
      } catch (e) { }
    }
    else if (_.contains(localStorageItems, cacheName))
    {
      try {
	localStorageParams[cacheName] = cacheValue;
	localStorage.params = JSON.stringify(localStorageParams);
      } catch (e) { }
    }
  };

  cache.remove = function(cacheName)
  {
    if (!initialized)
    {
      initializeCache();
    }
    if (_.contains(sessionStorageItems, cacheName))
    {
      try {
	delete sessionStorageParams[cacheName];
	sessionStorage.params = JSON.stringify(sessionStorageParams);
      } catch (e) { }
    }
    if (_.contains(localStorageItems, cacheName))
    {
      try {
	delete localStorageParams[cacheName];
	localStorage.params = JSON.stringify(sessionStorageParams);
      } catch (e) { }
    }
  };

  return cache;
});
