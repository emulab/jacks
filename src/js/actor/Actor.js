define(['underscore', 'backbone', 'js/cache',
	'js/actor/Login', 'js/actor/FetchSlices'],
function (_, Backbone, cache, Login, FetchSlices)
{
  'use strict';

  function Actor(rootDom, canvas, transition)
  {
    this.rootDom = rootDom;
    this.canvas = canvas;
    this.transition = transition;
    this.saList = [];
    this.amList = {};
    this.bundle = '';
    this.login = null;
    this.fetchSlices = null;

    swfobject.embedSWF(
      'SocketPool.swf', 'socketPool',
      '0', '0',
      '9.0.0', false,
      {}, {allowscriptaccess: 'always'}, {}, _.bind(this.swfComplete, this));
    swfobject.createCSS("#socketPool", "opacity: 1;");
  }

  Actor.prototype.swfComplete = function (event)
  {
    if (event.success)
    {
      var sa, am, caBundle;

      if (cache.get('salist'))
      {
	this.parseSAList(cache.get('salist'));
      }
      else
      {
	sa = $.ajax({
	  url: 'https://www.emulab.net/protogeni/boot/salist.json',
	  dataType: 'text'
	}).then(_.bind(this.parseSAList, this));
      }

      if (cache.get('amlist'))
      {
	this.parseAMList(cache.get('amlist'));
      }
      else
      {
	am = $.ajax({
	  url: 'https://www.emulab.net/protogeni/boot/amlist.json',
	  dataType: 'text'
	}).then(_.bind(this.parseAMList, this));
      }

      if (cache.get('cabundle'))
      {
	this.parseCABundle(cache.get('cabundle'));
      }
      else
      {
	caBundle = $.ajax({
	  url: 'https://www.emulab.net/protogeni/boot/genica.bundle',
	  dataType: 'text'
	}).then(_.bind(this.parseCABundle, this));
      }

      var waitForSwf = $.Deferred();
      setTimeout(function () {
	waitForSwf.resolve();
      }, 500);

      var that = this;
      $.when(sa, am, caBundle, waitForSwf.promise()).then(function () {
	that.transition.page(that.rootDom.find('#loadingContainer'),
			     that.rootDom.find('#loginContainer'));
	that.login = new Login(that.rootDom, that.canvas, that.transition,
			       that.saList);
	that.login.on('login-complete', _.bind(that.loginComplete, that));
      });
    }
    else
    {
      this.transition.startFail(this.rootDom.find('#loadingContainer'),
				'Failed to load socket flash file', event);
    }
  };

  Actor.prototype.parseSAList = function (response)
  {
    cache.set('salist', response);
    var inputOptions = '';
    var data = JSON.parse(response);
    var list = data['identity-providers'];
    this.saList = list;
  };

  Actor.prototype.parseAMList = function (response)
  {
    cache.set('amlist', response);
    var data = JSON.parse(response);
    var list = data['aggregate-managers'];

    var that = this;
    _.each(list, function (item) {
      that.amList[item.urn] = item['am-url'];
    });
  };

  Actor.prototype.parseCABundle = function (response)
  {
    cache.set('cabundle', response);
  };

  Actor.prototype.loginComplete = function ()
  {
    this.fetchSlices = new FetchSlices(this.rootDom, this.transition,
				       this.amList, this.canvas);
    this.fetchSlices.fetchXmlrpc();
  };

  return Actor;
});
