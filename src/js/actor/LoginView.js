define(['underscore', 'backbone', 'js/cache',
	'text!html/Login.html'],
function (_, Backbone, cache,
	  loginString)
{
  'use strict';

  var LoginView = Backbone.View.extend({

    events: {
      'keyup #clientPhrase': 'updatePassphrase',
      'click .fileTab': 'clickFileTab',
      'click .saTab': 'clickSaTab',
      'click #manualCert': 'clickManualCert',
      'click #passBack': 'clickPassphraseBack',
      'click #passForm': 'submitPassphraseForm',
      'click .suggested-button': 'clickSuggested',
      'click #other-sa-button': 'clickOtherSA'
    },

    initialize: function () {
      this.render();
    },

    render: function () {
      this.$el.html(loginString);
      this.renderSAList();
      this.$('.info').tooltip();
    },

    renderSAList: function () {
      var inputOptions = '';
      var isSelected = false;
      var list = this.options.saList;
      var suggested = [];

      for (var i = 0; i < list.length; ++i)
      {
	if (list[i]['web-cert-url'])
	{
	  var name = list[i]['name'];
	  if (! name || name === '')
	  {
	    name = list[i]['urn'].split('+')[1];
	  }
	  inputOptions += '<option value="' + i + '"';
	  if (list[i]['urn'] === 'urn:publicid:IDN+emulab.net+authority+sa' ||
	      list[i]['urn'] === 'urn:publicid:IDN+uky.emulab.net+authority+sa')
	  {
	    inputOptions += ' class="hidden"';
	    suggested.push({ index: i, item: list[i]});
	  }
	  else if (! isSelected)
	  {
	    inputOptions += ' selected="selected"';
	    isSelected = false;
	  }
	  inputOptions += '>' + name + '</option>';
	}
      }

      this.$('.sliceAuthorities').html(inputOptions);
      this.renderSuggestedSAList(suggested);
/*
      this.$('.sliceAuthorities').change(updateIdentityProvider);
      if (! cache.get('provider'))
      {
	updateIdentityProvider(0);
      }
*/
    },

    renderSuggestedSAList: function (suggested) {
      var imageHTML = '';
      _.each(suggested, function (value) {
	var icon = value.item.icon;
	var index = value.index;

	imageHTML += '<div class="SAImage pull-left"><a data-index="' + index + '" class="suggested-button">';
	if (icon && icon !== '')
	{
	  imageHTML += '<img style="margin-top: 10px;" src="' +
	    _.escape(icon) + '" />';
	}
	else
	{
	  imageHTML += value.item.name;
	}
	imageHTML += '</a></div>';
      });

      this.$('.recSAImages').html(imageHTML);
    },

    fillPassphraseForm: function (name, saName)
    {
      this.$('#userName').val(_.escape(name));
      this.$('#userNameContainer').show();
      this.$('#currentSA').val(saName);
      this.options.transition.page(this.$('#certificate'),
				   this.$('#passphrase'));
    },

    updatePassphrase: function (event) {
      if (event.keyCode === 13)
      {
	this.submitPassphraseForm();
      }
    },

    clickFileTab: function () {
      switchTab(this.$('.fileTab'), this.$('.fromFile'));
    },

    clickSaTab: function () {
      switchTab(this.$('.saTab'), this.$('.sa'));
    },

    clickManualCert: function () {
      var that = this;
      this.options.login.parseCert(this.$('#clientKey').val());
      this.options.transition.page(this.$('#certificate'),
				   this.$('#passphrase'));
      this.$('#passphrase').one('transitionend', function() {
	that.$('#clientPhrase').focus();
      });
    },

    clickPassphraseBack: function () {
      this.options.transition.page(this.$('#passphrase'),
				   this.$('#certificate'))
    },

    submitPassphraseForm: function () {
      this.options.login.tryPassphrase(this.$('#clientPhrase').val());
    },

    clickSuggested: function (event) {
      event.preventDefault();
      var index = event.currentTarget.dataset['index'];
      cache.set('provider', this.options.saList[index]);
      this.options.login.getCertFromSA();
    },

    clickOtherSA: function (event) {
      event.preventDefault();
      var index = parseInt(this.$('.sliceAuthorities').val());
      cache.set('provider', this.options.saList[indx]);
      this.options.login.getCertFromSA();
    }

  });

  function switchTab(switchTo, panel) {
    switchTo.siblings('.active').removeClass('active');
    panel.siblings('.active').removeClass('active');

    switchTo.addClass('active');
    panel.addClass('active');
  }

  return LoginView;
});
