define(['underscore', 'backbone', 'q', 'js/cache',
	'lib/jquery.xmlrpc'],
function (_, Backbone, Q, cache)
{
  'use strict';

  function FetchSlices (root, transition, amList, canvas)
  {
    this.root = root;
    this.transition = transition;
    this.amList = amList;
    this.canvas = canvas;
    this.userSlices = null;
  }

  FetchSlices.prototype.checkCode = function (response, action, isGeni)
  {
    if ((! isGeni && response[0].code !== 0) ||
	(isGeni && response[0].code.geni_code !== 0))
    {
      this.transition.startFail($('#loginContainer'), 'Failed: ' + action,
				response);
      throw 'Failed: ' + action + JSON.stringify(response);
    }
  };

  FetchSlices.prototype.fetchXmlrpc = function ()
  {
    var that = this;
    this.transition.startWaiting($('#passphrase'), null);
    var deferred = Q.defer();
    var promise = deferred.promise;
    if (!cache.get('userCredential'))
    {
      promise = promise.then(function () {
        that.transition.startWaiting(null, 'Fetching Your Credential...');
        return $.xmlrpc({
	  url: cache.get('provider')['pg-sa-url'],
	  methodName: 'GetCredential',
	  params: [{}]
        });
      }).then(function (response) {
	that.checkCode(response, 'Fetch Credential');
        cache.set('userCredential', response[0].value);
      });
    }
    promise.then(function () {
      that.transition.startWaiting(null, 'Finding Slices...');
      return $.xmlrpc({
        url: cache.get('provider')['pg-sa-url'],
        methodName: 'Resolve',
        params: [{credential: cache.get('userCredential'),
		  urn: cache.get('userUrn'),
		  type: 'User' }]
      });
    }).then(function (response) {
      that.checkCode(response, 'Find Slices');
      that.userSlices = response[0].value.slices;
      if (! that.userSlices)
      {
	that.transition.startFail($('#loginContainer'), 'Found no slices',
				  response);
	throw 'Found no slices: ' + JSON.stringify(response);
      }
      that.updateSliceList(_.bind(that.selectSlice, that));
      that.transition.stopWaiting($('#sliceListContainer'));
    }).fail(function (error) {
      that.transition.startFail($('#loginContainer'), 'Error during setup',
				error);
      console.log('ERROR: ', error);
    });
    deferred.resolve();
  };

  FetchSlices.prototype.selectSlice = function (sliceUrn)
  {
    var that = this;
    if ($('#loginContainer').hasClass('afterHide')) {
      this.transition.page($('#canvasContainer'), $('#loginContainer'));
      $('#waitContainer').removeClass('hides afterHide');
    }
    this.transition.startWaiting($('#sliceListContainer'),
				 'Resolving Slice...');

    var sliceCredential;
    var amUrl;
    var promise = $.xmlrpc({
      url: cache.get('provider')['pg-sa-url'],
      methodName: 'Resolve',
      params: [{credential: cache.get('userCredential'),
		urn: sliceUrn,
		type: 'Slice' }]
    });
    promise.then(function (response) {
      that.checkCode(response, 'Resolving Slice');
      that.transition.startWaiting(null, 'Fetching Credential...');
      var urn = response[0].value.component_managers[0];
      if (! urn)
      {
	that.transition.startFail($('#loginContainer'),
				  'Could not find AM URN for this slice',
				  null);
	throw 'Could not find URN for this slice';
      }
      amUrl = that.amList[urn];
      if (! amUrl)
      {
	that.transition.startFail($('#loginContainer'),
				  'Could not convert AM URN ' + urn 
				  + ' to URL', null);
	throw 'Could not convert AM URN to URL';
      }
      return $.xmlrpc({
	url: cache.get('provider')['pg-sa-url'],
	methodName: 'GetCredential',
	params: [{credential: cache.get('userCredential'),
		  urn: sliceUrn,
		  type: 'Slice' }]
      });
    }).then(function (response) {
      that.checkCode(response, 'Fetching Credential');
      that.transition.startWaiting(null, 'Fetching Manifest...');

      //  sliceCredential = { 'geni_type': 'geni_sfa',
      //          'geni_version': '3',
      //          'geni_value': response[0].value };

      sliceCredential = response[0].value;
      var options = {
	'geni_rspec_version': {
	  'type': 'ProtoGENI',
	  'version': '3'
	},
	'geni_slice_urn': sliceUrn
      };
      return $.xmlrpc({
        url: amUrl,
	methodName: 'ListResources',
        params: [[sliceCredential], options]
      });
    }).then(function (response) {
      that.checkCode(response, 'Fetching Manifest', true);
      that.transition.switchNav($('#small'));
      $('#canvasContainer').removeClass('afterHide');
      that.transition.page($('#loginContainer'), that.canvas.domRoot);
      that.canvas.show(response[0].value);
      that.transition.stopWaiting($('#sliceListContainer'));
      //      initJacks($('#canvasContainer'), response[0].value);
      var name = sliceUrn.split('+')[3];
      $('#lnSliceList .active').removeClass('active');
      $('.name' + name).addClass('active');
    }).fail(function (error) {
      that.transition.startFail($('#loginContainer'), 'Error during setup',
				error);
      console.log('ERROR: ', error);
    });
  };


  FetchSlices.prototype.updateSliceList = function (selectSlice)
  {
    var that = this;
    var options = $('<div/>');
    var options2 = $('<div/>');
    _.each(that.userSlices, function (urn) {
      var name = urn.split('+')[3];
      var newOption = $('<a class="list-group-item" href="">' + name + 
			'<span class="glyphicon glyphicon-chevron-right pull-right"></span>' +
			'</a>');
      newOption.click(function (event) {
	event.preventDefault();
	selectSlice(urn);
      });
      options.append(newOption);
      var newOption2 = $('<a class="list-group-item name' + name + '" href="">' + name + 
			 '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' +
			 '</a>');
      newOption2.click(function (event) {
	event.preventDefault();
	selectSlice(urn);
	$('#leftNav > .active').removeClass('active');
      });
      options2.append(newOption2);
    });
    $('#slice-list').html(options);
    $('#lnSliceList').html(options2);
  }

  return FetchSlices;
});
