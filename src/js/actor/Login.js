define(['underscore', 'backbone',
	'js/cache', 'js/actor/LoginView', 'js/ajaxforge'],
function (_, Backbone, cache, LoginView)
{
  'use strict';

  var Login = function (rootDom, canvas, transition, saList)
  {
    _.extend(this, Backbone.Events);
    this.root = rootDom;
    this.certWindow = null;
    this.canvas = canvas;
    this.transition = transition;
    this.saList = saList;
    this.view = new LoginView({
      el: rootDom.find('#loginContainer'),
      login: this,
      transition: transition,
      saList: saList
    });
    rootDom.find('button.logout').click(logout);
    window.addEventListener('message', _.bind(this.certFromMessage, this));
    // This cannot run directly in the initializer because we need to
    // give others a chance to bind to the events this might invoke.
    _.defer(_.bind(this.checkCache, this));
  }

  Login.prototype.checkCache = function ()
  {
    this.transition.page($('#loadingContainer'), $('#loginContainer'));
    $('#loginContainer').one('transitionend', function() {
      $('#clientPhrase').focus();
    });
    if (cache.get('userKey') && cache.get('userCert'))
    {
      var combined = cache.get('userKey') + '\n' + cache.get('userCert');
      $('#clientKey').val(combined);
      this.parseCert(combined);
      if (cache.get('passphrase') !== null &&
	  cache.get('passphrase') !== undefined)
      {
	this.tryPassphrase(cache.get('passphrase'));
      }
/*
      {
      	pageTransition($('#certificate'), $('#passphrase'));
	$('#passphrase').one('transitionend', function() {
	  $('#clientPhrase').focus();
	});
      }
*/
    }
  };

  Login.prototype.getCertFromSA = function ()
  {
    // The confirmation event is a message that is caught and handled
    // by LoginView
    var that = this;
    if (this.certWindow)
    {
      this.certWindow.close();
    }

    var url = cache.get('provider')['web-cert-url'];
    this.certWindow = window.open(url, 'Slice Authority Credential',
				  'height=400,width=600,toolbar=yes');
    $(this.certWindow.document).ready(function() {
      that.root.find('.windowOpen').removeClass('hidden');
      $(that.certWindow).focus();

      var interval = window.setInterval(function() {
    	if (that.certWindow.closed)
	{
    	  window.clearInterval(interval);
    	  that.root.find('.windowOpen').addClass('hidden');
    	}
      }, 250);
    });
  };

  Login.prototype.certFromMessage = function (event)
  {
    if (event.source === this.certWindow && event.data &&
	event.data.certificate && event.data.authority)
    {
      this.root.find('#clientKey').val(event.data.certificate);
      this.view.clickManualCert();
    }
  };

  Login.prototype.parseCert = function (source)
  {
    var inKey = false;
    var inCert = false;
    var cert = "";
    var key = "";
    var lines = source.split('\n');
    var i = 0;
    for (i = 0; i < lines.length; i += 1)
    {
      if (lines[i] === "-----BEGIN RSA PRIVATE KEY-----")
      {
	key = lines[i] + '\n';
	inKey = true;
      }
      else if (lines[i] === "-----END RSA PRIVATE KEY-----")
      {
	key += lines[i] + '\n';
	inKey = false;
      }
      else if (inKey)
      {
	key += lines[i] + '\n';
      }
      else if (lines[i] === "-----BEGIN CERTIFICATE-----")
      {
	cert += lines[i] + '\n';
	inCert = true;
      }
      else if (lines[i] === "-----END CERTIFICATE-----")
      {
	cert += lines[i] + '\n';
	inCert = false;
      }
      else if (inCert)
      {
	cert += lines[i] + '\n';
      }
    }

    cache.set('userKey', key);
    cache.set('userCert', cert);

    this.getCertFields(cert);
  };

  Login.prototype.getCertFields = function (certificate)
  {
    var cert = forge.pki.certificateFromPem(certificate);
    var found = false;
    var urn;
    var name;

    var altField;
    _.each(cert.extensions, function (field) {
      if (field.name === 'subjectAltName')
      {
	altField = field;
      }
    });
    if (altField)
    {
      _.each(altField.altNames, function (item) {
	if (item.type === 6 && item.value.substr(0, 12) === 'urn:publicid')
	{
	  urn = item.value;
	}
      });
    }
    if (urn)
    {
      name = urn.split('+')[3];
      cache.set('userUrn', urn);
      cache.set('userName', name);
      found = true;
    }

    if (found)
    {
      var authorityUrn = 'urn:publicid:IDN+' + urn.split('+')[1] +
	'+authority+sa';
      var index = 0;
      _.each(this.saList, function (provider, i) {
	if (provider.urn === authorityUrn)
	{
	  index = i;
	}
      });
      cache.set('provider', this.saList[index]);

      var currentSAName;
      if (cache.get('provider').name)
      {
	currentSAName = _.escape(cache.get('provider').name);
      }
      else
      {
	currentSAName = _.escape(cache.get('provider')['urn'].split('+')[1]);
      }
      this.view.fillPassphraseForm(name, currentSAName);
    }
  };

  Login.prototype.tryPassphrase = function (passphrase)
  {
    try
    {
      var decrypted = forge.pki.decryptRsaPrivateKey(cache.get('userKey'),
						     passphrase);
      var keyText = forge.pki.privateKeyToPem(decrypted);
      $.ajaxforge.initialize('SocketPool.swf', 'socketPool',
			     cache.get('cabundle'), keyText,
			     cache.get('userCert'));
      this.root.find('#passError').addClass('hidden');
      this.root.find('#passGroup').removeClass('has-error');
      cache.set('passphrase', passphrase);
      this.trigger('login-complete');
    }
    catch (e)
    {
      console.log('Error: ', e);
      this.root.find('#passError').html('Could not decrypt private key. Please check your passphrase.');
      this.root.find('#passError').removeClass('hidden');
      this.root.find('#passGroup').addClass('has-error');
    }
  };

  function logout(event)
  {
    event.preventDefault();
    cache.remove('passphrase');
    cache.remove('userName');
    cache.remove('userUrn');
    cache.remove('userCert');
    cache.remove('userKey');
    cache.remove('userCredential');
    cache.remove('provider');
    cache.remove('salist');
    cache.remove('amlist');
    cache.remove('cabundle');
    window.location.reload();
  }

  return Login;
});
