define(['underscore', 'backbone', 'lib/View', 'js/info/TextInputView'],
function (_, Backbone, View, TextInputView)
{
  'use strict';

  var TextField = View.extend({

    // options {
    //   title: Title of this field
    //   state: Initial state
    //   data: Opaque data object passed back on change events
    // }
    initialize: function (options)
    {
      this.input = new TextInputView();
      this.children.push(this.input);
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	var title = $('<h4>')
	  .html(_.escape(this.options.title))
	  .appendTo(this.$el);
	this.input.render()
	  .addClass('form-control')
	  .appendTo(this.$el);
	this.listenTo(this.input, 'change', this.change);
	this.update(this.options.state);
      }
      return this.superRender();
    },

    // state {
    //   values: List of values to place in this box
    //   isViewer: True if this is read-only viewer mode
    //   type: 'node' or 'site' or 'link' or 'none'
    //   disabled: True if this box should be disabled
    // }
    update: function (state)
    {
      if (state)
      {
	var unique = _.unique(state.values);
	var values = unique.join(', ');
	var disabled = (state.isViewer || unique.length > 1 ||
			state.disabled);
	this.input.update({
	  value: values,
	  disabled: disabled
	});
      }
    },

    change: function (state)
    {
      this.trigger('change', { value: state.value,
			       data: this.options.data });
    }

  });

  return TextField;
});
