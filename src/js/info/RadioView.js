define(['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  'use strict';

  var RadioView = View.extend({

    // options {
    //   state: Initial state
    //   data: opaque value returned in change event
    //   choices: List of choices (id strings)
    //   labels: List of labels to attach to value
    // }
    initialize: function (options)
    {
      console.log(options);
      this.value = undefined;
      this.choices = options.choices;
      this.labels = options.labels;
      this.disabledOptions = null;
      this.disabled = null;
      this.id = _.uniqueId('jacksradio_');
      this.inputElements = [];
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div style="margin-right: 20px"/>');
	var i = 0;
	for (; i < this.choices.length; i += 1)
	{
	  var inputEl = $('<input type="radio" name="' + this.id + '" value="' + this.choices[i] + '">');
	  var labelEl = $('<label/>')
	      .append(inputEl)
	      .append(this.labels[i]);
	  var divEl = $('<div class="checkbox">')
	      .append(labelEl);
	  this.$el.append(divEl);
	  this.inputElements.push(inputEl);
	  this.$on('change', inputEl, this.change);
	};
      }
      return this.superRender();
    },

    // state {
    //  value: Choice id of box to be checked
    //  disabled: True if radio is disabled
    //  shown: True if this component should be shown
    // }
    update: function (state)
    {
      if (state)
      {
	var valueIndex = _.indexOf(this.choices, state.value);
	_.each(this.inputElements, function (inputEl, index) {
	  if (this.value !== valueIndex)
	  {
	    if (index === valueIndex) {
	      inputEl.prop('checked', true);
	    } else {
	      inputEl.prop('checked', false);
	    }
	  }
	  if (this.disabled !== state.disabled)
	  {
	    if (state.disabled)
	    {
	      inputEl.attr('disabled', true);
	    }
	    else
	    {
	      inputEl.attr('disabled', false);
	    }
	  }
	}.bind(this));

	if (state.shown === true)
	{
	  this.show();
	}
	else if (state.shown === false)
	{
	  this.hide();
	}
	this.value = valueIndex;
	this.disabled = state.disabled;
      }
    },

    change: function (event)
    {
      var value = -1;
      _.each(this.inputElements, function (inputEl, index) {
	if (inputEl.prop('checked')) {
	  value = index;
	}
      });
      this.trigger('change', {
	value: value,
	data: this.options.data
      });
      console.log('change RadioView');
    }

  });

  return RadioView;
});
