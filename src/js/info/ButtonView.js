define(['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  'use strict';

  var ButtonView = View.extend({

    // options {
    //  state: Initial state
    //  data: opque value returned in change event
    // }
    initialize: function (options)
    {
      this.label = null;
      this.disabled = null;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<button type="button">');
	this.$el.addClass('btn');
	this.$on('click', this.$el, this.change);
      }
      return this.superRender();
    },

    // state {
    //  label: Button label
    //  disabled: True if button is disabled
    //  shown: True if button is shown
    // }
    update: function (state)
    {
      if (state)
      {
	if (state.label !== undefined && state.label !== null)
	{
	  this.setLabel(state.label);
	}

	if (state.disabled === true)
	{
	  this.disable();
	}
	else if (state.disabled === false)
	{
	  this.enable();
	}

	if (state.shown === true)
	{
	  this.show();
	}
	else if (state.shown === false)
	{
	  this.hide();
	}
      }
    },

    disable: function ()
    {
      if (this.disabled !== true)
      {
	this.$el.attr('disabled', true);
      }
      this.disabled = true;
    },

    enable: function ()
    {
      if (this.disabled !== false)
      {
	this.$el.attr('disabled', false);
      }
      this.disabled = false;
    },

    setLabel: function (label)
    {
      var text = label || '';
      if (text !== this.label)
      {
	this.$el.html(_.escape(text));
      }
      this.label = text;
    },

    change: function (event)
    {
      this.trigger('change', { data: this.options.data });
    }

  });

  return ButtonView;
});
