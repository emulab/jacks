/*
 * Info.js
 *
 * Module for viewing and editing the side info box
 *
 */

define(['underscore', 'backbone', 'lib/View',
	'js/info/TextField', 'js/info/ReadOnlyField',
	'js/info/ConstrainedField', 'js/info/ToggleField',
	'js/canvas/Constraints', 'js/info/ListManager',
	'js/canvas/InstallDisplay', 'js/canvas/ExecuteDisplay',
	'js/canvas/InterfaceDisplay', 'js/info/OpenflowField',
	'js/info/ButtonView', 'js/info/WarningField', 
	'js/canvas/ListDisplay', 'js/info/SiteField',
	'js/info/SelectField', 'js/info/ImageField'],
function (_, Backbone, View,
	  TextField, ReadOnlyField,
	  ConstrainedField, ToggleField,
	  Constraints, ListManager,
	  InstallDisplay, ExecuteDisplay,
	  InterfaceDisplay, OpenflowField,
	  ButtonView, WarningField,
	  ListDisplay, SiteField,
	  SelectField, ImageField)
{
  'use strict';


/*
  fieldOptions = [
    {
      id: 'logins',
      on: {
	mode: 'node',
	key: 'type',
	value: [],
      },
      type: {
	id: 'span',
	title: 'SSH to',
      },
    },
    {
      id: 'type',
      on: {
	mode: 'node',
      },
      type: {
	type: 'dropdown',
	title: 'Node Type',
	options: [
	]
      }
    },
  ];
*/

  var fieldOptions = [
    {
      key: 'name',
      on: 'any',
      type: TextField,
      title: 'Name'
    },
    {
      key: 'logins',
      on: 'node',
      type: ReadOnlyField,
      title: 'SSH to'
    },
    {
      key: 'type',
      on: 'node',
      type: ConstrainedField,
      optionKey: 'types',
      title: 'Node Type',
      help: 'The container type of this node. Either a virtual container or raw-pc for a bare metal machine.'
    },
    {
      key: 'hardware',
      on: 'node',
      type: ConstrainedField,
      hideOnDataset: true,
      optionKey: 'hardware',
      title: 'Hardware Type',
      help: 'The physical hardware that this node runs on.'
    },
    {
      key: 'image',
      on: 'node',
      hideOnDataset: true,
      type: ImageField,
      optionKey: 'images',
      title: 'Disk Image',
      placeholder: 'image URN or URL',
      help: 'The initial disk state and operating system of this node.'
    },
    {
      key: 'dataset_id',
      on: 'node',
      datasetOnly: true,
      type: TextField,
      title: 'Dataset URN',
      help: 'URN for this dataset. Listed on the portal page of every dataset.'
    },
    {
      key: 'dataset_mount',
      on: 'node',
      datasetOnly: true,
      type: TextField,
      title: 'Mount Point',
      help: 'The path that this dataset will be mounted on any linked node'
    },
    {
      key: 'dataset_option',
      on: 'node',
      datasetOnly: true,
      type: SelectField,
      title: 'Dataset Access',
      choices: ['remote', 'rwclone', 'readonly'],
      labels: ['Remote Access', 'Clone (locally editable, changes are ephemeral)', 'Read Only'],
      help: 'Choose whether to access the dataset remotely, create a read/write clone (no changes propagate back to master copy), or access it in read-only mode'
    },
    {
      key: 'nomac',
      on: 'node',
      hideOnDataset: true,
      type: ToggleField,
      title: 'Disable MAC Learning (For OVS Images Only)',
      help: 'MAC Learning is an OS feature which interferes with OVS. Leave it enabled unless you plan on running OVS on this node.'
    },
    {
      key: 'routable',
      on: 'node',
      hideOnDataset: true,
      type: ToggleField,
      title: 'Require Routable IP',
      help: 'Check this if you need a public IP address on this node that is reachable from the commodity Internet. If this box is unchecked, you may get a private IP.'
    },
    {
      key: 'routable_ip',
      on: 'node',
      type: ReadOnlyField,
      title: 'Routable IP',
      help: 'This public IP address can be used to contact the node.'
    },
    {
      key: 'icon',
      on: 'node',
      type: ConstrainedField,
      optionKey: 'icons',
      title: 'Icon',
      help: 'The icon is only used for displaying the topology. Use it as a visual shorthand.'
    },
    {
      key: 'install',
      on: 'node',
      type: ListManager,
      display: InstallDisplay,
      title: 'Install Tarball',
    },
    {
      key: 'execute',
      on: 'node',
      type: ListManager,
      display: ExecuteDisplay,
      title: 'Execute Command'
    },
    {
      key: 'linkType',
      on: 'link',
      type: ConstrainedField,
      optionKey: 'linkTypes',
      title: 'Link Type',
      help: 'The type of connection. Tunnels are virtual links that run over the control network. LANs and VLANs are provisioned on experimental interfaces inside of the switch fabric.'
    },
    {
      key: 'nontrivial',
      on: 'link',
      type: ToggleField,
      title: 'Force non-trivial',
      help: 'A trivial link is one that goes over the loopback interface. This option prevents links from using the loopback interface even when two virtual nodes are on the same physical node.'
    },
    {
      key: 'interswitch',
      on: 'link',
      type: ToggleField,
      title: 'Allow interswitch mapping',
      help: 'This option allows the link to span multiple switches on the experimental switch fabric.'
    },
    {
      key: 'openflow',
      on: 'link',
      type: OpenflowField,
      title: 'Enable Openflow',
      help: 'If the switch supports Openflow, this option will enable it on the ports of the link and use the specified controller.'
    },
    {
      key: 'sharedvlan',
      on: 'link',
      type: ConstrainedField,
      optionKey: 'sharedvlans',
      title: 'Shared VLan',
      help: 'Shared VLans are a way for multiple experiments to interact over the experimental switch fabric. If you pick a Shared VLan then all other links from any experiment that pick the same Shared VLan will see your traffic and vice versa.'
    },
    {
      key: 'warnings',
      on: 'any',
      type: WarningField,
      title: 'Warning'
    },
    {
      key: 'urn',
      on: 'site',
      type: SiteField,
      optionKey: 'aggregates',
      title: 'Aggregate'
    },
    {
      key: 'interfaces',
      on: 'link',
      type: ListManager,
      display: InterfaceDisplay,
      title: 'Interfaces'
    },
    {
      key: 'delete',
      on: 'node',
      type: ButtonView,
      label: 'Delete Node'
    },
    {
      key: 'delete',
      on: 'link',
      type: ButtonView,
      label: 'Delete Link'
    },
    {
      key: 'delete',
      on: 'site',
      type: ButtonView,
      label: 'Delete Site'
    },
  ];

  var Info = View.extend({

    // options {
    //   context: Overall context for Jacks
    //   data: Opaque object passed back on change
    // }
    initialize: function (options)
    {
      this.data = options.data;
      this.context = options.context;
      this.constraints = options.constraints;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	this.$el.addClass('nodeAttr fromLeft withButtonBar');
	var inner = $('<div/>')
	  .addClass('form-group')
	  .appendTo(this.$el);
	this._each(fieldOptions, function (option) {
	  var current = null;

	  if (option.type === ConstrainedField || option.type === SiteField)
	  {
	    var choices = this.context.canvasOptions[option.optionKey];
	    current = new option.type({
	      data: option,
	      choices: choices,
	      title: option.title,
	      constraints: this.constraints,
	      optionKey: option.optionKey,
	      placeholder: option.placeholder,
	      help: option.help
	    });
	    this.listenTo(current, 'change', this.change);
	  }
	  else if (option.type === ListManager)
	  {
	    current = new option.type({
	      data: option,
	      display: option.display,
	      title: option.title,
	      help: option.help
	    });
	    this.listenTo(current, 'addToList', this.addToList);
	    this.listenTo(current, 'removeFromList', this.removeFromList);
	    this.listenTo(current, 'changeList', this.changeList);
	  }
	  else if (option.type === SelectField)
	  {
	    current = new option.type({ data: option,
					title: option.title,
					help: option.help,
					choices: option.choices,
					labels: option.labels });
	    this.listenTo(current, 'change', this.change);
	  }
	  else if (option.type === ImageField)
	  {
	    current = new option.type({ data: option,
					title: option.title,
					help: option.help,
					dynamicImages: this.context.canvasOptions.dynamicImages,
					staticImages: this.context.canvasOptions.images
				      });
	    this.listenTo(current, 'change', this.change);
	  }
	  else
	  {
	    current = new option.type({ data: option,
					title: option.title,
					help: option.help });
	    this.listenTo(current, 'change', this.change);
	  }
	  inner.append(current.render());
	  if (option.key === 'delete')
	  {
	    current.$el.addClass('btn-danger');
	  }
	  this.children.push(current);
	});
      }
      return this.superRender();
    },

    // state {
    //   selection: array of selected items
    //   type: type of selected item
    //   isViewer: True if this is in viewer mode
    // }
    update:  function (state)
    {
      if (state)
      {
	this._each(this.children, function (field, index) {
	  var option = fieldOptions[index];
	  var disabled = (option.key === 'name' && state.type === 'site');
	  var fieldSelection = [];
	  this._each(state.selection, function (item) {
	    fieldSelection.push(item[option.key]);
	  });
	  // Lookup interfaces based on indexes if we are printing them.
	  if (option.key === 'interfaces' && state.selection.length === 1)
	  {
	    fieldSelection = [[]];
	    this._each(state.selection[0].interfaces, function (id) {
	      fieldSelection[0].push(state.model.interfaces[id]);
	    });
	  }
	  var freeform = false;
	  if (state.selection.length === 1 &&
	      state.selection[0].custom[option.key])
	  {
	    freeform = true;
	  }
	  var siteNodeCount = 1;
	  if (state.type === 'site' && state.selection.length === 1)
	  {
	    siteNodeCount = 0;
	    _.each(_.values(state.model.nodes), function (node) {
	      if (node.group === state.selection[0].id)
	      {
		siteNodeCount += 1;
	      }
	    });
	  }
	  var values = fieldSelection;

	  // If the image the user has selected was tagged nomac, then
	  // display the nomac checkbox as checked and disabled.
	  if (option.key === 'nomac' &&
	      state.selection.length === 1 &&
	      state.selection[0].image)
	  {
	    var image = state.selection[0].image;
	    var imageList = state.model.context.canvasOptions.images;
	    var imageObj = _.findWhere(imageList, { id: image });
	    if (imageObj && imageObj.nomac)
	    {
	      disabled = true;
	      values = [true];
	    }
	  }

	  var shownBecauseDataset = true;
	  var hiddenBecauseDataset = false;
	  if (option.datasetOnly || option.hideOnDataset)
	  {
	    _.each(state.selection, function (item) {
	      if (option.datasetOnly && item.type !== 'emulab-blockstore')
	      {
		shownBecauseDataset = false;
	      }
	      if (option.hideOnDataset && item.type === 'emulab-blockstore')
	      {
		hiddenBecauseDataset = true;
	      }
	    });
	  }
	  var datasetShown = shownBecauseDataset && ! hiddenBecauseDataset;
	  if ((option.on === 'any' ||
	       option.on === state.type) &&
	      (option.type !== ListDisplay ||
	       state.selection.length === 1) &&
	      !(option.type === ButtonView && option.key === 'delete' && state.isViewer) &&
	      !(option.type === ButtonView && option.key === 'delete' && option.on === 'site' &&
		siteNodeCount !== 0) &&
	     datasetShown)
	  {
	    field.update({
	      values: values,
	      isViewer: state.isViewer,
	      type: state.type,
	      disabled: disabled,
	      freeform: freeform,
	      model: state.model,
	      selection: state.selection,
	      label: option.label
	    });
	    field.show();
	  }
	  else
	  {
	    field.hide();
	  }
	});
      }
    },

    change: function (state)
    {
      var result = {
	key: state.data.key,
	value: state.value,
	data: this.data
      };
      if (state.data.type === ConstrainedField || state.data.type === SiteField)
      {
	result.hasFreeform = true;
	result.freeform = state.freeform;
	result.hasVersion = state.hasVersion;
	result.version = state.version;
      }
      this.trigger('change', result);
    },

    addToList: function (state)
    {
      this.trigger('addToList', {
	key: state.data.key,
	item: state.item,
	data: this.data
      });
    },

    removeFromList: function (state)
    {
      this.trigger('removeFromList', {
	key: state.data.key,
	index: state.index,
	data: this.data
      });
    },

    changeList: function (state)
    {
      this.trigger('changeList', {
	key: state.data.key,
	index: state.index,
	changed: state.changed,
	data: this.data
      });
    }

/*
    show: function (customFields)
    {
      if (this.context.show.selectInfo)
      {
	this.executeDisplay = new ListDisplay({
	  el: root.find('#execute-list'),
	  ItemDisplay: ExecuteDisplay
	});
	this.executeDisplay.on('add', _.bind(this.addExecute, this));
	this.executeDisplay.on('remove', _.bind(this.removeExecute, this));
	this.executeDisplay.on('change', _.bind(this.changeExecute, this));
	this.installDisplay = new ListDisplay({
	  el: root.find('#install-list'),
	  ItemDisplay: InstallDisplay
	});
	this.installDisplay.on('add', _.bind(this.addInstall, this));
	this.installDisplay.on('remove', _.bind(this.removeInstall, this));
	this.installDisplay.on('change', _.bind(this.changeInstall, this));
	this.interfaceDisplay = new ListDisplay({
	  el: root.find('#interface-list'),
	  ItemDisplay: InterfaceDisplay
	});
	this.interfaceDisplay.on('change', _.bind(this.changeInterface, this));
      }
    },

    update: function (selection, selectionType)
    {
    },


    handleOpenflowChange: function (event)
    {
      if (this.domRoot.find('#openflowControl').prop('checked'))
      {
	this.domRoot.find('#openflowField').show();
      }
      else
      {
	this.domRoot.find('#openflowField').hide();
	this.topoData.changeAttribute(this.highlights, 'openflow',
				      undefined, this.lastHighlightType);
      }
    },

    handleRoutableChange: function (event)
    {
      var isRoutable = false;
      if (this.domRoot.find('#routableControl').prop('checked'))
      {
	isRoutable = true;
      }
      this.topoData.changeAttribute(this.highlights, 'routable',
				    isRoutable, this.lastHighlightType);
    },

    bindAttribute: function (that, selector, field, optionList, allowCustom)
    {
      var dom = that.domRoot.find(selector);
      if (optionList)
      {
	if (allowCustom)
	{
	  dom = that.domRoot.find(selector).find('.dropdown');
	}
	dom.change(function (event) {
	  var value = undefined;
	  if (event.val !== '(any)')
	  {
	    value = _.where(optionList,
			    { name: event.val })[0].id;
	  }
	  that.topoData.changeAttribute(that.highlights, field, value,
					that.lastHighlightType);
	  if (field === 'image')
	  {
	    var versionValue = undefined;
	    if (event.val !== '(any)')
	    {
	      versionValue = _.findWhere(optionList,
					 { name: event.val }).version;
	    }
	    that.topoData.changeAttribute(that.highlights, 'imageVersion',
					  versionValue,
					  that.lastHighlightType);
	  }
	  that.showAttributes();
	});
      }
      if (allowCustom)
      {
	dom = that.domRoot.find(selector).find('.checkbox');
	
	dom.change(function () {
	  var isCustom = false;
	  if (that.domRoot.find(selector).find('.checkbox').prop('checked'))
	  {
	    isCustom = true;
	  }
	  var item = that.topoData.getType(that.lastHighlightType)[that.highlights[0]];
	  var custom = item.custom;
	  custom[field] = isCustom;
	  that.topoData.changeAttribute(that.highlights, 'custom',
					custom, that.lastHighlightType);
	  that.showAttributes();
	});
	
	dom = that.domRoot.find(selector).find('.custom');
      }
      if (! optionList || allowCustom)
      {
	dom.on('change keyup paste', function () {
	  var value = dom.val();
	  that.topoData.changeAttribute(that.highlights, field, value,
					that.lastHighlightType);
	  that.showAttributes();
	});
      }
    }
*/

  });

  return Info;
});
