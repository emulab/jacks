define(['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  'use strict';

  var TextInputView = View.extend({

    // options {
    //  data: opaque value returned in change event
    //  state: Initial state
    // }
    initialize: function (options)
    {
      this.value = null;
      this.disabled = null;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<input type="text">');
	this.$on('change keyup paste', this.$el, this.change);
      }
      return this.superRender();
    },

    // state {
    //  value: New value
    //  disabled: True if this input should be disabled
    //  shown: True if this input should be displayed
    // }
    update: function (state)
    {
      if (state)
      {
	this.setValue(state.value);
	if (state.disabled === true)
	{
	  this.disable();
	}
	else if (state.disabled === false)
	{
	  this.enable();
	}
	if (state.shown === true)
	{
	  this.show();
	}
	else if (state.shown === false)
	{
	  this.hide();
	}
      }
    },

    disable: function ()
    {
      if (this.disabled !== true)
      {
	this.$el.attr('readonly', 'readonly');
      }
      this.disabled = true;
    },

    enable: function ()
    {
      if (this.disabled !== false)
      {
	this.$el.removeAttr('readonly');
      }
      this.disabled = false;
    },

    setValue: function (inText)
    {
      var text = inText || '';
      if (text !== this.value)
      {
	this.$el.val(text);
      }
      this.value = text;
    },

    change: function (event)
    {
      this.value = this.$el.val();
      this.trigger('change', { value: this.value,
			       data: this.options.data });
    },

  });

  return TextInputView;
});
