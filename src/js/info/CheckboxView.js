define(['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  'use strict';

  var CheckboxView = View.extend({

    // options {
    //  state: Initial state
    //  data: opaque value returned in change event
    // }
    initialize: function (options)
    {
      this.checked = null;
      this.label = null;
      this.disabled = null;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div class="checkbox-info" style="margin-right: 20px"/>');
	this.labelEl = $('<label/>').appendTo(this.$el);
	this.checkbox = $('<input type="checkbox">')
	  .appendTo(this.labelEl);
	this.$on('change', this.checkbox, this.change);
      }
      return this.superRender();
    },

    // state {
    //  value: True if box is checked
    //  label: New label
    //  disabled: True if box is disabled
    //  shown: True if this component should be shown
    // }
    update: function (state)
    {
      if (state)
      {
	if (state.value === true)
	{
	  this.check();
	}
	else if (state.value === false)
	{
	  this.clear();
	}

	if (state.label !== undefined && state.label !== null)
	{
	  this.setLabel(state.label);
	}

	if (state.disabled === true)
	{
	  this.disable();
	}
	else if (state.disabled === false)
	{
	  this.enable();
	}

	if (state.shown === true)
	{
	  this.show();
	}
	else if (state.shown === false)
	{
	  this.hide();
	}
      }
    },

    check: function ()
    {
      if (this.checked !== true)
      {
	this.checkbox.prop('checked', true);
      }
      this.checked = true;
    },

    clear: function ()
    {
      if (this.checked !== false)
      {
	this.checkbox.prop('checked', false);
      }
      this.checked = false;
    },

    disable: function ()
    {
      if (this.disabled !== true)
      {
	this.checkbox.attr('disabled', true);
      }
      this.disabled = true;
    },

    enable: function ()
    {
      if (this.disabled !== false)
      {
	this.checkbox.attr('disabled', false);
      }
      this.disabled = false;
    },

    setLabel: function (label)
    {
      var text = label || '';
      if (text !== this.label)
      {
	var current = this.labelEl.contents().last();
	if (! current.is('input:checkbox'))
	{
	  current.remove();
	}
	this.labelEl.append(_.escape(text));
      }
      this.label = text;
    },

    change: function (event)
    {
      this.trigger('change', {
	value: this.$('input').prop('checked'),
	data: this.options.data
      });
    }

  });

  return CheckboxView;
});

