// This object manages all of the listdisplays which actually render
// individual lists

define(['underscore', 'backbone', 'lib/View',
	'js/canvas/InstallDisplay', 'js/canvas/ExecuteDisplay',
	'js/canvas/InterfaceDisplay', 'js/canvas/ListDisplay'],
function (_, Backbone, View,
	  InstallDisplay, ExecuteDisplay, InterfaceDisplay, ListDisplay)
{
  'use strict';

  var ListManager = View.extend({

    initialize: function (options)
    {
      this.title = options.title;
      this.displayType = options.display;
      this.data = options.data;
      this.display = null;
      this.container = null;
      this.disabled = null;
      this.outside = null;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = (el || $('<div/>'));
	this.outside = $('<div/>')
	  .append('<hr>')
	  .appendTo(this.$el);
	var title = $('<h4>')
	  .html(_.escape(this.title))
	  .appendTo(this.outside);
	this.container = $('<div/>')
	  .appendTo(this.outside);
	var list = $('<div>')
	  .addClass('list')
	  .appendTo(this.container);
	if (this.displayType === ExecuteDisplay ||
	    this.displayType === InstallDisplay)
	{
	  var add = $('<button>Add</button>')
	    .addClass('btn btn-success')
	    .attr('id', 'add')
	    .appendTo(this.container);
	}
	this.display = new ListDisplay({
	  el: this.container,
	  ItemDisplay: this.displayType
	});
	this.listenTo(this.display, 'add', this.add);
	this.listenTo(this.display, 'remove', this.remove);
	this.listenTo(this.display, 'change', this.change);
      }
      return this.superRender();
    },

    // state {
    //   values: List of values to place in this box
    //   isViewer: True if this is read-only viewer mode
    //   type: 'node' or 'site' or 'link' or 'none'
    //   disabled: True if this box should be disabled
    // }
    update: function (state)
    {
      if (state)
      {
	var disabled = state.disabled || state.isViewer;
	if (disabled !== this.disabled)
	{
	  if (disabled)
	  {
	    this.$('button').attr('disabled', 'true');
	    this.$('input').attr('readonly', 'readonly');
	  }
	  else
	  {
	    this.$('button').removeAttr('disabled');
	    this.$('input').removeAttr('readonly');
	  }
	  if (state.isViewer)
	  {
	    this.$('button').hide();
	  }
	}
	if (state.isViewer &&
	    (! state.values || state.values.length !== 1 ||
	     (state.values.length === 1 && state.values[0].length === 0)))
	{
	  this.outside.hide();
	}
	else
	{
	  this.outside.show();
	}
	if (state.values && state.values.length === 1)
	{
	  this.display.update(state.values[0]);
	}
	this.disabled = disabled;
      }
    },

    add: function (state)
    {
      if (this.displayType === ExecuteDisplay)
      {
	this.trigger('addToList', {
	  data: this.data,
	  item: {
	    command: '',
	    shell: '/bin/sh'
	  }
	});
      }
      else if (this.displayType === InstallDisplay)
      {
	this.trigger('addToList', {
	  data: this.data,
	  item: {
	    url: '',
	    install_path: ''
	  }
	});
      }
    },

    remove: function (state)
    {
      this.trigger('removeFromList', {
	index: state.index,
	data: this.data
      });
    },

    change: function (state)
    {
      this.trigger('changeList', {
	index: state.index,
	changed: state.changed,
	data: this.data
      });
    }

  });

/*
    addExecute: function ()
    {
      if (this.highlights.length === 1)
      {
	var current = this.topoData.nodes[this.highlights[0]];
	current.execute.push({ command: '', shell: '/bin/sh' });
	this.showAttributes();
      }
    },

    addInstall: function ()
    {
      if (this.highlights.length === 1)
      {
	var current = this.topoData.nodes[this.highlights[0]];
	current.install.push({ url: '', install_path: '' });
	this.showAttributes();
      }
    },

    removeExecute: function (removed)
    {
      if (this.highlights.length === 1)
      {
	var current = this.topoData.nodes[this.highlights[0]];
	current.execute.splice(removed.index, 1);
	this.showAttributes();
      }
    },

    removeInstall: function (removed)
    {
      if (this.highlights.length === 1)
      {
	var current = this.topoData.nodes[this.highlights[0]];
	current.install.splice(removed.index, 1);
	this.showAttributes();
      }
    },

    changeExecute: function (changed)
    {
      if (this.highlights.length === 1)
      {
	var current = this.topoData.nodes[this.highlights[0]];
	_.each(changed.changed, function (value, key) {
	  current.execute[changed.index][key] = value;
	});
	this.showAttributes();
      }
    },


    changeInstall: function (changed)
    {
      if (this.highlights.length === 1)
      {
	var current = this.topoData.nodes[this.highlights[0]];
	_.each(changed.changed, function (value, key) {
	  current.install[changed.index][key] = value;
	});
	this.showAttributes();
      }
    },

    changeInterface: function (changed)
    {
      if (this.highlights.length === 1)
      {
	var link = this.topoData.lans[this.highlights[0]];
	var id = link.interfaces[changed.index];
	var current = this.topoData.interfaces[id];
	console.log(current, changed);
	_.each(changed.changed, function (value, key) {
	  current[key] = value;
	});
      }
    },
*/

  return ListManager;
});
