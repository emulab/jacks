define(['underscore', 'backbone', 'lib/View', 'js/canvas/ValidList', 'js/info/ClusterPick', 'js/info/ConstrainedField'],
function (_, Backbone, View, ValidList, ClusterPick, ConstrainedField)
{
  'use strict';

  var SiteField = View.extend({

    initialize: function (options)
    {
      this.skipUpdate = false;
      this.picker = null;
      this.amList = this.makeAmList(options.choices);
      this.constrained = new ConstrainedField(options);
      //this.constrained.initialize(options);
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	var siteOptions = this.getOptions(_.pluck(this.options.choices, 'id'), false, [])
	this.$el = el || $(
	"<div><div class='form-group cluster-group'>" +
	  "    <h4>Site</h4>" +
	  "    <div class=''>" +
	  "      <select name='where' id='profile_where' " +
	  "              class='form-control'>" +
	  "        <option value='(any)'>(any)</option>" +
	  siteOptions +
	  "      </select>" +
	  "    </div></div>");
	this.$on('change', this.$('select'), this.change);
	this.picker = new ClusterPick(this.$('.cluster-group'), null, null, null, null);
	this.constrained.render();
      }
      return this.superRender();
    },

    update: function (state)
    {
      if (state)
      {
	this.picker.monitor = this.makeMonitor(state);
	this.picker.types = this.makeTypes(state);
	this.picker.amList = this.amList;
	this.picker.FEDERATEDLIST = this.makeFederatedList(state);
	var unique = _.unique(state.values);
	var dropdownValue = unique.join(', ');
	var newChoices = this.constrained.calculateChoicesIfNew(dropdownValue, false, state, false);
	if (this.skipUpdate)
	{
	  this.skipUpdate = false;
	}
	else
	{
	  if (newChoices)
	  {
	    this.updateChoices(newChoices.options, unique);
	    this.$('select').val(newChoices.choice);
	  }
	}
      }
    },

    updateChoices: function (constrainedChoices, values)
    {
      var allowed = constrainedChoices.allowed;
      var html = '<option value="(any)">(any)</option>';
      html += this.getOptions(constrainedChoices.allowed,
			      false, values);
      html += this.getOptions(constrainedChoices.rejected,
			      true, values);
      this.$off();
      this.$el.html(
	"<div class='form-group cluster-group'>" +
	  "    <h4>Site</h4>" +
	  "    <div class=''>" +
	  "      <select name='where' id='profile_where' " +
	  "              class='form-control'>" +
	  "      </select>" +
	  "    </div>" +
	  "</div>"
);
      this.$('#profile_where').html(html);
      this.$on('change', this.$('select'), this.change);
      this.picker.root = $('.cluster-group');
      this.picker.CreateClusterStatus();
    },

    getOptions: function (list, isDisabled, selected)
    {
      var result = '';
      var disabledString = '';
      if (isDisabled)
      {
	disabledString = ' disabled="true"';
      }
      var idToName = {};
      var idToHidden = {};
      _.each(this.options.choices, function (item) {
	idToName[item.id] = item.name;
	idToHidden[item.id] = item.hidden;
      });
      /*
      var sortFunction = function(a, b) {
	return idToName[a] < idToName[b];
      };
      var sorted = _.sortBy(list, sortFunction);
      console.log('sorted', sorted);
      console.log('list', list);
      */
      this._each(list, function(value) {
	if (idToHidden[value] === undefined ||
	    idToHidden[value] === false)
	{
	  var selectedString = '';
	  if (_.contains(selected, value)) {
	    selectedString = ' selected';
	  }
	  var text = idToName[value];
	  result += '\n<option value="' + value + '"' + disabledString +
	    selectedString + '>' + text + '</option>';
	}
      });
      return result;
    },

    makeMonitor: function (state)
    {
      var result = {};
      _.each(state.model.context.canvasOptions.aggregates, function (site) {
	var status = {
	  rawPCsAvailable: NaN,
	  rawPCsTotal: NaN,
	  VMsAvailable: NaN,
	  VMsTotal: NaN,
	  health: null
	};
	if (site.status === 'up')
	{
	  status.status = 'SUCCESS';
	  result[site.id] = status;
	}
	else if (site.status === 'down')
	{
	  status.status = 'FAILURE';
	  result[site.id] = status;
	}
      }.bind(this));
      return result;
    },

    makeTypes: function (state)
    {
      return {};
    },

    makeAmList: function (choices)
    {
      var amList = [];
      _.each(choices, function (choice) {
	if (choice.hidden === undefined || choice.hidden === false)
	{
	  amList.push(choice);
	}
      }.bind(this));
      return amList;
    },

    makeFederatedList: function (state)
    {
      return [];
    },

    change: function (event, newValue)
    {
      this.skipUpdate = true;
      var value = event
      if (newValue !== undefined)
      {
	value = newValue
      }
      if (value === '(any)' || value === '')
      {
	value = undefined;
      }
      var result = {
	value: value,
	data: this.options.data
      };
      this.trigger('change', result);
    }

  });

  return SiteField;
});
