define(['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  'use strict';

  var ImageField = View.extend({
    // options {
    //  dynamicImages: Object containing all dynamically fetched images
    //  staticImages: Images based on the context
    // }
    initialize: function (options)
    {
      this.value = undefined;
      this.picker = undefined;
      var system = [];
      _.each(options.staticImages, function (image) {
	var result = {};
	result.urn = image.id;
	result.description = image.name;
	system.push(result);
      });
      this.staticImages = {'my-images': [],
			   'project': [],
			   'system': system,
			   'public': []};
      if (options.dynamicImages)
      {
	this.dynamicImages = {};
	_.each(options.dynamicImages, function (category, key) {
	  var result = [];
	  _.each(category, function (image) {
	    var newImage = {};
	    newImage.urn = image.urn;
	    newImage.version = image.version;
	    newImage.description = image.description;
	    newImage.deprecated = image.deprecated;
	    newImage.deprecated_message = image.deprecated_message;
	    if (image.types)
	    {
	      newImage.types = {};
	      _.each(image.types.split(','), function (type) {
		newImage.types[type] = true;
	      }.bind(this));
	    }
	    result.push(newImage);
	  }.bind(this));
	  this.dynamicImages[key] = result;
	}.bind(this));
      }
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	var title = $('<h4>Disk Image</h4>')
	    .appendTo(this.$el);
	this.openContainer = $('<span class="input-group"/>')
	  .appendTo(this.$el);
	this.current = $('<input type="text" readonly '+
			 'class="form-control" '+
			 'value="">')
	  .appendTo(this.openContainer);
	this.buttonEl = $('<span class="input-group-btn"><button class="btn btn-success"'+
			  'style="height: 34px; margin: 0;" '+
			  'type="button">'+
			  '<span class="glyphicon glyphicon-pencil">'+
			  '</span></button></span>')
	  .appendTo(this.openContainer);
	this.pickerContainer =
	  $('<div class="image-picker-background"><div class="image-picker-container"/></div>')
	  .appendTo($('body'))
	  .hide();
	this.$on('click', this.openContainer.find('button'), _.bind(this.open, this));
      }
      return this.superRender();
    },

    update: function (state)
    {
      if (state)
      {
	if (this.picker === undefined)
	{
	  this.picker = new jacksmod.ImagePicker();
	  this.picker.el.appendTo(this.pickerContainer.find('.image-picker-container'));
	  this.picker.on('selected', this.confirm.bind(this));
	  this.picker.on('closed', this.cancel.bind(this));
	}
	this.hardware = _.unique(_.pluck(state.selection, 'hardware'));
	var unique = _.unique(state.values);
	if (unique.length != 1)
	{
	  this.current.val('Multiple Options');
	  this.value = undefined;
	}
	else
	{
	  if (unique[0])
	  {
	    this.current.val(ImageDisplay(unique[0]));
	  }
	  else
	  {
	    this.current.val('Default Image');
	  }
	  this.value = unique[0];
	}
	if (state.isViewer)
	{
	  this.openContainer.hide();
	}
      }
    },

    open: function (state)
    {
      this.pickerContainer.show();
      if (this.dynamicImages)
      {
	this.picker.pick(this.value, this.dynamicImages, this.hardware);
      }
      else
      {
	this.picker.pick(this.value, this.staticImages);
      }
    },

    cancel: function (state)
    {
      this.pickerContainer.hide();
    },

    confirm: function (state)
    {
      this.pickerContainer.hide();
      this.trigger('change', { value: state,
			       data: this.options.data });
    }
  });
      var globalImages = [
	{
	  urn: 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU16-64-STD',
	  version: '',
	  description: 'Ubuntu 16.04 standard image'
	},
	{
	  urn: 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU14-64-STD',
	  version: '',
	  description: 'Ubuntu 14.04 standard image'
	},
	{
	  urn: 'urn:publicid:IDN+emulab.net+image+emulab-ops//CENTOS66-64-STD',
	  version: '',
	  description: 'CentOS 6.6 standard image'
	},
	{
	  urn: 'urn:publicid:IDN+emulab.net+image+emulab-ops//CENTOS71-64-STD',
	  version: '',
	  description: 'CentOS 7.1 standard image'
	},
      	{
	  urn: 'urn:publicid:IDN+emulab.net+image+emulab-ops//FBSD103-64-STD',
	  version: '',
	  description: 'FreeBSD 10.3 standard image'
	},
      ];
      var userImages = [
	{
	  urn: 'urn:publicid:IDN+emulab.net+image+testbed//JONS_COOL_IMAGE',
	  version: '45ac6de',
	  description: 'This image is super cool because it was created in an awesome fashion. You should totally pick this image, dood.'
	},
	{
	  urn: 'urn:publicid:IDN+emulab.net+image+testbed//JONS_BAD_HAIR_DAY_IMAGE',
	  version: 'deadbe4f',
	  description: 'You don\'t want this image, man. It was created under a bad moon in the middle of a total solar eclipse and is cursed for all time.'
	},
      ];
      
  function ImageDisplay(v)
  {
    var sp = v.split('+');
    var display;
    if (sp.length >= 4)
    {
      if (sp[3].substr(0, 12) == 'emulab-ops//')
      {
	display = sp[3].substr(12);
      }
      else if (sp[3].substr(0, 11) == 'emulab-ops:')
      {
	display = sp[3].substr(11);
      }
      else
      {
	display = sp[3];
      }
    }
    else
    {
      display = v;
    }
    return display;
  }

  return ImageField;
});
