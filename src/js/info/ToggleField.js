define(['underscore', 'backbone', 'lib/View', 'js/info/CheckboxView'],
function (_, Backbone, View, CheckboxView)
{
  'use strict';

  var ToggleField = View.extend({

    // options {
    //   title: Title of this field
    //   state: Initial state
    //   data: Opaque data object passed back on change
    // }
    initialize: function (options)
    {
      this.checkbox = new CheckboxView();
      this.children.push(this.checkbox);
      this.help = options.help;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	if (this.help)
	{
	  var help = $('<span><strong>?</strong></span>')
	    .addClass('badge info-label pull-right')
	    .data('toggle', 'tooltip')
	    .attr('title', this.help)
	    .data('placement', 'bottom');
	  help.appendTo(this.$el);
	  help.tooltip();
	}
	this.checkbox.render()
	  .appendTo(this.$el);
	this.listenTo(this.checkbox, 'change', this.change);
      }
      return this.superRender();
    },

    // state {
    //   values: List of values to place here
    //   isViewer: True if this is read-only viewer mode
    //   disabled: True if box should be disabled
    // }
    update: function (state)
    {
      if (state)
      {
	var unique = _.unique(state.values);
	var shown = (unique.length === 1);
	var checked = false;
	if (shown && unique[0])
	{
	  checked = true;
	}
	this.checkbox.update({
	  value: checked,
	  label: this.options.title,
	  disabled: (state.isViewer || state.disabled),
	  shown: shown,
	});
      }
    },

    change: function (state)
    {
      this.trigger('change', {
	value: state.value,
	data: this.options.data
      });
    }

  });

  return ToggleField;
});
