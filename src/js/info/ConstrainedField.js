define(['underscore', 'backbone', 'lib/View', 'js/canvas/ValidList'],
function (_, Backbone, View, ValidList)
{
  'use strict';

  var ConstrainedField = View.extend({

    // options {
    //   title: Title of this field
    //   choices: List of possible options
    //   contraints: Constraint system on choices
    //   data: Opaque data object passed back on change events
    // }
    initialize: function (options)
    {
      this.title = options.title;
      this.choices = options.choices;
      this.constraints = options.constraints;
      this.optionKey = options.optionKey;
      this.data = options.data;
      this.help = options.help;
      this.placeholder = options.placeholder || '';
      this.dropdownField = null;
      this.freeformField = null;
      this.versionField = null;

      this.constrainedChoices = null;
      this.dropdownValue = null;
      this.value = null;
      this.isVersion = null;
      this.version = null;
      this.isFreeform = null;
      this.disabled = null;
      this.freeformDisabled = null;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	if (this.help)
	{
	  var help = $('<span><strong>?</strong></span>')
	    .addClass('badge info-label pull-right')
	    .data('toggle', 'tooltip')
	    .attr('title', this.help)
	    .data('placement', 'bottom');
	  help.appendTo(this.$el);
	  help.tooltip();
	}
	if (this.title)
	{
	  $('<h4>')
	    .html(_.escape(this.title))
	    .appendTo(this.$el);
	}
	this.dropdownField = $('<input type="hidden">')
	  .addClass('form-control dropdown')
	  .appendTo(this.$el);
	this.freeformField = $('<textarea placeholder="' + this.placeholder + '" spellcheck="false"></textarea>')
	  .addClass('form-control custom')
	  .hide()
	  .appendTo(this.$el);
	this.versionField = $('<input type="text" placeholder="version">')
	  .addClass('form-control custom')
	  .hide()
	  .appendTo(this.$el);
	this.$on('change', this.dropdownField, this.change);
	this.$on('change keyup paste', this.freeformField,
		 this.changeFreeform);
	this.$on('change keyup paste', this.versionField,
		 this.changeVersion);
      }
      return this.superRender();
    },

    // state {
    //   values: List of values to place in this box
    //   isViewer: True if this is read-only viewer mode
    //   type: 'node' or 'site' or 'link' or 'none'
    //   disabled: True if this box should be disabled
    //   freeform: True if we should be using the freeform box
    //   model: Global view of the topology
    //   selection: Current items selected (node, site, or link)
    // }
    update: function (state)
    {
      if (state)
      {
	var unique = _.unique(state.values);
	var values = unique.join(', ');

	var freeform = (state.freeform ||
			(values !== '' &&
			 (unique.length > 1 ||
			  ! this.hasChoice(unique))));
	var newChoices = this.calculateChoicesIfNew(values, freeform, state, false);
	if (newChoices !== null)
	{
	  this.updateChoices(newChoices.options);
	  this.dropdownField.select2('val', newChoices.choice);
	}

	if (values !== this.value)
	{
	  this.freeformField.val(values);
	}

	if (freeform !== this.isFreeform)
	{
	  if (freeform)
	  {
	    this.freeformField.show();
	  }
	  else
	  {
	    this.freeformField.hide();
	  }
	}

	var version = '';
	if (state.selection.length === 1 && state.selection[0].imageVersion)
	{
	  version = state.selection[0].imageVersion;
	}
	if (version !== this.version)
	{
	  this.versionField.val(version);
	}

	var isVersion = (freeform && this.optionKey === 'images' &&
			 state.selection.length === 1);
	if (isVersion !== this.isVersion)
	{
	  if (isVersion)
	  {
	    this.versionField.show();
	  }
	  else
	  {
	    this.versionField.hide();
	  }
	}

	var disabled = (state.isViewer || state.disabled || unique.length > 1)
	if (disabled !== this.disabled)
	{
	  if (disabled)
	  {
	    this.dropdownField.select2('enable', false);
	  }
	  else
	  {
	    this.dropdownField.select2('enable', true);
	  }
	}

	var freeformDisabled = freeform && (disabled || unique.length > 1);
	if (freeformDisabled !== this.freeformDisabled)
	{
	  if (freeformDisabled)
	  {
	    this.freeformField.attr('readonly', 'readonly');
	    this.versionField.attr('readonly', 'readonly');
	  }
	  else
	  {
	    this.freeformField.removeAttr('readonly');
	    this.versionField.removeAttr('readonly');
	  }
	}
	this.value = values; 
	this.isFreeform = freeform;
	this.version = version;
	this.isVersion = isVersion;
	this.disabled = disabled;
	this.freeformDisabled = freeformDisabled;
      }
    },

    calculateChoicesIfNew: function (values, freeform, state, alwaysNew)
    {
      var result = null;
      var dropdownValue = values;
      if (dropdownValue === '')
      {
	dropdownValue = '(any)';
      }
      if (freeform)
      {
	dropdownValue = 'Other...';
      }

      var validList = new ValidList(state.model, this.constraints);
      var constrainedChoices = this.runConstraints(state.selection,
						   state.type,
						   validList);
      if (alwaysNew || dropdownValue !== this.dropdownValue ||
	  this.isDifferentChoices(constrainedChoices))
      {
	result = {
	  choice: dropdownValue,
	  options: constrainedChoices
	};
      }
      this.constrainedChoices = constrainedChoices;
      this.dropdownValue = dropdownValue;
      return result;
    },

    updateChoices: function (constrainedChoices)
    {
      var allowed = constrainedChoices.allowed;
      var data = { results: [] };
      data.results.push({ id: '(any)', text: '(any)' });
      this._each(allowed, function(item) {
	data.results.push({
	  id: item,
	  text: _.findWhere(this.choices, { id: item }).name
	});
      });
      data.results.sort(compareText);
      data.results.push({ id: 'Other...', text: 'Other...' });
      if (constrainedChoices.rejected.length > 0)
      {
	var rejected = [];
	this._each(constrainedChoices.rejected, function (item) {
	  rejected.push({
	    id: item, 
	    text: _.findWhere(this.choices, { id: item }).name
	  });
	});
	rejected.sort(compareText);
	data.results.push({
	  text: 'Unavailable',
	  children: rejected,
	  disabled: true
	});
      }

      function format(item) {
	if (item.url)
	{
	  return '<img class="dropdown-icon" width="20" height="20" src="' +
	    item.url + '"> ' + _.escape(item.text);
	}
	else
	{
	  return _.escape(item.text);
	}
      }

      var that = this;
      this.dropdownField.select2('destroy');
      this.dropdownField.select2({
	initSelection: function (element, callback) {
	  var id = element.val();
	  var name = id;
	  var choice = _.findWhere(that.choices, { id: id });
	  if (choice)
	  {
	    name = choice.name;
	  }
	  var newItem = { id: id, text: name };
	  if (that.optionKey === 'icons' && id !== '(any)' && id !== 'Other...')
	  {
	    newItem.url = id;
	  }
	  callback(newItem);
	},
//	sortResults: function(results, container, query) {
//	  //if (query.term) {
//	    return results.sort(compareText);
//	  //}
//	},
	query: function (query) {
	  query.callback(data);
	},
	formatResult: format,
	formatSelection: format,
	escapeMarkup: function (m) { return m; }
      });
    },

    change: function (event)
    {
      var value;
      var isFreeform = false;
      if (event.val === '(any)')
      {
	value = undefined;
	isFreeform = false;
      }
      else if (event.val === 'Other...')
      {
	value = this.value;
	isFreeform = true;
      }
      else
      {
	value = event.val;
	isFreeform = false;
      }
      var result = {
	value: value,
	freeform: isFreeform,
	data: this.data
      };
      if (this.optionKey === 'images' && event.val !== 'Other...')
      {
	result.hasVersion = true;
	result.version = undefined;
	var choice = _.findWhere(this.choices, { id: value });
	if (choice)
	{
	  result.version = choice.version;
	}
      }
      this.trigger('change', result);
    },

    changeFreeform: function (event)
    {
      this.value = this.freeformField.val();
      var result = {
	value: this.value,
	freeform: this.isFreeform,
	data: this.data
      };
      if (this.optionKey === 'images')
      {
	this.version = this.versionField.val();
	result.hasVersion = true;
	result.version = this.version;
      }
      this.trigger('change', result);
    },

    changeVersion: function (event)
    {
      this.version = this.versionField.val();
      var result = {
	value: this.value,
	freeform: this.isFreeform,
	data: this.data
      };
      if (this.optionKey === 'images')
      {
	this.version = this.versionField.val();
	result.hasVersion = true;
	result.version = this.version;
      }
      this.trigger('change', result);
    },

    hasChoice: function (list)
    {
      var result = false;
      this._each(list, function(item) {
	result = result ||
	  _.findWhere(this.choices, { id: item }) !== undefined;
      });
      return result;
    },

    runConstraints: function (selection, selectionType, validList)
    {
      var result = {
	allowed: [],
	rejected: []
      };
      if (selection.length > 0)
      {
	if (this.optionKey === 'icons')
	{
	  result = {
	    allowed: _.pluck(this.choices, 'id'),
	    rejected: []
	  };
	}
	else
	{
	  if (selectionType === 'node')
	  {
	    result = validList.findNodes(selection[0],
					 this.optionKey);
	  }
	  else if (selectionType === 'link')
	  {
	    result = validList.findLinks(selection[0],
					 this.optionKey);
	  }
	  else if (selectionType === 'site')
	  {
	    result = validList.findSites(selection[0],
					 this.optionKey);
	  }
	}
      }
      return result;
    },

    isDifferentChoices: function (newChoices)
    {
      var isDifferent = false;
      var old = this.constrainedChoices;
      if (old === null)
      {
	isDifferent = true;
      }
      else
      {
	this._each(['allowed', 'rejected'], function (status) {
	  if (newChoices[status].length !== old[status].length)
	  {
	    isDifferent = true;
	  }
	  else
	  {
	    for (var i = 0; i < newChoices[status].length; i += 1)
	    {
	      if (newChoices[status][i] !== old[status][i])
	      {
		isDifferent = true;
		break;
	      }
	    }
	  }
	});
      }
      return isDifferent;
    }

  });
  
  function compareText(a, b)
  {
    if (a.text < b.text)
    {
      return -1;
    }
    else if (a.text == b.text)
    {
      return 0;
    }
    else
    {
      return 1;
    }
  }


  return ConstrainedField;
});
