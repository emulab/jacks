define(['underscore', 'backbone', 'lib/View', 'js/info/RadioView'],
function (_, Backbone, View, RadioView)
{
  'use strict';

  var SelectField = View.extend({

    // options {
    //   state: Initial sate
    //   data: Opaque data object passed back on change
    //   choices: id's to use for various choices
    //   labels: List of labels to attach to values
    // }
    initialize: function (options)
    {
      this.radio = new RadioView({ choices: options.choices,
				   labels: options.labels});
      this.children.push(this.radio);
      this.help = options.help;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	if (this.help)
	{
	  var help = $('<span><strong>?</strong></span>')
	    .addClass('badge info-label pull-right')
	    .data('toggle', 'tooltip')
	    .attr('title', this.help)
	    .data('placement', 'bottom');
	  help.appendTo(this.$el);
	  help.tooltip();
	}
	this.radio.render()
	  .appendTo(this.$el);
	this.listenTo(this.radio, 'change', this.change);
      }
      return this.superRender();
    },

    // state {
    //  value: List of values to place here
    //  isViewer: True if this is read-only view mode
    //  disabled: True if selector should be disabled
    // }
    update: function (state)
    {
      if (state)
      {
	var unique = _.unique(state.values);
	var shown = (unique.length === 1);
	var selected = undefined;
	if (shown)
	{
	  selected = unique[0];
	}
	this.radio.update({
	  value: selected,
	  disabled: state.disabled,
	  shown: shown
	});
      }
    },

    change: function (state)
    {
      this.trigger('change', {
	value: state.value,
	data: this.options.data
      });
    }
    
  });

  return SelectField;
});
