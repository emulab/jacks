define(['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  'use strict';

  var WarningField = View.extend({

    initialize: function (options)
    {
      this.oldContents = null;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('#warning-field-container');
	this.$el.html('');
	this.warnEl = $('<div id="warning-field" class="alert alert-danger" style="display: none; margin-top: 10px" role="alert"></div>')
	  .appendTo(this.$el);
      }
      this.rendered = true;
      this.update(this.options.state);
      return $('<div>');
    },

    // state {
    //   values: List of values to place in this box
    //   isViewer: True if this is read-only viewer mode
    //   type: 'node' or 'site' or 'link' or 'none'
    // }
    update: function (state)
    {
      if (state)
      {
	var contents = this.processValues(state.values, state.isViewer);
	if (contents !== this.oldContents)
	{
	  this.oldContents = contents;
	  if (contents)
	  {
	    this.$('.alert.alert-danger').show().html(contents);
	  }
	  else
	  {
	    this.$('.alert.alert-danger').hide();
	  }
	}
      }
    },

    processValues: function (values, isViewer)
    {
      var result = null;
      var warnings = {};
      _.each(values, function (value) {
	if (value)
	{
	  _.each(_.keys(value), function (key) {
	    if (value[key])
	    {
	      warnings[key] = true;
	    }
	  });
	}
      });
      var keys = _.keys(warnings);
      var hasWarnings = keys.length > 0;
      if (hasWarnings && ! isViewer)
      {
	result = '<p><strong>Warning:</strong></p>'
	_.each(keys, function (key) {
	  if (warnings[key])
	  {
	    if (messages[key])
	    {
	      result += '<p>' + messages[key] + '</p>';
	    }
	    else
	    {
	      console.log('Unknown Warning: ' + key);
	    }
	  }
	});
      }
      return result;
    },

  });

  var messages = {
    openflow: 'This LAN has enabled openflow, but has no controller specified.',
    adjacentNode: 'This LAN has settings that may be incompatible with at least one adjacent node.',
    defaultLinkType: 'This LAN has a default link type that may fail. Try setting the link type.',
    duplicateAddress: 'This LAN has an IP address which is duplicated elsewhere.',
    adjacentLink: 'This node has settings that may be incompatible with at least one adjacent LAN.',
    constraintNode: 'This node has settings that may be incompatible with each other or its aggregate.'
  };

  return WarningField;
});
