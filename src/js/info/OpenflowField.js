define(['underscore', 'backbone', 'lib/View',
	'js/info/CheckboxView', 'js/info/TextInputView'],
function (_, Backbone, View, CheckboxView, TextInputView)
{
  'use strict';

  var OpenflowDisplay = View.extend({

    // options {
    //   state: Initial state
    //   data: Opaque data object passed back on change events
    // }
    initialize: function (options)
    {
      this.checkbox = new CheckboxView();
      this.children.push(this.checkbox);
      this.input = new TextInputView();
      this.children.push(this.input);
      this.help = options.help;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	if (this.help)
	{
	  var help = $('<span><strong>?</strong></span>')
	    .addClass('badge info-label pull-right')
	    .data('toggle', 'tooltip')
	    .attr('title', this.help)
	    .data('placement', 'bottom');
	  help.appendTo(this.$el);
	  help.tooltip();
	}
	this.checkbox.render()
	  .appendTo(this.$el);
	this.input.render()
	  .attr('placeholder', 'ex: tcp:controller_ip:6633')
	  .appendTo(this.$el);
	this.listenTo(this.checkbox, 'change', this.changeCheckbox);
	this.listenTo(this.input, 'change', this.changeInput);
      }
      return this.superRender();
    },

    // state {
    //   values: List of values to place in this box
    //   isViewer: True if this is read-only viewer mode
    //   type: 'node' or 'site' or 'link' or 'none'
    //   disabled: True if this box should be disabled
    // }
    update: function (state)
    {
      if (state)
      {
	var unique = _.unique(state.values);
	if (unique.length === 1)
	{
	  var value = unique[0];
	  var checked = (value !== undefined);
	  if (! checked)
	  {
	    value = '';
	  }
	  var disabled = (state.disabled || state.isViewer)
	  this.checkbox.update({
	    value: checked,
	    disabled: disabled,
	    shown: true,
	    label: this.options.title
	  });
	  this.input.update({
	    value: value,
	    disabled: disabled,
	    shown: checked
	  });
	}
	else
	{
	  this.checkbox.hide();
	  this.input.hide();
	}
      }
    },

    changeCheckbox: function (state)
    {
      if (state.value)
      {
	this.trigger('change', { value: '', data: this.options.data });
      }
      else
      {
	this.trigger('change', { value: undefined, data: this.options.data });
      }
    },

    changeInput: function (state)
    {
      this.trigger('change', { value: state.value, data: this.options.data });
    },

  });

  return OpenflowDisplay;
});
