define(['underscore', 'backbone', 'lib/View'],
function (_, Backbone, View)
{
  'use strict';

  var ReadOnlyField = View.extend({

    initialize: function (options)
    {
      this.value = null;
      this.title = options.title;
      this.help = options.help;
    },

    render: function (el)
    {
      if (! this.rendered)
      {
	this.$el = el || $('<div/>');
	if (this.help)
	{
	  var help = $('<span><strong>?</strong></span>')
	    .addClass('badge info-label pull-right')
	    .data('toggle', 'tooltip')
	    .attr('title', this.help)
	    .data('placement', 'bottom');
	  help.appendTo(this.$el);
	  help.tooltip();
	}
	var title = $('<strong/>')
	  .html(_.escape(this.title))
	  .appendTo(this.$el);
	this.$el.append(' <span/>');
      }
      return this.superRender();
    },

    // state {
    //   values: List of values to place in this box
    //   isViewer: True if this is read-only viewer mode
    //   type: 'node' or 'site' or 'link' or 'none'
    //   disabled: True if this box should be disabled
    // }
    update: function (state)
    {
      if (state)
      {
	var valueList = [];
	this._each(state.values, function (value) {
	  if (value !== null && value !== undefined && value !== '')
	  {
	    valueList.push(_.escape(value));
	  }
	});
	if (valueList.length > 0)
	{
	  this.$('strong').show();
	  this.$('span').html(valueList.join('<br>')).show();
	}
	else
	{
	  this.$('strong').hide();
	  this.$('span').hide();
	}
      }
    }

  });

  return ReadOnlyField;
});
