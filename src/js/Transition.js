define(['underscore', 'backbone'],
function (_, Backbone)
{
  'use strict';

  function Transition(rootDom)
  {
    this.rootDom = rootDom;
    this.lastWaitText = null;
    this.pageHasFailed = false;
  }

  Transition.prototype.page = function (goFrom, goTo)
  {
    this.rootDom.find('button.logout').show();
/*
    if (!(goFrom.hasClass('hides')))
    {
      var item = goFrom;
      item.addClass('hides');
      item.one('transitionend', function() {
	item.addClass('afterHide');

	goTo.removeClass('afterHide');
	setTimeout(function() {
	  goTo.removeClass('hides');
	}, 20);
      });
    }
*/
    goFrom.hide();
    goTo.show();
    goTo.removeClass('hides');
    goTo.removeClass('afterHide');
  };

  Transition.prototype.startWaiting = function (oldNode, text)
  {
//    var shouldAppend = (this.lastWaitText === null);
    var shouldAppend = true;
    if (oldNode)
    {
      this.page(oldNode, this.rootDom.find('#waitContainer'));
    }
//    this.hideWait(this.lastWaitText);
    if (text)
    {
      this.lastWaitText = $('<h1>' + text + '</h1>');
      if (shouldAppend)
      {
	this.rootDom.find('#waitText').append(this.lastWaitText);
      }
    }
    else
    {
      this.rootDom.find('#waitText').html('');
    }
  };

  Transition.prototype.stopWaiting = function (newNode)
  {
    this.page(this.rootDom.find('#waitContainer'), newNode);
    this.rootDom.find('#waitText').html('');
//    this.hideWait(this.lastWaitText);
//    this.lastWaitText = null;
  };

  Transition.prototype.hideWait = function (node)
  {
    var that = this;
    if (node)
    {
      node.addClass('hides')
	.one('transitionend', function (event) {
	  node.remove();
	  if (that.lastWaitText)
	  {
	    that.rootDom.find('#waitText').append(that.lastWaitText);
	  }
	});
    }
  }


  Transition.prototype.startFail = function (node, message, error)
  {
    if (! this.pageHasFailed)
    {
      this.page(node, this.rootDom.find('#failContainer'));
      $('#failTitle').html(_.escape(message));
      if (error)
      {
	this.rootDom.find('#failMessage').html(_.escape(vkbeautify.json(JSON.stringify(error))));
      }
      this.pageHasFailed = true;
    }
  };

  Transition.prototype.switchNav = function (target)
  {
    this.rootDom.find('.navbar').each(function() {
      $(this).addClass('hidden');
    });
    target.removeClass('hidden');
  };

  return Transition;
});
