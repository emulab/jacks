define(['q'],
function (Q)
{
  'use strict';

  var isInitialized = false;
  var socketpool = null;
  var socketCounter = 0;
  var clientPool = {};
  var settingsPool = {};

  var serverCerts = [];
  var clientCerts = [];
  var clientKey = [];

  var defaultPoolUrl = 'SocketPool.swf';
  var defaultPoolId = 'socketPool';

  // $.ajaxforge(url, settings)
  // $.ajaxforge(settings)
  $.ajaxforge = function (arg1, arg2)
  {
//    $.ajaxforge.initialize(defaultPoolUrl, defaultPoolId);
    var settings;
    if (arguments.length === 2)
    {
      settings = arg2;
      settings.url = arg1;
    }
    else
    {
      settings = arg1;
    }

    settings.deferred = Q.defer();

    var url = $('<a>', { href: settings.url } )[0];
    var host = url.hostname;
    var path = url.pathname + url.search + url.hash;
    var sendData = settings.data;

    var newClient = initClient(url.protocol + '//' + host + ':' + url.port,
                               socketCounter);
    clientPool[socketCounter] = newClient;
    settingsPool[socketCounter] = settings;
    sendClient(newClient, path, sendData, socketCounter);

    socketCounter += 1;

    return settings.deferred.promise.then(function (response) {
      return settings.converters['xml json']($.parseXML(response.body));
    });
  };

  $.ajaxforge.initialize = function (flashUrl, flashId, serverCertText,
                                     clientKeyText, clientCertText)
  {
    setServerCert(serverCertText);
    setClientKey(clientKeyText);
    setClientCert(clientCertText);
    if (! isInitialized)
    {
//      swfobject.embedSWF(flashUrl, flashId, '0', '0', '9.0.0',
//                         false, {}, {allowscriptaccess: 'always'}, {});
      socketpool = forge.net.createSocketPool({
        flashId: flashId,
        policyPort: 843,
        msie: false
        });

      isInitialized = true;
    }
  };

  function initClient(host, socketId)
  {
    var result = null;
    try
    {
//      console.log('initClient -- ' + host);
      var arg = {
        url: host,
        socketPool: socketpool,
        connections: 1,
        // optional cipher suites in order of preference
        caCerts : serverCerts,
        cipherSuites: [
          forge.tls.CipherSuites.TLS_RSA_WITH_AES_128_CBC_SHA,
          forge.tls.CipherSuites.TLS_RSA_WITH_AES_256_CBC_SHA],
        verify: function(c, verified, depth, certs)
        {
          return verified;
        },
        primeTlsSockets: false
      };
      if (clientCerts.length > 0)
      {
        arg.getCertificate = function(c, request) { return clientCerts; };
        arg.getPrivateKey = function(c, cert) { return clientKey; };
      }
      result = forge.http.createClient(arg);
    }
    catch(ex)
    {
      console.log('ERROR: client_init', host);
      console.dir(ex);
    }
  
    return result;
  }


  function sendClient(client, path, data, socketId)
  {
    var requestArg = {
      path: path,
      method: 'GET'
    };
    if (data != "")
    {
      requestArg.method = 'POST';
      requestArg.headers = [{'Content-Type': 'text/xml'}];
      requestArg.body = data;
    }
    var request = forge.http.createRequest(requestArg);
    console.log('sendClient', request);
    try {
    client.send({
      request: request,
      connected: function(e)
      {
//        console.log('forge.tests.tls', 'connected', e);
      },
      headerReady: function(e)
      {
//        console.log('forge.tests.tls', 'header ready', e);
      },
      bodyReady: function(e)
      {
//        console.log('bodyReady', e);
        settingsPool[socketId].deferred.resolve(e.response);
      },
      error: function(e)
      {
        console.log('error', e);
        settingsPool[socketId].deferred.reject([e.type, e.message,
                                                e.cause]);
        e.socket.close();
      }
    });
    } catch (e) {
      console.log('sendError', e);
    }
    return false;
  }

  function setServerCert(newCert)
  {
    try
    {
      serverCerts = [];
      var list = newCert.split("-----END CERTIFICATE-----");
      for (var i = 0; i < list.length - 1; ++i)
      {
        serverCerts.push(list[i] + "-----END CERTIFICATE-----\n");
      }
    }
    catch(ex)
    {
      console.log('ERROR setServerCert:');
      console.dir(ex);
    }
  }

  function setClientCert(newCert)
  {
    try
    {
      clientCerts = [];
      if (typeof(newCert) === "string")
      {
        var list = newCert.split("-----END CERTIFICATE-----");
        for (var i = 0; i < list.length - 1; ++i)
        {
          clientCerts.push(list[i] + "-----END CERTIFICATE-----\n");
        }
      }
      else
      {
        clientCerts = newCert;
      }
    }
    catch (ex)
    {
      console.log('ERROR setting client cert:');
      console.dir(ex);
    }
  }

  function setClientKey(newKey)
  {
    try
    {
      clientKey = newKey;
    }
    catch(ex)
    {
      console.log('ERROR setting client key:');
      console.dir(ex);
    }
  }

  return $.ajaxforge;
});
