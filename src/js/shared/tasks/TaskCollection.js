/*
 * Copyright (c) 2008-2013 University of Utah and the Flux Group.
 * 
 * {{{GENIPUBLIC-LICENSE
 * 
 * GENI Public License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and/or hardware specification (the "Work") to
 * deal in the Work without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Work, and to permit persons to whom the Work
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Work.
 * 
 * THE WORK IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE WORK OR THE USE OR OTHER DEALINGS
 * IN THE WORK.
 * 
 * }}}
 */

/**
 * Collection of tasks
 * 
 * @author mstrum
 * 
 */

/* global define: true */
define(['underscore', 'backbone'],
function (_, Backbone)
{
  'use strict';

  function TaskCollection(newCollection)
  {
    if (newCollection === undefined)
    {
      this.collection = [];
    }
    else
    {
      this.collection = newCollection;
    }
  }

  TaskCollection.prototype.add = function (task)
  {
    this.collection.push(task);
  };
	
  TaskCollection.prototype.remove = function (task)
  {
    this.collection = _.remove(this.collection, task);
  };

  TaskCollection.prototype.contains = function (task)
  {
    return _.contains(this.collection, task);
  };

  TaskCollection.prototype.getLength = function ()
  {
    return this.collection.length;
  };

  /**
   * 
   * @return Same collection as an Array
   * 
   */
  TaskCollection.prototype.getAsArray = function ()
  {
    return _.toArray(this.collection);
  };

  /**
   * 
   * @return Combination of all relatedTo values in tasks
   * 
   */
  TaskCollection.prototype.getRelatedTo = function ()
  {
    var relatedTo = [];
    _.each(this.collection, function (childTask) {
      _.each (childTask.relatedTo, function (childTaskRelatedTo) {
	if (! _.contains(relatedTo, childTaskRelatedTo))
	{
	  relatedTo.push(childTaskRelatedTo);
	}
      });
    });
    return relatedTo;
  };

  /**
   * 
   * @return All tasks including child tasks
   * 
   */
  TaskCollection.prototype.getAll = function ()
  {
    var taskList = [];
    _.each(this.collection, function (childTask) {
      taskList.push(childTask.getAllTasks());
    });
    return new TaskCollection(_.flatten(taskList, true));
  };

  /**
   * 
   * @return Inactive tasks
   * 
   */
  TaskCollection.prototype.getInactive = function ()
  {
    return this.getWithState(Task.STATE_INACTIVE);
  };

  /**
   * 
   * @return Active tasks
   * 
   */
  TaskCollection.prototype.getActive = function ()
  {
    return getWithState(Task.STATE_ACTIVE);
  };

  /**
   * 
   * @return Tasks which haven't finished
   * 
   */
  TaskCollection.prototype.getNotFinished = function ()
  {
    return getOtherThanState(Task.STATE_FINISHED);
  };

  /**
   * 
   * @return All tasks including descendants which aren't finished 
   * 
   */
  TaskCollection.prototype.getAllNotFinished = function ()
  {
    return NotFinished.All.NotFinished;
  };

  /**
   * 
   * @param state State which the tasks should be in
   * @return Tasks which are in the given state
   * 
   */
  TaskCollection.prototype.getWithState = function (state)
  {
    var newTasks = new TaskCollection();
    _.each(this.collection, function (task) {
      if (task.getState === state)
      {
	newTasks.add(task);
      }
    });
    return newTasks;
  };

  /**
   * 
   * @param state State the tasks should not be in
   * @return Tasks not in the given state
   * 
   */
  TaskCollection.prototype.getOtherThanState = function (state)
  {
    var newTasks = new TaskCollection();
    _.each(this.collection, function (task) {
      if (task.getState !== state)
      {
	newTasks.add(task);
      }
    });
    return newTasks;
  };

  /**
   * 
   * @param type Class of tasks we are looking for
   * @return Tasks which are of the Class given
   * 
   */
  TaskCollection.prototype.getOfClass = function (type)
  {
    var newTasks = new TaskCollection();
    _.each(this.collection, function (task) {
      if (task.type === type)
      {
	newTasks.add(task);
      }
    });
    return newTasks;
  };

  /**
   * 
   * @param item Item which tasks should be related to
   * @return Tasks related to 'item'
   * 
   */
  TaskCollection.prototype.getRelatedTo = function (item)
  {
    return this.getRelatedToAny([item]);
  };

  /**
   * 
   * @param items Items which tasks should be related to
   * @return Tasks related to any items from 'items'
   * 
   */
  TaskCollection.prototype.getRelatedToAny = function (items)
  {
    var newTasks = new TaskCollection();
    _.each(this.collection, function (task) {
      _.each(items, function (item) {
	if (_.contains(task.relatedTo, item))
	{
	  newTasks.add(task);
	  break;
	}
      });
    });
    return newTasks;
  };

  return TaskCollection;
});
