/*
 * Copyright (c) 2008-2013 University of Utah and the Flux Group.
 * 
 * {{{GENIPUBLIC-LICENSE
 * 
 * GENI Public License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and/or hardware specification (the "Work") to
 * deal in the Work without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Work, and to permit persons to whom the Work
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Work.
 * 
 * THE WORK IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE WORK OR THE USE OR OTHER DEALINGS
 * IN THE WORK.
 * 
 * }}}
 */

/**
 * Details about an error that occured in a task
 * 
 * @author mstrum
 * 
 */

/* global define: true */
define(['underscore', 'backbone'],
function (_, Backbone)
{
  'use strict';

  /**
   * 
   * @param message Message of the error
   * @param id Error number
   * @param errorData Data related to the error
   * 
   */
  function TaskError(message, id, errorData)
  {
    this.message = message;
    if (this.message === undefined)
    {
      this.message = '';
    }
    this.id = id;
    if (this.id === undefined)
    {
      this.id = 0;
    }
    this.errorData = errorData;
    if (this.errorData === undefined)
    {
      this.errorData = null;
    }
  }

  // Task timed out
  this.prototype.TIMEOUT = 0;

  // Unexpected problem while executing code, like null pointers
  this.prototype.CODE_UNEXPECTED = 1;

  // Error found while executing in code, like an incorrect format detected
  this.prototype.CODE_PROBLEM = 2;

  // Error outside of the scope of the running code, like an error
  // from JavaScript
  this.prorotype.FAULT = 3;

  TaskError.prototype.toString = function ()
  {
    return "[TaskError ID=" + this.errorID + " Message=\"" +
      this.message + "\", ]";
  };

  return TaskError;
});
