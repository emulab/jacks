/*
 * Copyright (c) 2008-2013 University of Utah and the Flux Group.
 * 
 * {{{GENIPUBLIC-LICENSE
 * 
 * GENI Public License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and/or hardware specification (the "Work") to
 * deal in the Work without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Work, and to permit persons to whom the Work
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Work.
 * 
 * THE WORK IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE WORK OR THE USE OR OTHER DEALINGS
 * IN THE WORK.
 * 
 * }}}
 */

/**
 * Base class for all tasks and task groups
 * 
 * @author mstrum
 * 
 */

/*global define: true */
define(['underscore', 'backbone',
	'js/shared/utils/VirtualTable',
	'js/shared/sharedMain',
	'js/shared/logging/LogMessage', 'js/shared/tasks/TaskError',
	'js/shared/tasks/TaskCollection'],
function (_, Backbone, VirtualTable, sharedMain,
	  LogMessage, TaskError,
	  TaskCollection)
{
  'use strict';

  var taskDefault = {
    fullName: '',
    description: '',
    shortName: '',
    parent: null,
    timeout: 0,
    delay: 0,
    retryWhenPossible: true,
    relatedTo: null,
    forceSerial: false,
    type: ''
  };

  /**
   * 
   * @param taskFullName Name of the task, including contextual queues
   * @param taskDescription Description of what the task does
   * @param taskShortName Shortest name of the task understandable in context
   * @param taskParent Parent to assign the task to
   * @param taskTimeout Time (in ms) after which the task should be canceled if not completed by after started
   * @param taskDelay Time to delay the task from starting after it would have started, not from right now
   * @param taskRetryWhenPossible Retry the task if possible?  For example, if busy and can try again later.
   * @param taskRelatedTo Objects related to the task (manager, slice, sliver, etc.)
   * @param taskForceSerial Force this task to run by itself, even in something like a parallel task gorup
   * 
   */
  function Task(taskFullName,
		taskDescription,
		taskShortName,
		taskParent,
		taskTimeout,
		taskDelay,
		taskRetryWhenPossible,
		taskRelatedTo,
		taskForceSerial)
  {
    _.extend(this, Backbone.Events);
    this.vtable = new VirtualTable();
    // EXTEND Functions
    this.vtable.register('runTimeout', defaultRunTimeout);
    this.vtable.register('afterComplete', defaultAfterComplete);
    this.vtable.register('afterError', defaultAfterError);
    this.vtable.register('getAllTasks', defaultGetAllTasks);
    this.vtable.register('getRemaining', defaultGetRemaining);
    this.vtable.register('getRunning', defaultGetRunning);

    // IMPLEMENT Functions
    this.vtable.register('runCancel', function () {});
    this.vtable.register('runStart', function () {});
    this.vtable.register('runCleanup', function () {});

    // String which names the task without any context (ex. create @ utah)
    this.fullName = taskFullName;
    // Description of what the task does
    this.description = taskDescription;
    // String which names the task within context (ex. create)
    this.shortName = taskShortName;
    // Parent TaskGroup
    this.parent = taskParent;
    // Seconds before the task should timeout and cancel
    this.timeout = taskTimeout;
    // Delay in seconds before a task should run after it is ready to run
    this.delay = taskDelay;
    // Should this task retry if possible? Ex: xml-rpc server returns busy
    this.retryWhenPossible = taskRetryWhenPossible;
    // Items the task is related to
    // Ex: manager if the task is listing resources from there
    this.relatedTo = taskRelatedTo;
    // Make sure this runs and next tasks don't run until done
    this.forceSerial = taskForceSerial;

    _.defaults(this, taskDefault);
    if (! this.relatedTo)
    {
      this.relatedTo = [];
    }
    this.ensureAllTasksInRelated();

    this.autoCount = 0;
    // Is the task active, inactive, or finished?
    this._state = this.STATE_INACTIVE;
    // What is the task doing or how has it ended?
    // Waiting, running, delayed, success, etc.
    this._status = this.STATUS_NA;
    // Last message for the task, look at log messages related to this
    // task to get all
    this._message = '';
    // Number of times the task should be retried before failing
    this.numberTries = 0;
    // General timer
    this.timer = null;
    // Timer to update the message of the task while other things are happening
    this.autoUpdate = null;
    // Data related to the task, either given to use when started or set
    // while a task is running to return a value
    this.data = null;
    // Should be set if the task needs to convey that there are warnings
    this.hasWarnings = false;
    // TaskError which caused the task to fail
    this.error = null;
    // Force this to run now, no matter what
    this.forceRunNow = false;
  }

   // Task is not doing anything, typically because it wasn't started yet
  Task.prototype.STATE_INACTIVE = 'Inactive';

  // Task has been started and is running
  Task.prototype.STATE_ACTIVE = 'Active';

  // Task will not perform any future actions, because it was canceled,
  // failed or successful
  Task.prototype.STATE_FINISHED = 'Finished';

  //---------------------------------------------------------------------------

  // Just a default status so it's not blank
  Task.prototype.STATUS_NA = 'N/A';

  // Task has been scheduled for a future time and is waiting
  Task.prototype.STATUS_DELAYED = 'Delayed';

  // Task is running
  Task.prototype.STATUS_RUNNING = 'Running';

  // Task has failed
  Task.prototype.STATUS_FAILED = 'Failed';

  // Task was successful
  Task.prototype.STATUS_SUCCESS = 'Success';

  // Task was either canceled by the user or in code
  Task.prototype.STATUS_CANCELED = 'Canceled';

  Task.prototype.getState = function ()
  {
    return this._state;
  };
		
  /**
   * Sets the state of the task to the given value.  Dispatches a status
   * changed event.
   * 
   * If finished, sets the end time. If started, sets the beginning time.
   * 
   * @param newState State the task should be in
   * 
   */
  Task.prototype.setState = function (newState)
  {
    if(newState !== this._state)
    {
      this._state = newState;
      this.dispatchStatusChange();
    }
  };

  Task.prototype.getStatus = function ()
  {
    return this._status;
  };

  /**
   * Sets the status of the task to the given value.  Dispatches a status
   * changed event.
   * 
   * @param newStatus Status the task should be in
   * 
   */
  Task.prototype.setStatus = function (newStatus)
  {
    if(newStatus !== this._status)
    {
      this._status = newStatus;
      this.dispatchStatusChange();
    }
  };

  Task.prototype.getMessage = function ()
  {
    return this._message;
  };

  Task.prototype.setMessage = function (newMessage)
  {
    if(newMessage !== this._message)
    {
      this._message = newMessage;
      this.dispatchStatusChange();
    }
  };

  /**
   * 
   * @return fullName if set, shortName otherwise
   * 
   */
  Task.prototype.getName = function ()
  {
    var result = this.shortName;
    if(this.fullName.length > 0)
    {
      result = this.fullName;
    }
    return result;
  };

  /**
   * 
   * @return shortName if set, fullName otherwise
   * 
   */
  Task.prototype.getShortestName = function ()
  {
    var result = this.fullName;
    if(this.shortName.length > 0)
    {
      result = shortName;
    }
    return result;
  };

  /**
   * 
   * @return Furthest back ancestor task which has no parent
   * 
   */
  Task.prototype.getRoot = function ()
  {
    var result = this;
    while (result.parent !== null)
    {
      result = result.parent;
    }
    return result;
  };

  /**
   * 
   * @return All ancestor tasks
   * 
   */
  Task.prototype.getAncestors = function ()
  {
    var ancestors = new TaskCollection();
    var ancesterTask = this.parent;
    while(ancesterTask !== null)
    {
      ancestors.add(ancesterTask);
      ancesterTask = ancesterTask.parent;
    }
    return ancestors;
  };

  /**
   *
   * @return All logs related to this and only this task
   *
   */
  Task.prototype.getLogs = function ()
  {
    return sharedMain.logger.getLogs().getRelatedTo([this]);
  };

  /**
   *
   * Ensures that all ancestor tasks are listed in relatedTo
   * 
   */
  Task.prototype.ensureAllTasksInRelated = function ()
  {
    if(this.relatedTo.indexOf(this) === -1)
    {
      this.relatedTo.push(this);
    }
    var ancesterTask = this.parent;
    while(ancesterTask !== null)
    {
      this.relatedTo.push(ancesterTask);
      ancesterTask = ancesterTask.parent;
    }
  };

  /**
   * Adds a log message for this task
   * 
   * @param newTitle Short name
   * @param newMessage Long description
   * @param level Type of message
   * 
   */
  Task.prototype.addMessage = function (newTitle, newMessage, level,
					importance, setMessage)
  {
    if (level === undefined)
    {
      level = 0;
    }
    if (importance === undefined)
    {
      importance = 1;
    }
    if (setMessage === undefined)
    {
      setMessage = true;
    }
    var newLogMessage = new LogMessage(this.relatedTo, this.getName(),
				       newTitle, newMessage,
				       this.getShortestName(), level,
				       importance, this);
    sharedMain.logger.add(newLogMessage);
    if(setMessage)
    {
      this.setMessage(newTitle);
    }
    if(level === LogMessage.prototype.LEVEL_WARNING)
    {
      this.hasWarnings = true;
    }
    this.dispatchLogged();
    return newLogMessage;
  };

  /**
   * Either runs or starts the delayed run
   * 
   */
  Task.prototype.start = function ()
  {
    this.setState(this.STATE_ACTIVE);
    if(this.delay > 0)
    {
      this.startDelay();
    }
    else
    {
      this.onStart();
    }
  };

  /**
   * Cancels this task and reports canceled to parent. Does nothing if already finished
   * 
   * Calls runCancel()
   * 
   */
  Task.prototype.cancel = function ()
  {
    if(this.getState() !== this.STATE_FINISHED)
    {
      this.cancelTimers();
      this.setStatus(this.STATUS_CANCELED);
      this.setState(this.STATE_FINISHED);
      this.addMessage('Canceled', 'Task was canceled',
                      LogMessage.prototype.LEVEL_INFO,
		      LogMessage.prototype.IMPORTANCE_HIGH);
      this.dispatchFinished();
      this.runCancel();
      if(this.parent !== null)
      {
	parent.canceledTask(this);
      }
      this.runCleanup();
      this.removeHandlers();
    }
  };

  /**
   * IMPLEMENT if task needs to deal with a cancel
   * 
   */
  Task.prototype.runCancel = function ()
  {
    /* IMPLEMENT */
    this.vtable.invoke('runCancel', this);
  };

  /**
   * Stops and removes all timers
   * 
   */
  Task.prototype.cancelTimers = function ()
  {
    if (this.timer != null)
    {
      clearTimeout(this.timer);
      this.timer = null;
    }
    if(this.autoUpdate != null)
    {
      clearTimeout(this.autoUpdate);
      this.autoUpdate = null;
    }
  };

  /**
   * Adds the optional delay if requested.
   * 
   * After delay, onStart will be called
   * 
   */
  Task.prototype.startDelay = function ()
  {
    this.cancelTimers();
    if (this.delay > 0)
    {
      this.timer = setTimeout(_.bind(this.onDelay, this), delay*1000);
      this.autoUpdate = setTimeout(_.bind(this.onAutoUpdate, this), 1000);
      this.autoCount = 0;

      this.addMessage('Delayed ' + delay + ' seconds...',
		      'Delayed for ' + delay + ' seconds');
      this.setStatus(this.STATUS_DELAYED);
    }
  };

  /**
   * Called after the delayed time, calls onStart to begin task
   * @param evt
   * 
   */
  Task.prototype.onDelay = function (event)
  {
    this.cancelTimers();
    this.onStart();
  };

  /**
   * Called to auto-update the message for the task, useful during periods of time when the task is waiting
   * @param evt
   * 
   */
  Task.prototype.onAutoUpdate = function (event)
  {
    clearTimeout(this.autoUpdate);
    this.autoUpdate = setTimeout(_.bind(this.onAutoUpdate, this), 1000);
    this.autoCount += 1;
    this.setMessage('Delayed ' +
		    math.max(0, this.delay - this.autoCount) +
		    ' more seconds...');
  };

  /**
   * Starts running the task and adds a timeout if specified.
   * 
   * Make sure to run cancelTimer() when timeout should be ignored.
   * 
   * Calls runStart()
   * 
   */
  Task.prototype.onStart = function ()
  {
    if(this.timeout > 0)
    {
      this.startTimeout();
    }
    this.addMessage('Started', 'Task has started running');
    this.setStatus(this.STATUS_RUNNING);
    if(this.parent != null)
    {
      this.parent.startedTask(this);
    }
    this.numberTries += 1;
    this.runStart();
  };

  /**
   * If timeout is set, starts timeout timer to timeout the task at the timeout time...yeah, I'm a poet
   * 
   */
  Task.prototype.startTimeout = function ()
  {
    this.cancelTimers();
    if (this.timeout > 0)
    {
      this.timer = setTimeout(_.bind(this.onTimeout, this),
			      this.timeout * 1000);
    }
  };

  /**
   * Called when a timeout occurs.
   * Call cancelTimer() to stop this event from occuring
   * Calls runTimeout() to see if a retry is needed
   * 
   * @param evt
   * 
   */
  Task.prototype.onTimeout = function (event)
  {
    clearTimeout(this.timer);
    this.timer = null;
    
    var message = 'Timeout of ' + this.timeout + ' seconds elapsed';
    this.addMessage('Timed out after ' + this.timeout + ' seconds', message, 
		    LogMessage.prototype.LEVEL_WARNING,
		    LogMessage.prototype.IMPORTANCE_HIGH);
    this.setStatus(this.STATUS_FAILED);
    if (this.getState() === this.STATE_ACTIVE)
    {
      if (this.runTimeout())
      {
	this.afterError(new TaskError(message, TaskError.TIMEOUT));
      }
    }
  };

  /**
   * Attempt to retry if possible.  EXTEND if default behavior isn't enough.
   * Cleanup anything needed if onStart() will be called again.
   * Default behavior is to run onStart() if retryWhenPossible is set
   * 
   * @return TRUE if an error should be reported (default = !retryWhenPossible)
   * 
   */
  Task.prototype.runTimeout = function ()
  {
    /* EXTEND */
    this.vtable.invoke('runTimeout', this);
  };

  function defaultRunTimeout()
  {
    if(this.retryWhenPossible)
    {
      this.onStart();
    }
    return ! this.retryWhenPossible;
  }

  /**
   * IMPLEMENT to do the task at hand
   * 
   * MUST call afterComplete(), afterError(), or cancel()
   * after task is finished
   * 
   */
  Task.prototype.runStart = function ()
  {
    /* IMPLEMENT */
    this.vtable.invoke('runStart', this);
  };

  /**
   * MUST be called after the task completes successfully.
   * EXTEND if default behavior not enough.
   * 
   */
  Task.prototype.afterComplete = function (addCompletedMessage)
  {
    /* EXTEND */
    this.vtable.invoke('afterComplete', this, addCompletedMessage);
  };

  function defaultAfterComplete
  {
    if (addCompletedMessage === NULL)
    {
      addCompletedMessage = true;
    }
    this.cancelTimers();
    this.setStatus(this.STATUS_SUCCESS);
    this.setState(this.STATE_FINISHED);
    if(addCompletedMessage)
    {
      this.addMessage('Completed', 'Task has completed',
		      LogMessage.prototype.LEVEL_INFO,
		      LogMessage.prototype.IMPORTANCE_HIGH);
    }
    this.dispatchFinished();
    if(this.parent !== null)
    {
      this.parent.completedTask(this);
    }

    this.runCleanup();
    this.removeHandlers();
  }

  /**
   * MUST be called after an error
   * EXTEND if default behavior not enough.
   * 
   * @param taskError Details about the error which occurred
   * 
   */
  Task.prototype.afterError = function (taskError)
  {
    /* EXTEND */
    this.vtable.invoke('afterError', this, taskError);
  };

  function defaultAfterError(taskError)
  {
    this.error = taskError;
    this.cancelTimers();
    if (taskError.errorId === TaskError.prototype.CODE_PROBLEM)
    {
      this.addMessage('Problem: ' +
		      StringUtil.shortenString(taskError.message, 30, true),
		      'Task ran into a problem stopping further progress. ' +
		      taskError.message,
		      LogMessage.prototype.LEVEL_FAIL,
		      LogMessage.prototype.IMPORTANCE_HIGH);
    }
    else if (taskError.errorId === TaskError.prototype.CODE_UNEXPECTED)
    {
      addMessage('Unexpected error: ' +
		 StringUtil.shortenString(taskError.message, 30, true),
		 'Task ran into an unexpected problem. ' + taskError.message,
		 LogMessage.prototype.LEVEL_FAIL,
		 LogMessage.prototype.IMPORTANCE_HIGH);
    }
    else if (taskError.errorId === TaskError.prototype.FAULT)
    {
      var faultMessage = StringUtil.shortenString(taskError.message, 30, true);
      addMessage('Fault: ' + faultMessage,
		 'Task encountered a fault: ' + taskError.message,
		 LogMessage.prototype.LEVEL_FAIL,
		 LogMessage.prototype.IMPORTANCE_HIGH);
    }
    else if (taskError.errorId === TaskError.prototype.TIMEOUT)
    {
      addMessage('Timeout', 'Task timed out. ' + taskError.message,
		 LogMessage.prototype.LEVEL_FAIL,
		 LogMessage.prototype.IMPORTANCE_HIGH);
    }
    this.setStatus(this.STATUS_FAILED);
    this.setState(this.STATE_FINISHED);
    this.dispatchFinished();
    this.runCleanup();
    this.removeHandlers();
    if (this.parent !== null)
    {
      this.parent.erroredTask(this);
    }
  };

  /**
   * Returns a flat list of both this task and any child taks
   * EXTEND if there are any child tasks.
   * 
   */
  Task.prototype.getAllTasks = function ()
  {
    /* EXTEND */
    return this.vtable.invoke('getAllTasks', this);
  };

  function defaultGetAllTasks()
  {
    return [this];
  }

  /**
   * Returns how many tasks are remaining.  TaskGroups overload this to
   * account for child tasks.
   *
   * EXTEND if there are any child tasks
   * 
   * @return Number of tasks, including this one, are not finished
   * 
   */
  Task.prototype.getRemaining = function ()
  {
    /* EXTEND */
    return this.vtable.invoke('getRemaining', this);
  };

  function defaultGetRemaining()
  {
    var result = 1;
    if(this.getState() === this.STATE_FINISHED)
    {
      result = 0;
    }
    return result;
  }

  /**
   * Returns how many tasks are running.  TaskGroups overload this to account
   * for child tasks.
   *
   * EXTEND if there are any child tasks
   * 
   * @return Number of tasks, including this one, are running
   * 
   */
  Task.prototype.getRunning = function ()
  {
    /* EXTEND */
    return this.vtable.invoke('getRunning', this);
  };

  function defaultGetRunning()
  {
    var result = 0;
    if(this.getState() === this.STATE_ACTIVE)
    {
      result = 1;
    }
    return result;
  }

  /**
   * IMPLEMENT if cleanup needs to happen after task finishes, like removing event handlers
   * 
   */
  Task.prototype.runCleanup = function ()
  {
    /* IMPLEMENT */
    this.vtable.invoke('runCleanup', this);
  };

  /**
   * Removes all of the event handlers. Override this and do nothing to stop this behavior.
   * 
   */
  Task.prototype.removeHandlers = function ()
  {
    this.off();
  };

  /**
   * Dispatches EVENT_LOGGED
   * 
   */
  Task.prototype.dispatchLogged = function ()
  {
    this.trigger(TaskEvent.LOGGED, this);
  };

  /**
   * Dispatches EVENT_STATUS
   * 
   */
  Task.prototype.dispatchStatusChange = function ()
  {
    this.trigger(TaskEvent.STATUS, this);
  };

  /**
   * Dispatches EVENT_FINISHED
   * 
   */
  Task.prototype.dispatchFinished = function ()
  {
    this.trigger(TaskEvent.FINISHED, this);
  };

  Task.prototype.print = function(leftString, outputLogs)
  {
    if (leftString === undefined)
    {
      leftString = '';
    }
    if (outputLogs === undefined)
    {
      outputLogs = false;
    }
    var value = leftString + '[Task\n' +
      leftString + '\tState=' + State + '\n' +
      leftString + '\tStatus=' + Status + '\n' +
      leftString + '\tName=' + Name + '\n' +
      leftString + '\tDescription=' + description + '\n' +
      leftString + '\tMessage=' + _message + '\n' +
      leftString + ']\n';
    if(this.error !== null)
    {
      value += leftString + '\t' + this.error.toString() + '\n';
    }
    if(outputLogs)
    {
      value + this.getLogs().toString();
    }
    return value + leftString + '[/Task]\n';
  };

  Task.prototype.toString = function ()
  {
    return this.print();
  };

  return Task;
});
