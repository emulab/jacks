/*
 * Copyright (c) 2013 University of Utah and the Flux Group.
 * 
 * {{{GENIPUBLIC-LICENSE
 * 
 * GENI Public License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and/or hardware specification (the "Work") to
 * deal in the Work without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Work, and to permit persons to whom the Work
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Work.
 * 
 * THE WORK IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE WORK OR THE USE OR OTHER DEALINGS
 * IN THE WORK.
 * 
 * }}}
 */

/**
 * Event within our world
 * 
 * @author mstrum
 * 
 */

/*global define: true */
define([],
function () {
  'use strict';

  /**
   * 
   * @param newType Type of event
   * @param newChangedObject Object which the event is about
   * @param newAction Action the object has taken
   * 
   */
  function FlackEvent(newType, newChangedObject, newAction)
  {
    this.type = newType;
    this.changedObject = newChangedObject;
    this.action = 0;
    if (newAction)
    {
      this.action = newAction;
    }
  }

  FlackEvent.prototype.clone = function ()
  {
    return new FlackEvent(this.type, this.changedObject, this.action);
  };
		

  // Actions
  FlackEvent.prototype.ACTION_CREATED = 0;
  FlackEvent.prototype.ACTION_STATUS = 1;
  FlackEvent.prototype.ACTION_CHANGED = 2;
  FlackEvent.prototype.ACTION_POPULATING = 3;
  FlackEvent.prototype.ACTION_POPULATED = 4;
  FlackEvent.prototype.ACTION_REMOVING = 5;
  FlackEvent.prototype.ACTION_REMOVED = 6;
  FlackEvent.prototype.ACTION_ADDED = 7;
  FlackEvent.prototype.ACTION_SELECTED = 9;
  FlackEvent.prototype.ACTION_NEW = 10;
		
  // Types of objects which change and get events thrown for
  FlackEvent.prototype.CHANGED_MANAGERS = "managers_changed";
  FlackEvent.prototype.CHANGED_MANAGER = "manager_changed";
  FlackEvent.prototype.CHANGED_USER = "user_changed";
  FlackEvent.prototype.CHANGED_TASK = "task_changed";
  FlackEvent.prototype.CHANGED_LOG = "log_changed";
  FlackEvent.prototype.CHANGED_UNIVERSE = "universe_changed";

  // GENI
  FlackEvent.prototype.CHANGED_SLICES = "slices_changed";
  FlackEvent.prototype.CHANGED_SLICE = "slice_changed";
  FlackEvent.prototype.CHANGED_SLIVER = "sliver_changed";
  FlackEvent.prototype.CHANGED_AUTHORITIES = "authorities_changed";
  FlackEvent.prototype.CHANGED_USERDISKIMAGES = "userdiskimages_changed";

  // Emulab
  FlackEvent.prototype.CHANGED_EXPERIMENTS = "experiments_changed";
  FlackEvent.prototype.CHANGED_EXPERIMENT = "experiment_changed";

  return FlackEvent;
});
