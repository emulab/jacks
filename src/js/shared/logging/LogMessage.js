/*
 * Copyright (c) 2013 University of Utah and the Flux Group.
 * 
 * {{{GENIPUBLIC-LICENSE
 * 
 * GENI Public License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and/or hardware specification (the "Work") to
 * deal in the Work without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Work, and to permit persons to whom the Work
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Work.
 * 
 * THE WORK IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE WORK OR THE USE OR OTHER DEALINGS
 * IN THE WORK.
 * 
 * }}}
 */

/**
 * Details about some event which happened
 * 
 * @author mstrum
 * 
 */

/*global define: true */
define(['js/shared/sharedMain', 'js/shared/utils/stringUtil'],
function (sharedMain, stringUtil) {
  'use strict';

  /**
   * 
   * @param newRelatedTo Objects this message is related to (geni objects, tasks, etc.)
   * @param newTitleFullName Full name of the message understandable out of context (e.g. list @ url)
   * @param newTitleMessage Short version of the message to include in the title
   * @param newMessage Full message (e.g. full XML-RPC result)
   * @param newTitleShortName Short name of the message understandable in context (e.g. list)
   * @param newLevel Is this message an error, a warning, just info, etc.
   * @param newImportance Is this message important?
   * 
   */
  function LogMessage(newRelatedTo, newTitleFullName, newTitleMessage,
                      newMessage, newTitleShortName, newLevel,
                      newImportance, newOrigin)
  {
    // Items this message is related to (manager, slice, user, task, etc.)
    this.relatedTo = newRelatedTo || [];

    // Human-readable name, understandable out of context
    // ex: "List Resources @ URL"
    this.fullTitleName = newTitleFullName || "";

    // Human-readable short status to include in the title
    // ex: ("Success", "Failed", "Started", etc.)
    this.titleMessage = newTitleMessage || "";

    // Human-readable name, understandable in context ("List Resources")
    this.shortTitleName = newTitleShortName || "";

    // Full text
    this.message = newMessage || "";

    // What level of message is this?
    this.level = newLevel || 0;

    // How important is this message?
    this.importance = newImportance || 1;
    this.origin = newOrigin;

    // When did this event occur?
    this.timeStamp = new Date();
  }

  // Levels
  LogMessage.prototype.LEVEL_INFO = 0;
  LogMessage.prototype.LEVEL_WARNING = 1;
  LogMessage.prototype.LEVEL_FAIL = 2;
  LogMessage.prototype.LEVEL_DIE = 3;

  LogMessage.prototype.levelToString = function (level)
  {
    switch (level)
    {
    case LogMessage.prototype.LEVEL_INFO:
      return "Info";
    case LogMessage.prototype.LEVEL_WARNING:
      return "*Warning*";
    case LogMessage.prototype.LEVEL_FAIL:
      return "**FAIL**";
    case LogMessage.prototype.LEVEL_DIE:
      return "***DIE***";
    default:
      return "Unknown (" + level + ")";
    }
  };
		
  // Importance
  LogMessage.prototype.IMPORTANCE_HIGH = 0;
  LogMessage.prototype.IMPORTANCE_LOW = 1;
		
  /**
   * 
   * @return fullTitleName if set, shortTitleName otherwise
   * 
   */
  LogMessage.prototype.titleName = function ()
  {
    if (this.fullTitleName && this.fullTitleName.length > 0)
    {
      return this.fullTitleName;
    }
    else
    {
      return this.shortTitleName;
    }
  };
		
  /**
   * 
   * @return "TitleName: titleMessage" if TitleName available,
   * titleMessage otherwise
   * 
   */
  LogMessage.prototype.title = function ()
  {
    if (this.titleName().length > 0)
    {
      return this.titleName() + ": " + this.titleMessage;
    }
    else
    {
      return this.titleMessage;
    }
  };

  /**
   * 
   * @return "shortTitleName: titleMessage" if shortTitleName set,
   *         "fullTitleName: titleMessage" if fullTitleName set, 
   *         "titleMessage" otherwise
   * 
   */
  LogMessage.prototype.shortestTitle = function ()
  {
    if (this.shortTitleName.length > 0)
    {
      return this.shortTitleName + ": " + this.titleMessage;
    }
    else if (this.fullTitleName.length > 0)
    {
      return this.fullTitleName + ": " + this.titleMessage;
    }
    else
    {
      return this.titleMessage;
    }
  };
		
  /**
   * 
   * @return Shortened version of the message (80 characters or so)
   * 
   */
  LogMessage.prototype.shortMessage = function ()
  {
    return stringUtil.shortenString(this.message, 80, true);
  };

  /**
   * Sees if this log message is related to any of the given items.
   * 
   * @param objects Items tested to see if this message is related to
   * @return true if this message is related to anything from 'objects'
   * 
   */
  LogMessage.prototype.relatedToAny = function (objects)
  {
    var i = 0;
    for (i = 0; i < objects.length; i += 1)
    {
      if (this.relatedTo.indexOf(objects[i]) !== -1)
      {
        return true;
      }
    }
    return false;
  };

  LogMessage.prototype.toString = function (includeVersionArg)
  {
    var includeVersion = true;
    if (includeVersionArg !== undefined)
    {
      includeVersion = includeVersionArg;
    }
    var versionString = "";
    if (includeVersion)
    {
      versionString = sharedMain.ClientString + "\n";
    }
    return "-MSG------------------------------\n" +
      "UTC Time: " + this.timeStamp.toUTCString() + "\n" +
      versionString +
      "Level: " + this.levelToString(this.level) + "\n" +
      "Title: " + this.title() + "\n" +
      "Message:\n" + this.message + "\n" +
      "\n------------------------------END-";
  };

  return LogMessage;
});
