/*
 * Copyright (c) 2013 University of Utah and the Flux Group.
 * 
 * {{{GENIPUBLIC-LICENSE
 * 
 * GENI Public License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and/or hardware specification (the "Work") to
 * deal in the Work without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Work, and to permit persons to whom the Work
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Work.
 * 
 * THE WORK IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE WORK OR THE USE OR OTHER DEALINGS
 * IN THE WORK.
 * 
 * }}}
 */

/**
 * Handles collections of log messages
 * 
 * @author mstrum
 * 
 */

/*global define: true */
define(['js/shared/sharedMain', 'js/shared/logging/LogMessage'],
function (sharedMain, LogMessage) {
  'use strict';

  function LogMessageCollection()
  {
    this.collection = [];
  }

  LogMessageCollection.prototype.add = function (msg)
  {
    this.collection.push(msg);
  };
		
  LogMessageCollection.prototype.remove = function (msg)
  {
    var idx = this.collection.indexOf(msg);
    if (idx > -1)
    {
      this.collection.splice(idx, 1);
    }
  };
		
  LogMessageCollection.prototype.contains = function (msg)
  {
    return this.collection.indexOf(msg) > -1;
  };
		
  LogMessageCollection.prototype.length = function ()
  {
    return this.collection.length;
  };
		
  LogMessageCollection.prototype.important = function ()
  {
    var results = new LogMessageCollection();
    var i = 0;
    for (i = 0; i < this.collection.length; i += 1)
    {
      if (this.collection[i].importance === LogMessage.prototype.IMPORTANCE_HIGH)
      {
        results.add(this.collection[i]);
      }
    }
    return results;
  };
		
  /**
   * Returns log messages related to anything in related,
   * can be a task to get a task's logs or an entity like a manager
   * 
   * @param related Items for which log messages should be related to
   * @return Messages related to 'related'
   * 
   */
  LogMessageCollection.prototype.getRelatedTo = function (related)
  {
    var relatedLogs = new LogMessageCollection();
    var i = 0;
    for (i = 0; i < this.collection.length; i += 1)
    {
      if (this.collection[i].relatedToAny(related))
      {
        relatedLogs.add(this.collection[i]);
      }
    }
    return relatedLogs;
  };
  
  LogMessageCollection.prototype.toString = function ()
  {
    var output = "******* Messages *******\n" + sharedMain.ClientString +
          "\n************************\n";
    var i = 0;
    for (i = 0; i < this.collection.length; i += 1)
    {
      output +=  this.collection[i].toString(false) + "\n";
    }
    return output;
  };

  return LogMessageCollection;
});
