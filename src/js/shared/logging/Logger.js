/*
 * Copyright (c) 2013 University of Utah and the Flux Group.
 * 
 * {{{GENIPUBLIC-LICENSE
 * 
 * GENI Public License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and/or hardware specification (the "Work") to
 * deal in the Work without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Work, and to permit persons to whom the Work
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Work.
 * 
 * THE WORK IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE WORK OR THE USE OR OTHER DEALINGS
 * IN THE WORK.
 * 
 * }}}
 */

/**
 * Top level handler for log messages throughout the geni library
 * 
 * @author mstrum
 * 
 */

/*global define: true */
define(['underscore', 'backbone', 'js/shared/sharedMain',
        'js/shared/FlackEvent', 'js/shared/logger/LogMessageCollection'],
function (_, Backbone, sharedMain, FlackEvent, LogMessageCollection) {
  'use strict';

  function Logger()
  {
    this._logs = new LogMessageCollection();
    _.extend(this, Backbone.Events);
  }

  Logger.prototype.getLogs = function ()
  {
    return this._logs;
  };

  /**
   * Adds the message and dispatches an event
   * 
   * @param msg Message to add
   * 
   */
  Logger.prototype.add = function (msg)
  {
    this._logs.add(msg);
    this.trigger(FlackEvent.CHANGED_LOG,
                 new FlackEvent(FlackEvent.CHANGED_LOG, msg,
                                FlackEvent.ACTION_CREATED));
  };
		
  /**
   * Dispatches a selected event for the owner.  Kind of dirty, but not
   * used very much.
   * 
   * @param owner null or tasker indicates all messages, otherwise owner
   * is selected
   * 
   */
  Logger.prototype.view = function (owner)
  {
    if (owner === sharedMain.tasker)
    {
      this.trigger(FlackEvent.CHANGED_LOG,
                   new FlackEvent(FlackEvent.CHANGED_LOG, null,
                                  FlackEvent.ACTION_SELECTED));
    }
    else
    {
      this.trigger(FlackEvent.CHANGED_LOG,
                   new FlackEvent(FlackEvent.CHANGED_LOG, owner,
                                  FlackEvent.ACTION_SELECTED));
    }
  };

  return Logger;
});
