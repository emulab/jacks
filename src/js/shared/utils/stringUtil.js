/*
 * Copyright (c) 2013 University of Utah and the Flux Group.
 * 
 * {{{GENIPUBLIC-LICENSE
 * 
 * GENI Public License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and/or hardware specification (the "Work") to
 * deal in the Work without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Work, and to permit persons to whom the Work
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Work.
 * 
 * THE WORK IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE WORK OR THE USE OR OTHER DEALINGS
 * IN THE WORK.
 * 
 * }}}
 */

/**
 * Common functions used for dealing with strings
 * 
 * @author mstrum
 * 
 */

/*global define: true */
define([],
function () {
  'use strict';

  var stringUtil = {};

  // Makes the first letter uppercase
  stringUtil.firstToUpper = function (phrase)
  {
    return  phrase.substring(1, 0).toUpperCase() +
      phrase.substring(1).toLowerCase();
  };

  stringUtil.replaceString = function (original, find, replace)
  {
    return original.split(find).join(replace);
  };

  // Shortens the given string to a length, taking out from the middle
  stringUtil.shortenString = function (phrase, size, shortenEnd)
  {
    // Remove any un-needed elements
    phrase = phrase.replace("https://", "").replace("http://", "");
			
    if (phrase.length <= size)
    {
      return phrase;
    }
			
    var removeChars = phrase.length - size + 3;
    if (shortenEnd)
    {
      return phrase.substring(0, size - 3) + "...";
    }
    else
    {
      var upTo = (phrase.length / 2) - (removeChars / 2);
      return phrase.substring(0, upTo) + "..." + 
        phrase.substring(upTo + removeChars);
    }
  };

  stringUtil.makeSureEndsWith = function (original, letter)
  {
    if (original.charAt(original.length - 1) !== letter)
    {
      return original + letter;
    }
    else
    {
      return original;
    }
  };
		
  stringUtil.notSet = function (value)
  {
    return value === null || value.length === 0;
  };
		
  stringUtil.getDotString = function (name)
  {
    return stringUtil.replaceString(stringUtil.replaceString(name, ".", ""),
                                    "-", "");
  };

/*		
  stringUtil.errorToString = function (e)
  {
    //return Capabilities.isDebugger ? e.getStackTrace() : e.toString()
  };
*/
	
  stringUtil.mhzToString = function (speed)
  {
//    var numberFormatter:NumberFormatter = new NumberFormatter(LocaleID.DEFAULT);
//    numberFormatter.fractionalDigits = 2;
//    numberFormatter.trailingZeros = false;
			
    if (speed < 1000)
    {
      return speed + " Mhz";
    }
    else
    {
      return speed / 1000 + " Ghz";
    }
  };

  stringUtil.mbToString = function (size)
  {
//    var numberFormatter:NumberFormatter = new NumberFormatter(LocaleID.DEFAULT);
//    numberFormatter.fractionalDigits = 2;
//    numberFormatter.trailingZeros = false;
			
    if (size < 1024)
    {
      return size + " MB";
    }
    else if (size < 1048576)
    {
      return size / 1024 + " GB";
    }
    else
    {
      return size / 1048576 + " TB";
    }
  };

  return stringUtil;
});
