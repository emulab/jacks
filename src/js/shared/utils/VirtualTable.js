define(['underscore'],
function (_)
{
  'use strict';

  function VirtualTable()
  {
    this.registry = {};
  }

  VirtualTable.prototype.register = function (methodName, method)
  {
    if (this.registry[methodName] !== undefined)
    {
      this.registry[methodName].push(method);
    }
    else
    {
      this.registry[methodName] = [method];
    }
  }

  VirtualTable.prototype.invoke = function (methodName, object)
  {
    var methods = this.registry[methodName];
    if (methods !== undefined)
    {
      var index = methods.length - 1;
      var args = [makeParentMethod(object, methods, index)];
      args = args.concat(Array.prototype.slice.call(arguments, 2));
      return methods[index].apply(object, args);
    }
    else
    {
      return null;
    }
  }

  function makeParentMethod(object, methods, index)
  {
    if (index === 0)
    {
      return null;
    }
    else
    {
      return function () {
	var args = [makeParentMethod(object, methods, index - 1)];
	args = args.concat(Array.prototype.slice.call(arguments));
	return methods[index - 1].apply(object, args);
      };
    }
  }

  return VirtualTable;
});
