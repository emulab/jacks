define(['js/shared/FlackEvent'],
function(FlackEvent){

describe('shared.FlackEvent', function(){

  it('#FlackEvent()', function(){
    var testEvent = new FlackEvent("type",
                                   1,
                                   3);
    expect(testEvent.type).to.equal("type");
    expect(testEvent.changedObject).to.equal(1);
    expect(testEvent.action).to.equal(3);
  })

  it('#clone()', function(){
    var testEvent = new FlackEvent("type",
                                   1,
                                   3);
    var cloneEvent = testEvent.clone();
    expect(cloneEvent.type).to.equal("type");
    expect(cloneEvent.changedObject).to.equal(1);
    expect(cloneEvent.action).to.equal(3);
  })
})

});
