define(['js/shared/logging/LogMessage'],
function(LogMessage){

describe('shared.logging.LogMessage', function(){

  it('#LogMessage()', function(){
    var testMessage = new LogMessage([1, 2, 3],
                                     "title full name",
                                     "title message",
                                     "message",
                                     "title short name",
                                     1,
                                     2,
                                     3);
    expect(testMessage).not.to.be.null;
    expect(-1).to.equal(-1);
  })

  it('#titleName()', function(){
    testFullTitle = new LogMessage(null,
                                   "title full name",
                                   "title message",
                                   "message",
                                   "title short name");
    testShortTitle = new LogMessage(null,
                                    "",
                                    "title message",
                                    "message",
                                    "title short name");
    expect(testFullTitle.titleName()).to.equal("title full name");
    expect(testShortTitle.titleName()).to.equal("title short name");
  })

  it('#title()', function(){
    testFullTitle = new LogMessage(null,
                                   "title full name",
                                   "title message",
                                   "message",
                                   "title short name");
    testShortTitle = new LogMessage(null,
                                    "",
                                    "title message",
                                    "message",
                                    "title short name");
    testNoTitle = new LogMessage(null,
                                 "",
                                 "title message");
    expect(testFullTitle.title()).to.equal("title full name: title message");
    expect(testShortTitle.title()).to.equal("title short name: title message");
    expect(testNoTitle.title()).to.equal("title message");
  })

  it('#shortestTitle()', function(){
    testFullTitle = new LogMessage(null,
                                   "title full name",
                                   "title message",
                                   "message",
                                   "");
    testShortTitle = new LogMessage(null,
                                    "",
                                    "title message",
                                    "message",
                                    "title short name");
    testNoTitle = new LogMessage(null,
                                 "",
                                 "title message");
    expect(testFullTitle.shortestTitle()).to.equal("title full name: title message");
    expect(testShortTitle.shortestTitle()).to.equal("title short name: title message");
    expect(testNoTitle.shortestTitle()).to.equal("title message");
  })

  it('#shortMessage()', function(){
    testMessage = new LogMessage(null,
                                 "title full name",
                                 "title message",
                                 "this is not a long message");
    expect(testMessage.shortMessage()).to.equal("this is not a long message");
    for(var i = 0; i < 10; i++) {
      testMessage.message += testMessage.message;
    }
    expect(testMessage.shortMessage()).to.have.length(80);
  })

  it('#relatedToAny()', function(){
    testMessage = new LogMessage([1, 2, 3],
                                 "title full name",
                                 "title message",
                                 "message",
                                 "title short name",
                                 1,
                                 2,
                                 3);
    expect(testMessage.relatedToAny([])).to.be.false;
    expect(testMessage.relatedToAny([4, -1])).to.be.false;
    expect(testMessage.relatedToAny([0, 1, 7])).to.be.true;
  })
})

});
