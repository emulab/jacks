define(['js/shared/logging/LogMessage', 'js/shared/logging/LogMessageCollection'],
function(LogMessage, LogMessageCollection){

describe('shared.logging.LogMessageCollection', function(){

  it('#LogMessageCollection()()', function(){
    var testCollection = new LogMessageCollection();
    expect(testCollection.length()).to.equal(0);
  })

  it('#add()', function(){
    var testCollection = new LogMessageCollection();
    var testMessage = new LogMessage(null,
                                     "",
                                     "title message");
    testCollection.add(testMessage);
    testCollection.add(testMessage);
    testCollection.add(testMessage);
    expect(testCollection.length()).to.equal(3);
  })

  it('#remove()', function(){
    var testCollection = new LogMessageCollection();
    var testMessage = new LogMessage(null,
                                     "",
                                     "title message");
    testCollection.add(testMessage);
    testCollection.add(testMessage);
    testCollection.add(testMessage);
    testCollection.remove(testMessage);
    expect(testCollection.length()).to.equal(2);
  })

  it('#contains()', function(){
    var testCollection = new LogMessageCollection();
    var testMessage = new LogMessage(null,
                                     "",
                                     "title message");
    testCollection.add(testMessage);
    expect(testCollection.contains(testMessage)).to.be.true;
  })

  it('#important()', function(){
    var testCollection = new LogMessageCollection();
    var importantMessage = new LogMessage(1,
                                          "",
                                          "title message",
                                          "message",
                                          "title short name",
                                          0,
                                          0);
    var unimportantMessage = new LogMessage(1,
                                            "",
                                            "title message",
                                            "message",
                                            "title short name",
                                            0,
                                            1);
    testCollection.add(importantMessage);
    testCollection.add(unimportantMessage);
    expect(testCollection.important().length()).to.equal(1);
  })

  it('#getRelatedTo()', function(){
    var testCollection = new LogMessageCollection();
    testCollection.add(new LogMessage(testCollection));
    testCollection.add(new LogMessage());
    expect(testCollection.getRelatedTo(testCollection).length()).to.equal(1);
  })
})

});
