define(['js/shared/utils/stringUtil'],
function(stringUtil){

describe('shared.utils.stringUtil', function(){

  it('#firstToUpper()', function(){
    expect(stringUtil.firstToUpper("test")).to.equal("Test");
    expect(stringUtil.firstToUpper("Test")).to.equal("Test");
  })

  it('#replaceString()', function(){
    expect(stringUtil.replaceString("this is a test", " is ", " is not ")).to.equal("this is not a test");
  })

  it('#shortenString()', function(){
    expect(stringUtil.shortenString("this is a test", 14, false)).to.equal("this is a test");
    expect(stringUtil.shortenString("this is a test", 14, true)).to.equal("this is a test");
    expect(stringUtil.shortenString("this is a test", 13, false)).to.equal("this ... test");
    expect(stringUtil.shortenString("this is a test", 13, true)).to.equal("this is a ...");
  })

  it('#makeSureEndsWith()', function(){
    expect(stringUtil.makeSureEndsWith("test", '.')).to.equal("test.");
    expect(stringUtil.makeSureEndsWith("test.", '.')).to.equal("test.");
  })

  it('#notSet()', function(){
    expect(stringUtil.notSet("test")).to.not.be.true;
    expect(stringUtil.notSet(null)).to.be.true;
    expect(stringUtil.notSet("")).to.be.true;
  })

  it('#getDotString()', function(){
    expect(stringUtil.getDotString("this.is.a.test")).to.equal("thisisatest");
    expect(stringUtil.getDotString("--this-is-a-test-")).to.equal("thisisatest");
    expect(stringUtil.getDotString(".-this.is-a.test..")).to.equal("thisisatest");
  })

  it('#mhzToString()', function(){
    expect(stringUtil.mhzToString(1)).to.equal("1 Mhz");
    expect(stringUtil.mhzToString(1000)).to.equal("1 Ghz");
  })

  it('#mbToString()', function(){
    expect(stringUtil.mbToString(1)).to.equal("1 MB");
    expect(stringUtil.mbToString(1024)).to.equal("1 GB");
    expect(stringUtil.mbToString(1048576)).to.equal("1 TB");
  })
})

});
