define(['js/shared/utils/arrayUtil'],
function(arrayUtil){

describe('shared.utils.arrayUtil', function(){

  it('#overlap()', function(){
    expect(arrayUtil.overlap([1], [2])).to.be.false;
    expect(arrayUtil.overlap([], [2])).to.be.false;
    expect(arrayUtil.overlap([], [])).to.be.false;
    expect(arrayUtil.overlap(null, [])).to.be.false;
    expect(arrayUtil.overlap([], null)).to.be.false;
    expect(arrayUtil.overlap(null, null)).to.be.false;
    expect(arrayUtil.overlap([2, 1, 3], [2])).to.be.true;
    expect(arrayUtil.overlap([1, 2, 3], [2])).to.be.true;
    expect(arrayUtil.overlap([1, 3, 2], [2])).to.be.true;
  })
})

});
