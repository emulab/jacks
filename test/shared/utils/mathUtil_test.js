define(['js/shared/utils/mathUtil'],
function(mathUtil){

describe('shared.utils.mathUtil', function(){

  it('#randomNumberBetween()', function(){
    // For now, just run a bunch and check the bounds for sanity.
    for (var i = 0; i < 1000; i++) {
      expect(mathUtil.randomNumberBetween(1, 100)).to.be.within(1, 100);
      expect(mathUtil.randomNumberBetween(-100, 100)).to.be.within(-100, 100);
      expect(mathUtil.randomNumberBetween(0, 0)).to.equal(0);
      expect(mathUtil.randomNumberBetween(0, 1)).to.be.within(0, 1);
    }
  })
})

});
