require.config({
  baseUrl:'../src/',
  'paths': {
    'bootstrap': 'lib/bootstrap.min',
    'jquery': 'lib/require-jquery',
    'underscore': 'lib/underscore-min',
    'backbone': 'lib/backbone-min',
    'jquery-xmlrpc': 'lib/jquery.xmlrpc.min.js'
  },
  'shim': 
  {
    backbone: {
      'deps': ['jquery', 'underscore'],
      'exports': 'Backbone'
    },
    underscore: {
      'exports': '_'
    },
    'jquery-xmlrpc':  {
      'deps': ['jquery']
    }
  }   
});

require([
  '../test/shared/FlackEvent_test',
  //'../test/shared/FlackDispatcher_test',
  '../test/shared/logging/LogMessage_test',
  '../test/shared/logging/LogMessageCollection_test',
  //'../test/shared/logging/Logger_test',
  '../test/shared/utils/arrayUtil_test',
  '../test/shared/utils/mathUtil_test',
  '../test/shared/utils/stringUtil_test'
], function() {
     if (window.mochaPhantomJS) {
       mochaPhantomJS.run();
     } else {
       mocha.run();
     }
});
